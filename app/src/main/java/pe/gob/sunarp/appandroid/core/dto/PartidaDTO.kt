package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.Partida

@JsonClass(generateAdapter = true)
data class PartidaDTO(
    @field:Json(name = "estado")
    val estado: Int?,
    @field:Json(name = "msj")
    val msj: String?,
    @field:Json(name = "refNumPart")
    val refNumPart: String?,
    @field:Json(name = "refNumPartMP")
    val refNumPartMP: String?,
    @field:Json(name = "codigoGla")
    val codigoGla: String?,
    @field:Json(name = "codigoLibro")
    val codigoLibro: String?,
    @field:Json(name = "numPartida")
    val numPartida: String?,
    @field:Json(name = "numPlaca")
    val numPlaca: String?
) : BaseModel() {
    fun toModel() = Partida(
        estado ?: 0,
        msj.orEmpty(),
        refNumPart.orEmpty(),
        refNumPartMP.orEmpty(),
        codigoGla.orEmpty(),
        codigoLibro.orEmpty(),
        numPartida.orEmpty(),
        numPlaca.orEmpty()
    )
}