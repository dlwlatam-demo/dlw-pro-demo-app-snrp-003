package pe.gob.sunarp.appandroid.ui.transactions.history

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.history.model.Transaction
import javax.inject.Inject

@HiltViewModel
class TransactionHistoryViewModel @Inject constructor(
    private val repository: TransactionRemoteRepository
) : BaseViewModel() {

    val transactions = SingleLiveEvent<List<Transaction>>()
    val loading = SingleLiveEvent<Boolean>()

    fun getTransactions() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getTransactions()) {
                is DataResult.Success -> onSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onSuccess(data: List<Transaction>) {
        loading.value = false
        transactions.value = data
    }

}