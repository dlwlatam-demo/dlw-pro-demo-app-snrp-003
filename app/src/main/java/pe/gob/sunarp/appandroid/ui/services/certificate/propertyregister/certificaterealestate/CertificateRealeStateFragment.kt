package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certificaterealestate

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.widget.PopupMenu
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.CertificateType
import pe.gob.sunarp.appandroid.core.model.LegalRecord
import pe.gob.sunarp.appandroid.core.model.Partida
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarCriRequest
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoRegistralInmobiliarioBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem


@AndroidEntryPoint
class CertificateRealeStateFragment :
    BaseFragment<FragmentCertificadoRegistralInmobiliarioBinding>(FragmentCertificadoRegistralInmobiliarioBinding::inflate){


    private val viewModel: CertificateRealeStateViewModel by viewModels()
    private var typeCertSelect = "S"
    private var typePartidaFicha = "1"
    private var certificate: CertificateType? = null
    private var legalRecord: LegalRecord? = null
    private var request: ValidarCriRequest? = null



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }
    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        certificate = arguments?.getParcelable("certificate")
        legalRecord = arguments?.getParcelable("legalRecord")

        with(binding) {
            tvSelectZone.text = certificate?.name
            etRegisterOffice.setOnFocusChangeListener { view, hasFocus ->
                if(hasFocus) {
                    etNumero.clearFocus()
                    hideKeyboard()
                }
            }
            etNumero.setOnFocusChangeListener { view, hasFocus ->
                if(!hasFocus) {
                    hideKeyboard()
                }
            }
            etNumero.onChanged { validateFields() }
            etNumero.filterOnlyMayus()
            rgTypeCert.setOnCheckedChangeListener { _, i ->
                etNumero.clean()
                when (i) {
                    R.id.rb_type_cert_deprecated -> {
                        rbTypeSolicitateFicha.visibility = View.GONE
                        rbTypeSolicitatePart.isChecked = true
                        typeCertSelect = "P"
                        typePartidaFicha = "1"
                    }
                    else -> {
                        typeCertSelect = "S"
                        rbTypeSolicitateFicha.visibility = View.VISIBLE
                        typePartidaFicha = "1"
                    }
                }
                validateFields()
            }
            rgTypeSolicitate.setOnCheckedChangeListener { _, i ->
                etNumero.clean()
                typePartidaFicha = when (i) {
                    R.id.rb_type_solicitate_part -> "1"
                    else -> "2"
                }
                validateFields()
            }
            btnSearch.setOnClickListener {
                etNumero.clearFocus()
                hideKeyboard()
                request = ValidarCriRequest(
                    viewModel.getZoneCode(),
                    viewModel.getCodeOffice(),
                    certificate?.areaId.orEmpty(),
                    etNumero.text.toString(),
                    typeCertSelect,
                    certificate?.codGrupoLibroArea.orEmpty(),
                    typePartidaFicha
                )
                viewModel.validarPartida(request!!)
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getParameters()
            onChange(loading) { showLoading(it) }
            onChange(offices) { setListenerOffice(it) }
            onChange(estadoPartida) { savePartida(it) }

        }
    }


    private fun savePartida(partida: Partida) {

        if (partida.estado == 0) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.businessName = Constants.EMPTY
            paymentDataItem.typePerson = Constants.PERSON_NATURAL
            paymentDataItem.partida = partida
            paymentDataItem.zoneCode = viewModel.getZoneCode()
            paymentDataItem.zoneCodeOffice = viewModel.getCodeOffice()
            paymentDataItem.request = request


            findNavController().navigate(
                R.id.action_certificateRealeStateFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to null,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )

        } else {
            binding.container.snack(partida.msj)
        }
    }

    private fun setListenerOffice(registrationOffices: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(), R.layout.layout_simple_spinner_item, R.id.tv_title, registrationOffices))
            setOnItemClickListener { parent, _, position, _ ->
                tag = (parent.getItemAtPosition(position) as RegistrationOffice).name
                viewModel.selectOfficeForList((parent.getItemAtPosition(position) as RegistrationOffice))
                validateFields()
                binding.etNumero.clean()
            }
        }
    }


    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    private fun validateFields() {
        with(binding) {
            if (
                etNumero.text.toString().isNotEmpty() &&
                etRegisterOffice.validFormTagEmpty()
            ) {
                btnSearch.enable()
            } else {
                btnSearch.disable()
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}