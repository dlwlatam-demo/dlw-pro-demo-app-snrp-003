package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.CertificateOwnershipAsync
import pe.gob.sunarp.appandroid.core.model.VehicleReceiptAsync

@JsonClass(generateAdapter = true)
data class VehicleReceiptAsyncDTO(
    @field:Json(name = "guid")
    val guid: String?,
    @field:Json(name = "status")
    val status: String?,
    @field:Json(name = "transId")
    val transId: String?
) : BaseModel() {
    fun toModel() = VehicleReceiptAsync(
        guid.orEmpty(),
        status.orEmpty(),
        transId.orEmpty()
    )
}