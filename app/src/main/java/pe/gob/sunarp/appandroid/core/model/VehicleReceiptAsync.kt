package pe.gob.sunarp.appandroid.core.model

class VehicleReceiptAsync(
    val guid: String,
    val status: String,
    val transId: String,
)