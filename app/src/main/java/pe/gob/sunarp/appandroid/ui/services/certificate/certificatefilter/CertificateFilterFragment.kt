package pe.gob.sunarp.appandroid.ui.services.certificate.certificatefilter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.clean
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.CertificateType
import pe.gob.sunarp.appandroid.core.model.LegalRecord
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoFiltroBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity

@AndroidEntryPoint
class CertificateFilterFragment :

    BaseFragment<FragmentCertificadoFiltroBinding>(FragmentCertificadoFiltroBinding::inflate) {

    private val viewModel: CertificateFilterViewModel by viewModels()
    private lateinit var type: String
    private var certificate: CertificateType? = null
    private var legalRecord: LegalRecord? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }
    override fun setTittle() {
        if (activity is HomeActivity) {
            val title =
                if (type == "L") R.string.label_certificate_lit_request else R.string.label_certificate_request
            putTitleToolbar((getString(title)))
        }
    }

    private fun initViews() {
        type = arguments?.getString("type").orEmpty()
        setTittle()
        binding.etLegalRecord.clean()
        binding.etCertificateType.clean()

        binding.btnRequest.setOnClickListener {
            if (type == "L") {
                findNavController().navigate(
                    R.id.action_certificateFilterFragment_to_certificateLiteralDepartmentFragment,
                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                )
            } else {
                when (legalRecord?.id) {
                    Constants.REGISTRO_DE_PROPIEDAD ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_PREDIOS, Constants.CERTIFICADO_POSITIVO_PREDIOS_B, Constants.CERTIFICADO_NEGATIVO_PREDIOS, Constants.CERTIFICADO_NEGATIVO_PREDIOS_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedAdvertisingFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_REGISTRAL_BUSQUEDA_CATASTRAL, Constants.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B, Constants.CERTIFICADO_REGISTRAL_INMOBILIARIO_ENTREGA_PAPEL -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certificateRealeStateFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_DE_CARGAS_Y_GRAVAMENES -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certificateChargesFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            else -> {
                                pendingImplement()
                            }
                        }
                    Constants.REGISTRO_DE_PERSONAS_JURIDICAS ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_NEGATIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedLegalPersonFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certificadoVigenciaPoderPersJuridicaFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO -> {
                                findNavController().navigate(
                                    R.id.action_certificatefilterfragment_to_certificadovigenciaorganodirectivofragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ -> {
                                findNavController().navigate(
                                    R.id.action_certificatefilterfragment_to_certificadovigenciapoderpjfragmentt,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            else -> {
                                pendingImplement()
                            }
                        }

                    Constants.REGISTRO_DE_PERSONAS_NATURALES ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA, Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B, Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA, Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B, Constants.CERTIFICADO_NEGATIVO_UNION_DE_HECHO, Constants.CERTIFICADO_POSITIVO_UNION_DE_HECHO, Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL, Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B, Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS, Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B, Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS, Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedUnionFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_VIGENCIA_DESIGNACION_APOYO -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedSupportDesignationFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedConservatorFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_VIGENCIA_PODER, Constants.CERTIFICADO_VIGENCIA_PODER_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedValidityOfpowerFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            else -> {
                                pendingImplement()
                            }
                        }
                    Constants.REGISTRO_DE_BIENES_INMUEBLES ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR, Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B, Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR, Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedVehicleOwnershipFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_REGISTRAL_VEHICULAR, Constants.CERTIFICADO_REGISTRAL_VEHICULAR_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedVehicleRegistrationFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES, Constants.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedChargesFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES, Constants.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B -> {
                                findNavController().navigate(
                                    R.id.action_certificateFilterFragment_to_certifiedBoatsFragment,
                                    bundleOf("certificate" to certificate, "legalRecord" to legalRecord)
                                )
                            }
                            else -> {
                                pendingImplement()
                            }
                        }
                    else -> { //Others
                        pendingImplement()
                    }
                }
            }
        }
    }

    private fun pendingImplement() {
        binding.container.snack("Pendiente por implementar")
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getLegalRecords()
            onChange(certificateTypes) { fillCertificateTypes(it) }
            onChange(legalRecords) { fillLegalRecords(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun fillCertificateTypes(items: List<CertificateType>) {
        validateOptions()
        binding.etCertificateType.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                val certificateSelected = (parent.getItemAtPosition(position) as CertificateType)
                tag = certificateSelected.areaId
                certificate = certificateSelected
                validateOptions()
            }
            setText(adapter.getItem(0).toString(), false)
            val certificateSelected = (adapter.getItem(0) as CertificateType)
            tag = certificateSelected.areaId
            certificate = certificateSelected
            validateOptions()
        }
    }

    private fun fillLegalRecords(items: List<LegalRecord>) {
        validateOptions()
        with(binding) {
            etLegalRecord.apply {
                clean()
                etCertificateType.clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, _, position, _ ->
                    val legalSelected = (parent.getItemAtPosition(position) as LegalRecord)
                    tag = legalSelected.id
                    legalRecord = legalSelected
                    viewModel.getCertificateTypes(type, legalSelected.id)
                    validateOptions()
                }
            }
        }
    }

    private fun validateOptions() {
        with(binding) {
            val certificateType = (etCertificateType.tag ?: "").toString()
            val legalRecord = (etLegalRecord.tag ?: "").toString()
            if (certificateType.isNotEmpty() && legalRecord.isNotEmpty())
                btnRequest.enable()
        }

    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}