package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.supportdesignation

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import javax.inject.Inject

@HiltViewModel
class CertifiedSupportDesignationViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository
) : BaseViewModel() {

    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val loading = SingleLiveEvent<Boolean>()
    val onFormError = SingleLiveEvent<String>()

    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onSaveProcess = SingleLiveEvent<SaveProcess>()
    val onPartidaProcess = SingleLiveEvent<Partida>()

    fun getSpinners() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validatePartida(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.validatePartida(
                    nroPartida,
                    legalRecord,
                    certificate,
                    office
                )
            ) {
                is DataResult.Success -> validatePartidaProcess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    }


    fun validatePartidaProcess(data: Partida) {
        loading.value = false
        onPartidaProcess.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}