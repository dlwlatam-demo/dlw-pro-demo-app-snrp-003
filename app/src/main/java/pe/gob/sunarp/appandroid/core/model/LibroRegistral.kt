package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LibroRegistral(
    val codLibro: String?,
    val nomLibro: String?,
    val areaRegId: String?,
    val grupoLibroArea: String?,
): Parcelable {
    override fun toString(): String {
        return nomLibro.orEmpty()
    }
}