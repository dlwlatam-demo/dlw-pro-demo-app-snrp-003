package pe.gob.sunarp.appandroid.ui.services.property.propertylist

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.DataModel
import pe.gob.sunarp.appandroid.core.model.UserInformation
import pe.gob.sunarp.appandroid.data.property.PropertyRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PropertyListViewModel @Inject constructor(
    private val repository: PropertyRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val onSuccessProperties = SingleLiveEvent<List<DataModel>>()

    fun getPropertyList(
        user: UserInformation
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getPropertiesList(user)) {
                is DataResult.Success -> onSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onSuccess(data: List<DataModel>) {
        loading.value = false
        onSuccessProperties.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}