package pe.gob.sunarp.appandroid.ui.base

import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult

interface PaymentInterface {
    fun sendPaymentConfirmation(data: VisaNetResult)
    fun sendPaymentFailed(ok: Boolean, result: VisaNetError)
}