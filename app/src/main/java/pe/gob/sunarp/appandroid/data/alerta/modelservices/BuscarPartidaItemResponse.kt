package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BuscarPartidaItemResponse(

	@Json(name="descripcion")
	val descripcion: String? = null,

	@Json(name="tipo")
	val tipo: String? = null,

	@Json(name="codLibro")
	val codLibro: String? = null,

	@Json(name="numPartida")
	val numPartida: String? = null,

	@Json(name="ficha")
	val ficha: String? = null,

	@Json(name="regPubId")
	val regPubId: String? = null,

	@Json(name="tomo")
	val tomo: String? = null,

	@Json(name="oficRegId")
	val oficRegId: String? = null,

	@Json(name="placa")
    var placa: String? = null,

	@Json(name="partiSarp")
	val partiSarp: String? = null
)
