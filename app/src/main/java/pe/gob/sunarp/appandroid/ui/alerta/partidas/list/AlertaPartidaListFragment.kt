package pe.gob.sunarp.appandroid.ui.alerta.partidas.list

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.data.alerta.modelservices.PartidaItemResponse
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaPartidaListBinding
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaPartidaListItemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaPartidaListFragment : BaseFragment<FragmentAlertaPartidaListBinding>(FragmentAlertaPartidaListBinding::inflate) {

    private val viewModel: AlertaPartidaListViewModel by viewModels()
    private lateinit var adapterMandatos: ItemPartidaAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }
    private fun initViews() {
        with(binding) {
            initViewModels()
            btnAdd.setOnClickListener {
                findNavController().navigate(R.id.action_nav_alerta_mandato_list_to_alertaPartidaAddFragment)
            }
            adapterMandatos = ItemPartidaAdapter(arrayListOf(), { mandatoitem ->
                activity?.let {
                    val alertaCancelAccountDialog = AlertaPartidaRemoverDialog()
                    alertaCancelAccountDialog.apply {
                        onCancelListener {
                            this.dismiss()
                        }
                        onAcceptListener {
                            this.dismiss()
                            viewModel.cancelPartida(mandatoitem)
                        }
                        show(it.supportFragmentManager, "drop")
                    }
                }
            }, {
                findNavController().navigate(
                    R.id.action_nav_alerta_partida_list_to_alertaPartidaDetailFragment,
                    bundleOf("partida" to it)
                )
            })
            rvListMandatos.adapter = adapterMandatos
            rvListMandatos.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(listData) { setListData(it) }
            getListData()
        }
    }

    private fun setListData(listdata: List<PartidaItemResponse>) {
        adapterMandatos.items.clear()
        adapterMandatos.items.addAll(listdata)
        adapterMandatos.notifyDataSetChanged()
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItemPartidaAdapter(
        var items: MutableList<PartidaItemResponse> = arrayListOf(),
        private val alertRemove: (item: PartidaItemResponse) -> Unit,
        private val openDetail: (item: PartidaItemResponse) -> Unit
    ) : RecyclerView.Adapter<ItemPartidaAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutAlertaPartidaListItemBinding =
                LayoutAlertaPartidaListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item, alertRemove, openDetail)
        }

        override fun getItemCount() = items.size

        inner class ViewHolder(private val binding: LayoutAlertaPartidaListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(
                item: PartidaItemResponse,
                deleteFuntion: (item: PartidaItemResponse) -> Unit,
                openDetailFunction: (item: PartidaItemResponse) -> Unit
            ) = with(binding) {
                statusContainer.setOnClickListener { openDetailFunction(item) }
                btnDelete.setOnClickListener { deleteFuntion(item) }
                tvTitle.text = item.nuPart
                tvType.text = item.area
                tvZona.text = "${item.region} - ${item.sede}"
                tvFecha.text = item.fecReg
                tvInscripcion.text = item.estado
                if (item.estado.orEmpty().lowercase() == "activa") {
                    tvInscripcion.setTextColor(Color.parseColor("#00a5a5"))
                }
                tvPublicidad.text = item.estadoAP
                if (item.estadoAP.orEmpty().lowercase() == "activa") {
                    tvPublicidad.setTextColor(Color.parseColor("#00a5a5"))
                }
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
