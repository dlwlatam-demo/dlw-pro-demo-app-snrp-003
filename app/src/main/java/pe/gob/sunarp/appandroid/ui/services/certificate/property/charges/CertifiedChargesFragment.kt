package pe.gob.sunarp.appandroid.ui.services.certificate.property.charges


import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoCargasBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertifiedChargesFragment :
    BaseFragment<FragmentCertificadoCargasBinding>(FragmentCertificadoCargasBinding::inflate),
    PaymentInterface {


    private val viewModel: CertifiedChargesViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }
    override fun onStart() {
        super.onStart()
        binding.etRequest.apply {
            val items = resources.getStringArray(R.array.property_charges)
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
        }

    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()

        with(binding) {
            etRequest.apply {
                val items = resources.getStringArray(R.array.property_charges)
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnClickListener { hideKeyboard() }
                setOnItemClickListener { parent, _, position, _ ->
                    val item = (parent.getItemAtPosition(position) as String)
                    etNroPartida.hint = "Ingresa número de $item"
                    tag = position
                    validateFields()
                    etNroPartida.clean()
                    hideKeyboard()
                }
            }

            etNroPartida.filterOnlyMayus()
            etNroPartida.onChanged { validateFields() }
            tilExpediente.editText?.onChanged { validateFields() }

            btnApply.setOnClickListener {
                hideKeyboard()
                var nroPartida = etNroPartida.text.toString()
                certificate?.let {
                    etRequest.tag?.let { tipoPartida ->
                        var tipoNumero = "P"
                        if (tipoPartida == 1) {
                            tipoNumero = "M"
                        }
                        viewModel.validatePartidaCGV(
                            nroPartida,
                            tipoNumero,
                            legalRecord,
                            certificate,
                            office,
                        )
                    }
                }
            }

            etRegisterOffice.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etRequest.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }

        }
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(onPartidaProcess) { savePartida(it) }
        }
    }


    private fun savePartida(partida: Partida) {

        if (partida.estado == 0) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.businessName = Constants.EMPTY
            paymentDataItem.typePerson = Constants.PERSON_NATURAL
            paymentDataItem.partida = partida
            paymentDataItem.expediente = binding.etExpediente.text.toString()

            findNavController().navigate(
                R.id.action_certifiedChargesFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to office,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )
        } else {
            binding.container.snack(partida.msj)
        }
    }

    fun validateFields() {
        with(binding) {
            if (
                etRequest.validFormTagEmpty() &&
                etRegisterOffice.validFormTagEmpty() &&
                etNroPartida.validFormNotEmpty()
            ) {
                btnApply.enable()
            } else btnApply.disable()
        }
    }


    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
            setOnClickListener { hideKeyboard() }
        }
    }

    override fun sendPaymentConfirmation(data: VisaNetResult) {
        /*
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificate?.let {
                saveProcess?.let { it1 ->
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, null
                    )
                }
            }
        }
        */
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber, Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = CertifiedChargesFragmentDirections
            .actionCertifiedChargesFragmentToPaymentResultFragment(
                transId = transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}