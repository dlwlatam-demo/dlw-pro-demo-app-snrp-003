package pe.gob.sunarp.appandroid.ui.office.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.databinding.LayoutDetailItemOfficeBinding

class InforDetailOfficeHistoryAdapter(
    private val items: List<ItemDetail> = arrayListOf()
) : RecyclerView.Adapter<InforDetailOfficeHistoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutDetailItemOfficeBinding =
            LayoutDetailItemOfficeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutDetailItemOfficeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ItemDetail) = with(binding) {
            with(item) {
               tvTittle.text = title
               tvSubtitle.text = detail
            }
        }
    }


}