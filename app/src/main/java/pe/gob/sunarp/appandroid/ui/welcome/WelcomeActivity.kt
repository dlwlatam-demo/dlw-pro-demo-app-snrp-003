package pe.gob.sunarp.appandroid.ui.welcome

import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.ActivityWelcomeBinding
import pe.gob.sunarp.appandroid.ui.base.BaseActivity

@AndroidEntryPoint
class WelcomeActivity : BaseActivity<ActivityWelcomeBinding>() {

    override fun viewBinding(): ActivityWelcomeBinding = ActivityWelcomeBinding.inflate(layoutInflater)

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_welcome)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

}