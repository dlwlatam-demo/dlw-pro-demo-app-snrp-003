package pe.gob.sunarp.appandroid.ui.transactions.clarification

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoClarificacionBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.SuccessDialog
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page

@AndroidEntryPoint
class ClarificationRetrievalFragment :
    BaseFragment<FragmentCertificadoClarificacionBinding>(FragmentCertificadoClarificacionBinding::inflate) {

    private val viewModel: CertificateClarificationViewModel by viewModels()
    private var transId: Int? = null
    private var type: String? = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        type = arguments?.getString("type")
        setTittle()
        binding.apply {
            val description = when (type) {
                "ACL" -> getString(R.string.certificate_clarification_description)
                "SUB" -> getString(R.string.retrieval_description)
                else -> ""
            }

            tvDescription.text = description
            btnSend.setOnClickListener {
                if (type.equals("ACL")) {
                    viewModel.sendClarification("$transId", etObservation.text.toString())
                } else {
                    viewModel.sendRetrieval("$transId", etObservation.text.toString())
                }

            }
        }
    }

    override fun setTittle() {
        type?.let {
            if (activity is HomeActivity) {
                val title = when (it) {
                    "ACL" -> getString(R.string.certificate_clarification)
                    "SUB" -> getString(R.string.retrieval_title)
                    else -> ""
                }
                putTitleToolbar(title)
            }
        }
    }

    private fun initViewModels() {
        transId = arguments?.getInt("transId")
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(onComplete) { showDialog() }
        }
    }

    private fun showDialog() {
        activity?.let {
            val newFragment = SuccessDialog()
            newFragment.show(it.supportFragmentManager, "success")
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}