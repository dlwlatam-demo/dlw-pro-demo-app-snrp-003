package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.validityofpower

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import javax.inject.Inject

@HiltViewModel
class CertifiedValidityOfpowerViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository
) : BaseViewModel() {

    val docTypes = SingleLiveEvent<List<DocumentType>>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val books = SingleLiveEvent<List<BooksByOffice>>()

    val loading = SingleLiveEvent<Boolean>()
    val onFormError = SingleLiveEvent<String>()

    val isValidDocumentDNI = SingleLiveEvent<DocumentDNI>()
    val isValidDocumentCE = SingleLiveEvent<DocumentCE>()
    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onSaveProcess = SingleLiveEvent<SaveProcess>()
    val onPaymentProcess = SingleLiveEvent<PaymentProcess>()

    val onPartidaProcess = SingleLiveEvent<Partida>()
    val onPartidaProcessTomo = SingleLiveEvent<List<String>>()

    fun getSpinners() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getBooks(areaRegId: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getBooks(areaRegId)) {
                is DataResult.Success -> onBooksSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validatePartidaTomo(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        libroArea: String?,
        tipoPartidaFicha: String?,
        folio: String?,
        tomo: String?
    ) {
        viewModelScope.launch {
            loading.value = true
            var nro = Constants.EMPTY
            if (tipoPartidaFicha.equals("1")) {
                nro = nroPartida
            } else {
                nro = "${tomo}-${folio}"
            }

            when (
                val result = repository.validatePartidaTomo(
                    nro,
                    legalRecord,
                    certificate,
                    office,
                    libroArea,
                    tipoPartidaFicha,
                )
            ) {
                is DataResult.Success -> validatePartidaTomoProcess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validatePartidaVigencia(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.validatePartidaVigencia(
                    nroPartida,
                    legalRecord,
                    certificate,
                    office
                )
            ) {
                is DataResult.Success -> validatePartidaProcess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validatePartidaTomoProcess(data: List<String>) {
        loading.value = false
        onPartidaProcessTomo.value = data
    }


    fun validatePartidaProcess(data: Partida) {
        loading.value = false
        onPartidaProcess.value = data
    }

    private fun onBooksSuccess(data: List<BooksByOffice>) {
        loading.value = false
        books.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }
}