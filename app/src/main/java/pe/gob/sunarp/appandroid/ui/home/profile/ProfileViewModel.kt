package pe.gob.sunarp.appandroid.ui.home.profile

import android.graphics.Bitmap
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.profile.information.ProfileRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.home.profile.model.ProfileItem
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: ProfileRemoteRepository
) : BaseViewModel() {

    val profile = SingleLiveEvent<ProfileItem>()
    val loading = SingleLiveEvent<Boolean>()
    val onLoadPhoto = SingleLiveEvent<Bitmap>()

    fun getUserInformation() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getUserInformation()) {
                is DataResult.Success -> sendData(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
            onLoadPhoto.value = repository.getPhoto()
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun sendData(data: ProfileItem) {
        loading.value = false
        profile.value = data
    }


}