package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.databinding.LayoutTransactionStatusItemBinding

class StatusInformationAdapter(
    private val items: List<StatusInfoItem>
) : RecyclerView.Adapter<StatusInformationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemBinding =
            LayoutTransactionStatusItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutTransactionStatusItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: StatusInfoItem) = with(binding) {
            with(item) {
                tvName.text = title
                tvDescription.text = description
            }
        }
    }


}