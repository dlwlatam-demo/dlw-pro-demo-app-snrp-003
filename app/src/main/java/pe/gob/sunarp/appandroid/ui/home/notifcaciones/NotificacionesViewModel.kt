package pe.gob.sunarp.appandroid.ui.home.notifcaciones

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.solicitud.SolicitudRemoteRepository
import pe.gob.sunarp.appandroid.data.solicitud.model.SolicitudNotiItemResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class NotificacionesViewModel @Inject constructor(
    private val repository: SolicitudRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val listData = SingleLiveEvent<List<SolicitudNotiItemResponse>>()


    fun getListNotificaciones() = viewModelScope.launch {
        loading.value = true
        when (val result = repository.getNotificationes()) {
            is DataResult.Success -> onGetListNotificacionesSuccess(result.data)
            is DataResult.Failure -> onError(result.exception)
        }
    }

    private fun onGetListNotificacionesSuccess(data: List<SolicitudNotiItemResponse>) {
        loading.value = false
        listData.value = data
    }


    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}