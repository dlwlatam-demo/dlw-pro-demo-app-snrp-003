package pe.gob.sunarp.appandroid.ui.office.route

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.huawei.hms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.maps.MapsRemoteRepository
import pe.gob.sunarp.appandroid.data.maps.request.DataDirectionsRequest
import pe.gob.sunarp.appandroid.data.maps.request.DirectionsResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject


@HiltViewModel
class OfficeRouteViewModel @Inject constructor(
    private val repository: MapsRemoteRepository,
) : BaseViewModel() {

    lateinit var destiny: LatLng
    lateinit var myposition: LatLng
    lateinit var gkey: String
    val loading = SingleLiveEvent<Boolean>()
    val drawingLinesdrawingLines = SingleLiveEvent<DirectionsResponse>()



    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun initData(destiny: LatLng, myposition: LatLng, gkey: String) {
        this.destiny = destiny
        this.myposition = myposition
        this.gkey = gkey
    }

    fun mapChargeCompleate() {
        val url = getURLRoute()
        Log.e("TAG", "mapChargeCompleate: $url")
        getDataMapsDirections()
    }

    private fun getURLRoute(): String {
        val strOrigin = "origin=${myposition.latitude},${myposition.longitude}"

        val strDest = "destination=${destiny.latitude},${destiny.longitude}"

        val sensor = "sensor=false"
        val mode = "mode=walking"
        val key = "key=$gkey"

        val parameters = "$strOrigin&$strDest&$sensor&$mode&$key"

        return "https://maps.googleapis.com/maps/api/directions/json?$parameters"
    }

    private fun getDataSend(): DataDirectionsRequest
        = DataDirectionsRequest(
            origin =  "${myposition.latitude},${myposition.longitude}",
            destination =  "${destiny.latitude},${destiny.longitude}",
            key =  gkey,
        )


    private fun getDataMapsDirections() {
//        loading.value = true
        val dataSend  = getDataSend()
        viewModelScope.launch {
            when (val result = repository.getMapaApis(dataSend)) {
                is DataResult.Success -> onGetDirectionSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onGetDirectionSuccess(data: DirectionsResponse) {
        drawingLinesdrawingLines.value = data
        loading.value = false
    }


}