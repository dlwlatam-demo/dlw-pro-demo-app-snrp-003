package pe.gob.sunarp.appandroid.ui.alerta.mandatos.list

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaMandatoRemoverBinding


class AlertaMandatoRemoverDialog : DialogFragment() {

    private var onCancelListener: (() -> Unit)? = null
    private var onAcceptListener: (() -> Unit)? = null

    private var _binding: LayoutAlertaMandatoRemoverBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = LayoutAlertaMandatoRemoverBinding.inflate(LayoutInflater.from(context))
        binding.btnBack.setOnClickListener {
            onCancelListener?.invoke()
        }
        binding.btnAccept.setOnClickListener {
            onAcceptListener?.invoke()
        }

        return AlertDialog.Builder(requireActivity(), R.style.MyThemeOverlayAlertDialog)
            .setView(binding.root)
            .setCancelable(false)
            .create()
    }

    fun onCancelListener(listener: () -> Unit) {
        this.onCancelListener = listener
    }

    fun onAcceptListener(listener: () -> Unit) {
        this.onAcceptListener = listener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}