package pe.gob.sunarp.appandroid.ui.transactions.history.model

data class Transaction (
    val reason: String?,
    val transactionNumber: Int?,
    val service: String?,
    val advertisingNumber: String?,
    val date: String?,
    val key: Int?,
    val ammount: Double?,
    val type: String?,
    val color: Int,
    val buttonText: String?
)