package pe.gob.sunarp.appandroid.core.custom.ui.otp.handler

import androidx.core.widget.TextViewCompat.setTextAppearance
import pe.gob.sunarp.appandroid.core.custom.ui.exhaustive
import pe.gob.sunarp.appandroid.core.custom.ui.hideKeyboard
import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.SUNOTPViewInterface
import pe.gob.sunarp.appandroid.core.custom.ui.otp.model.SUNOTPState
import pe.gob.sunarp.appandroid.core.custom.ui.otp.model.SUNOTPStateRes
import pe.gob.sunarp.appandroid.core.custom.ui.otp.view.SUNDigitTextView

internal class SUNOTPHandler(listener: SUNOTPViewInterface) {
    private val view = listener.getView()
    private val container = listener.getComponentView()

    fun setState(state: SUNOTPState) {
        when (state) {
            SUNOTPState.DEFAULT -> toDefaultState()
            SUNOTPState.FOCUSED -> toFocusedState()
            SUNOTPState.VALIDATING -> toValidatingState()
            SUNOTPState.VALIDATED -> toValidatedState()
            SUNOTPState.ERROR -> toErrorState()
        }.exhaustive
    }

    // methods to change the container

    private fun toDefaultState() {
        with(SUNOTPStateRes.DEFAULT) {
            // container
            focusableEditText(true)

            with(view) {
                sunOTPEditText.setText("")
                messageTextView.text =
                    container.context.getString(message, container.seconds)
                setupClickListener()
                setTextAppearance(messageTextView, textAppearance)
            }
            container.playCountDownTimer()
            container.listener?.onStarCount()
            // digits
            stateDigits { it.editableState() }
        }
    }

    private fun toFocusedState() {
        with(SUNOTPStateRes.FOCUSED) {
            // container
            focusableEditText(true)

            with(view) {
                setupClickListener()
                setTextAppearance(messageTextView, textAppearance)
            }
            // digits
            stateDigits { it.editableState() }
        }
    }

    private fun toResendCodeState() {
        with(SUNOTPStateRes.RESEND) {
            // container
//            container.timer?.cancel()
            focusableEditText(false)

            with(view) {
                sunOTPEditText.hideKeyboard()
                messageTextView.text = container.context.getString(message)
                setupClickListener {
                    toDefaultState()
                    container.listener?.onResendCode()
                }
                setTextAppearance(messageTextView, textAppearance)
            }
            // digits
            stateDigits { it.editableState() }
        }
    }

    private fun toValidatingState() {
        with(SUNOTPStateRes.VALIDATING) {

            // container
//            container.timer?.cancel()
            focusableEditText(false)

            with(view) {
                sunOTPEditText.hideKeyboard()
                messageTextView.text =
                    container.context.getString(message)
                setupClickListener()
                setTextAppearance(messageTextView, textAppearance)
//            circularProgressIndicator.show()
            }
            // digits
            stateDigits { it.validatedState() }
        }
    }

    private fun toValidatedState() {
        with(SUNOTPStateRes.VALIDATED) {
            // container
            container.timer?.cancel()
            focusableEditText(false)

            with(view) {
                sunOTPEditText.hideKeyboard()
                messageTextView.text = container.context.getString(message)
                setupClickListener()
                setTextAppearance(messageTextView, textAppearance)
//            circularProgressIndicator.hide()
            }
            // digits
            stateDigits { it.validatedState() }
        }
    }

    private fun toErrorState() {
        with(SUNOTPStateRes.ERROR) {
            // container
//            container.timer?.cancel()
            focusableEditText(true)

            with(view) {
                messageTextView.text = container.context.getString(message)
                setupClickListener()
                setTextAppearance(messageTextView, textAppearance)
//            circularProgressIndicator.hide()
            }
            // digits
            stateDigits { it.errorState() }
        }
    }

    fun updateDefaultText(timeCountDown: Int) {
//        view.messageTextView.text = "${(timeCountDown / 60).toString().padStart(2, '0')}:${(timeCountDown % 60).toString().padStart(2, '0')}"
        if (timeCountDown == 0) {
            container.listener?.onStopCount()
            toResendCodeState()
        }
    }

    private fun setupClickListener(onClickListener: (() -> Unit)? = null) {
        view.messageTextView.setOnClickListener {
            onClickListener?.invoke()
        }
    }

    // methods to change shape of digits (background)

    private fun focusableEditText(focusable: Boolean) {
        view.sunOTPEditText.isFocusableInTouchMode = focusable
        view.sunOTPEditText.isFocusable = focusable
    }

    private fun stateDigits(state: (SUNDigitTextView) -> Unit) {
        for (sunDigitTextView in container.digits) {
            state(sunDigitTextView)
        }
    }
}