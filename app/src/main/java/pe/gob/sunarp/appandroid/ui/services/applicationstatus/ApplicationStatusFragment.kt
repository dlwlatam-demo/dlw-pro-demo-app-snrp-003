package pe.gob.sunarp.appandroid.ui.services.applicationstatus

import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.databinding.FragmentApplicationStatusBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class ApplicationStatusFragment :
    BaseFragment<FragmentApplicationStatusBinding>(FragmentApplicationStatusBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            radioGroup.setOnCheckedChangeListener { _, i ->
                when (i) {
                    R.id.rb_filter_number -> enableNumberFields()
                    else -> enableYearFields()
                }
            }

            etFirstField.onChanged { validateFields() }
            etSecondField.onChanged { validateFields() }

            btnSearch.setOnClickListener {
                val transId: String =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_filter_number) etFirstField.text.toString()
                    else etSecondField.text.toString()
                val year =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_filter_year) etFirstField.text.toString() else ""
                findNavController().navigate(
                    R.id.action_applicationStatusFragment_to_transactionStatusFragment,
                    bundleOf(
                        "transId" to transId.toIntOrNull(),
                        "year" to year,
                        "page" to "APP_STATUS"
                    )
                )
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            if ((radioGroup.checkedRadioButtonId == R.id.rb_filter_number && etFirstField.text.toString()
                    .isNotEmpty()) || (radioGroup.checkedRadioButtonId == R.id.rb_filter_year && etFirstField.text.toString()
                    .isNotEmpty() && etSecondField.text.toString().isNotEmpty())
            ) {
                btnSearch.enable()
            } else {
                btnSearch.disable()
            }
        }
    }

    private fun enableNumberFields() {
        with(binding) {
            etFirstField.apply {
                hint = getString(R.string.application_status_request_number)
                clean()
                filters = arrayOf<InputFilter>(
                    LengthFilter(15)
                )
            }
            tilSecondField.hide()
        }
    }

    private fun enableYearFields() {
        with(binding) {
            etFirstField.apply {
                clean()
                hint = getString(R.string.application_status_year_input)
                filters = arrayOf<InputFilter>(
                    LengthFilter(4)
                )
            }
            tilSecondField.show()
            etSecondField.clean()
        }
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }

}