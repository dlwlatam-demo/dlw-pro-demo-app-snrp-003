package pe.gob.sunarp.appandroid.ui.home

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: LoginRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) : BaseViewModel() {

    val model = SingleLiveEvent<DrawerModel>()
    val logOutSuccess = SingleLiveEvent<Boolean>()

    fun getDrawerInformation() {
        viewModelScope.launch {
            model.value = DrawerModel(
                repository.getUserPhoto(),
                repository.getUserName(),
                repository.getUserDoc()
            )
        }
    }

    fun logOut() {
        viewModelScope.launch {
            when (val result = repository.logout()) {
                is DataResult.Success -> sendData(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun sendData(data: Boolean) {
        logOutSuccess.value = data
    }

    fun getPreferences() = preferences
}