package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DeleteMandatoRequest(

	@Json(name="nuCont")
	val nuCont: String? = null,

	@Json(name="aaCont")
	val aaCont: String? = null
)
