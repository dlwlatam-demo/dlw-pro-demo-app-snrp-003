package pe.gob.sunarp.appandroid.ui.transactions.payment.model

import lib.visanet.com.pe.visanetlib.presentation.custom.VisaNetViewAuthorizationCustom

data class VisaNetItem(
    val data: Map<String, Any>,
    val custom: VisaNetViewAuthorizationCustom
)