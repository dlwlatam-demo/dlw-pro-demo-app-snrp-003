package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UpdateAccountRequest(
    @field:Json(name = "idRgst")
    val idRgst: String,
    @field:Json(name = "apPate")
    val apPate: String,
    @field:Json(name = "apMate")
    val apMate: String,
    @field:Json(name = "nombre")
    val nombre: String,
    @field:Json(name = "tiDocu")
    val tiDocu: String,
    @field:Json(name = "noDocu")
    val noDocu: String,
    @field:Json(name = "direccion")
    val direccion: String,
    @field:Json(name = "cnumCel")
    val cnumCel: String,
    @field:Json(name = "idOperTele")
    val idOperTele: String,
    @field:Json(name = "flEnvioSms")
    val flEnvioSms: String,
)