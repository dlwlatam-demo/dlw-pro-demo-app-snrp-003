package pe.gob.sunarp.appandroid.ui.register.confirm

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.core.model.RegisterInformation
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterConfirmViewModel @Inject constructor(val repository: UserRegistrationRemoteRepository) :
    BaseViewModel() {

    val onFormError = SingleLiveEvent<String>()
    val onError = SingleLiveEvent<String?>()
    val isConfirm = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun confirm(
        registerInformation: RegisterInformation,
        password: String,
        passwordConfirm: String
    ) {
        viewModelScope.launch {
            showLoading.value = true
            when (val status = validateForm(password, passwordConfirm)) {
                is FormStatus.Ok -> {
                    when (val result = repository.registerUser(
                        RegisterInformation(
                            email = registerInformation.email,
                            gender = registerInformation.gender,
                            documentType = registerInformation.documentType,
                            documentNumber = registerInformation.documentNumber,
                            names = registerInformation.names,
                            firstLastname = registerInformation.firstLastname,
                            secondLastname = registerInformation.secondLastname,
                            date = registerInformation.date,
                            numberPhone = registerInformation.numberPhone,
                            password = password
                        )
                    )) {
                        is DataResult.Success -> {
                            isConfirm.value = result.data
                            showLoading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> onError(Exception(status.error))
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        onError.value = err.message
        super.onError(err)
    }

    private fun validateForm(
        password: String,
        passwordConfirm: String
    ): FormStatus {

        if (!password.isValidString(Constants.REGEX_PASS)) return FormStatus.Ko("No cumple con los requisitos de seguridad: 1 mayúscula, 1 minúscula, 1 número, mínimo 8 caracteres")
        if (password.isEmpty()) return FormStatus.Ko("Olvidó el campo Contraseña")
        if (passwordConfirm.isEmpty()) return FormStatus.Ko("Olvidó el Campo confirmar contraseña")
        if (passwordConfirm != password) return FormStatus.Ko("Las contraseñas no coinciden")

        return FormStatus.Ok
    }

}