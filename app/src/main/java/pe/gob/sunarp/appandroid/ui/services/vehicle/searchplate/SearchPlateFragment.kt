package pe.gob.sunarp.appandroid.ui.services.vehicle.searchplate

import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.filterOnlyMayus
import pe.gob.sunarp.appandroid.core.filterOnlyMayusLiteralPlaca
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentSearchPlateBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class SearchPlateFragment :
    BaseFragment<FragmentSearchPlateBinding>(FragmentSearchPlateBinding::inflate) {

    private val viewModel: SearchPlateViewModel by viewModels()
    private var registerOfficeSelected: RegistrationOffice? = null
    private var isPlateValidate: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        isPlateValidate = false
        with(binding) {
            etRegisterOffices.hide()
            registerOfficesContainer.hide()
            etPlate.filterOnlyMayusLiteralPlaca()
            etPlate.onChanged { validateFields() }
            /*registerOfficesContainer.setOnClickListener {
                hideKeyboard()
            }*/
            officesContainer.setOnClickListener {
                hideKeyboard()
            }
            /*etRegisterOffices.setOnClickListener {
                hideKeyboard()
            }*/
            btnSearch.setOnClickListener {
                val vehicleInformation = VehicleInformation(
                    etPlate.text.toString().uppercase().replace("-", ""),
                    "00",
                    "00"
                )

                viewModel.getVehicleInformation(vehicleInformation) { isPlateValidate ->
                    Log.e("******", isPlateValidate.toString())
                    if (isPlateValidate){
                        findNavController().navigate(
                            R.id.action_searchPlateFragment_to_vehicleInformationFragment,
                            bundleOf("information" to vehicleInformation)
                        )
                    }
                }



            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getZones()
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(plateSuccess) {
                Log.e("Punto de control","****")
                isPlateValidate = true
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffices.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                registerOfficeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                tag = registerOfficeSelected?.name.orEmpty()
                validateFields()
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            val registerOffices = (etRegisterOffices.tag ?: "").toString()
            if (etPlate.text.toString().isNotEmpty())
                btnSearch.enable()
            else
                btnSearch.disable()
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }






    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}