package pe.gob.sunarp.appandroid.ui.services.certificate.property.vehicleregistration


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoVehiculoRegistrationBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertifiedVehicleRegistrationFragment :
    BaseFragment<FragmentCertificadoVehiculoRegistrationBinding>(
        FragmentCertificadoVehiculoRegistrationBinding::inflate
    ),
    PaymentInterface {


    private val viewModel: CertifiedVehicleRegistrationViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var saveProcess: SaveProcess? = null
    private var isNav = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        initViewModels()
    }

    override fun onStart() {
        super.onStart()
        binding.etRequest.clean()
        binding.tilRequestPlaca.editText?.clean()
        binding.tilRequestNumber.editText?.clean()
        viewModel.documentSaved.value = ""
        binding.btnApply.disable()
        binding.etRequest.apply {
            val items = resources.getStringArray(R.array.property_vehicular_register)
            Log.e("puntoControl", "creacion de spinner")
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )

        }

    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews(view: View) {
        val context: Context = requireContext()

        val rootView = view.findViewById<View>(R.id.container)
        rootView.setOnClickListener { view ->
            if (view !is TextInputEditText) {
                hideKeyboard(context, rootView)
            }
        }

        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()

        with(binding) {
            etPartida.clean()
            etPlaca.clean()
            etPartida.filterOnlyMayus()
            etPlaca.filterOnlyMayus()
            Log.e("PuntoControl-docTySaved", viewModel.documentTypeSaved.value.toString())
            Log.e("PuntoControl-docSaved", viewModel.documentSaved.value.toString())
            if (viewModel.documentTypeSaved.value.toString() == "Placa") {
                etPartida.visibility = View.GONE
                etPlaca.visibility = View.VISIBLE
            } else {
                etPartida.visibility = View.VISIBLE
                etPlaca.visibility = View.GONE

            }
           if (viewModel.documentSaved.value.toString() != "null" && viewModel.documentSaved.value.toString() != "") {
               Log.e("Activa boton", "true")
               btnApply.enable()
           }

            etRequest.apply {
                /*
                val items = resources.getStringArray(R.array.property_vehicular_register)
                Log.e("etRequest", items.size.toString())
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                */
                setOnClickListener { hideKeyboard() }
                setOnItemClickListener { parent, _, position, _ ->
                    val item = (parent.getItemAtPosition(position) as String)
                    tag = position
                    etPartida.clean()
                    etPlaca.clean()
                    if(item == "Partida") {
                        etPartida.visibility = View.VISIBLE
                        etPlaca.visibility = View.GONE
                    } else {
                        etPartida.visibility = View.GONE
                        etPlaca.visibility = View.VISIBLE
                    }
                    hideKeyboard()
                    validateFields()
                    /*
                    if (viewModel.documentSaved.value.toString() != "null" && viewModel.documentSaved.value.toString() != "") {
                        Log.e("Activa boton", "true")
                        btnApply.enable()
                    }
                    else {
                        btnApply.disable()
                    }
                    */
                }
                // val itemsString = items.joinToString(", ")

                // Log.e("etRequest2", itemsString)
            }

            tilRequestNumber.editText?.onChanged { validateFields() }
            tilRequestPlaca.editText?.onChanged { validateFields() }

            btnApply.setOnClickListener {
                var nroDocumento =""
                if (isPartida()) {
                    binding.tilRequestPlaca.editText?.clean()
                    nroDocumento = binding.tilRequestNumber.editText?.text.toString()
                    viewModel.documentTypeSaved.value = "Partida"
                    viewModel.documentSaved.value = nroDocumento

                } else {
                    binding.tilRequestNumber.editText?.clean()
                    nroDocumento = binding.tilRequestPlaca.editText?.text.toString().replace("-", "")
                    viewModel.documentTypeSaved.value = "Placa"
                    viewModel.documentSaved.value = nroDocumento

                }
                Log.e("PuntoControl", nroDocumento)
                hideKeyboard(context, rootView)
                Log.e("PuntoControl", "1")
                if (isNav && nroDocumento != "") {
                    Log.e("PuntoControl", "2")
                    var tipoNumero = "C"
                    if (isPartida()) {
                        tipoNumero = "P"
                    }
                    Log.e("PuntoControl", tipoNumero)
                    viewModel.validatePartidaCGV(
                        nroDocumento,
                        tipoNumero,
                        legalRecord,
                        certificate,
                        office,
                    )
                }
                if (!isNav && nroDocumento != "") {
                    certificate?.let {
                        etRequest.tag?.let { tipoPartida ->
                            var tipoNumero = "C"
                            if (tipoPartida == 1) {
                                tipoNumero = "P"
                            }
                            Log.e("PuntoControl-tipoNumero", tipoNumero)
                            Log.e("PuntoControl-numDocu", nroDocumento)
                            Log.e("PuntoControl-tipParti", tipoPartida.toString())
                            viewModel.validatePartidaCGV(
                                nroDocumento,
                                tipoNumero,
                                legalRecord,
                                certificate,
                                office,
                            )
                        }
                    }
                }
            }
        }
    }

    private fun isPartida() = binding.etRequest.tag == 1

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(onPartidaProcess) { savePartida(it) }
        }
    }

    private fun hideKeyboard(context: Context, rootView: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(rootView.windowToken, 0)
    }
    private fun savePartida(partida: Partida) {

        if (partida.estado == 0) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.businessName = Constants.EMPTY
            paymentDataItem.typePerson = Constants.PERSON_NATURAL
            Log.e("num placa", partida.numPlaca)
            paymentDataItem.partida = partida
            if (!isPartida()) {
                paymentDataItem.partida!!.numPlaca = binding.etPlaca.text.toString().replace("-", "")
            }

            findNavController().navigate(
                R.id.action_certifiedVehicleRegistrationFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to office,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )
            //clearView()
        } else {
            binding.container.snack(partida.msj)
        }
    }

    fun validateFields() {
        with(binding) {
            if (
                etRequest.validFormTagEmpty() &&
                etRegisterOffice.validFormTagEmpty() &&
                if(!isPartida()) etPlaca.validFormNotEmpty() else etPartida.validFormNotEmpty()
            ) {
                btnApply.enable()
            } else {
                btnApply.disable()
            }
            /*
            if (viewModel.documentSaved.value.toString() != "null" && viewModel.documentSaved.value.toString() != "" ) {
                btnApply.enable()
                isNav = true
                Log.e("PuntoControl", isNav.toString())
            }else {
                btnApply.disable()
            }
            */
        }
    }


    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
                hideKeyboard()
            }
        }
    }


    override fun sendPaymentConfirmation(data: VisaNetResult) {
        /*
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificate?.let {
                saveProcess?.let { it1 ->
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, null
                    )
                }
            }
        }
        */
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment("0", Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = CertifiedVehicleRegistrationFragmentDirections
            .actionCertifiedVehicleRegistrationFragmentToPaymentResultFragment(
                transId = transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

    private fun clearView(){
        binding.etRequest.clean()
        binding.etPlaca.clean()
        binding.etPartida.clean()
    }

}