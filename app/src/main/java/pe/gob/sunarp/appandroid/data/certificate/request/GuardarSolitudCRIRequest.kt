package pe.gob.sunarp.appandroid.data.certificate.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class GuardarSolitudCRIRequest(
    @field:Json(name = "codCerti")
    val codCerti: String?,
    @field:Json(name = "codArea")
    val codArea: String?,
    @field:Json(name = "codLibro")
    val codLibro: String?,
    @field:Json(name = "oficinaOrigen")
    val oficinaOrigen: String?,
    @field:Json(name = "refNumPart")
    val refNumPart: String?,
    @field:Json(name = "refNumPartMP")
    val refNumPartMP: String?,
)