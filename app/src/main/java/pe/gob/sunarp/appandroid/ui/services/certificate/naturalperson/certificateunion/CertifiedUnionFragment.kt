package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.certificateunion


import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoUnionBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertifiedUnionFragment :
    BaseFragment<FragmentCertificadoUnionBinding>(FragmentCertificadoUnionBinding::inflate),
    PaymentInterface {

    private val viewModel: CertifiedUnionViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var documentType: DocumentType? = null
    private var isSelectedDocumentType: Boolean = false
    private var isSelectedRegisterZone: Boolean = false
    private var tipoDocPN: String = ""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()
        isSelectedRegisterZone = false

        with(binding) {

            enableFieldsPerson()
            clearInputValues()
            tvDocumentType.tag = "09"
            tipoDocPN = "09"
            viewModel.getDocumentType(Constants.PERSON_NATURAL)

            /*tvDocumentType.setOnItemClickListener { parent, _, position, _ ->
                documentType =
                    (parent.getItemAtPosition(position) as DocumentType)
                tvDocumentType.tag = documentType?.id
                clearInputValues()
            }*/
            etDocumentNumber.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etFirstLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etSecondLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etNames.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tvDocumentType.setOnFocusChangeListener { view, hasFocus ->
                if(!hasFocus) {
                    hideKeyboard()
                } else {
                    clearInputValues()
                }
            }

            tvDocumentType.setOnItemClickListener { parent, _, position, _ ->
                documentType =
                    (parent.getItemAtPosition(position) as DocumentType)
                tvDocumentType.tag = documentType?.id
                Log.e("tvDocumentType", tvDocumentType.tag.toString())
                isSelectedDocumentType = true
                clearInputValues()
                when (certificate?.certificateId.toString()) {
                    Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA,
                    Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA_B,
                    Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR,
                    Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B,
                    Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA,
                    Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B,
                    Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS,
                    Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B,
                    Constants.CERTIFICADO_POSITIVO_PREDIOS,
                    Constants.CERTIFICADO_POSITIVO_PREDIOS_B,
                    Constants.CERTIFICADO_POSITIVO_UNION_DE_HECHO -> {
                        if (binding.tvDocumentType.tag.toString() == "") {
                            tipoDocPN = "09"
                            Log.e("+tipoDocPNPosVac+", tipoDocPN)
                        } else {
                            tipoDocPN = binding.tvDocumentType.tag.toString()
                            Log.e("++++tipoDocPNPos++++", tipoDocPN)
                        }
                    }
                    Constants.CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA,
                    Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B,
                    Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR,
                    Constants.CERTIFICADO_NEGATIVO_PREDIOS,
                    Constants.CERTIFICADO_NEGATIVO_PREDIOS_B,
                    Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL,
                    Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B,
                    Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA,
                    Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B,
                    Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS,
                    Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B,
                    Constants.CERTIFICADO_NEGATIVO_UNION_DE_HECHO -> {
                        if (binding.tvDocumentType.tag.toString() == "") {
                            tipoDocPN == "09"
                            Log.e("+tipoDocPNNegVac+", tipoDocPN)
                        } else {
                            tipoDocPN = binding.tvDocumentType.tag.toString()
                            Log.e("++++tipoDocPNNeg++++", tipoDocPN)
                        }
                    }
                }
            }

            etDocumentNumber.onChanged { validateFields() }

            etFirstLastname.onChanged { validateFields() }
            etFirstLastname.filterMayus()
            etSecondLastname.onChanged { validateFields() }
            etSecondLastname.filterMayus()
            etNames.onChanged { validateFields() }
            etNames.filterMayus()



            etDocumentNumber.onChanged {
                tvDocumentType.tag?.let{ type ->
                    val maxLength = 16 // Número máximo de caracteres permitidos

                    val inputFilters = arrayOf<InputFilter>(InputFilter.LengthFilter(maxLength))

                    etDocumentNumber.filters = inputFilters
                    evaluateDocumentNumber(
                        type.toString(),
                        it.length,
                        it.toString()
                    )
                }
            }

            btnApply.setOnClickListener {
                var documentNumber = etDocumentNumber.text.toString()
                var firstLastname = etFirstLastname.text.toString()
                var secondLastname = etSecondLastname.text.toString()
                var names = etNames.text.toString()
                var businessName = Constants.EMPTY

                var paymentDataItem =
                    PaymentDataItem(names, firstLastname, secondLastname)
                paymentDataItem.documentNumebr = documentNumber
                paymentDataItem.businessName = businessName
                paymentDataItem.typePerson = Constants.PERSON_NATURAL

                findNavController().navigate(
                    R.id.action_certifiedUnionFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to documentType,
                        "ammount" to certificate?.preOffice!!.toDouble(),
                        "typeDocumentPoN" to tipoDocPN
                    )
                )
            }
        }
    }

    private fun selectDNIDocument(docs: List<DocumentType>) {
        val selectedDoc = docs.find { it.name == "DNI" } // Encuentra el elemento "DNI"

        selectedDoc?.let {
            val selectedIndex = docs.indexOf(it)
            Log.e("Index",selectedIndex.toString())
            binding.tvDocumentType.setText(it.name, false)
            binding.tvDocumentType.setSelection(3)
        }
        isSelectedDocumentType = true
    }

    private fun evaluateDocumentNumber(
        documentType: String,
        length: Int,
        text: String,
    ) {
        when (documentType) {
            Constants.DOCUMENT_TYPE_DNI -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_DNI
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text,
                    )
                    validateFields()
                }
            }
            Constants.DOCUMENT_TYPE_CE -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_CE
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text
                    )
                }
            }
        }
    }

    private fun enableFieldsPerson() {
        with(binding) {
            tilFirstLastname.show()
            tilSecondLastname.show()
            tilNames.show()
        }
    }


    private fun enableFieldsLegal() {
        with(binding) {
            tilFirstLastname.hide()
            tilSecondLastname.hide()
            tilNames.hide()
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            viewModel.getDocumentType("N")
            onChange(offices) { fillOffices(it) }
            onChange(docTypes) {
                fillDocType(it)
                selectDNIDocument(it)
            }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(isValidDocumentDNI) { validDocumentDNI(it) }
        }
    }


    private fun validDocumentDNI(documentDNI: DocumentDNI) {
        with(binding) {

            tilNames.editText?.disable()
            tilFirstLastname.editText?.disable()
            tilSecondLastname.editText?.disable()

            tilNames.editText?.setText(documentDNI.names)
            tilFirstLastname.editText?.setText(documentDNI.paternal)
            tilSecondLastname.editText?.setText(documentDNI.maternal)
            validateFields()
        }
    }


    private fun clearInputValues() {
        with(binding) {

            etDocumentNumber?.clean()
            etNames?.clean()
            etFirstLastname?.clean()
            etSecondLastname?.clean()
            etDocumentNumber?.enable()

            tvDocumentType.tag?.let { type ->
                when (type) {
                    Constants.DOCUMENT_TYPE_DNI -> {
                        etFirstLastname?.disable()
                        etSecondLastname?.disable()
                        etNames?.disable()
                    }
                    Constants.DOCUMENT_TYPE_CE -> {
                        etFirstLastname?.disable()
                        etSecondLastname?.disable()
                        etNames?.disable()
                    }
                    else -> {
                        etFirstLastname?.enable()
                        etSecondLastname?.enable()
                        etNames?.enable()
                    }
                }
            }
        }
    }

    fun validateFields() {
        with(binding) {
            //Log.e("1", etDocumentNumber.text.toString().isNotEmpty().toString())
            //Log.e("2", etDocumentNumber.text?.length!!.toString())
            //Log.e("3", etFirstLastname.text.toString().isNotEmpty().toString())
            //Log.e("4", etNames.text.toString().isNotEmpty().toString())
            //Log.e("5", isSelectedDocumentType.toString())
            // Log.e("6", isSelectedRegisterZone.toString())
            if (etDocumentNumber.text.toString().isNotEmpty() &&
                etDocumentNumber.text?.length!! >= 8 &&
                etFirstLastname.text.toString().isNotEmpty() &&
                etNames.text.toString().isNotEmpty() &&
                isSelectedDocumentType &&
                isSelectedRegisterZone) {
                btnApply.enable()
            } else {
                btnApply.disable()
            }
        }
    }

    private fun fillDocType(docs: List<DocumentType>) {
        binding.tvDocumentType.clean()
        binding.tvDocumentType.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                docs
            )
        )
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                isSelectedRegisterZone = true
                validateFields()
            }
        }
    }


    override fun sendPaymentConfirmation(data: VisaNetResult) {
        /*
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificate?.let {
                saveProcess?.let { it1 ->
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, null
                    )
                }
            }
        }
        */
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber, Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = CertifiedUnionFragmentDirections
            .actionCertifiedUnionFragmentToPaymentResultFragment(
                transId = transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}