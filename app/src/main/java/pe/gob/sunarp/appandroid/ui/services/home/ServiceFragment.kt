package pe.gob.sunarp.appandroid.ui.services.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.navigateTo
import pe.gob.sunarp.appandroid.data.ServiceOption
import pe.gob.sunarp.appandroid.data.Utils
import pe.gob.sunarp.appandroid.databinding.FragmentServicesBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.home.adapter.ServiceAdapter

class ServiceFragment : BaseFragment<FragmentServicesBinding>(FragmentServicesBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ServiceAdapter(Utils.getServicesItems())
        adapter.setOnServicePressed { option ->
            when (option) {
                ServiceOption.FOLLOW_UP -> openFollowUpLink()
                ServiceOption.PROP_INQ -> openPropertySearch()
                ServiceOption.CERT_LIT_ADV -> openCertLitAdv()
                ServiceOption.CERT_ADV -> openCertAdv()
                ServiceOption.SEARCH_BY_NAME -> openSearchByName()
                ServiceOption.LEG_PERS_SEARCH -> openSearchByLegalPerson()
                ServiceOption.VEH_CONS -> openSearchPlate()
                ServiceOption.APP_STATUS -> openApplicationStatus()
                ServiceOption.CONSULT_TIVE -> openConsultTive()
                ServiceOption.ALERTA_NORMATIVA -> openAlertaNormativaUpLink()
            }
        }
        binding.rvServices.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }
    }

    private fun openApplicationStatus() {
        navigateTo(R.id.action_servicesFragment_to_applicationStatusFragment)
    }

    private fun openSearchByName() {
        navigateTo(R.id.action_servicesFragment_to_searchByNameFragment)
    }

    private fun openSearchByLegalPerson() {
        navigateTo(R.id.action_servicesFragment_to_searchByLegalPersonFragment)
    }

    private fun openSearchPlate() {
        navigateTo(R.id.action_servicesFragment_to_searchPlateFragment)
    }


    private fun openCertAdv() {
        findNavController().navigate(
            R.id.action_servicesFragment_to_certificateFilterFragment,
            bundleOf("type" to "C")
        )
    }

    private fun openCertLitAdv() {
        findNavController().navigate(
            R.id.action_servicesFragment_to_certificateFilterFragment,
            bundleOf("type" to "L")
        )
    }

    private fun openPropertySearch() {
        navigateTo(R.id.action_servicesFragment_to_propertySearchFragment)
    }


    private fun openFollowUpLink() {
        val url = "https://siguelo.sunarp.gob.pe/siguelo/?ref=appandr"
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    private fun openAlertaNormativaUpLink() {
        val url = "https://scr.sunarp.gob.pe/alerta-normativa-registral/"
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    private fun openConsultTive() {
        navigateTo(R.id.action_servicesFragment_to_searchTiveFragment)
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}