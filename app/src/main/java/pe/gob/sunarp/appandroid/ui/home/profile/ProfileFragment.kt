package pe.gob.sunarp.appandroid.ui.home.profile

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.databinding.FragmentProfileBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.profile.model.ProfileItem

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val viewModel: ProfileViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
    }

    private fun initViewModels() {
        initViewModelsErrorToken()
        with(viewModel) {
            getUserInformation()
            profile.observe(viewLifecycleOwner) { profileItem ->
                profileItem?.let {
                    initViews(it)
                }
            }

            onLoadPhoto.observe(viewLifecycleOwner) { userPhoto ->
                userPhoto?.let { binding.avatar.setImageBitmap(it) }
            }

            loading.observe(viewLifecycleOwner) {
                binding.loadingContainer.apply {
                    if (it) loading.show() else loading.hide()
                }
            }
        }
    }

    private fun initViews(profileInformation: ProfileItem) {
        binding.apply {
            tvName.text =
                "${profileInformation.name} ${profileInformation.firstLastname} ${profileInformation.secondLastname}"
            tvGender.text = getString(R.string.gender, profileInformation.gender.formatGender()).formatText()
            tvBirthday.text = getString(R.string.birthday, profileInformation.birthday.formatDate()).formatText()
            tvDNI.text = getString(R.string.dni, profileInformation.docNumber).formatText()
            tvMobilePhone.text =
                getString(R.string.mobile_phone, profileInformation.mobilePhone).formatText()
            tvEmail.text = getString(R.string.email, profileInformation.email).formatText()

            editProfile.ivIcon.setBackgroundResource(R.drawable.ic_edit)
            editProfile.tvOption.text = getString(R.string.edit_profile)
            editProfile.itemContainer.setOnClickListener {
                findNavController().navigate(
                    R.id.action_profileFragment_to_editProfileFragment,
                    bundleOf("profileInformation" to profileInformation)
                )
            }

            history.ivIcon.setBackgroundResource(R.drawable.ic_history)
            history.tvOption.text = getString(R.string.see_history)
            history.itemContainer.setOnClickListener {
                findNavController().navigate(R.id.action_profileFragment_to_transactions)
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}