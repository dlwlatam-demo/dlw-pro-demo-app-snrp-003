package pe.gob.sunarp.appandroid.ui.services.tive.history

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import pe.gob.sunarp.appandroid.databinding.FragmentTiveSearchHistoryBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.tive.history.adapter.TiveHistoryAdapter

@AndroidEntryPoint
class TiveHistoryFragment :
    BaseFragment<FragmentTiveSearchHistoryBinding>(FragmentTiveSearchHistoryBinding::inflate) {

    private val viewModel: TiveHistoryViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
    }

    private fun initViews(items: List<TiveSearchRequest>) {
        val tiveHistoryAdapter = TiveHistoryAdapter(items)
        binding.rvTransactions.apply {
            tiveHistoryAdapter.setOpenDocument { query ->
                viewModel.getSearch(query)
            }
            adapter = tiveHistoryAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getHistory()
            onChange(queries) { initViews(it) }
            onChange(loading) { showLoading(it) }
            onChange(selectimage) { openVisor(it) }
        }
    }

    private fun openVisor(image: String) {
        findNavController().navigate(
            R.id.action_go_to_tiveShowDocumentFragment_from_history_fragment,
            bundleOf("image64" to image)
        )
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it) }
        }
    }
}
