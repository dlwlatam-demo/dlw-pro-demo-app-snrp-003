package pe.gob.sunarp.appandroid.ui.register.verify

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.SUNOTPListener
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRegisterVerifyBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class RegisterVerifyFragment :
    BaseFragment<FragmentRegisterVerifyBinding>(FragmentRegisterVerifyBinding::inflate) {

    private val viewModel: RegisterVerifyViewModel by viewModels()
    private var otpCode: String = ""
    private var numberPhone: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
        setObservers()
        numberPhone = arguments?.getString("numberPhone") ?: ""
    }

    private fun initViews() {
        binding.textViewCodeResend.hide()
    }

    private fun setListeners() {
        with(binding) {
            actionRegisterVerify.setOnClickListener { viewModel.verify(otpCode) }
            textViewCodeResend.setOnClickListener { viewModel.resend(numberPhone) }

            actionRegisterBack.setOnClickListener {
                findNavController().popBackStack()
            }

            sunOTP.listener = object : SUNOTPListener {
                override fun onShowCount(time: String) {
                    textViewCounterTime.text = time
                }

                override fun onStarCount() {
                    textViewCounterTime.show()
                    textViewCodeResend.hide()
                }

                override fun onStopCount() {
                    textViewCounterTime.hide()
                    textViewCodeResend.show()
                }

                override fun onResendCode() {
                    textViewCounterTime.show()
                }

                override fun onComplete(otp: CharSequence) {
                    otpCode = otp.toString()
                }
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            onErrorResend.observe(viewLifecycleOwner) { wasNotForwarded(it) }
            onError.observe(viewLifecycleOwner) { anError(it) }
            isVerified.observe(viewLifecycleOwner) { goToNextView(it) }
            isResend.observe(viewLifecycleOwner) { wasForwarded(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun wasNotForwarded(message: String?) {
        binding.sunOTP.default()
        super.showError(message)
    }

    private fun wasForwarded(message: String?) {
        binding.sunOTP.default()
        super.showError(message)
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun anError(message: String?) {
        binding.sunOTP.error()
        super.showError(message)
    }

    private fun goToNextView(message: String?) {
        binding.sunOTP.validated()
        super.showError(message)
        findNavController().navigate(
            R.id.action_RegisterVerifyFragment_to_RegisterCreateFragment,
            bundleOf("numberPhone" to numberPhone)
        )
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}