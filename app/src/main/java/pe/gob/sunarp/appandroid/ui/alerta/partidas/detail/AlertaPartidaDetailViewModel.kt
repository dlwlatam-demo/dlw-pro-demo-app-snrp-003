package pe.gob.sunarp.appandroid.ui.alerta.partidas.detail

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.DetailMandatoResponse
import pe.gob.sunarp.appandroid.data.alerta.modelservices.PartidaItemResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaPartidaDetailViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val dataCorreo = SingleLiveEvent<List<DetailMandatoResponse>>()
    val dataSMS = SingleLiveEvent<List<DetailMandatoResponse>>()

    init {
     dataCorreo.value = arrayListOf()
     dataSMS.value = arrayListOf()
    }

    fun getData(partida: PartidaItemResponse) {
        getDataServicesCorreo(partida, "PTD")
        getDataServicesCorreo(partida, "PUB")
        getDataServicesSMS(partida, "PTD")
        getDataServicesSMS(partida, "PUB")
    }

    private fun getDataServicesCorreo(partida: PartidaItemResponse, param: String) {
        viewModelScope.launch {
            when (val result = repository.getNotificationDetail(partida, "EML", param)) {
                is DataResult.Success -> onGetDataServicesCorreoSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun getDataServicesSMS(partida: PartidaItemResponse, param: String) {
        viewModelScope.launch {
            when (val result = repository.getNotificationDetail(partida, "SMS", param)) {
                is DataResult.Success -> onGetDataServicesSMSSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onGetDataServicesCorreoSuccess(data: List<DetailMandatoResponse>) {
        dataCorreo.value = data
    }

    private fun onGetDataServicesSMSSuccess(data: List<DetailMandatoResponse>) {
        dataSMS.value = data
    }


    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}

