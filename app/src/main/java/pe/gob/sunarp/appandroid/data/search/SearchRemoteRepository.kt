package pe.gob.sunarp.appandroid.data.search

import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import kotlinx.coroutines.*
import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipAsyncDTO

class SearchRemoteRepository(
    private val dataSource: SearchDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    var resultado: Boolean = false
    suspend fun getZones(): DataResult<List<SearchZone>> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getZones(
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getRegisterAreas(): DataResult<List<RegisterArea>> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getRegisterAreas(
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getPersonTypes(areaId: String): DataResult<List<PersonType>> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getPersonTypes(
                    areaId,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getOwnershipQuery(information: ZoneRegisterInformation): DataResult<List<CertificateOwnership>> =
        withContext(dispatcher) {
            if (information.tipoTitular.equals("D")) {
                information.derecho = information.names
                information.names = ""
            } else if (information.tipoTitular.equals("S")) {
                information.sociedad = information.names
                information.names = ""
            }

            when (val result =
                dataSource.getOwnershipQuery(
                    information.toRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }

        }

    suspend fun getOwnershipQueryAsync(information: ZoneRegisterInformation): DataResult<CertificateOwnershipAsyncDTO> =
        withContext(dispatcher) {
            if (information.tipoTitular.equals("D")) {
                information.derecho = information.names
                information.names = ""
            } else if (information.tipoTitular.equals("S")) {
                information.sociedad = information.names
                information.names = ""
            }
            Log.e("getOwnershipQuery", information.names )

            when (val result =
                dataSource.getOwnershipQueryAsync(
                    information.toRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }

        }


    suspend fun getOwnershipQueryAsyncStatus(guid: String): DataResult<CertificateOwnershipAsyncDTO> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getOwnershipQueryAsyncStatus(
                    guid,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }

        }




    suspend fun searchLegalPerson(search: SearchLegalPerson): DataResult<List<LegalPerson>> =
        withContext(dispatcher) {

            when (val result =
                dataSource.searchByLegalPerson(
                    search.legalName, search.office, search.acronym,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_LEGAL_PERSON_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

}