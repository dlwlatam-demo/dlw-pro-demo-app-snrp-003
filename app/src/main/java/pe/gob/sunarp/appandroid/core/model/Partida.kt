package pe.gob.sunarp.appandroid.core.model

data class Partida(
    val estado: Int,
    val msj: String,
    var refNumPart: String,
    val refNumPartMP: String,
    val codigoGla: String,
    var codigoLibro: String,
    var numPartida: String,
    var numPlaca: String,
    var numFicha: String = ""
)