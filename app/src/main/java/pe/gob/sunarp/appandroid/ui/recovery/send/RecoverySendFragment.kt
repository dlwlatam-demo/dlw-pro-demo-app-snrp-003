package pe.gob.sunarp.appandroid.ui.recovery.send

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRecoverySendBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class RecoverySendFragment :
    BaseFragment<FragmentRecoverySendBinding>(FragmentRecoverySendBinding::inflate) {

    private val viewModel: RecoverySendViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setObservers()
    }

    private fun setListeners() {
        with(binding) {
            actionRecoverySend.setOnClickListener {
                viewModel.send(textInputEmail.editText?.text.toString())
            }

            binding.btnBack.setOnClickListener {
                findNavController().navigate(R.id.action_recoverySendFragment_to_loginActivity)
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            viewModel.isSent.observe(viewLifecycleOwner) { goToView() }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun goToView() {
        val bundle = bundleOf("emailAddress" to binding.textInputEmail.editText?.text.toString())
        findNavController().navigate(
            R.id.action_RecoverySendFragment_to_RecoveryVerifyFragment,
            bundle
        )
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}