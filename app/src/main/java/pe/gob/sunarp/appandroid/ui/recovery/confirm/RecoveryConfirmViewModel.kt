package pe.gob.sunarp.appandroid.ui.recovery.confirm

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RecoveryConfirmViewModel @Inject constructor(private val repository: LoginRemoteRepository) :
    BaseViewModel() {

    val isConfirmed = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun confirm(password: String, passwordConfirm: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val status = validateForm(password, passwordConfirm)) {
                is FormStatus.Ok -> {
                    when (val result = repository.updatePassword(password)) {
                        is DataResult.Success -> {
                            isConfirmed.value = true
                            //onError(Exception(result.data))
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> onError(Exception(status.error))
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private fun validateForm(password: String, passwordConfirm: String): FormStatus {
        if (!password.isValidString(Constants.REGEX_PASS)) return FormStatus.Ko("No cumple con los requisitos de seguridad: 1 mayúscula, 1 minúscula, 1 número, mínimo 8 caracteres")
        if (password.isEmpty()) return FormStatus.Ko("Olvidó el campo contraseña")
        if (passwordConfirm.isEmpty()) return FormStatus.Ko("Olvidó el campo confirmar contraseña")
        if (passwordConfirm != password) return FormStatus.Ko("Las contraseñas no coinciden")
        return FormStatus.Ok
    }
}