package pe.gob.sunarp.appandroid.ui.alerta.home.adapter

data class AlertaHomeItem(
    val icon: Int,
    val color: String,
    val title: String,
    val subtitle: String,
    val navigation: Int? = null
)