package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.data.vehicle.request.VehicleGenerateReceiptRequest
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
class VehiclePayment(
    val ammount: String,
    val zoneCode: String,
    val officeCode: String,
    val plate: String,
    val username: String,
    val niubizRequest: NiubizInformation
) : Parcelable {

    fun toRequest() = VehicleGenerateReceiptRequest(
        ammount,
        zoneCode,
        officeCode,
        plate,
        niubizRequest.toRequest()
    )

    fun toPaymentResult(visa: VisaNetResult): PaymentResult {
        var format = SimpleDateFormat("yyMMddHHmmSS")

        val newDate: Date? = format.parse(visa.data.transactionDate.orEmpty())

        format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()

        items.add(
            Pair2(PaymentResultItem(
                "Servicio:",
                "Boleta informativa"
            ), null)
        )

        items.add(Pair2(PaymentResultItem("Resultado de la operación:", visa.data.actionDescription.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Número de la tarjeta:", visa.data.card.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Nombre / Razón social:", username), null))
        items.add(Pair2(PaymentResultItem("Descripción:", "Boleta Informativa"), null))
        items.add(Pair2(PaymentResultItem("Fecha / Hora:", format.format(newDate ?: Date())), null))
        items.add(Pair2(PaymentResultItem("Monto pagado:", "S/ ${visa.data.amount}"), null))
        items.add(Pair2(PaymentResultItem("Tipo de moneda:", visa.order.currency.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Tipo de pago", visa.data.brand.orEmpty()), null))
        items.add(Pair2(
            PaymentResultItem("Pedido N°", visa.data.purchaseNumber.orEmpty().replace("Tarjeta", "Tarjeta de Crédito")), null
        ))
        return PaymentResult("Su pago se realizó con éxito", "", Page.VEHICLE_INFORMATION, items)
    }
}