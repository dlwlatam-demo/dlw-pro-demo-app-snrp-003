package pe.gob.sunarp.appandroid.core.custom.ui.otp.listener

import pe.gob.sunarp.appandroid.core.custom.ui.otp.view.SUNOTP
import pe.gob.sunarp.appandroid.databinding.ViewSunOtpBinding

internal interface SUNOTPViewInterface : GenericViewInterface<ViewSunOtpBinding> {
    fun getComponentView(): SUNOTP
}