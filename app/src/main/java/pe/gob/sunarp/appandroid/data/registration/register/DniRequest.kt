package pe.gob.sunarp.appandroid.data.registration.register

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class DniRequest(
    @field:Json(name = "fecEmi")
    val issueDate: String,
    @field:Json(name = "dni")
    val documentNumber: String,
    @field:Json(name = "guid")
    val guid: String? = null
) : BaseModel() {
    constructor() : this("", "", "")
}