package pe.gob.sunarp.appandroid.ui.office.detail

data class ItemDetail(val title: String, val detail: String)
