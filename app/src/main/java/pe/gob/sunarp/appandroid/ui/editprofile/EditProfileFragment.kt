package pe.gob.sunarp.appandroid.ui.editprofile

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.DocumentType
import pe.gob.sunarp.appandroid.core.model.Gender
import pe.gob.sunarp.appandroid.core.model.ProfileInformation
import pe.gob.sunarp.appandroid.databinding.FragmentEditProfileBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.DatePickerFragment
import pe.gob.sunarp.appandroid.ui.home.profile.model.ProfileItem
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.R as AndroidR

@AndroidEntryPoint
class EditProfileFragment :
    BaseFragment<FragmentEditProfileBinding>(FragmentEditProfileBinding::inflate),
    DatePickerDialog.OnDateSetListener {

    private val viewModel: EditProfileViewModel by viewModels()
    private var uri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        initViews()
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(gender) { fillGenderType(it) }
            onChange(docTypes) { fillDocType(it) }
            onChange(onUpdateSuccess) { onUpdateSuccess() }
            onChange(onLoadPhoto) { onLoadPhoto(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    private fun onLoadPhoto(photo: Bitmap?) {
        photo?.let { binding.ivAvatar.setImageBitmap(it) }
    }

    private fun onUpdateSuccess() {
        findNavController().navigate(R.id.action_EditProfileFragment_to_ProfileFragment)
        binding.container.snack("Se guardaron los datos exitosamente.")
    }

    private fun initViews() {
        val profile: ProfileItem? =
            arguments?.getParcelable("profileInformation")

        binding.apply {
            etNames.setText(profile?.name)
            etNames.filterMayus()
            etFirstLastname.setText(profile?.firstLastname)
            etFirstLastname.filterMayus()
            etSecondLastname.setText(profile?.secondLastname)
            etSecondLastname.filterMayus()
            etBirthday.setText(profile?.birthday?.formatDate())
            etDNI.setText(profile?.docNumber)
            etEmail.setText(profile?.email)
            etMobilePhone.setText(profile?.mobilePhone)
            etBirthday.setOnClickListener { showDatePicker(profile?.birthday) }
            avatarContainer.setOnClickListener { updateAvatar() }
            btnSave.setOnClickListener { updateUserInformation() }
            tvGender.setOnItemClickListener { parent, _, position, _ ->
                tvGender.tag = (parent.getItemAtPosition(position) as Gender).id
            }
        }
    }

    private fun showDatePicker(date: String?) {
        val calendar = Calendar.getInstance()
        date?.let {
            val dateItems = date.split("-")
            if (dateItems.size >= 3) {
                calendar[Calendar.MONTH] = dateItems[1].toInt() - 1
                calendar[Calendar.DAY_OF_MONTH] = dateItems[2].toInt()
                calendar[Calendar.YEAR] = dateItems[0].toInt()
            }
        }

        DatePickerFragment( this@EditProfileFragment, calendar)
            .show(
                requireActivity().supportFragmentManager,
                this@EditProfileFragment.tag
            )
    }

    private fun updateUserInformation() {
        binding.apply {
            viewModel.updateUserInformation(
                ProfileInformation(
                    gender = tvGender.tag?.toString() ?: "", // Si tvGender.tag es nulo, usa una cadena vacía
                    documentType = tvDNIType.tag?.toString() ?: "", // Si tvDNIType.tag es nulo, usa una cadena vacía
                    documentNumber = etDNI.text.toString(),
                    names = etNames.text.toString(),
                    firstLastname = etFirstLastname.text.toString(),
                    secondLastname = etSecondLastname.text.toString(),
                    birthday = etBirthday.text.toString().formatDateToSendServer(),
                    mobilePhone = etMobilePhone.text.toString(),
                )
            )
        }
    }

    private var resultCameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                uri?.let {
                    uri = it.createImage(
                        requireActivity(),
                        AUTHORITY,
                        requireContext().createImageFile(),
                        IMAGE_MAX_SIZE,
                        IMAGE_QUALITY
                    )
                    viewModel.updateImage(uri?.getBase64(requireActivity()))
                }
            }
        }

    private var resultGalleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result?.data?.data?.let { data ->
                    Log.d("resultGalleryLauncher", data.toString())
                    uri = data.createImage(
                        requireActivity(),
                        AUTHORITY,
                        requireContext().createImageFile(),
                        IMAGE_MAX_SIZE,
                        IMAGE_QUALITY
                    )
                    uri?.let { newUri ->
                        Log.d("resultGalleryLauncher", newUri.toString())
                        val base64 = newUri.getBase64(requireActivity())
                        viewModel.updateImage(base64)
                        if (BuildConfig.DEBUG)
                            requireContext().writeToFile(base64)
                    }
                }
            }
        }

    private fun updateAvatar() {
        context?.let {
            if (it.cameraPermissionGranted()) {
                openGetImageDialog()
            } else {
                it.requestCameraPermission(requireActivity())
            }
        }
    }

    private fun openGetImageDialog(): Boolean {
        context?.let {
            val alertDialog = AlertDialog.Builder(it)
                .setTitle(R.string.select_image_dialog)
                .setItems(R.array.update_photo_options, dialogListener)
                .create()
            if (!alertDialog.isShowing) {
                alertDialog.show()
            }
        }
        return true
    }

    private val dialogListener = DialogInterface.OnClickListener { _, which ->
        when (which) {
            0 -> camera()
            1 -> gallery()
        }
    }

    private fun gallery() {
        try {
            val intent = createImageChooserIntent(getString(R.string.image_chooser_title))
            intent?.let { resultGalleryLauncher.launch(it) }
        } catch (ex: Exception) {
            showError(ex.message.orEmpty())
        }
    }

    private fun camera() {
        try {
            createImage()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            resultCameraLauncher.launch(intent)
        } catch (ex: Exception) {
            showError(ex.message.orEmpty())
        }
    }

    @Throws(IOException::class)
    private fun createImage() {
        uri = FileProvider.getUriForFile(
            requireContext(),
            AUTHORITY, requireContext().createImageFile()
        )
    }

    private fun fillDocType(docs: List<DocumentType>) {
        val registerInformation: ProfileItem? = arguments?.getParcelable("profileInformation")
        if (docs.isNotEmpty()) {
            val docTypes = docs.firstOrNull { it.id == registerInformation?.docType }
            if (docTypes != null) {
                binding.tvDNIType.setText(docTypes.name)
                binding.tvDNIType.tag = docTypes.id
            }
        }
    }

    private fun fillGenderType(genders: List<Gender>) {
        val registerInformation: ProfileItem? =
            arguments?.getParcelable("profileInformation")
        val gender = genders.first { it.id == registerInformation?.gender }
        binding.tvGender.setText(gender.name)
        binding.tvGender.tag = gender.id
        binding.tvGender.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                genders
            )
        )
    }

    companion object {
        const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.FileProvider"
        const val IMAGE_MAX_SIZE = 650
        const val IMAGE_QUALITY = 80
    }

    override fun onDateSet(
        view: android.widget.DatePicker,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val selectedDate: String = SimpleDateFormat("dd/MM/yyyy").format(calendar.time)
        binding.etBirthday.setText(selectedDate)
        binding.etBirthday.tag = calendar
    }


    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}