package pe.gob.sunarp.appandroid.ui.welcome.splash

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.finishActivity
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.databinding.FragmentSplashBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.ErrorModel
import pe.gob.sunarp.appandroid.ui.base.ErrorType


@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    private lateinit var logoAnimation: AnimationDrawable
    private lateinit var bgAnimation: AnimationDrawable

    private val viewModel: SplashViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initObservers() {
        viewModel.apply {
            validateVersion()
            skipOnBoarding()
        }

    }

    private fun initViews() {
        with(viewModel) {
            onChange(okValidateVersion) { validVersion(it)}
        }
        binding.logo.apply {
            setBackgroundResource(R.drawable.logo_change)
            logoAnimation = background as AnimationDrawable
        }
        binding.background.backgroundContainer.apply {
            setBackgroundResource(R.drawable.bg_change)
            bgAnimation = background as AnimationDrawable
        }

        binding.background.bglogo.visibility = View.GONE
        Handler(Looper.getMainLooper()).postDelayed({
            binding.background.bglogo.visibility = View.VISIBLE
        }, 900)

        logoAnimation.start()
        bgAnimation.start()


    }

    private fun validVersion(isValid: Boolean) {
        if(isValid) {
            viewModel.skipBoarding.observe(viewLifecycleOwner) { skip ->
                val route =
                    if (skip) R.id.action_splashFragment_to_preSignInFragment else R.id.action_splashFragment_to_onBoardingFragment
                Handler(Looper.getMainLooper()).postDelayed({
                    findNavController().navigate(route)
                }, 3000)
            }
        } else {
            validateError(ErrorModel(ErrorType.ERROR_TYPE_VERSION_APP, "Para continuar por favor actualice el app"))
        }
    }


    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }

}