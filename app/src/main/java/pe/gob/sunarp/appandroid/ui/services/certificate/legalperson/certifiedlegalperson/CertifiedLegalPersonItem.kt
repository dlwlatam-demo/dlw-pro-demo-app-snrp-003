package pe.gob.sunarp.appandroid.ui.services.certificate

data class CertifiedLegalPersonItem(

    val codCerti: String,
    val codArea: String,
    val oficinaOrigen: String,
    val tpoPersona: String,
) {
    val apePaterno: String = ""
    val apeMaterno: String = ""
    val nombre: String = ""
    val razSoc: String = ""
    val tpoDoc: String = ""
    val numDoc: String = ""
    val email: String = ""
    val tipPerPN: String = ""
    val apeMatPN: String = ""
    val nombPN: String = ""
    val razSocPN: String = ""
    val tipoDocPN: String = ""
    val numDocPN: String = ""
    val apePatPN: String = ""
    val usrId: String = ""
}
