package pe.gob.sunarp.appandroid.data.payment

import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import lib.visanet.com.pe.visanetlib.VisaNet
import lib.visanet.com.pe.visanetlib.data.custom.Channel
import lib.visanet.com.pe.visanetlib.presentation.custom.VisaNetViewAuthorizationCustom
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.authorization
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.PaymentKeys
import pe.gob.sunarp.appandroid.core.model.PaymentModel
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.BasicPaymentData
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import javax.inject.Inject

import java.text.SimpleDateFormat
import java.util.Date

class PaymentRemoteRepository @Inject constructor(
    private val dataSource: PaymentDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    fun getPaymentData(ammount: String): PaymentItem = preferences.getPaymentInformation(ammount)

    suspend fun getPaymentKeys(ammount: String, uniqueId: Int, payment: BasicPaymentData): DataResult<VisaNetItem> =
        withContext(dispatcher) {
            when (val result = dataSource.getPaymentKeys(
                PaymentRequest(
                    BuildConfig.SUNARP_PAYMENT_INSTANCES,
                    BuildConfig.SUNARP_PAYMENT_KEY
                ),
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
            )) {
                is DataResult.Success -> getVisanetToken(result.data.toModel(), ammount, uniqueId, payment)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    private suspend fun getVisanetToken(
        keys: PaymentKeys,
        ammount: String,
        uniqueId: Int,
        payment: BasicPaymentData
    ): DataResult<VisaNetItem> =
        withContext(dispatcher) {
            when (val resultTransactionID = getTransactionID()) {
                is DataResult.Success -> {
                    when (val result = dataSource.getVisanetToken(
                        keys.urlVisanetToken.orEmpty(),
                        authorization(keys.accesskeyId.orEmpty(), keys.secretAccessKey.orEmpty())
                    )) {
                        is DataResult.Success -> {
                            keys.email = payment.email
                            keys.name = payment.name
                            keys.lastnames = payment.getLastName()
                            Log.e("pinHash-url:", keys.urlVisanet.toString())
                            Log.e("pinHash-token:", result.data.toString())
                            when (val result2 = dataSource.getVisanetPinHash(
                                "${keys.urlVisanet.orEmpty()}${keys.merchantId}",
                                result.data.orEmpty()
                            )) {
                                is DataResult.Success -> {
                                    keys.email = payment.email
                                    keys.name = payment.name
                                    keys.lastnames = payment.getLastName()
                                    Log.e("pinHash:", result2.data.pinHash.toString())
                                    keys.pinHash = result2.data.pinHash
                                    keys.pinHashExpired = result2.data.pinHashExpired
                                    DataResult.Success(
                                        generateVisanetConfig(
                                            PaymentModel(
                                                keys,
                                                result.data.orEmpty()
                                            ), ammount, resultTransactionID.data.toInt()
                                        )
                                    )
                                }
                                is DataResult.Failure -> DataResult.Failure(result2.exception)
                            }
                        }
                        is DataResult.Failure -> DataResult.Failure(result.exception)
                    }
                }
                is DataResult.Failure -> DataResult.Failure(resultTransactionID.exception)
            }
        }

    suspend fun savePayment(
        context: Context,
        request: SavePaymentRequest,
        type: String,
        description: String,
        currency: String,
        source: Page
    ): DataResult<PaymentResult> =
        withContext(dispatcher) {
            when (val result = dataSource.savePayment(
                request,
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_REQUEST_HOST)
            )) {
                is DataResult.Success -> DataResult.Success(
                    result.data.toModel(context, request, type, description, currency, source)
                )
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    private fun generateVisanetConfig(
        model: PaymentModel,
        ammount: String,
        uniqueId: Int
    ): VisaNetItem {
        with(model) {
            val data: HashMap<String, Any> = HashMap()
            data[VisaNet.VISANET_SECURITY_TOKEN] = token
            data[VisaNet.VISANET_CHANNEL] = Channel.MOBILE
            data[VisaNet.VISANET_COUNTABLE] = true
            data[VisaNet.VISANET_MERCHANT] = keys.merchantId.orEmpty()
            //data[VisaNet.VISANET_MERCHANT] = "650001263"
            data[VisaNet.VISANET_PURCHASE_NUMBER] = uniqueId.toString()
            Log.e("purchaseNumber: ", uniqueId.toString())
            preferences.purchaseNumber = uniqueId.toString()
            Log.e("purchaseNumber: ", preferences.purchaseNumber)

            data[VisaNet.VISANET_AMOUNT] = ammount.toDoubleOrNull() ?: 1.0
            data[VisaNet.VISANET_USER_TOKEN] = model.keys.email.orEmpty()

            val mddData = HashMap<String, String>()
            mddData["4"] = preferences.email // keys.mdd4.orEmpty()
            mddData["21"] = keys.mdd21.orEmpty()
            mddData["32"] = preferences.userDocNumber //keys.mdd32.orEmpty()
            mddData["63"] = preferences.userDocNumber
            mddData["75"] = keys.mdd75.orEmpty()

            val mDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fechaCreacion = mDateFormat.parse(preferences.createdAt)
            var fecha = Date()
            val mDifference = kotlin.math.abs(fechaCreacion.time - fecha.time)
            val mDifferenceDates = mDifference / (24 * 60 * 60 * 1000)

            mddData["77"] = mDifferenceDates.toString() // keys.mdd77.orEmpty()

            data[VisaNet.VISANET_MDD] = mddData
            data[VisaNet.VISANET_ENDPOINT_URL] = BuildConfig.VISANET_URL
            data[VisaNet.VISANET_CERTIFICATE_HOST] = BuildConfig.VISANET_CERTIFICATE_HOST
            data[VisaNet.VISANET_CERTIFICATE_PIN] = "sha256/${keys.pinHash}"

            //detail user

            data[VisaNet.VISANET_REGISTER_EMAIL] = keys.email.orEmpty()
            data[VisaNet.VISANET_REGISTER_NAME] = keys.name.orEmpty()
            data[VisaNet.VISANET_REGISTER_LASTNAME] = keys.lastnames.orEmpty()


            val custom = VisaNetViewAuthorizationCustom()
            custom.logoImage = R.drawable.logo_color
            return VisaNetItem(data, custom)

        }

    }


    suspend fun getTransactionID(): DataResult<String> =
        withContext(dispatcher) {
            when (val result = dataSource.getTransactionID(
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
            )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

}