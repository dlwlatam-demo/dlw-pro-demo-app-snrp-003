package pe.gob.sunarp.appandroid.core.api

import android.util.Log
import com.squareup.moshi.Moshi
import okhttp3.Request
import okio.Timeout
import pe.gob.sunarp.appandroid.core.model.Error
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

class DataResultCall<T>(private val delegate: Call<T>) : Call<DataResult<T>> {

    override fun enqueue(callback: Callback<DataResult<T>>) {
        delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    response.body()?.let { body ->
                        callback.onResponse(
                            this@DataResultCall,
                            Response.success(DataResult.Success(body))
                        )
                    } ?: callback.onResponse(
                        this@DataResultCall, Response.success(
                            DataResult.Failure(
                                Exception("No se obtuvo información en la respuesta")
                            )
                        )
                    )
                } else if(response.code() == 401) {
                    callback.onResponse(
                        this@DataResultCall,
                        Response.success(DataResult.Failure(TokenException()))
                    )
                }  else if(response.code() in 500..599) {
                    callback.onResponse(
                        this@DataResultCall,
                        Response.success(DataResult.Failure(NotControlledException()))
                    )
                } else {
                    callback.onResponse(
                        this@DataResultCall,
                        Response.success(DataResult.Failure(Exception(getErrorMessage(response))))
                    )
                }
            }

            override fun onFailure(call: Call<T>, throwable: Throwable) {
                //e.g. timeout and others
                Log.d(this.javaClass.simpleName, throwable.message.toString())
                if (throwable.message?.contains("timeout", true) == true) {
                    callback.onResponse(
                        this@DataResultCall,
                        Response.success(DataResult.Failure(TimeOutException()))
                    )
                } else {
                    callback.onResponse(
                        this@DataResultCall,
                        Response.success(DataResult.Failure(NotControlledException()))
                    )
                }
            }
        })
    }

    override fun clone(): Call<DataResult<T>> = DataResultCall(delegate.clone())

    override fun execute(): Response<DataResult<T>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun isExecuted() = delegate.isExecuted


    override fun cancel() = delegate.cancel()

    override fun isCanceled() = delegate.isCanceled

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()

    private fun getErrorMessage(response: Response<T>): String {
        val error = Moshi.Builder().build().adapter(Error::class.java).fromJson(
            HttpException(response).response()?.errorBody()?.string() ?: ""
        )
        val serviceError = HttpException(response)
        val errorException = (serviceError as Exception)
        Log.d(this.javaClass.simpleName, errorException.message.toString())
        Log.d(
            this.javaClass.simpleName,
            serviceError.response()?.errorBody()?.string() ?: ""
        )
        return when (val code = response.code()) {
            in 400..499 -> {
                if (code == 401) {
                    "Se ha presentado un error de autenticación, favor verifique su sesión"
                } else if (error?.errorDescription == "Bad credentials") {
                    "Email o contraseña incorrectos"
                } else
                    error?.description ?: "Ha ocurrido un error"
            }
            else -> // 500..599 and others
                "Se ha presentado un error en la aplicación, favor volver a intentarlo en 5 minutos"
        }
    }
}