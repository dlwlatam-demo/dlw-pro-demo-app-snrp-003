package pe.gob.sunarp.appandroid.core.custom.ui.otp.model

import android.text.InputType

enum class SUNOTPInputType(val id: Int, val inputType: Int, val filter: SUNOTPInputFilter) {

    NUMERIC(0, InputType.TYPE_CLASS_NUMBER, SUNOTPInputFilter("[0-9]*")),
    ALPHANUMERIC(1, InputType.TYPE_CLASS_TEXT, SUNOTPInputFilter("[0-9a-zA-ZñÑ]*"));

    companion object {
        @Throws(Exception::class)
        fun get(id: Int): SUNOTPInputType {
            return when (id) {
                NUMERIC.id -> NUMERIC
                ALPHANUMERIC.id -> ALPHANUMERIC
                else -> throw error("Unexpected id: $id")
            }
        }
    }
}