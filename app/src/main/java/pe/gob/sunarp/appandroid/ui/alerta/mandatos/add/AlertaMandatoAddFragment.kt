package pe.gob.sunarp.appandroid.ui.alerta.mandatos.add

import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.filterMaxLengthCE
import pe.gob.sunarp.appandroid.core.filterMaxLengthDNI
import pe.gob.sunarp.appandroid.core.filterMaxLengthEleven
import pe.gob.sunarp.appandroid.core.filterMaxLengthOthers
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaMandatoAddBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class AlertaMandatoAddFragment :
    BaseFragment<FragmentAlertaMandatoAddBinding>(FragmentAlertaMandatoAddBinding::inflate){

    private val viewModel: AlertaMandatoAddViewModel by viewModels()
    var typeDocument: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            initViewModels()
            etDocumento.onChanged { validateForm() }
            etNombre.onChanged { validateForm() }
            etNombre.filterMayus()
            etPrimerApellido.onChanged { validateForm() }
            etPrimerApellido.filterMayus()
            etSegundoApellido.onChanged { validateForm() }
            etSegundoApellido.filterMayus()
            btnAccept.setOnClickListener {
                hideKeyboard()
                viewModel.createMandato(
                    etDocumento.text.toString(),
                    etNombre.text.toString(),
                    etPrimerApellido.text.toString(),
                    etSegundoApellido.text.toString(),
                )
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(tipoDocumentos) { setTipoDocumentos(it) }
            onChange(tipoDocumentoSelect) { validateForm() }
            onChange(successCreateMandato) { onSuccessCreateMandato(it) }
            getParameters()
        }
    }

    private fun onSuccessCreateMandato(result: Boolean) {
        if (result) {
            findNavController().navigateUp()
        }
    }

    private fun validateForm() {
        with(binding) {
            with(viewModel) {
                if (
                    (tipoDocumentoSelect.value != null) &&
                    etDocumento.text.toString().isNotEmpty() &&
                    etNombre.text.toString().isNotEmpty() &&
                    etPrimerApellido.text.toString().isNotEmpty()
                ) {
                    btnAccept.enable()
                } else {
                    btnAccept.disable()
                }
            }
        }
    }

    private fun setTipoDocumentos(items: List<ItemSpinner>) {
        with(binding) {
            val originalInputType = etDocumento.inputType
            val originalFilters = etDocumento.filters
            etTipoDocumento.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    etDocumento.clean()
                    this.tag = item.id
                    typeDocument = item.id
                    Log.e("tag typeDocument", typeDocument)

                    // Actualizar etDocumento aquí según el typeDocument seleccionado
                    if (typeDocument == "09") {
                        etDocumento.inputType = originalInputType
                        etDocumento.filters = originalFilters
                        etDocumento.filterMaxLengthDNI()
                        Log.e("tag typeDocument", "DNI")
                    } else if (typeDocument == "03") {
                        etDocumento.inputType = originalInputType
                        etDocumento.filters = originalFilters
                        etDocumento.inputType = InputType.TYPE_CLASS_TEXT
                        etDocumento.filterMaxLengthCE()
                        Log.e("tag typeDocument", "CE")
                    } else {
                        etDocumento.inputType = originalInputType
                        etDocumento.filters = originalFilters
                        etDocumento.inputType = InputType.TYPE_CLASS_TEXT
                        etDocumento.filterMaxLengthEleven()
                        Log.e("tag typeDocument", "OTRO")
                    }

                    viewModel.setSelectTipoDocumento(item)
                }
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}