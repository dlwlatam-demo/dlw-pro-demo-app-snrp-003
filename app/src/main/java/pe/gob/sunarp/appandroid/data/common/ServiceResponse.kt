package pe.gob.sunarp.appandroid.data.common

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class ServiceResponse(
    @field:Json(name = "codResult")
    val responseId: String?,
    @field:Json(name = "msgResult")
    val description: String?,
    @field:Json(name = "guid")
    val guid: String?
) : BaseModel() {
    constructor() : this("", "", "")
}