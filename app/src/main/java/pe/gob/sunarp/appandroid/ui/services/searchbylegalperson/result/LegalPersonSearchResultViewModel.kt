package pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.result

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.LegalPerson
import pe.gob.sunarp.appandroid.core.model.SearchLegalPerson
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class LegalPersonSearchResultViewModel @Inject constructor(
    private val repository: SearchRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val legalPersons = SingleLiveEvent<List<LegalPerson>>()

    fun searchLegalPerson(search: SearchLegalPerson) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.searchLegalPerson(search)) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onRegistrationOfficeSuccess(data: List<LegalPerson>) {
        loading.value = false
        legalPersons.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}