package pe.gob.sunarp.appandroid.data.solicitud.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class SolicitudNotiItemResponse(

	@Json(name="estado")
	val estado: String,

	@Json(name="solicitudId")
	val solicitudId: Int,

	@Json(name="strBusq")
	val strBusq: String,

	@Json(name="fecHor")
	val fecHor: String
): Parcelable
