package pe.gob.sunarp.appandroid.ui.office.search

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.databinding.LayoutItemSearchMapBinding
import java.util.*

class ItemSearchOfficeAdapter(
    private val items: List<ListOfficeResultItem> ,
    private val resultSearch: MutableList<ListOfficeResultItem> = mutableListOf(),
    private val onClickFuntion: (ListOfficeResultItem) -> Unit
) : RecyclerView.Adapter<ItemSearchOfficeAdapter.ViewHolder>(), Filterable {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutItemSearchMapBinding =
            LayoutItemSearchMapBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(resultSearch[position], onClickFuntion)
    }

    override fun getItemCount() = resultSearch.size

    inner class ViewHolder(private val binding: LayoutItemSearchMapBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ListOfficeResultItem, onClickFuntion: (ListOfficeResultItem) -> Unit) = with(binding) {
            with(item) {
               tvTitle.text = office
               tvAddress.text = address
               tvDistance.text = distance
                ivIcon.setImageResource(when(type) {
                    Constants.MAP_OFFICE_TYPE_OFICINA_REGISTRAL -> R.drawable.ic_office_type_of_registral
                    Constants.MAP_OFFICE_TYPE_OFICINA_RECEPTORA -> R.drawable.ic_office_type_of_receptora
                    Constants.MAP_OFFICE_TYPE_SEDE_CENTRAL -> R.drawable.ic_office_type_sede_central
                    Constants.MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL -> R.drawable.ic_office_type_tribunal_registral
                    Constants.MAP_OFFICE_TYPE_ZONA_REGISTRAL -> R.drawable.ic_office_type_zona_registral
                    Constants.MAP_OFFICE_TYPE_OFICINA_OTRAS -> R.drawable.ic_office_type_oficina_otras
                    else -> R.drawable.ic_office_type_oficina_otras
                })
                btnOfiRegistral.setOnClickListener { onClickFuntion(item) }
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                resultSearch.clear()
                val charString = charSequence.toString().lowercase(Locale.getDefault())
                if (charString.isEmpty()) {
                    resultSearch.addAll(items)
                } else {
                    items.filter { it.office.contains(charString, ignoreCase = true)}
                        .let {resultSearch.addAll(it)}
                }
                val filterResults = FilterResults()
                filterResults.values = resultSearch
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                notifyDataSetChanged()
            }
        }
    }


}