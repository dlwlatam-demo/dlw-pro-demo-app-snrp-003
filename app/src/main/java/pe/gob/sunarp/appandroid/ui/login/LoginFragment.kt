package pe.gob.sunarp.appandroid.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.databinding.FragmentLoginBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val viewModel: LoginViewModel by viewModels()

    override fun onStart() {
        super.onStart()
        with(binding) {
            if (BuildConfig.BUILD_TYPE == "debug") {
                textInputEmail.editText?.setText(getString(R.string.temp_value_user))
                textInputPassword.editText?.setText(getString(R.string.temp_value_pass))
            }

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            initViewModelsErrorToken()

            viewModel.showLoading.observe(viewLifecycleOwner) {
                if (it) loadingContainer.loading.show() else loadingContainer.loading.hide()
            }

            viewModel.isAuthenticated.observe(viewLifecycleOwner) {
                it?.let { _ ->
                    findNavController().navigate(R.id.action_FragmentLogin_to_HomeActivity)
                    finishActivity()
                }
            }

            actionEnter.setOnClickListener {
                viewModel.login(
                    textInputEmail.editText?.text.toString(),
                    textInputPassword.editText?.text.toString()
                )
//                findNavController().navigate(R.id.action_FragmentLogin_to_HomeActivity)
//                finishActivity()
            }

            textInputEmail.editText?.onChanged { validateFields() }
            textInputPassword.editText?.onChanged { validateFields() }

            textViewGoToRecovery.setOnClickListener {
                findNavController().navigate(R.id.action_FragmentLogin_to_RecoveryActivity)
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (textInputEmail.editText?.text ?: "").toString().isValidString(Constants.REGEX_EMAIL_ADDRESS) &&
                (textInputPassword.editText?.text ?: "").toString().isNotEmpty()
            ) {
                actionEnter.enable()
            } else {
                actionEnter.disable()
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
