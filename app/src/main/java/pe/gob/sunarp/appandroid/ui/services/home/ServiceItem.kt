package pe.gob.sunarp.appandroid.ui.services.home

import pe.gob.sunarp.appandroid.data.ServiceOption

data class ServiceItem(
    val key: ServiceOption,
    val title: String,
    val subtitle: String,
    val icon: Int,
)