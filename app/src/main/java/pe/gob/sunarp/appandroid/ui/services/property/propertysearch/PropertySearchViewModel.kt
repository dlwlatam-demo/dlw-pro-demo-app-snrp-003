package pe.gob.sunarp.appandroid.ui.services.property.propertysearch

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_CE
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_DNI
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.DocumentCE
import pe.gob.sunarp.appandroid.core.model.DocumentDNI
import pe.gob.sunarp.appandroid.core.model.DocumentType
import pe.gob.sunarp.appandroid.data.profile.information.ProfileRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PropertySearchViewModel @Inject constructor(
    private val repository: ProfileRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val docTypes = SingleLiveEvent<List<DocumentType>>()
    val isValidDocumentDNI = SingleLiveEvent<DocumentDNI>()
    val isValidDocumentCE = SingleLiveEvent<DocumentCE>()

    fun getDocTypes() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getDocTypeList()) {
                is DataResult.Success -> onDocSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validateDocument(documentType: String, documentNumber: String, issueDate: String = "") {
        viewModelScope.launch {
            loading.value = true
            when (documentType) {
                DOCUMENT_TYPE_DNI -> {
                    when (val result = repository.validateDni(issueDate,documentNumber)) {
                        is DataResult.Success -> onDniSuccess(result.data)
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                DOCUMENT_TYPE_CE -> {
                    when (val result = repository.validateCe(documentNumber)) {
                        is DataResult.Success -> onCESuccess(result.data)
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
            }
        }
    }

    private fun onCESuccess(data: DocumentCE) {
        loading.value = false
        isValidDocumentCE.value = data 
    }

    private fun onDniSuccess(data: DocumentDNI) {
        loading.value = false
        isValidDocumentDNI.value = data
    }

    private fun onDocSuccess(data: List<DocumentType>) {
        loading.value = false
        docTypes.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}