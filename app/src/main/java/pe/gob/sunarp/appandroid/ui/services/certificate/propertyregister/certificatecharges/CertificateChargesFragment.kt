package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certificatecharges

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarPartidaCargadaDesgravamenRequest
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoCargosBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem


@AndroidEntryPoint
class CertificateChargesFragment :
    BaseFragment<FragmentCertificadoCargosBinding>(FragmentCertificadoCargosBinding::inflate){

    private val viewModel: CertificateChargesViewModel by viewModels()
    private var certificate: CertificateType? = null
    private var legalRecord: LegalRecord? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        certificate = arguments?.getParcelable("certificate")
        legalRecord = arguments?.getParcelable("legalRecord")

        with(binding) {
            etNumero.filterOnlyMayus()
            //etFolio.filterOnlyMayus()
            //etTomo.filterOnlyMayus()

            binding.spOficinaRegistral.clean()
            etNumero.onChanged { validateFields() }
            etFolio.onChanged { validateFields() }
            etTomo.onChanged { validateFields() }
            btnSearch.setOnClickListener {
                etNumero.clearFocus()

                hideKeyboard()
                viewModel.solicitarCertificado(certificate, etNumero.text.toString(), etFolio.text.toString(), etTomo.text.toString() )
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getParameters()
            onChange(loading) { showLoading(it) }
            onChange(offices) { setListenerOffice(it) }
            onChange(appliesTypes) { setApplyTypes(it) }
            onChange(selectAppliesTypes) { listenerApplySelect() }
            onChange(librosRegistrales) { setLibrosRegistrales(it) }
            onChange(selectLibroRegistral) { validateFields() }
            onChange(estadoPartida) { savePartida(it) }
        }
    }

    private fun savePartida(partida: Partida) {

        if (partida.estado == 0) {

            val request = ValidarPartidaCargadaDesgravamenRequest(
                viewModel.getSelectOficinaRegistral()?.regPubId.orEmpty(),
                viewModel.getSelectOficinaRegistral()?.oficRegId.orEmpty(),
                certificate?.areaId.orEmpty(),
                if (viewModel.isNroPartidaSelect()) binding.etNumero.text.toString() else "${binding.etTomo.text.toString()}-${binding.etFolio.text.toString()}",
                certificate?.codGrupoLibroArea.orEmpty(),
                if(viewModel.isNroPartidaSelect()) "-" else viewModel.getLibroRegistral()?.codLibro.orEmpty(),
                viewModel.getSelectApplyTypes()?.id?: "01"
            )

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.typePerson = Constants.PERSON_NATURAL
            paymentDataItem.partida = partida
            paymentDataItem.zoneCode = Constants.EMPTY
            paymentDataItem.zoneCodeOffice = Constants.EMPTY
            paymentDataItem.requestPCD = request
            paymentDataItem.tomo = binding.etTomo.text.toString()
            paymentDataItem.folio = binding.etFolio.text.toString()

            findNavController().navigate(
                R.id.action_certificateChargesFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to null,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )

        } else {
            binding.container.snack(partida.msj)
        }
    }

    private fun listenerApplySelect() {
        with(binding) {
            validateFields()
            if(viewModel.isNroPartidaSelect()) {
                etNumero.visibility = View.VISIBLE
                boxTomoFolio.visibility = View.GONE
            } else {
                etNumero.visibility = View.GONE
                boxTomoFolio.visibility = View.VISIBLE
                viewModel.getLibrosRegistrales(certificate)
            }
        }
    }

    private fun setListenerOffice(items: List<RegistrationOffice>) {
        with(binding) {
            spOficinaRegistral.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as RegistrationOffice)
                    this.tag = item.regPubId + item.oficRegId
                    viewModel.setSelectOficinaRegistral(item)
                    validateFields()
                }
            }
        }
    }


    private fun setApplyTypes(items: List<ItemSpinner>) {
        with(binding) {
            etSolicitarPor.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    viewModel.setSelectApplyTypes(item)
                }
            }
        }
    }

    private fun setLibrosRegistrales(items: List<LibroRegistral>) {
        with(binding) {
            etCertificateType.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as LibroRegistral)
                    this.tag = item.codLibro
                    viewModel.selectLibroRegistral(item)
                    validateFields()
                }
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (spOficinaRegistral.tag ?: "").toString().isNotEmpty() &&
                (etSolicitarPor.tag ?: "").toString().isNotEmpty() &&
                if (viewModel.isNroPartidaSelect()) {
                    etNumero.text.toString().isNotEmpty()
                } else {
                    (etCertificateType.tag ?: "").toString().isNotEmpty() &&
                            etFolio.text.toString().isNotEmpty() &&
                            etTomo.text.toString().isNotEmpty()
                }
            ) {
                btnSearch.enable()
            } else {
                btnSearch.disable()
            }
        }
    }


    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}