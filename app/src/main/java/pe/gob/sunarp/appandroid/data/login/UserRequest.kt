package pe.gob.sunarp.appandroid.data.login

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserRequest(
    @field:Json(name = "username")
    val email: String,
    @field:Json(name = "password")
    val password: String,
    @field:Json(name = "grant_type")
    val grantType: String
)