package pe.gob.sunarp.appandroid.data.search

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.*
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.search.request.SearchRequest
import retrofit2.http.*
import retrofit2.http.Header

interface SearchDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/zonas")
    suspend fun getZones(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<List<SearchZoneDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/areasRegistrales")
    suspend fun getRegisterAreas(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<List<RegisterAreaDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/tipoPersonas/{codGrupoLibroArea}")
    suspend fun getPersonTypes(
        @Path("codGrupoLibroArea") areaId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
    ): DataResult<List<PersonTypeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/consultaTitularidad")
    suspend fun getOwnershipQuery(
        @Body request: SearchRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
        //Timeout expanded
    ): DataResult<List<CertificateOwnershipDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/consultaTitularidadAsync")
    suspend fun getOwnershipQueryAsync(
        @Body request: SearchRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
        //Timeout expanded
    ): DataResult<CertificateOwnershipAsyncDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/async/status/consultaTitularidad/{guid}")
    suspend fun getOwnershipQueryAsyncStatus(
        @Path("guid") areaId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
        //Timeout expanded
    ): DataResult<CertificateOwnershipAsyncDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/personaJuridica/busqueda")
    suspend fun searchByLegalPerson(
        @Query("nombre") name: String,
        @Query("oficina") office: String,
        @Query("siglas") acronym: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<List<LegalPersonDTO>>


}