package pe.gob.sunarp.appandroid.core.custom.ui

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatImageView
import com.google.android.material.card.MaterialCardView
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem

class PreDetailOfficeMapView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : MaterialCardView(context, attrs) {
    private val tvNameLocal: TextView
    private val tvtypeLocalAndDis: TextView
    private val btnShare: MaterialCardView
    private val btnRoute: MaterialCardView
    private val btnRouteInner: AppCompatImageView
    private val card: MaterialCardView
    init {
        inflate(context, R.layout.layout_custom_pre_detail_office_map_view, this)

        val closeBtn: AppCompatImageButton = findViewById(R.id.btn_delete)
        closeBtn.setOnClickListener { visibility = View.GONE }
        tvNameLocal = findViewById(R.id.tv_name_local)
        tvtypeLocalAndDis = findViewById(R.id.tv_type_and_distance)
        btnShare = findViewById(R.id.box_btn_share)
        btnRoute = findViewById(R.id.box_btn_route)
        btnRouteInner = findViewById(R.id.btn_route)
        card = findViewById(R.id.card)

    }

    fun setInfo( data: ListOfficeResultItem,
                onClickFunction: () -> Unit,
                onShareFunction: (data: ListOfficeResultItem) -> Unit,
                onRouteFunction: (data: ListOfficeResultItem) -> Unit)
    {
        tvNameLocal.text = data.office
        tvtypeLocalAndDis.text = data.type
        data.distance?.let {
            tvtypeLocalAndDis.text = "${data.type} a $it"
        }
        card.setOnClickListener { onClickFunction() }
        btnShare.setOnClickListener { onShareFunction(data)}
        btnRoute.setOnClickListener { onRouteFunction(data)}
        visibility = View.VISIBLE
    }

    fun enableRoutes(enable: Boolean) {
        btnRoute.isEnabled = enable
        btnRoute.backgroundTintList = context.resources.getColorStateList(if (enable) R.color.white else R.color.gray, context.theme)
    }

    fun hide() {
        visibility = View.GONE
    }

    fun show() {
        visibility = View.VISIBLE
    }
}