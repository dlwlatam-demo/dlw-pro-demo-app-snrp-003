package pe.gob.sunarp.appandroid.ui.transactions.status.model

import pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group.StatusInfoGroup
import pe.gob.sunarp.appandroid.data.transaction.dto.Flags
import pe.gob.sunarp.appandroid.data.transaction.dto.Status

data class TransactionStatus(
    val status: String,
    val information: StatusInfoGroup,
    val userKeyId: String?,
    val certificateId: String?,
    val ammountToPay: String,
    val flags: Flags?,
    val statusData: Status?,
    val certificateType: String?,
    val solicitudId: String?
)