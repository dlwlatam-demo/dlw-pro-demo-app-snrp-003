package pe.gob.sunarp.appandroid.ui.alerta.restablecerpass

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaRestablecerPassBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaRestablecerPassFragment : BaseFragment<FragmentAlertaRestablecerPassBinding>(FragmentAlertaRestablecerPassBinding::inflate) {

    var guid: String = ""
    private val viewModel: AlertaRestablecerPassViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        guid = arguments?.getString("guid", "")?: ""
        setListeners()
        setObservers()
    }

    private fun setListeners() {
        with(binding) {
            actionRecoveryConfirm.setOnClickListener { actionConfirm() }
            textInputPasswordConfirm.editText?.onChanged { validateFields() }
            textInputPassword.editText?.onChanged { validateFields() }
        }
    }


    private fun validateFields() {
        with(binding) {
            if (
                (textInputPassword.editText?.text.toString().isNotEmpty()) &&
                (textInputPasswordConfirm.editText?.text.toString().isNotEmpty()) &&
                (textInputPassword.editText?.text.toString().length == textInputPasswordConfirm.editText?.text.toString().length)

            ) {
                actionRecoveryConfirm.enable()
            } else {
                actionRecoveryConfirm.disable()
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            isConfirmed.observe(viewLifecycleOwner) { goToNextView(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun actionConfirm() {
        with(binding) {
            viewModel.confirm(
                textInputPassword.editText?.text.toString(),
                textInputPasswordConfirm.editText?.text.toString(),
                guid
            )
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun goToNextView(result: Boolean) {
        if (result) {
            toast("Ingreso de nueva contraseña exitoso")
            findNavController().navigate(R.id.action_alertarestablecerpassfragment_to_alertLoginFragment)
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
