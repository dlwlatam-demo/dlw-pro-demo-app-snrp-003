package pe.gob.sunarp.appandroid.ui.transactions.pdfviewer

import android.app.AlertDialog
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.decodeToBytes
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.FragmentPdfViewerBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.AlertType
import pe.gob.sunarp.appandroid.ui.common.AlertaCommonDialog
import pe.gob.sunarp.appandroid.ui.register.confirm.RegisterConfirmFragmentDirections
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Date

@AndroidEntryPoint
class PdfViewerFragment :
    BaseFragment<FragmentPdfViewerBinding>(FragmentPdfViewerBinding::inflate) {

    private val viewModel: PdfViewerViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
    }

    private fun initViewModels() {
        initViewModelsErrorToken()
        val transId = arguments?.getInt("transId")
        val type = arguments?.getString("id")
        if (type.equals("DOC"))
            viewModel.getDocument("$transId")
        else if (type.equals("NW"))
            viewModel.getNewsletter("$transId")

        viewModel.documentLoaded.observe(viewLifecycleOwner) { item ->

            if (!item.isNullOrEmpty()) {
                binding.pdfViewer
                    .fromBytes(item.decodeToBytes())
                    .swipeHorizontal(false)
                    .onError { error ->
                        error.message?.let { it1 -> toast(it1) }
                    }
                    .load()
                Log.e("pdfViewer", item.length.toString())
                writeBytesAsPdf(item.decodeToBytes())
            } else {
                binding.container.snack("No existe documento a mostrar")
            }

        }

        viewModel.loading.observe(viewLifecycleOwner) {
            binding.loadingContainer.apply {
                if (it) loading.show() else loading.hide()
            }
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

    private fun writeBytesAsPdf(bytes : ByteArray) {

        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

        val pattern = "yyyy_MM_dd_HH_mm_ss"
        val simpleDateFormat = SimpleDateFormat(pattern)
        val date: String = simpleDateFormat.format(Date())
        val fileName = "SUNARP_" + date;
        try {
            var file = File.createTempFile(fileName, ".pdf", path)
            var os = FileOutputStream(file);
            os.write(bytes);
            os.close();

            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Descarga de PDF")
            builder.setMessage("Se ha descargado su Boleta en la carpeta Download")

            builder.show()
        } catch(e:Exception) {
            e.printStackTrace()
        }
    }
}