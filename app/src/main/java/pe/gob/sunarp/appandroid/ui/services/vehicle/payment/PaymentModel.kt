package pe.gob.sunarp.appandroid.ui.services.vehicle.payment

import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

data class PaymentModel (
        val transId: String,
    val result: PaymentResult
        )