package pe.gob.sunarp.appandroid.data.property

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.PropertyDTO
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.property.request.PropertyRequest
import retrofit2.http.Body

import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface PropertyDataSource {
    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/consultaPropiedad")
    suspend fun getPropertiesInformation(
        @Body request: PropertyRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<PropertyDTO>
}