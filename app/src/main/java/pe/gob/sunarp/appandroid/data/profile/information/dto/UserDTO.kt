package pe.gob.sunarp.appandroid.data.profile.information.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class UserDTO(
    @field:Json(name = "idUser")
    val idUser: String?,
    @field:Json(name = "email")
    val email: String?,
    @field:Json(name = "sexo")
    val gender: String?,
    @field:Json(name = "tipoDoc")
    val docType: String?,
    @field:Json(name = "nroDoc")
    val docNumber: String?,
    @field:Json(name = "nombres")
    val names: String?,
    @field:Json(name = "priApe")
    val firstLastname: String?,
    @field:Json(name = "segApe")
    val secondLastname: String?,
    @field:Json(name = "fecNac")
    val birthday: String?,
    @field:Json(name = "nroCelular")
    val mobilePhone: String?,
    @field:Json(name = "userPhoto")
    val photo: String?,
    @field:Json(name = "userKeyId")
    val userKeyId: String?,
    @field:Json(name = "createdAt")
    val createdAt: String?,
) : BaseModel() {
    constructor() : this("", "", "", "", "", "", "", "", "", "", "", "", "")
}