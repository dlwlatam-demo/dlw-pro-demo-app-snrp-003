package pe.gob.sunarp.appandroid.data.transaction

import android.content.Context
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.Document
import pe.gob.sunarp.appandroid.core.model.Newsletter
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.transaction.request.TransactionRequest
import pe.gob.sunarp.appandroid.ui.transactions.history.model.Transaction
import pe.gob.sunarp.appandroid.ui.transactions.status.model.TransactionStatus
import javax.inject.Inject

class TransactionRemoteRepository @Inject constructor(
    private val dataSource: TransactionDataSource,
    private val preferences: SunarpSharedPreferences,
    private val context: Context,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getTransactions(): DataResult<List<Transaction>> =
        withContext(dispatcher) {
            val timestamp = System.currentTimeMillis()
            when (val result =
                dataSource.getTransactions(
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_REQUEST_HOST), timestamp
                )) {
                is DataResult.Success -> {
                    DataResult.Success(result.data
                        .map {
                            it.toTransaction(context)
                        })
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun getTransactionStatus(transId: String): DataResult<TransactionStatus> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getTransaction(
                    transId,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel(context))
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getAplicationStatus(year: String, transId: String): DataResult<TransactionStatus> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getApplicationTransaction(
                    year, transId,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel(context))
                is DataResult.Failure -> DataResult.Failure(result.exception)

            }
        }

    suspend fun dropProcess(key: String): DataResult<Boolean> =
        withContext(dispatcher) {

            when (val result =
                dataSource.dropProcess(
                    TransactionRequest(key, preferences.userId),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description))
                    }
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun sendClarification(key: String, description: String): DataResult<Boolean> =
        withContext(dispatcher) {

            when (val result =
                dataSource.certClarification(
                    TransactionRequest(key, preferences.userId, description),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description))
                    }
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun sendRetrieval(key: String, description: String): DataResult<Boolean> =
        withContext(dispatcher) {

            when (val result =
                dataSource.retrieval(
                    TransactionRequest(key, preferences.userId, description),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description))
                    }
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun getDocument(key: String): DataResult<Document> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getDocument(
                    key,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> {
                    DataResult.Success(result.data)
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun getNewsletter(key: String): DataResult<Newsletter> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getNewsletter(
                    key,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_REQUEST_HOST)
                )) {
                is DataResult.Success -> {
                    DataResult.Success(result.data.toModel())
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }
}