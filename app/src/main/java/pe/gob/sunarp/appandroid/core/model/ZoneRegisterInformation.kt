package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest
import pe.gob.sunarp.appandroid.data.search.request.SearchRequest

@Parcelize
data class ZoneRegisterInformation(
    val zones: List<String>,
    var firstLastname: String,
    var secondLastname: String,
    var names: String,
    val tipoTitular: String,
    val areaRegistral: String,
    val areaRegistralDesc: String,
    val niubizRequest: NiubizInformation? = null,
    val isLegalPerson: Boolean = false,
    var derecho: String = "",
    var sociedad: String = ""
) : Parcelable {
    fun getFullname() = "$names $firstLastname $secondLastname"

    fun toRequest() =
        SearchRequest(
            zones,
            firstLastname,
            secondLastname,
            if(tipoTitular == "N") names else "",
            tipoTitular,
            areaRegistral,
            niubizRequest?.toRequest(),
            if(tipoTitular != "N") names else "",
            "APPSNRPANDRO",
            derecho,
            sociedad
        )
}

@Parcelize
data class NiubizInformation(
    val codAccion: String,
    val codAutoriza: String,
    val codtienda: String,
    val concepto: String,
    val decisionCs: String,
    val dscCodAccion: String,
    val dscEci: String,
    val eci: String,
    val estado: String,
    val eticket: String,
    val fechaYhoraTx: String,
    val idUnico: String,
    val idUser: String,
    val impAutorizado: String,
    val nomEmisor: String,
    val numOrden: String,
    val numReferencia: String,
    val oriTarjeta: String,
    val pan: String,
    val resCvv2: String,
    val response: String,
    val reviewTransaction: String,
    val transId: String,
) : Parcelable {
    fun toRequest() = NiubizRequest(
        codAccion,
        codAutoriza,
        codtienda,
        concepto,
        decisionCs,
        dscCodAccion,
        dscEci,
        eci,
        estado,
        eticket,
        fechaYhoraTx,
        idUnico,
        idUser,
        impAutorizado,
        nomEmisor,
        numOrden,
        numReferencia,
        oriTarjeta,
        pan,
        resCvv2,
        response,
        reviewTransaction,
        transId
    )
}