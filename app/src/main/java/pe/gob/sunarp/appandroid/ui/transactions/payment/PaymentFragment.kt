package pe.gob.sunarp.appandroid.ui.transactions.payment

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult

import pe.gob.sunarp.appandroid.databinding.FragmentPaymentBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.common.AlertTerminosDialog
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class PaymentFragment : BaseFragment<FragmentPaymentBinding>(FragmentPaymentBinding::inflate),
    PaymentInterface {

    private val viewModel: PaymentViewModel by viewModels()
    private var paymentToPay: String = ""
    private var transId = 0
    private var transType: String? = null
    private var transDescription: String? = null
    private var page: Page? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        initViews(view)
    }

    private fun initViews(view: View) {
        with(binding) {
            initViewModelsErrorToken()
            btnSend.setOnClickListener {
                openPaymentScreen()
            }

            btnSend.isEnabled = false
            val checkBoxTerms = view.findViewById<CheckBox>(R.id.check_box_terms)
            checkBoxTerms.setOnCheckedChangeListener { _, isChecked ->
                btnSend.isEnabled = isChecked
            }

            tvTerminos.paintFlags = tvTerminos.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            val fm = activity!!.supportFragmentManager
            tvTerminos.setOnClickListener {
                AlertTerminosDialog().apply {
                    setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert)
                    show(fm, this.javaClass.name)
                }
            }
        }
    }

    override fun sendPaymentConfirmation(data: VisaNetResult) {
        viewModel.savePayment(
            requireContext(),
            transId,
            transType.orEmpty(),
            transDescription.orEmpty(),
            data
        )
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(ok, result.toPayment(result.data.purchaseNumber, Page.TRANSACTIONS))
    }

    private fun openPaymentScreen() {
        with(binding) {
            viewModel.create(
                payment = PaymentItem(
                    etNames.text.toString(),
                    etFirstLastname.text.toString(),
                    etSecondLastname.text.toString(),
                    etEmail.text.toString(),
                    paymentToPay, ""
                ), terms = checkBoxTerms.isChecked,
                transId = transId
            )
        }
    }

    private fun initViewModels() {
        transId = arguments?.getInt("transId") ?: 0
        transType = arguments?.getString("transType")
        transDescription = arguments?.getString("transDesc")
        val pageArg = arguments?.getSerializable("page")
        page = if (pageArg is Page) pageArg else null
        paymentToPay = arguments?.getString("paymentAmount").orEmpty()
        with(viewModel) {
            if (page == Page.TRANSACTIONS) getPaymentInformation(paymentToPay)
            onChange(paymentInformation) { item -> fillFields(item) }
            onChange(onSuccessPayment) { navigateToResult(true, it) }
            onChange(onVisaConfiguration) { showVisaNet(it) }
            onChange(loading) { setLoading(it) }
            onChange(onFormError) { showError(it) }
        }
    }

    private fun showVisaNet(item: VisaNetItem) {
        try {
            VisaNet.authorization(activity, item.data, item.custom)
        } catch (e: Exception) {
            Log.i("ERROR", "onClick: " + e.message)
        }
    }

    private fun setLoading(it: Boolean) {
        binding.loadingContainer.apply {
            if (it) loading.show() else loading.hide()
        }
    }

    private fun navigateToResult(ok: Boolean, paymentResult: PaymentResult) {
        val action = PaymentFragmentDirections
            .actionPaymentFragmentToPaymentResultFragment(
                paymentResult = paymentResult,
                transId = transId.toString(),
                transType = transType,
                ok = ok
            )
        //Cambio de autor no identificado
        findNavController().navigate(action)
    }

    private fun fillFields(it: PaymentItem) {
        with(binding) {
            etNames.setText(it.name)
            etFirstLastname.setText(it.firstLastname)
            etSecondLastname.setText(it.secondLastname)
            etEmail.setText(it.email)
            tvTotalCost.text =
                getString(R.string.total_cost_label, it.ammount.formatMoneySunarp()).formatEndText()
        }

    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}