package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.RegisterArea

@JsonClass(generateAdapter = true)
data class RegisterAreaDTO(
    @field:Json(name = "codGrupoLibroArea")
    val id: String?,
    @field:Json(name = "descGrupoLibroArea")
    val name: String?,
    @field:Json(name = "precOfic")
    val precOfi: Float?,
) : BaseModel() {
    fun toModel() = RegisterArea(id.orEmpty(), name.orEmpty(),precOfi ?: 0.0f)
}