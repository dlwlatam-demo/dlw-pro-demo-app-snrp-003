package pe.gob.sunarp.appandroid.ui.services.tive.search

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import pe.gob.sunarp.appandroid.databinding.FragmentTiveSearchBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class SearchTiveFragment :
    BaseFragment<FragmentTiveSearchBinding>(FragmentTiveSearchBinding::inflate) {

    private val viewModel: SearchTiveViewModel by viewModels()
    private var office: RegistrationOffice? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        with(binding) {
            typeGroup.setOnCheckedChangeListener { _, i ->
                hideKeyboard()
                when (i) {
                    R.id.rb_type_search_title -> hintTitleForm()
                    else -> hinpubForm()
                }
            }
            etNroPlaca.filterOnlyMayus()
            etYear.onChanged { validateFields() }
            etNroTitle.onChanged { validateFields() }
            etNroPlaca.onChanged { validateFields() }
            etCode.onChanged { validateFields() }
            btnSearch.setOnClickListener {
                hideKeyboard()
                viewModel.getSearch(
                    TiveSearchRequest(viewModel.getZoneCode(office), viewModel.getCodeOffice(office), etYear.text.toString(),
                        etNroTitle.text.toString(), etNroPlaca.text.toString().replace("-", ""), etCode.text.toString(),
                        if (typeGroup.checkedRadioButtonId == R.id.rb_type_search_title) "T"
                        else "P"
                    )
                )
            }
            btnHistorySearch.setOnClickListener {
                hideKeyboard()
                gotToHistory()
            }
        }
    }

    private fun hinpubForm() {
        binding.etYear.hint = "Año de la publicidad"
        binding.etNroTitle.hint = "N° de publicidad"
    }

    private fun hintTitleForm() {
        binding.etYear.hint = "Año de la título"
        binding.etNroTitle.hint = "N° de título"
    }

    private fun validateFields() {
        with(binding) {
            if (
                etYear.validFormNotEmpty() &&
                etNroTitle.validFormNotEmpty() &&
                etNroPlaca.validFormNotEmpty() &&
                etCode.validFormNotEmpty() &&
                etRegisterOffice.validFormTagEmpty()
            ) {
                btnSearch.enable()
            } else {
                btnSearch.disable()
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getParameters()
            onChange(compleateData) { chargeImage(it) }
            onChange(loading) { showLoading(it) }
            onChange(offices) { setListenerOffice(it) }
        }
    }

    private fun setListenerOffice(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(
                ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items)
            )
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
        }
    }

    private fun chargeImage(success: Boolean) {
        if(success) {
            gotToHistory()
        }
    }

    private fun gotToHistory() {
        findNavController().navigate(
            R.id.action_go_to_tiveShowhistoryFragment_from_search
        )
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}