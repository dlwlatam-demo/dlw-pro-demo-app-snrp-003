package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserRegisterAlertRequest(
    @field:Json(name = "email")
    val email: String,
    @field:Json(name = "appVersion")
    val appVersion: String,
    @field:Json(name = "guid")
    val guid: String
)