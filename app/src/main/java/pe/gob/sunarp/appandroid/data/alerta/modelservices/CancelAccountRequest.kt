package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CancelAccountRequest(
    @field:Json(name = "idRgst")
    val idRgst: Int,
)