package pe.gob.sunarp.appandroid.data.payment.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.PaymentKeys

@JsonClass(generateAdapter = true)
data class PaymentDataDTO(
    @field:Json(name = "instancia")
    val instance: String?,
    @field:Json(name = "merchantId")
    val merchantId: String?,
    @field:Json(name = "accesskeyId")
    val accesskeyId: String?,
    @field:Json(name = "secretAccessKey")
    val secretAccessKey: String?,
    @field:Json(name = "pinHash")
    val pinHash: String?,
    @field:Json(name = "expireDate")
    val pinHashExpired: String?,
    @field:Json(name = "urlVisanetToken")
    val urlVisanetToken: String?,
    @field:Json(name = "urlVisanet")
    val urlVisanet: String?,
    @field:Json(name = "mdd4")
    val mdd4: String?,
    @field:Json(name = "mdd21")
    val mdd21: String?,
    @field:Json(name = "mdd32")
    val mdd32: String?,
    @field:Json(name = "mdd75")
    val mdd75: String?,
    @field:Json(name = "mdd77")
    val mdd77: String?,
) : BaseModel() {
    val email: String = ""
    val name: String = ""
    val lastnames: String = ""

    fun toModel(): PaymentKeys = PaymentKeys(
        instance,
        merchantId,
        accesskeyId,
        secretAccessKey,
        pinHash,
        pinHashExpired,
        urlVisanetToken,
        urlVisanet,
        mdd4,
        mdd21,
        mdd32,
        mdd75,
        mdd77,
        email,
        name,
        lastnames,
    )
}

