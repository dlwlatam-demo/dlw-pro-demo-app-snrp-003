package pe.gob.sunarp.appandroid.ui.home

import android.graphics.Bitmap

data class DrawerModel(
    val photo: Bitmap?,
    val username: String,
    val docNumber: String,
)