package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.LiteralCertificate

@JsonClass(generateAdapter = true)
data class LiteralCertificateDTO(
    @field:Json(name = "codZona")
    val zoneCode: String?,
    @field:Json(name = "codOficina")
    val officeCode: String?,
    @field:Json(name = "nombreOfic")
    val officeName: String?,
    @field:Json(name = "numPartida")
    val certNumber: String?,
    @field:Json(name = "codLibro")
    val codLibro: String?,
    @field:Json(name = "numeroPlaca")
    val plateNumber: String?,
    @field:Json(name = "fichaId")
    val fichaId: String?,
    @field:Json(name = "tomoId")
    val tomoId: String?,
    @field:Json(name = "fojaId")
    val fojaId: String?,
    @field:Json(name = "direccionPredio")
    val address: String?,
    @field:Json(name = "areaRegisDescripcion")
    val areaRegisDescription: String?,
    @field:Json(name = "libroDescripcion")
    val libroDescription: String?,
    @field:Json(name = "regPubSiglas")
    val regPubSiglas: String?,
    @field:Json(name = "baja")
    val baja: String?,
    @field:Json(name = "codigoGla")
    val codigoGla: String?,


) {
    fun toModel() = LiteralCertificate(
        officeCode.orEmpty(),
        officeName.orEmpty(),
        zoneCode.orEmpty(),
        certNumber.orEmpty(),
        plateNumber.orEmpty(),
        fichaId.orEmpty(),
        tomoId.orEmpty(),
        fojaId.orEmpty(),
        address.orEmpty(),
        areaRegisDescription.orEmpty(),
        codLibro.orEmpty(),
        libroDescription.orEmpty(),
        regPubSiglas.orEmpty(),
        baja.orEmpty(),
        codigoGla.orEmpty()
    )

}