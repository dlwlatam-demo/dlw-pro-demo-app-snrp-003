package pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.result.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.model.LegalPerson
import pe.gob.sunarp.appandroid.databinding.LayoutLegalPersonSearchItemBinding

class LegalPersonSearchResultAdapter :
    ListAdapter<LegalPerson, LegalPersonSearchResultAdapter.ViewHolder>(LegalPersonItemCallback) {

    object LegalPersonItemCallback : DiffUtil.ItemCallback<LegalPerson>() {
        override fun areItemsTheSame(oldItem: LegalPerson, newItem: LegalPerson): Boolean =
            oldItem.legalPerson == newItem.legalPerson

        override fun areContentsTheSame(oldItem: LegalPerson, newItem: LegalPerson): Boolean =
            oldItem == newItem
    }

    var legalPerson: List<LegalPerson> = emptyList()
        set(value) {
            field = value
            onListOrFilterChange()
        }

    var filter: CharSequence = ""
        set(value) {
            field = value
            onListOrFilterChange()
        }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val binding = LayoutLegalPersonSearchItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    private fun onListOrFilterChange() {
        if (filter.length < 2) {
            submitList(legalPerson)
            return
        }
        val pattern = filter.toString().lowercase().trim()
        val filteredList = legalPerson.filter { pattern in it.legalPerson.lowercase() }
        submitList(filteredList)
    }

    inner class ViewHolder(val binding: LayoutLegalPersonSearchItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: LegalPerson) = with(binding) {
            with(item) {
                requestIncl.apply {
                    tvTitle.text = "Partida:"
                    tvDescription.text = request
                }
                legalPersonIncl.apply {
                    tvTitle.text = legalPerson
                    val params = guidelineForm.layoutParams as ConstraintLayout.LayoutParams
                    params.guidePercent = 1f
                    guidelineForm.layoutParams = params
                    tvDescription.hide()
                }
                officeIncl.apply {
                    tvTitle.text = "Oficina:"
                    tvDescription.text = office
                }
                acronymIncl.apply {
                    tvTitle.text = "Siglas:"
                    tvDescription.text = acronym
                }
            }
        }
    }
}