package pe.gob.sunarp.appandroid.core.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    @field:Json(name = "user_name")
    val userName: String?,
    @field:Json(name = "access_token")
    val accessToken: String,
    @field:Json(name = "refresh_token")
    val refreshToken: String,
    @field:Json(name = "jti")
    val jti: String,
    @field:Json(name = "expires_in")
    val expiresIn: Int,
) : BaseModel() {
    constructor() : this("user", "", "", "", -1)
}