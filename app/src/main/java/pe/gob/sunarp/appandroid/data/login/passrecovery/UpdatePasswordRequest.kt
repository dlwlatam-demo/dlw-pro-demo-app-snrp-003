package pe.gob.sunarp.appandroid.data.login.passrecovery

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UpdatePasswordRequest(
    @field:Json(name = "guid")
    val guid: String?,
    @field:Json(name = "password")
    val password: String,
)