package pe.gob.sunarp.appandroid.core.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Error(
    @field:Json(name = "code")
    val code: String?,
    @field:Json(name = "error")
    val error: String?,
    @field:Json(name = "error_description")
    val errorDescription: String?,
    @field:Json(name = "description")
    val description: String?,
    @field:Json(name = "component")
    val component: String?
)