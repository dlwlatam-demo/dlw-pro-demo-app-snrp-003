package pe.gob.sunarp.appandroid.ui.payments.certificatepayment

import android.annotation.SuppressLint
import android.graphics.Paint
import android.graphics.Typeface
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.clean
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.onChanged
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.filterMaxLengthCE
import pe.gob.sunarp.appandroid.core.filterMaxLengthDNI
import pe.gob.sunarp.appandroid.core.filterMaxLengthEleven
import pe.gob.sunarp.appandroid.core.filterMaxLengthOthers
import pe.gob.sunarp.appandroid.core.filterMaxLengthRUC
import pe.gob.sunarp.appandroid.core.clearFiltersAndListeners
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.formatMoneySunarp
import pe.gob.sunarp.appandroid.core.formatMoneySunarpNew
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.validFormTagEmpty
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoPaymentBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.common.AlertTerminosDialog
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.TypePersonPayment
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertificatePaymentFragment:

    BaseFragment<FragmentCertificadoPaymentBinding>(FragmentCertificadoPaymentBinding::inflate),
    PaymentInterface {

    private val viewModel: CertificatePaymentViewModel by viewModels()
    private var certificate: CertificateType? = null
    private var saveProcess: SaveProcess? = null
    private var paymentAmmount: Double = 0.0
    private var literalCertificate: LiteralCertificate? = null
    private var certificateService: CertificateService? = null
    private var numSeats: Int = 0

    private var legalRecord: LegalRecord? = null
    private var dataItem: PaymentDataItem? = null
    private var office: RegistrationOffice? = null
    private var documentType: DocumentType? = null
    private var nuAsieSelect: String? = null
    private var imPagiSIR: String? = null
    private var nuSecuSIR: String? = null
    private var totalPaginasPartidaFicha: Int? = null
    private var cantPaginas: Int? = null
    private var cantPaginasExon: Int? = null
    private var paginasSolicitadas: Int? = null
    private var codigoGla: String? = null
    private var codLibro: String? = null
    private var codCerti: String? = null
    private var tomo: String? = null
    private var folio: String? = null
    private var isNaturalPerson: Boolean? = true
    var typeDocument: String = "09"
    private var isCheckBoxChecked = false
    var tpoPersona: String = "N"
    var typeDocPoN: String? = null


    val TAG = "CertificatePaymentFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        val originalInputType = binding.etDocumentNumber.inputType
        val originalFilters = binding.etDocumentNumber.filters
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        dataItem = arguments?.getParcelable("dataItem")
        office = arguments?.getParcelable("office")
        documentType = arguments?.getSerializable("documentType") as DocumentType?
        typeDocPoN = arguments?.getString("typeDocumentPoN")

        literalCertificate = arguments?.getParcelable("literalCertificate")
        certificateService = arguments?.getParcelable("certificateService")
        paymentAmmount = arguments?.getDouble("ammount") ?: 1.0
        numSeats = arguments?.getInt("numSeats") ?: 0
        nuAsieSelect = arguments?.getString("numSeatsSelected")
        imPagiSIR = arguments?.getString("imPagiSIR")
        nuSecuSIR = arguments?.getString("nuSecuSIR")
        totalPaginasPartidaFicha = arguments?.getInt("totalPaginasPaginasPartidaFicha")
        cantPaginas = arguments?.getInt("cantPaginas")
        cantPaginasExon = arguments?.getInt("cantPaginasExon")
        paginasSolicitadas = arguments?.getInt("paginasSolicitadas")
        codigoGla = arguments?.getString("codigoGla")
        folio =  dataItem?.folio
        tomo =  dataItem?.tomo

        
        codLibro = literalCertificate?.codLibro.toString()
        codCerti = certificate?.certificateId.toString()
        Log.e("Prueba de valores", nuAsieSelect + imPagiSIR + nuSecuSIR + totalPaginasPartidaFicha.toString())

        with(binding) {
            etDocumentNumber.filterMaxLengthDNI()
            tvDocumentType.setOnItemClickListener { parent, view, position, id ->
                val item = (parent.getItemAtPosition(position) as DocumentType)
                etDocumentNumber.clean()
                typeDocument = item.id
                Log.e("tag typeDocument", typeDocument)

                if (typeDocument == "09") {
                    etDocumentNumber.inputType = originalInputType
                    etDocumentNumber.filters = originalFilters
                    etDocumentNumber.filterMaxLengthDNI()
                    Log.e("tag typeDocument", "DNI")
                } else if (typeDocument == "03") {
                    etDocumentNumber.inputType = originalInputType
                    etDocumentNumber.filters = originalFilters
                    etDocumentNumber.inputType = InputType.TYPE_CLASS_TEXT
                    etDocumentNumber.filterMaxLengthCE()
                    Log.e("tag typeDocument", "CE")
                } else {
                    etDocumentNumber.inputType = originalInputType
                    etDocumentNumber.filters = originalFilters
                    etDocumentNumber.inputType = InputType.TYPE_CLASS_TEXT
                    etDocumentNumber.filterMaxLengthOthers()
                    Log.e("tag typeDocument", "OTRO")
                }

            }
            tvDocumentType.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }

            val userInformation = viewModel.getPersonData("$paymentAmmount")
            etFirstLastname.setText(userInformation.firstLastname.trim())
            etSecondLastname.setText(userInformation.secondLastname.trim())
            etDocumentNumber.setText(userInformation.documentNumber.trim())
            enablePersonFields()
            tilNames.editText?.onChanged { validateFields() }
            tilFirstLastname.editText?.onChanged { validateFields() }
            tilSecondLastname.editText?.onChanged { validateFields() }
            tilDocumentNumber.editText?.onChanged { validateFields() }

            btnPay.isEnabled = false
            checkBoxTerms.setOnCheckedChangeListener { _, isChecked ->
                isCheckBoxChecked = isChecked
                validateFields()

            }
            tvTerminos.paintFlags = tvTerminos.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            val fm = activity!!.supportFragmentManager
            tvTerminos.setOnClickListener {
                AlertTerminosDialog().apply {
                    setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert)
                    show(fm, this.javaClass.name)
                }
            }
            purchaseDetail.apply {
                tvTitle.text = "Detalle de la compra:"
                tvTitle.setTypeface(null, Typeface.BOLD)
                tvDescription.text = certificate?.name.orEmpty()
            }

            totalCost.apply {
                tvTitle.text = "Costo total:"
                tvTitle.setTypeface(null, Typeface.BOLD)
                tvDescription.text = paymentAmmount.formatMoneySunarpNew()
            }

            viewModel.getDocumentType(Constants.PERSON_NATURAL)

            radioGroup.setOnCheckedChangeListener { _, i ->
                var type = Constants.PERSON_NATURAL
                when (i) {
                    R.id.rb_person -> {
                        tpoPersona = "N"
                        enablePersonFields()
                        typeDocument = "09"
                        etNames.clean()
                        etDocumentNumber.clean()
                        Log.e("tipo de documento DNI", typeDocument)
                        etDocumentNumber.clearFiltersAndListeners()
                        etDocumentNumber.filterMaxLengthDNI()
                    }
                    else -> {
                        type = Constants.PERSON_LEGAL
                        tpoPersona = "J"
                        typeDocument = "05"
                        enableLegalFields()
                        etNames.clean()
                        etDocumentNumber.clean()
                        Log.e("tipo de documento RUC", typeDocument)
                        etFirstLastname.clean()
                        etSecondLastname.clean()

                        etDocumentNumber.clearFiltersAndListeners()
                        etDocumentNumber.filterMaxLengthRUC()
                    }
                }

                viewModel.getDocumentType(type)
            }

            btnPay.setOnClickListener {
                val firstLastname =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_person) etFirstLastname.text.toString() else Constants.EMPTY
                val secondLastname =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_person) etSecondLastname.text.toString() else Constants.EMPTY
                val name =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_person) etNames.text.toString() else Constants.EMPTY
                val legalPerson =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_legal) etNames.text.toString() else Constants.EMPTY
                val docNumber = etDocumentNumber.text.toString()
                val userInformation = viewModel.getPersonData("$paymentAmmount")
                //val typeDoc = ett
                val paymentModel = PaymentItem(
                    name.ifEmpty { legalPerson },
                    firstLastname,
                    secondLastname,
                    userInformation.email,
                    "$paymentAmmount",
                    docNumber,
                    if (radioGroup.checkedRadioButtonId == R.id.rb_person) TypePersonPayment.PERSON_NATURAL else TypePersonPayment.PERSON_LEGAL
                )

                when (legalRecord?.id) {
                    Constants.REGISTRO_DE_PROPIEDAD ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_PREDIOS,
                            Constants.CERTIFICADO_POSITIVO_PREDIOS_B,
                            Constants.CERTIFICADO_NEGATIVO_PREDIOS,
                            Constants.CERTIFICADO_NEGATIVO_PREDIOS_B -> {
                                certificate?.let {
                                    Log.e("rutaZero","")
                                    Log.e("tpoPersona",dataItem?.typePerson.toString())
                                    // typeDocPoN = arguments?.getString("typeDocumentPoN")
                                    typeDocPoN = dataItem?.documentType.toString()
                                    Log.e("typeDocument",typeDocPoN.toString())
                                    viewModel.saveProcessPrediosPositivosNegativos(
                                        paymentModel,
                                        dataItem?.documentNumebr.toString(),
                                        dataItem?.lastname.toString(),
                                        dataItem?.lastname2.toString(),
                                        dataItem?.name.toString(),
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(), // tpoPersona,
                                        typeDocument,
                                        dataItem?.businessName.toString(),
                                        typeDocPoN.toString()
                                    )
                                }
                            }
                            Constants.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_A,
                            Constants.CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B -> {
                                certificate?.let {
                                    Log.e("rutaOne","")
                                    var temporalyVar = dataItem?.partida?.refNumPart.toString()
                                    dataItem?.partida?.refNumPart = dataItem?.partida?.numPartida.toString()
                                    dataItem?.partida?.numPartida = temporalyVar
                                    viewModel.saveSolicitudCertificate(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        dataItem?.request!!,
                                        tpoPersona,
                                        dataItem?.zoneCode.toString(),
                                        dataItem?.zoneCodeOffice.toString(),
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_DE_CARGAS_Y_GRAVAMENES -> {
                                certificate?.let {
                                    Log.e("rutaA","")

                                    viewModel.saveSolicitudCertificateSaveProcessRequestCharges(
                                        dataItem!!,
                                        dataItem?.partida!!,
                                        paymentModel,
                                        certificate,
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                        }
                    Constants.REGISTRO_DE_PERSONAS_JURIDICAS ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_NEGATIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA,
                            Constants.CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA_B -> {
                                certificate?.let {
                                    Log.e("rutaA+","")
                                    typeDocPoN = dataItem?.documentType.toString()
                                    viewModel.saveProcessLegalPerson(
                                        paymentModel,
                                        dataItem?.documentNumebr.toString(),
                                        dataItem?.lastname.toString(),
                                        dataItem?.lastname2.toString(),
                                        dataItem?.name.toString(),
                                        legalRecord,
                                        certificate,
                                        office,
                                        "J",
                                        typeDocument,
                                        dataItem?.businessName.toString(),
                                        typeDocPoN.toString()
                                    )
                                }
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA -> {
                                certificate?.let {

                                    Log.e("ruta0","")
                                    viewModel.saveSolicitudCertificateSaveProcessRequest(
                                        dataItem!!,
                                        dataItem?.partida!!,
                                        paymentModel,
                                        certificate,
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO -> {

                                Log.e("ruta1","")
                                viewModel.saveSolicitudCertificateSaveProcessRequestOrgano(
                                    dataItem!!,
                                    dataItem?.partida!!,
                                    paymentModel,
                                    certificate,
                                    "J",
                                    typeDocument
                                )
                            }
                            Constants.CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ -> {

                                Log.e("ruta2","")
                                viewModel.saveSolicitudCertificateSaveProcessRequestPJ(
                                    dataItem!!,
                                    dataItem?.partida!!,
                                    paymentModel,
                                    certificate,
                                    tpoPersona,
                                    typeDocument
                                )
                            }
                        }
                    Constants.REGISTRO_DE_PERSONAS_NATURALES ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA,
                            Constants.CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B,
                            Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA,
                            Constants.CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B,
                            Constants.CERTIFICADO_NEGATIVO_UNION_DE_HECHO,
                            Constants.CERTIFICADO_POSITIVO_UNION_DE_HECHO,
                            Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL,
                            Constants.CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B,
                            Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS,
                            Constants.CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B,
                            Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS,
                            Constants.CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B -> {
                                certificate?.let {
                                    Log.e("ruta3","")
                                    viewModel.saveProcessUnion(
                                        paymentModel,
                                        dataItem?.documentNumebr.toString(),
                                        dataItem?.lastname.toString(),
                                        dataItem?.lastname2.toString(),
                                        dataItem?.name.toString(),
                                        legalRecord,
                                        certificate,
                                        office,
                                        "N",
                                        typeDocument,
                                        dataItem?.businessName.toString(),
                                        typeDocPoN.toString()
                                    )
                                }
                            }
                            Constants.CERTIFICADO_VIGENCIA_DESIGNACION_APOYO -> {
                                certificate?.let {
                                    Log.e("ruta4","")
                                    viewModel.saveProcessPartida(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        dataItem?.asientos!!,
                                        "",
                                        "",
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR -> {
                                certificate?.let {
                                    Log.e("rutaB","")
                                    Log.e("numPartida", dataItem?.partida!!.numPartida)
                                    viewModel.saveProcessPartidaPNVigencias(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        dataItem?.asientos!!,
                                        tomo.toString(),
                                        folio.toString(),
                                        "",
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_VIGENCIA_PODER,
                            Constants.CERTIFICADO_VIGENCIA_PODER_B -> {
                                certificate?.let {
                                    Log.e("rutaC","")
                                    Log.e("numPartida", dataItem?.partida!!.numPartida)

                                    viewModel.saveProcessPartidaPNVigencias(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        dataItem?.asientos!!,
                                        tomo.toString(),
                                        folio.toString(),
                                        "",
                                        tpoPersona,
                                        typeDocument
                                    )
                                    Log.e("CPF-Partida", dataItem!!.partida?.numPartida.toString())
                                    Log.e("CPF-refPartida", dataItem!!.partida?.refNumPart.toString())
                                }
                            }
                        }
                    Constants.REGISTRO_DE_BIENES_INMUEBLES ->
                        when (certificate?.certificateId) {
                            Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR,
                            Constants.CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B,
                            Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR,
                            Constants.CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B -> {
                                certificate?.let {
                                    Log.e("rutaD","")
                                    viewModel.saveProcessVehicleOwnership(
                                        paymentModel,
                                        dataItem?.documentNumebr.toString(),
                                        dataItem?.lastname.toString(),
                                        dataItem?.lastname2.toString(),
                                        dataItem?.name.toString(),
                                        legalRecord,
                                        certificate,
                                        office,
                                        tpoPersona,
                                        typeDocument,
                                        dataItem?.businessName.toString(),
                                        typeDocPoN.toString(),
                                    )
                                }

                            }
                            Constants.CERTIFICADO_REGISTRAL_VEHICULAR,
                            Constants.CERTIFICADO_REGISTRAL_VEHICULAR_B -> {
                                certificate?.let {
                                    Log.e("rutaE","")
                                    Log.e("numPartida", dataItem?.partida!!.numPartida)
                                    Log.e("numPlaca", dataItem?.partida!!.numPlaca)

                                    viewModel.saveProcessPartida(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        Constants.EMPTY,
                                        "",
                                        "",
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES,
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B -> {

                                certificate?.let {
                                    Log.e("rutaF","")
                                    Log.e("numPartida", dataItem?.partida!!.numPartida)

                                    viewModel.saveProcessPartida(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        Constants.EMPTY,
                                        dataItem?.expediente.toString(),
                                        "",
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES,
                            Constants.CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B -> {

                                certificate?.let {
                                    Log.e("rutaG","")
                                    Log.e("numPartida", dataItem?.partida!!.numPartida)
                                    Log.e("nomEmbar en CPF", dataItem!!.nomEmbarcacion.toString())

                                    viewModel.saveProcessPartida(
                                        dataItem?.partida!!,
                                        paymentModel,
                                        legalRecord,
                                        certificate,
                                        office,
                                        dataItem?.typePerson.toString(),
                                        Constants.EMPTY,
                                        "",
                                        dataItem!!.nomEmbarcacion.toString(),
                                        tpoPersona,
                                        typeDocument
                                    )
                                }
                            }
                        }
                    else -> { //Others
                        literalCertificate?.let {
                            Log.e("RutaCertificadoLiteral", "")
                            viewModel.create(
                                paymentModel,
                                checkBoxTerms.isChecked,
                                certificate?.certificateId.orEmpty(),
                                it,
                                "$cantPaginas",
                                cantPaginasExon = "$cantPaginasExon",
                                requestedPages = "$paginasSolicitadas",
                                office,
                                tpoPersona,
                                typeDocument
                            )
                        }
                    }
                }
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(onVisaConfiguration) { showVisaNet(it) }
            onChange(onSaveProcess) { saveOnSaveProcess(it) }
            onChange(onPaymentProcess) { onPaymentProcess(it) }
            onChange(sendPaymentExConfirmation) { sendPaymentExConfirmation(it) }
            onChange(loading) { showLoading(it) }
            onChange(docTypes) { fillDocType(it) }
            onChange(onPaymentProcessEx) { onPaymentProcessEx(it) }
        }
    }

    private fun onPaymentProcessEx(process: PaymentProcess) {
        val fullName = process.solicitante
        navigateToResult(
            true,
            process.toPaymentResultEx(fullName, certificate?.name.orEmpty(), paymentProcess = process)
        )
    }

    private fun fillDocType(docs: List<DocumentType>) {
        binding.tvDocumentType.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    docs
                )
            )
            setText(adapter.getItem(0).toString(), false)
        }
    }

    @SuppressLint("LongLogTag")
    private fun onPaymentProcess(paymentProcess: PaymentProcess) {

        Log.d(TAG, "onPaymentProcess")

        with(binding) {
            val firstLastname = etFirstLastname.text.toString()
            val secondLastname = etSecondLastname.text.toString()
            val name = etNames.text.toString()

            Log.d(TAG, "onPaymentProcess ${paymentProcess.visaNetResult?.data}")

            paymentProcess.visaNetResult?.let {

                val fullname = "$name $firstLastname $secondLastname"
                navigateToResult(
                    true,
                    it.toPaymentResult(fullname, certificate?.name.orEmpty(), paymentProcess = paymentProcess)
                )
            }
        }

    }

    private fun saveOnSaveProcess(process: SaveProcess) {
        saveProcess = process
    }

    @SuppressLint("LongLogTag")
    private fun showVisaNet(item: VisaNetItem) {

        Log.d(TAG, "showVisaNet")
        VisaNet.authorization(activity, item.data, item.custom)

    }

    @SuppressLint("LongLogTag")
    override fun sendPaymentConfirmation(data: VisaNetResult) {
        Log.d(TAG, "sendPaymentConfirmation")
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificateService?.let {
                saveProcess?.let { it1 ->
                    Log.e("ruta1**", "")
                    Log.e("nuAsiSelect", nuAsieSelect.toString())
                    Log.e("imPagSir", imPagiSIR.toString())
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, it, null, nuAsieSelect?:"", imPagiSIR?:"" , nuSecuSIR?:"", totalPaginasPartidaFicha?:0, cantPaginas?:0, cantPaginasExon?:0,paginasSolicitadas?:0, codigoGla?:"", codLibro?:"", codCerti?:""

                    )
                }
            } ?: run {
                certificateService =
                    CertificateService(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)

                saveProcess?.let { it1 ->
                    Log.e("ruta 2**", "")
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, certificateService!!, dataItem,nuAsieSelect?:"", imPagiSIR?:"" , nuSecuSIR?:"", totalPaginasPartidaFicha?:0, cantPaginas?:0, cantPaginasExon?:0,paginasSolicitadas?:0, codigoGla?:"", codLibro?:"", codCerti?:""
                    )
                } ?: run {
                    Log.d(TAG, "saveProcess null")
                }
            }
        }
    }

    @SuppressLint("LongLogTag")
    private fun sendPaymentExConfirmation(saveProcess: SaveProcess) {
        Log.d(TAG, "sendPaymentExConfirmation")
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificateService?.let {
                saveProcess.let { it1 ->
                    viewModel.paymentProcessEx(
                         docNumber, "$paymentAmmount", it1, it, null,nuAsieSelect?:"", imPagiSIR?:"" , nuSecuSIR?:"", totalPaginasPartidaFicha?:0, cantPaginas?:0, cantPaginasExon?:0, paginasSolicitadas?:0
                    )
                }
            } ?: run {
                certificateService =
                    CertificateService(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)

                saveProcess.let { it1 ->
                    viewModel.paymentProcessEx(
                         docNumber, "$paymentAmmount", it1, certificateService!!, dataItem,nuAsieSelect?:"", imPagiSIR?:"" , nuSecuSIR?:"", totalPaginasPartidaFicha?:0, cantPaginas?:0, cantPaginasExon?:0, paginasSolicitadas?:0
                    )
                }
            }
        }
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {

        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber , Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = CertificatePaymentFragmentDirections
            .actionCertificatePaymentFragmentToPaymentResultFragment(
                transId = transId,
                paymentResult = paymentResult,
                certificate = certificate,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun enableLegalFields() {
        with(binding) {
            btnPay.isEnabled = false
            etNames.filterMayus()
            tilFirstLastname.hide()
            etFirstLastname.enable()
            etFirstLastname.clean()
            tilSecondLastname.hide()
            etSecondLastname.clean()
            etSecondLastname.enable()
            etDocumentNumber.enable()
            etDocumentNumber.clean()
            etDocumentNumber.filterMaxLengthRUC()
            tvDocumentType.validFormTagEmpty()
//            tvDocumentType.setText("RUC")
            etNames.hint = getString(R.string.label_legal_person)
            etNames.clean()
            checkBoxTerms.isChecked = false
            isNaturalPerson = false
        }
    }

    private fun enablePersonFields() {
        with(binding) {
            val userInformation = viewModel.getPersonData("$paymentAmmount")
            tilFirstLastname.show()
            //etFirstLastname.setText(userInformation.firstLastname.trim())
            tilSecondLastname.show()
            //etSecondLastname.setText(userInformation.secondLastname.trim())
            //etDocumentNumber.setText(userInformation.documentNumber.trim())
//            tvDocumentType.setText("DNI")
            etNames.filterMayus()
            etFirstLastname.filterMayus()
            etSecondLastname.filterMayus()
            etNames.hint = getString(R.string.label_names)
            etNames.setText(userInformation.name)
            isNaturalPerson = true
            checkBoxTerms.isChecked = false
        }
    }

    private fun showLoading(show: Boolean) {
        with(binding) {
            loadingContainer.apply {
                if (show) {
                    loading.show()
                } else {
                    loading.hide()
                }
            }
        }
    }
    private fun validateFields() {
        with(binding) {
            btnPay.isEnabled = false
            if (isNaturalPerson == true) {
                if (etDocumentNumber.text?.isNotEmpty() == true && etNames.text?.isNotEmpty()!! && etFirstLastname.text?.isNotEmpty()!! && isCheckBoxChecked) {
                    btnPay.isEnabled = true
                }
            } else {
                if (etDocumentNumber.text?.isNotEmpty() == true && etNames.text?.isNotEmpty()!! && isCheckBoxChecked) {
                    btnPay.isEnabled = true
                }
            }
        }
    }


    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}