package pe.gob.sunarp.appandroid.core.api

import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

class DataResultAdapter<R>(private val successType: Type) : CallAdapter<R, Call<DataResult<R>>> {
    override fun responseType() = successType

    override fun adapt(call: Call<R>): Call<DataResult<R>> {
        return DataResultCall(call)
    }
}