package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.CertificateOwnershipAsync

@JsonClass(generateAdapter = true)
data class CertificateOwnershipAsyncDTO(
    @field:Json(name = "guid")
    val guid: String?,
    @field:Json(name = "status")
    val status: String?,
    @field:Json(name = "response")
    val response: List<CertificateOwnershipDTO>?
) : BaseModel() {
    fun toModel() = CertificateOwnershipAsync(
        guid.orEmpty(),
        status.orEmpty(),
        response.orEmpty()
    )
}