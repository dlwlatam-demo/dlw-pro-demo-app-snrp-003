package pe.gob.sunarp.appandroid.core.model

import java.io.Serializable

data class DocumentDNI(
    val documentType: String,
    val dni: String,
    val paternal: String,
    val maternal: String,
    val names: String,
    val birthdayDate: String,
    val issueDate: String
) : Serializable