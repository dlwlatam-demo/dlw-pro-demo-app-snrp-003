package pe.gob.sunarp.appandroid.ui.common

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import pe.gob.sunarp.appandroid.databinding.AlertTerminosBinding

class AlertTerminosDialog : AppCompatDialogFragment() {

    private lateinit var binding: AlertTerminosBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = AlertTerminosBinding.inflate(LayoutInflater.from(context))
        val builder = AlertDialog.Builder(context!!)
            .setView(binding.root)

        with(binding) {
            btnClose.setOnClickListener { dismiss() }
            webview.settings.setJavaScriptEnabled(true)

            webview.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    url?.let {
                        view?.loadUrl(it)
                    }
                    return true
                }
            }
            webview.loadUrl("https://www.sunarp.gob.pe/politicas-privacidad.aspx")
        }

        return builder.create()
    }
}