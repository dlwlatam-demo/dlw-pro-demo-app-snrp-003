package pe.gob.sunarp.appandroid.ui.services.vehicle.information

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.core.toBitmap
import pe.gob.sunarp.appandroid.databinding.FragmentVehicleInformationBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class VehicleInformationFragment :
    BaseFragment<FragmentVehicleInformationBinding>(FragmentVehicleInformationBinding::inflate) {

    private val viewModel: VehicleInformationViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModels()
    }

    private fun initView() {
        val information: VehicleInformation? = arguments?.getParcelable("information")
        binding.btnRequestReceipt.setOnClickListener {
            information?.let { inf -> viewModel.validateReceipt(inf) }
        }
    }

    private fun initViewModels() {
        val information: VehicleInformation? = arguments?.getParcelable("information")
        with(viewModel) {
            initViewModelsErrorToken()
            information?.let { getVehicleInformation(information) }
            onChange(vehicleImage) { loadImage(it) }
            onChange(receipt) { onReceiptValidated() }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun onReceiptValidated() {
        val information: VehicleInformation? = arguments?.getParcelable("information")
        findNavController().navigate(
            R.id.action_vehicleInformationFragment_to_vehiclePaymentFragment,
            bundleOf("information" to information)
        )
    }


    private fun loadImage(image: String) {
        binding.ivVehicleInformation.setImageBitmap(image.toBitmap())
    }

    private fun showLoading(show: Boolean) {
        with(binding) {
            loadingContainer.apply {
                if (show) {
                    loading.show()
                    statusContainer.hide()
                } else {
                    statusContainer.show()
                    loading.hide()
                }
            }
        }

    }

    override fun showError(error: String?) {
        Log.e("ERROR", "$error")
        with(binding) {
            statusContainer.hide()
            tvError.apply {
                text = error.orEmpty()
                show()
            }
            super.showError(error)
        }

    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}