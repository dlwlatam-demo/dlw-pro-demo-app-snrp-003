package pe.gob.sunarp.appandroid.data.common

import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject

class SharedPreferenceRepository @Inject constructor(
    private val preferences: SunarpSharedPreferences,
) {

    fun getOnboardingPreference() = preferences.onBoardingShowed

    fun setOnboardingPreference(value: Boolean) {
        preferences.onBoardingShowed = value
    }

    fun getDoc() = preferences.userDocNumber

    fun getDocType() = preferences.userDocType

}