package pe.gob.sunarp.appandroid.ui.register.confirm

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.finishActivity
import pe.gob.sunarp.appandroid.core.model.RegisterInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRegisterConfirmBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.AlertType
import pe.gob.sunarp.appandroid.ui.common.AlertaCommonDialog

@AndroidEntryPoint
class RegisterConfirmFragment :
    BaseFragment<FragmentRegisterConfirmBinding>(FragmentRegisterConfirmBinding::inflate) {

    private val viewModel: RegisterConfirmViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setObservers()
    }

    private fun setListeners() {
        with(binding) {
            actionRecoveryConfirm.setOnClickListener { it ->
                it.disable()
                textInputPassword.disable()
                textInputPasswordConfirm.disable()
                val registerInformation: RegisterInformation? =
                    arguments?.getParcelable("registerInformation")
                registerInformation?.let { ri ->
                    viewModel.confirm(
                        registerInformation = ri,
                        password = textInputPassword.editText?.text.toString(),
                        passwordConfirm = textInputPasswordConfirm.editText?.text.toString()
                    )
                }
            }
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
                finishActivity()
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            isConfirm.observe(viewLifecycleOwner) { goToNextView() }
            onError.observe(viewLifecycleOwner) { showError(it) }
            onFormError.observe(viewLifecycleOwner) { showError(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun goToNextView() {
        with(binding) {
            actionRecoveryConfirm.enable()
            textInputPassword.disable()
            textInputPasswordConfirm.disable()
            context?.let {
                activity?.let {
                    AlertaCommonDialog().apply {
                        onCancelListener {
                            this.dismiss()
                            findNavController().navigate(RegisterConfirmFragmentDirections.actionRegisterConfirmFragmentToLoginActivity())}
                        setData(title = "Envío Satisfactorio", detalle = "Creación de cuenta exitosa")
                        show(it.supportFragmentManager, "error")
                        setAlertType(type = AlertType.SUCCESS)
                    }
                }
            }
            //requireActivity().finish()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    override fun showError(error: String?) {
        with(binding) {
            actionRecoveryConfirm.enable()
            textInputPassword.enable()
            textInputPasswordConfirm.enable()
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}