package pe.gob.sunarp.appandroid.ui.alerta.validatecode

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaValidateCodeViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) :
    BaseViewModel() {
    val isValidateCode = SingleLiveEvent<String>()
    val showLoading = SingleLiveEvent<Boolean>()
    val newGuid = SingleLiveEvent<String>()

    fun validateCode(code: String, guid: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.activateUserById(guid, code)) {
                is DataResult.Success -> sendGUID(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun resendCode(guid: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.sendValidationCode(guid)) {
                is DataResult.Success -> OnSuccessResendCode(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private fun sendGUID(guid: String) {
        isValidateCode.value = guid
        showLoading.value = false
    }

    private fun OnSuccessResendCode(guid: String) {
        newGuid.value = guid
        showLoading.value = false
    }
}
