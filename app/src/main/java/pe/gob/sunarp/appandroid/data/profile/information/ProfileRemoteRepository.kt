package pe.gob.sunarp.appandroid.data.profile.information

import android.graphics.Bitmap
import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_CE
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_DNI
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.core.toBitmap
import pe.gob.sunarp.appandroid.data.Utils
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.profile.information.dto.ProfileDTO
import pe.gob.sunarp.appandroid.data.profile.information.request.DeviceRequest
import pe.gob.sunarp.appandroid.data.profile.information.request.ProfileRequest
import pe.gob.sunarp.appandroid.data.profile.information.request.UploadPhotoRequest
import pe.gob.sunarp.appandroid.data.registration.register.CeRequest
import pe.gob.sunarp.appandroid.data.registration.register.DniRequest
import pe.gob.sunarp.appandroid.ui.home.profile.model.ProfileItem
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ProfileRemoteRepository @Inject constructor(
    private val dataSource: ProfileDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getUserInformation(): DataResult<ProfileItem> =
        withContext(dispatcher) {
            val device =
                DeviceRequest(BuildConfig.SUNARP_APP_ID, "", "", "", "1", "3.0", "", "", "", "")
            when (val result = dataSource.getProfileInformation(
                ProfileRequest(device),
                preferences.accessToken.toBearer(), BuildConfig.SUNARP_SECURITY_HOST
            )) {
                is DataResult.Success -> {
                    UserProfile.tipoDoc = result.data.userData?.docType.toString()
                    UserProfile.email = result.data.userData?.email.toString()
                    UserProfile.nombres = result.data.userData?.names.toString()
                    UserProfile.nroDoc = result.data.userData?.docNumber.toString()
                    UserProfile.priApe = result.data.userData?.firstLastname.toString()
                    UserProfile.segApe = result.data.userData?.secondLastname.toString()

                    Log.e("UserProfileTipoDoc", UserProfile.tipoDoc)
                    preferences.saveUserProfile(result.data)
                    DataResult.Success(map(result.data))
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun getGenderList(): DataResult<List<Gender>> =
        withContext(dispatcher) {
            DataResult.Success(Utils.getGender())
        }

    fun getPhoto(): Bitmap? = preferences.userPhoto.toBitmap()

    suspend fun getDocTypeList(): DataResult<List<DocumentType>> =
        withContext(dispatcher) {
            when (val result = dataSource.getDocumentTypes(
                preferences.accessToken.toBearer(), BuildConfig.SUNARP_PARAMETERS_HOST
            )) {
                is DataResult.Success -> {
                    DataResult.Success(result.data
                        .filter { it.docTypeId == DOCUMENT_TYPE_DNI || it.docTypeId == DOCUMENT_TYPE_CE }
                        .map {
                            it.toModel()
                        })
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }


    suspend fun updateUserPhoto(photo: String): DataResult<Boolean> =
        withContext(dispatcher) {
            when (val result = dataSource.updateUserPhoto(
                UploadPhotoRequest(photo),
                preferences.accessToken.toBearer(), BuildConfig.SUNARP_SECURITY_HOST
            )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        preferences.userPhoto = photo
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description ?: ""))
                    }
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun updateUserInformation(user: ProfileInformation): DataResult<Boolean> =
        withContext(dispatcher) {
            val c: Date = Calendar.getInstance().time

            val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val formattedDate: String = df.format(c)
            when (val result = dataSource.updateUserInformation(
                user.toRequest("token", "appVersion", "ipAddress", formattedDate, 0, 0),
                preferences.accessToken.toBearer(), BuildConfig.SUNARP_SECURITY_HOST
            )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description ?: ""))
                    }
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    private fun map(data: ProfileDTO): ProfileItem {
        with(data) {
            return ProfileItem(
                userId = userData?.idUser ?: "",
                name = userData?.names ?: "",
                firstLastname = userData?.firstLastname ?: "",
                secondLastname = userData?.secondLastname ?: "",
                gender = userData?.gender ?: "",
                birthday = userData?.birthday ?: "",
                docType = userData?.docType ?: "",
                docNumber = userData?.docNumber ?: "",
                mobilePhone = userData?.mobilePhone ?: "",
                email = userData?.email ?: "",
                photo = null
            )
        }
    }

    suspend fun validateDni(issueDate: String, documentNumber: String): DataResult<DocumentDNI> =
        withContext(dispatcher) {
            when (val result = dataSource.validateDocumentDNI(
                DniRequest(issueDate, documentNumber),
                preferences.accessToken.toBearer(), BuildConfig.SUNARP_QUERIES_KEY
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validateCe(documentNumber: String): DataResult<DocumentCE> =
        withContext(dispatcher) {
            when (val result = dataSource.validateDocumentCE(
                CeRequest(documentNumber),
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_QUERIES_KEY)
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }
}