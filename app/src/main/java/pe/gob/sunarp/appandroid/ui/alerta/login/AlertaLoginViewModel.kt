package pe.gob.sunarp.appandroid.ui.alerta.login

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaLoginViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) :
    BaseViewModel() {
    val isAuthenticated = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun login(username: String, password: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.loginAlerta(username, password)) {
                is DataResult.Success -> sendProfileData()
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun isLoginAlert()  = preferences.isLoginAlert()

    private fun sendProfileData() {
        isAuthenticated.value = true
        showLoading.value = false
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }
}
