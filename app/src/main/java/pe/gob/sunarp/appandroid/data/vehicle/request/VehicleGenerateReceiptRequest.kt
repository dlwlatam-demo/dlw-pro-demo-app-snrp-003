package pe.gob.sunarp.appandroid.data.vehicle.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest

@JsonClass(generateAdapter = true)
data class VehicleGenerateReceiptRequest(
    @field:Json(name = "costo")
    val cost: String,
    @field:Json(name = "codZona")
    val zoneCode: String,
    @field:Json(name = "codOficina")
    val officeCode: String,
    @field:Json(name = "placa")
    val plate: String,
    @field:Json(name = "visanetResponse")
    val visanet: NiubizRequest?,
    @field:Json(name = "ip")
    val ipAddress: String? = "0.0.0.0",
    @field:Json(name = "usuario")
    val idUser: String? = "APPSNRPANDRO"
)