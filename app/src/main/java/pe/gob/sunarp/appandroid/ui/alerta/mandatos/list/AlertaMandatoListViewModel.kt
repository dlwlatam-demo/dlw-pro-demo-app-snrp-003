package pe.gob.sunarp.appandroid.ui.alerta.mandatos.list

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.DeleteMandatoRequest
import pe.gob.sunarp.appandroid.data.alerta.modelservices.MandatoItemResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaMandatoListViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) :
    BaseViewModel() {
    val loading = SingleLiveEvent<Boolean>()
    val listData = SingleLiveEvent<List<MandatoItemResponse>>()

    fun getListData() =
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.selectAllMandatos()) {
                is DataResult.Success -> onGetListDataSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    private fun onGetListDataSuccess(data: List<MandatoItemResponse>) {
        listData.value = data
        loading.value = false
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun cancelMandato(mandatoitem: MandatoItemResponse) = viewModelScope.launch {
        loading.value = true
        when (val result = repository.deleteMandato(DeleteMandatoRequest(mandatoitem.nuCont, mandatoitem.aaCont))) {
            is DataResult.Success -> onCancelMandatoSuccess(result.data)
            is DataResult.Failure -> onError(result.exception)
        }
    }

    private fun onCancelMandatoSuccess(result: Boolean) {
        getListData()
    }
}
