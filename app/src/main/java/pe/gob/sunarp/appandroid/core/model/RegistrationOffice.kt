package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RegistrationOffice(
    val name: String,
    val regPubId: String,
    val oficRegId: String,
    val codOficina: String,
    val preOfficjurisIde: String,
    var select: Boolean = false,
): Parcelable {
    override fun toString(): String {
        return name
    }
}