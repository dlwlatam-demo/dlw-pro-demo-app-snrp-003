package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchZone(
    val id: String,
    val name: String,
    var selected: Boolean = false
): Parcelable {
    override fun toString(): String {
        return name
    }
}