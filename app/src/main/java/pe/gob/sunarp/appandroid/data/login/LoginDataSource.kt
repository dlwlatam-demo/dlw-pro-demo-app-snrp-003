package pe.gob.sunarp.appandroid.data.login

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.LogOut
import pe.gob.sunarp.appandroid.core.model.User
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.login.passrecovery.PassCodeValidationRequest
import pe.gob.sunarp.appandroid.data.login.passrecovery.PassRecoveryRequest
import pe.gob.sunarp.appandroid.data.login.passrecovery.UpdatePasswordRequest
import retrofit2.http.*

interface LoginDataSource {
    @FormUrlEncoded

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_OAUTH_URL + "/oauth/token")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<User>

    @Headers(Constants.HEADER_API_GATEWAY)
    @DELETE(Constants.CONTEXTO_OAUTH_URL + "/invalidarToken/{jti}")
    suspend fun logout(
        @Path("jti") jti: String,
        @Header("Host") host: String?
    ): DataResult<LogOut>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/seguridad/recupera")
    suspend fun requestRecoveryCode(
        @Body request: PassRecoveryRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/seguridad/recupera/valida")
    suspend fun validateRecoveryCode(
        @Body request: PassCodeValidationRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/seguridad/cambio/clave")
    suspend fun updatePassword(
        @Body request: UpdatePasswordRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_OAUTH_URL + "/getLastVersion/abiapp")
    suspend fun getLastVersionAppGms(
        @Header("Host") host: String?,
        @Query("timestamp") timestamp: Long
    ): DataResult<LogOut>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_OAUTH_URL + "/getLastVersion/hmsapp")
    suspend fun getLastVersionAppHms(
        @Header("Host") host: String?,
        @Query("timestamp") timestamp: Long
    ): DataResult<LogOut>


}