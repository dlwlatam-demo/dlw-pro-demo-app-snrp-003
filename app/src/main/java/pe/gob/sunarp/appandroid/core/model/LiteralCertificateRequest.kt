package pe.gob.sunarp.appandroid.core.model

data class LiteralCertificateRequest(
    val regPubId: String,
    val oficRegId: String,
    val areaRegId: String,
    val codGrupo: String,
    val tipoPartidaFicha: String,
    val numPart: String,
    val coServ: String,
    val coTipoRgst: String,
) {

    companion object {

        fun toRequest(
            tipoPartidaFicha: String,
            numPart: String,
            legal: LegalRecord?,
            certificateType: CertificateType?,
            office: RegistrationOffice?,
            service: CertificateService?
        ): LiteralCertificateRequest {
            return LiteralCertificateRequest(
                regPubId = office?.regPubId.orEmpty(),
                oficRegId = office?.oficRegId.orEmpty(),
                areaRegId = legal?.id.orEmpty(),
                codGrupo = certificateType?.codGrupoLibroArea.orEmpty(),
                tipoPartidaFicha = tipoPartidaFicha,
                numPart = numPart,
                coServ = service?.id.orEmpty(),
                coTipoRgst = service?.registrationTypeId.orEmpty()
            )
        }

    }
}