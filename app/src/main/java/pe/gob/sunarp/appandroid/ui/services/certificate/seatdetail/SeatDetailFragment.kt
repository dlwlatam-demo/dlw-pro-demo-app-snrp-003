package pe.gob.sunarp.appandroid.ui.services.certificate.seatdetail

import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.isNotEmpty
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentDetailSeatsBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.seatdetail.adapter.SeatDetailAdapter

@AndroidEntryPoint
class SeatDetailFragment :
    BaseFragment<FragmentDetailSeatsBinding>(FragmentDetailSeatsBinding::inflate) {

    private val viewModel: SeatDetailViewModel by viewModels()
    private var certificate: CertificateType? = null
    private var literalCertificate: LiteralCertificate? = null
    private var certificateService: CertificateService? = null
    private var legalRecord: LegalRecord? = null
    private var requestCertificate = false
    private var valueToPay: Double = 0.0
    private var countSelectList: Int = 0
    private var cantPaginas: Int = 0
    private var cantPaginasExon: Int = 0
    private var paginasSolicitadas: Int = 0
    private var pagoExon: Boolean = false
    private var office: RegistrationOffice? = null
    private var nuAsieSelect: String = ""
    private var nuAsieSelectSARP: String = ""
    private var isVEHOrRMC: Boolean = false
    private var imPagiSIR: String = ""
    private var nuSecuSIR: String = ""
    private var totalPaginasPartidaFicha: Int = 0
    private val nuAsieSelectList: MutableList<String> = mutableListOf()
    private val imPagiSIRList: MutableList<String> = mutableListOf()
    private val nuSecuSIRList: MutableList<String> = mutableListOf()
    private var numDocumento: String? = null
    var isSelectAll: Boolean = false
    private var codigoGla: String = ""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        initViewModels()

    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews(view: View) {
        certificate = arguments?.getParcelable("certificate")
        literalCertificate = arguments?.getParcelable("literalCertificate")
        certificateService = arguments?.getParcelable("certificateService")
        office = arguments?.getParcelable("office")
        legalRecord = arguments?.getParcelable("legalRecord")
        numDocumento = arguments?.getString("numDocumento")
        codigoGla = literalCertificate?.codigoGla.toString()
        Log.e("Numero de Partida", numDocumento.toString())

        setTittle()
        with(binding) {
            tvCertificateType.text = certificate?.name.orEmpty()

            if (legalRecord?.id == Constants.REGISTRO_DE_BIENES_MUEBLES) {
                registerOffice.tvTitle.text = "Registro Público:"
                registerOffice.tvDescription.text = literalCertificate?.regPubSiglas.orEmpty()
                certificateItem.tvTitle.text = "Oficina Registral:"
                certificateItem.tvDescription.text = literalCertificate?.officeName.orEmpty()
                addressItem.tvTitle.text = "Partida:"
                addressItem.tvDescription.text = literalCertificate?.certNumber.orEmpty()
                registerArea.tvTitle.text = "Placa:"
                registerArea.tvDescription.text = literalCertificate?.plateNumber.orEmpty()
                registerTo.tvTitle.text = "Estado de Vehículo:"
                registerTo.tvDescription.text = literalCertificate?.baja.orEmpty()
            } else {
                registerOffice.tvTitle.text = "Oficina registral:"
                registerOffice.tvDescription.text = literalCertificate?.officeName.orEmpty()
                certificateItem.tvTitle.text = "Partida:"
                certificateItem.tvDescription.text = literalCertificate?.certNumber.orEmpty()
                addressItem.tvTitle.text = "Dirección:"
                addressItem.tvDescription.text = literalCertificate?.address.orEmpty()
                registerArea.tvTitle.text = "Área registral:"
                registerArea.tvDescription.text = literalCertificate?.areaRegisDescription.orEmpty()
                registerTo.tvTitle.text = "Registro de:"
                registerTo.tvDescription.text = literalCertificate?.libroDescription.orEmpty()
            }
            tvAmmountToPay.text = getString(R.string.literal_departure_ammount_to_pay, "0.00")

            radioGroup.setOnCheckedChangeListener { _, i ->

                requestCertificate = false
                btnRequest.text = getString(R.string.literal_departure_calculate)
                if (rvSeats.isNotEmpty()) {
                    val adapter = rvSeats.adapter as SeatDetailAdapter
                    adapter.clearItems(i == R.id.rb_all_pages)

                    when (i) {
                        R.id.rb_all_pages -> {
                            isSelectAll = true
                            nuAsieSelect = "TODAS"
                            imPagiSIR = "TODAS"
                            nuSecuSIR = ""
                            countSelectList = adapter.getItemCount()
                            adapter.isEnableCheckBox = false

                            if (adapter.items.first().pageNumber.isNotEmpty()) {
                                cantPaginas =
                                    adapter.items.filter { it.exonerado2 == "No" && it.enable }
                                        .sumOf { it.pageNumber.toInt() }

                                cantPaginasExon =
                                    adapter.items.filter { it.exonerado2 != "No" && it.enable }
                                        .sumOf { it.pageNumber.toInt() }
                                isVEHOrRMC = true
                            } else {
                                cantPaginas =
                                    adapter.items.filter { it.flagExonerado == "No" && it.enable }
                                        .sumOf { it.nuPaginasCantidad.toInt() }

                                cantPaginasExon =
                                    adapter.items.filter { it.flagExonerado != "No" && it.enable }
                                        .sumOf { it.nuPaginasCantidad.toInt() }
                            }
                            Log.e("Paginas", cantPaginas.toString())
                            Log.e("Paginas Exoneradas", cantPaginasExon.toString())

                            if (countSelectList > 0) {
                                btnRequest.isEnabled = true;
                            }
                        }

                        R.id.rb_unselect -> {
                            isSelectAll = false
                            adapter.isEnableCheckBox = true
                            countSelectList = 0
                            cantPaginas = 0
                            cantPaginasExon = 0
                            nuAsieSelect = ""
                            imPagiSIR = ""
                            Log.e("Paginas", cantPaginas.toString())
                            Log.e("Paginas Exoneradas", cantPaginasExon.toString())
                            btnRequest.isEnabled = false;
                            tvAmmountToPay.text =
                                getString(R.string.literal_departure_ammount_to_pay, "0.00")
                        }
                    }
                }
            }


            val rvSeats: RecyclerView = view.findViewById(R.id.rv_seats)
            //if(rvSeats.isNotEmpty()) {
                rvSeats.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
                    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                        val adapter = rvSeats.adapter as SeatDetailAdapter
                        when (e.action) {
                            MotionEvent.ACTION_UP -> {
                                adapter.setOnSeatsPressed(object :
                                    SeatDetailAdapter.OnSeatCheckedChangeListener {
                                    override fun onSeatCheckedChanged(
                                        position: Int,
                                        isChecked: Boolean
                                    ) {
                                        if (adapter.items.first().pageNumber.isNotEmpty()) {
                                            cantPaginas =
                                                adapter.items.filter { it.exonerado2 == "No" && it.enable }
                                                    .sumOf { it.pageNumber.toInt() }

                                            cantPaginasExon =
                                                adapter.items.filter { it.exonerado2 != "No" && it.enable }
                                                    .sumOf { it.pageNumber.toInt() }
                                            isVEHOrRMC = true

                                        } else {
                                            cantPaginas =
                                                adapter.items.filter { it.flagExonerado == "No" && it.enable }
                                                    .sumOf { it.nuPaginasCantidad.toInt() }

                                            cantPaginasExon =
                                                adapter.items.filter { it.flagExonerado != "No" && it.enable }
                                                    .sumOf { it.nuPaginasCantidad.toInt() }
                                        }

                                        Log.e("Paginas", cantPaginas.toString())
                                        Log.e("Paginas Exoneradas", cantPaginasExon.toString())

                                        val seat = adapter.items[position]

                                        // Si el asiento está marcado, lo agregamos a selectedSeatNumbers
                                        if (isChecked) {
                                            totalPaginasPartidaFicha += if (seat.pageNumber.isNotEmpty()) {
                                                nuAsieSelectList.add(seat.seatNumber)
                                                //imPagiSIRList.add(seat.pageNumber)
                                                //nuSecuSIRList.add(seat.pageNumber)
                                                seat.pageNumber.toInt()
                                                isVEHOrRMC = true

                                            } else {
                                                nuAsieSelectList.add(seat.detalle)
                                                imPagiSIRList.add(seat.paginas)
                                                nuSecuSIRList.add(seat.nuSecu )
                                                seat.nuPaginasCantidad.toInt()
                                            }


                                        } else {
                                            // Si el asiento está desmarcado, lo removemos de selectedSeatNumbers
                                            totalPaginasPartidaFicha -= if (seat.pageNumber.isNotEmpty()) {
                                                nuAsieSelectList.remove(seat.seatNumber)
                                                //imPagiSIRList.remove(seat.pageNumber)
                                                //nuSecuSIRList.remove(seat.pageNumber)
                                                seat.pageNumber.toInt()
                                            } else {
                                                nuAsieSelectList.remove(seat.detalle)
                                                imPagiSIRList.remove(seat.paginas)
                                                nuSecuSIRList.remove(seat.nuSecu)
                                                seat.nuPaginasCantidad.toInt()
                                            }

                                        }

                                        // Aquí tienes la lista de los números de asiento seleccionados
                                        Log.e("Asientos Seleccionados", nuAsieSelectList.toString())
                                        Log.e("imPagiSIR", imPagiSIRList.toString())
                                        Log.e("nuSecuSIR", nuSecuSIRList.toString())


                                        if (countSelectList >= 0) {
                                            if (isChecked) {
                                                countSelectList++;
                                            } else {
                                                countSelectList--;
                                            }
                                        }

                                        if (countSelectList > 0) {
                                            btnRequest.isEnabled = true;
                                        } else {
                                            countSelectList = 0;
                                            btnRequest.isEnabled = false;
                                            tvAmmountToPay.text = getString(
                                                R.string.literal_departure_ammount_to_pay,
                                                "0.00"
                                            )
                                        }
                                    }
                                })

                            }
                        }

                        return false
                    }

                    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
                        // No es necesario implementar nada aquí
                    }

                    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
                        // No es necesario implementar nada aquí
                    }
                })
            //}

            btnRequest.setOnClickListener {
                val adapter = rvSeats.adapter as SeatDetailAdapter
                if (isVEHOrRMC) {
                    totalPaginasPartidaFicha = adapter.items.map { it.pageNumber.toIntOrNull() ?: 0 }.sum()
                } else {
                    totalPaginasPartidaFicha = adapter.items.map { it.nuPaginasCantidad.toIntOrNull() ?: 0 }.sum()
                }

                Log.e("TotPagPartFicha", totalPaginasPartidaFicha.toString())
                if (!requestCertificate) {
                    if ( cantPaginas > 0 ) {
                        pagoExon = false
                        viewModel.getPaymentValue(
                            certificateService?.id.orEmpty(),
                            "$cantPaginas"
                        )
                    } else if ( cantPaginasExon > 0 ){
                        viewModel.getPaymentValue(
                            certificateService?.id.orEmpty(),
                            "0"
                        )
                        pagoExon = true
                        tvAmmountToPay.text =
                            getString(R.string.literal_departure_ammount_to_pay, "0.00")
                    }

                } else {
                    if (pagoExon) {
                        AlertDialog.Builder(requireContext()).apply {
                            setTitle("")
                            setMessage(("Estimado usuario, se va a proceder a generar una solicitud de certificado literal de los asientos exonerados seleccionados el cual no tiene costo"))
                            setCancelable(false)
                            setNegativeButton("CANCELAR") { _, _ ->

                            }
                            setPositiveButton("ACEPTAR") { _, _ ->
                                findNavController().navigate(
                                    R.id.action_seatDetailFragment_to_certificatePaymentFragment,
                                    bundleOf(
                                        "certificate" to certificate,
                                        "ammount" to valueToPay,
                                        "literalCertificate" to literalCertificate,
                                        "numSeats" to adapter.getNumSeats(),
                                        "certificateService" to certificateService,
                                        "office" to office,
                                        "numSeatsSelected" to nuAsieSelect,
                                        "numSeatsSelectedSARP" to nuAsieSelectSARP,
                                        "imPagiSIR" to imPagiSIR,
                                        "nuSecuSIR" to nuSecuSIR,
                                        "totalPaginasPaginasPartidaFicha" to totalPaginasPartidaFicha,
                                        "cantPaginas" to cantPaginas,
                                        "cantPaginasExon" to cantPaginasExon,
                                        "paginasSolicitadas" to paginasSolicitadas,
                                        "codigoGla" to codigoGla

                                    )
                                )
                            }
                            create().show()
                        }

                    } else {
                        if (isSelectAll) {
                            nuAsieSelect = "TODAS"
                            imPagiSIR = "TODAS"
                            nuSecuSIR = ""
                        }

                        findNavController().navigate(
                            R.id.action_seatDetailFragment_to_certificatePaymentFragment,
                            bundleOf(
                                "certificate" to certificate,
                                "ammount" to valueToPay,
                                "literalCertificate" to literalCertificate,
                                "numSeats" to adapter.getNumSeats(),
                                "certificateService" to certificateService,
                                "office" to office,
                                "numSeatsSelected" to nuAsieSelect,
                                "numSeatsSelectedSARP" to nuAsieSelectSARP,
                                "imPagiSIR" to imPagiSIR,
                                "nuSecuSIR" to nuSecuSIR,
                                "totalPaginasPaginasPartidaFicha" to totalPaginasPartidaFicha,
                                "cantPaginas" to cantPaginas,
                                "cantPaginasExon" to cantPaginasExon,
                                "paginasSolicitadas" to paginasSolicitadas,
                                "codigoGla" to codigoGla
                            )
                        )
                    }
                }
            }
        }

    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            requestData()
            onChange(seatDetail) { fillSeatsDetail(it) }
            onChange(onPaymentValue) { fillPayment(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun fillPayment(value: Double) {
        with(binding) {
            requestCertificate = true
            valueToPay = value
            nuAsieSelect = nuAsieSelectList.joinToString(",")
            imPagiSIR = imPagiSIRList.joinToString(",")
            nuSecuSIR = nuSecuSIRList.joinToString(",")
            paginasSolicitadas = cantPaginas+ cantPaginasExon
            //totalPaginasPartidaFicha = paginasSolicitadas
            if (numDocumento?.startsWith("P", ignoreCase = true) == true) {
                nuAsieSelectSARP = nuAsieSelect
                imPagiSIR = ""
                nuSecuSIR = ""
            } else {
                if (isVEHOrRMC) {
                    nuAsieSelectSARP = nuAsieSelect
                    imPagiSIR = ""
                    nuSecuSIR = ""
                } else {
                    nuAsieSelectSARP = ""
                }

            }

            btnRequest.text = getString(R.string.literal_departure_request)
            tvAmmountToPay.text =
                getString(
                    R.string.literal_departure_ammount_to_pay,
                    value.formatMoneySunarpCustom()
                ).formatEndText()
        }
    }

    fun Double?.formatMoneySunarpCustom(): String {
        return String.format("%.2f", this)
    }

    private fun requestData() {
        if (certificateService?.id.orEmpty() == Constants.SERVICE_VEH_CODE && certificateService?.registrationTypeId.orEmpty() == Constants.SERVICE_VEH_REG_TYPE) {
            Log.e("Debug", "option 01")
            Log.e("Debug", certificateService?.id.orEmpty())
            literalCertificate?.let {
                viewModel.getSeatsDetailVEH(
                    it.zoneCode,
                    it.officeCode,
                    it.certNumber,
                    it.plateNumber
                )
            }
        } else if (certificateService?.id.orEmpty() == Constants.SERVICE_RMC_CODE && certificateService?.registrationTypeId.orEmpty() == Constants.SERVICE_RMC_REG_TYPE) {
            Log.e("Debug", "option 02")
            Log.e("Debug", certificateService?.id.orEmpty())
            literalCertificate?.let {
                viewModel.getSeatsDetailRMC(
                    it.zoneCode,
                    it.officeCode,
                    it.certNumber
                )
            }
        } else {
            literalCertificate?.let {
                Log.e("Debug", "option 03")
                Log.e("Debug", certificateService?.id.orEmpty())
                viewModel.getSeatsDetail(
                    it.zoneCode,
                    it.officeCode,
                    it.codLibro,
                    it.certNumber,
                    it.fichaId,
                    it.tomoId,
                    it.fojaId,
                    "",
                    certificateService?.id.orEmpty(),
                    certificateService?.registrationTypeId.orEmpty()
                )
            }
        }
    }

    private fun fillSeatsDetail(list: List<SeatDetail>) {
        with(binding) {
            val transactionsAdapter = SeatDetailAdapter(
                list.toMutableList(),
                legalRecord?.id == Constants.REGISTRO_DE_BIENES_MUEBLES,
                legalRecord?.id == Constants.REGISTRO_DE_PROPIEDAD,
                true
            )
            transactionsAdapter.setOnSeatsPressed { value: Boolean, pos: Int ->
                requestCertificate = false
                btnRequest.text = getString(R.string.literal_departure_calculate)
//                transactionsAdapter.items[pos].enable = value
//                transactionsAdapter.hasStableIds().let {
//                    if (it) {
//                        transactionsAdapter.notifyItemChanged(pos)
//                    }
//                }
            }

            rvSeats.apply {
                adapter = transactionsAdapter
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }
        }
    }

    private fun showLoading(show: Boolean) {
        with(binding) {
            loadingContainer.apply {
                if (show) {
//                    statusContainer.hide()
//                    paymentContainer.hide()
                    loading.show()

                } else {
//                    statusContainer.show()
//                    paymentContainer.show()
                    loading.hide()
                }
            }
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it) }
        }
    }

}

private operator fun Int.plusAssign(any: Any) {

}

private operator fun Boolean.invoke(function: () -> Unit) {

}
