package pe.gob.sunarp.appandroid.ui.register.send

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.finishActivity
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRegisterSendBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class RegisterSendFragment :
    BaseFragment<FragmentRegisterSendBinding>(FragmentRegisterSendBinding::inflate) {

    private val viewModel: RegisterSendViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        setListeners()
    }

    private fun setListeners() {
        with(binding) {
            actionRegisterSend.setOnClickListener {
                hideKeyboard()
                viewModel.send(textInputNumberPhone.editText?.text.toString())
            }
            actionRegisterBack.setOnClickListener {
                finishActivity()
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            isSent.observe(viewLifecycleOwner) { goToNextView(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun goToNextView(message: String?) {
        binding.container.snack(message)
        val bundle =
            bundleOf("numberPhone" to binding.textInputNumberPhone.editText?.text.toString())
        findNavController().navigate(
            R.id.action_RegisterSendFragment_to_RegisterVerifyFragment,
            bundle
        )
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}