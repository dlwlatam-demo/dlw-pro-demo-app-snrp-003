package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class VehicleDataDTO(
    @field:Json(name = "datosVehiculo")
    val image: String?,
    ) : BaseModel()