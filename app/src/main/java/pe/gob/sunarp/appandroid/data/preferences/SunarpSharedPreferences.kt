package pe.gob.sunarp.appandroid.data.preferences

import android.content.SharedPreferences
import pe.gob.sunarp.appandroid.core.getBoolean
import pe.gob.sunarp.appandroid.core.getString
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.core.model.User
import pe.gob.sunarp.appandroid.core.putBoolean
import pe.gob.sunarp.appandroid.core.putString
import pe.gob.sunarp.appandroid.data.alerta.modelservices.LoginResponse
import pe.gob.sunarp.appandroid.data.profile.information.dto.ProfileDTO
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SunarpSharedPreferences @Inject constructor(private val preferences: SharedPreferences) {
    companion object {
        const val KEY_ONBOARDING = "onBoarding"
        const val KEY_ACCESS_TOKEN = "accessToken"
        const val KEY_JTI = "jti"
        const val KEY_GUID = "guid"
        const val KEY_NAME = "name"
        const val KEY_FIRSTLASTNAME = "firstname"
        const val KEY_SECONDLASTNAME = "secondname"
        const val KEY_DOC_NUMBER = "document"
        const val KEY_DOC_TYPE = "documentType"
        const val KEY_EMAIL = "email"
        const val KEY_USER_ID = "userId"
        const val KEY_PHOTO = "photo"
        const val KEY_USER_KEY = "userKeyId"
        const val KEY_FEC_CREA = "createdAt"

        const val KEY_ALERTA_APMATE = "alerta_apMate"
        const val KEY_ALERTA_RUC = "alerta_ruc"
        const val KEY_ALERTA_IDRGST = "alerta_idRgst"
        const val KEY_ALERTA_CNUMCEL = "alerta_cnumCel"
        const val KEY_ALERTA_APPATE = "alerta_apPate"
        const val KEY_ALERTA_TIDOCU = "alerta_tiDocu"
        const val KEY_ALERTA_NAME_TIDOCU = "alerta_name_tiDocu"
        const val KEY_ALERTA_DIRECCION = "alerta_direccion"
        const val KEY_ALERTA_RZSOCL = "alerta_rzSocl"
        const val KEY_ALERTA_IDOPERTELE = "alerta_idOperTele"
        const val KEY_ALERTA_NOMBRE = "alerta_nombre"
        const val KEY_ALERTA_NODOCU = "alerta_noDocu"
        const val KEY_ALERTA_CONOTI = "alerta_coNoti"
        const val KEY_ALERTA_TIPERS = "alerta_tiPers"
        const val KEY_ALERTA_INNOTI = "alerta_inNoti"
        const val KEY_ALERTA_FLENVIOSMS = "alerta_flEnvioSms"
        const val KEY_ALERTA_NUTELE = "alerta_nuTele"
        const val KEY_ALERTA_EMAIL = "alerta_email"
        const val KEY_ALERTA_CELNUMBER = "alerta_celNumber"
        const val KEY_ALERTA_LOGIN_PASS = "alerta_login_pass"
        const val KEY_ALERTA_LOGIN_EMAIL = "alerta_login_email"
        val Tipo_Documents = listOf(
            ItemSpinner("09", "Documento Nacional de Identidad"),
            ItemSpinner("03", "Carnet de Extranjería"),
            ItemSpinner("08", "Pasaporte"),
            ItemSpinner("50", "Documento Nacional Extranjero"),
        )
        const val KEY_PURCHASE_NUMBER = "purchaseNumber"
    }

    fun saveUserData(user: User) {
        this.accessToken = user.accessToken
        this.jti = user.jti
    }

    fun saveUserDataAlerta(user: LoginResponse) {
        this.alertApMate = user.apMate.orEmpty()
        this.alertRuc = user.ruc.orEmpty()
        this.alertIdRgst = user.idRgst.orEmpty()
        this.alertCnumCel = user.cnumCel.orEmpty()
        this.alertApPate = user.apPate.orEmpty()
        this.alertTiDocu = user.tiDocu.orEmpty()
        this.alertNameTiDocu = Tipo_Documents.find{ user.tiDocu == it.id}?.name ?: ""
        this.alertDireccion = user.direccion.orEmpty()
        this.alertRzSocl = user.rzSocl.orEmpty()
        this.alertIdOperTele = user.idOperTele.orEmpty()
        this.alertNombre = user.nombre.orEmpty()
        this.alertNoDocu = user.noDocu.orEmpty()
        this.alertCoNoti = user.coNoti.orEmpty()
        this.alertTiPers = user.tiPers.orEmpty()
        this.alertInNoti = user.inNoti.orEmpty()
        this.alertFlEnvioSms = user.flEnvioSms.orEmpty()
        this.alertNuTele = user.nuTele.orEmpty()
        this.alertEmail = user.email.orEmpty()
        this.alertCelNumber = user.celNumber.orEmpty()
    }

    fun getUserDataAlerta() =
        LoginResponse(
            this.alertApMate,
            this.alertRuc,
            this.alertIdRgst,
            this.alertCnumCel,
            this.alertApPate,
            this.alertTiDocu,
            this.alertDireccion,
            this.alertRzSocl,
            this.alertIdOperTele,
            this.alertNombre,
            this.alertNoDocu,
            this.alertCoNoti,
            this.alertTiPers,
            this.alertInNoti,
            this.alertFlEnvioSms,
            this.alertNuTele,
            this.alertEmail,
            this.alertCelNumber,
            this.alertNameTiDocu
        )

    fun isLoginAlert() = alertEmail.isNotEmpty()

    fun saveUserProfile(user: ProfileDTO) {
        this.userName = user.userData?.names.orEmpty()
        this.userFirstLastname = user.userData?.firstLastname.orEmpty()
        this.userSecondLastName = user.userData?.secondLastname.orEmpty()
        this.userPhoto = user.userData?.photo.orEmpty()
        this.userDocNumber = user.userData?.docNumber.orEmpty()
        this.userId = user.userData?.idUser.orEmpty()
        this.email = user.userData?.email.orEmpty()
        this.userDocType = user.userData?.docType.orEmpty()
        this.userKeyId = user.userData?.userKeyId.orEmpty()
        this.createdAt = user.userData?.createdAt.orEmpty()
    }

    fun getPaymentInformation(ammount: String) =
        PaymentItem(
            this.userName,
            this.userFirstLastname,
            this.userSecondLastName,
            this.email,
            ammount,
            this.userDocNumber
        )


    var onBoardingShowed: Boolean
        get() = preferences.getBoolean(KEY_ONBOARDING)
        set(value) = preferences.putBoolean(KEY_ONBOARDING, value)

    var accessToken: String
        get() = preferences.getString(KEY_ACCESS_TOKEN).orEmpty()
        set(value) = preferences.putString(KEY_ACCESS_TOKEN, value)

    var alertaLoginEmail: String
        get() = preferences.getString(KEY_ALERTA_LOGIN_EMAIL).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_LOGIN_EMAIL, value)

    var alertaLoginPass: String
        get() = preferences.getString(KEY_ALERTA_LOGIN_PASS).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_LOGIN_PASS, value)

    var jti: String
        get() = preferences.getString(KEY_JTI).orEmpty()
        set(value) = preferences.putString(KEY_JTI, value)

    var guid: String?
        get() = preferences.getString(KEY_GUID)
        set(value) = preferences.putString(KEY_GUID, value)

    var userName: String
        get() = preferences.getString(KEY_NAME).orEmpty()
        set(value) = preferences.putString(KEY_NAME, value)

    var userFirstLastname: String
        get() = preferences.getString(KEY_FIRSTLASTNAME).orEmpty()
        set(value) = preferences.putString(KEY_FIRSTLASTNAME, value)

    var userSecondLastName: String
        get() = preferences.getString(KEY_SECONDLASTNAME).orEmpty()
        set(value) = preferences.putString(KEY_SECONDLASTNAME, value)

    var email: String
        get() = preferences.getString(KEY_EMAIL).orEmpty()
        set(value) = preferences.putString(KEY_EMAIL, value)

    var userPhoto: String
        get() = preferences.getString(KEY_PHOTO).orEmpty()
        set(value) = preferences.putString(KEY_PHOTO, value)

    var userDocNumber: String
        get() = preferences.getString(KEY_DOC_NUMBER).orEmpty()
        set(value) = preferences.putString(KEY_DOC_NUMBER, value)

    var userDocType: String
        get() = preferences.getString(KEY_DOC_TYPE).orEmpty()
        set(value) = preferences.putString(KEY_DOC_TYPE, value)

    var userId: String
        get() = preferences.getString(KEY_USER_ID).orEmpty()
        set(value) = preferences.putString(KEY_USER_ID, value)

    var alertApMate: String
        get() = preferences.getString(KEY_ALERTA_APMATE).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_APMATE, value)
    var alertRuc: String
        get() = preferences.getString(KEY_ALERTA_RUC).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_RUC, value)
    var alertIdRgst: String
        get() = preferences.getString(KEY_ALERTA_IDRGST).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_IDRGST, value)
    var alertCnumCel: String
        get() = preferences.getString(KEY_ALERTA_CNUMCEL).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_CNUMCEL, value)
    var alertApPate: String
        get() = preferences.getString(KEY_ALERTA_APPATE).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_APPATE, value)
    var alertTiDocu: String
        get() = preferences.getString(KEY_ALERTA_TIDOCU).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_TIDOCU, value)
    var alertNameTiDocu: String
        get() = preferences.getString(KEY_ALERTA_NAME_TIDOCU).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_NAME_TIDOCU, value)
    var alertDireccion: String
        get() = preferences.getString(KEY_ALERTA_DIRECCION).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_DIRECCION, value)
    var alertRzSocl: String
        get() = preferences.getString(KEY_ALERTA_RZSOCL).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_RZSOCL, value)
    var alertIdOperTele: String
        get() = preferences.getString(KEY_ALERTA_IDOPERTELE).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_IDOPERTELE, value)
    var alertNombre: String
        get() = preferences.getString(KEY_ALERTA_NOMBRE).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_NOMBRE, value)
    var alertNoDocu: String
        get() = preferences.getString(KEY_ALERTA_NODOCU).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_NODOCU, value)
    var alertCoNoti: String
        get() = preferences.getString(KEY_ALERTA_CONOTI).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_CONOTI, value)
    var alertTiPers: String
        get() = preferences.getString(KEY_ALERTA_TIPERS).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_TIPERS, value)
    var alertInNoti: String
        get() = preferences.getString(KEY_ALERTA_INNOTI).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_INNOTI, value)
    var alertFlEnvioSms: String
        get() = preferences.getString(KEY_ALERTA_FLENVIOSMS).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_FLENVIOSMS, value)
    var alertNuTele: String
        get() = preferences.getString(KEY_ALERTA_NUTELE).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_NUTELE, value)
    var alertEmail: String
        get() = preferences.getString(KEY_ALERTA_EMAIL).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_EMAIL, value)
    var alertCelNumber: String
        get() = preferences.getString(KEY_ALERTA_CELNUMBER).orEmpty()
        set(value) = preferences.putString(KEY_ALERTA_CELNUMBER, value)
    var userKeyId: String
        get() = preferences.getString(KEY_USER_KEY).orEmpty()
        set(value) = preferences.putString(KEY_USER_KEY, value)
    var createdAt: String
        get() = preferences.getString(KEY_FEC_CREA).orEmpty()
        set(value) = preferences.putString(KEY_FEC_CREA, value)

    var purchaseNumber: String
        get() = preferences.getString(KEY_PURCHASE_NUMBER).orEmpty()
        set(value) = preferences.putString(KEY_PURCHASE_NUMBER, value)

    fun logoutAlert() {
        saveUserDataAlerta(LoginResponse())
    }
}
