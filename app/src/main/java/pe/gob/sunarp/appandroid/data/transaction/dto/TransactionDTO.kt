package pe.gob.sunarp.appandroid.data.transaction.dto

import android.content.Context
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.formatDateGetYear
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.ui.transactions.history.model.Transaction

@JsonClass(generateAdapter = true)
data class TransactionDTO(
    @field:Json(name = "tipo")
    val type: String?,
    @field:Json(name = "transId")
    val id: Int?,
    @field:Json(name = "fecHor")
    val date: String?,
    @field:Json(name = "servicioId")
    val serviceId: Int?,
    @field:Json(name = "cuentaId")
    val accountId: Int?,
    @field:Json(name = "strBusq")
    val serviceLabel: String?,
    @field:Json(name = "costo")
    val ammount: Double?,
    @field:Json(name = "key")
    val key: Int?,
    @field:Json(name = "aaPubl")
    val aaPubl: String?,
    @field:Json(name = "nuPubl")
    val nuPubl: String?
) : BaseModel() {

    private fun getNroPubAndYear(): String {
        return if (nuPubl!= null && nuPubl.isNotBlank()) {
            "$nuPubl - ${date?.formatDateGetYear()}"
        } else "-"
    }

    fun toTransaction(context: Context): Transaction {
        val title = when (type) {
            TransactionType.ADVERTISING.type -> context.getString(R.string.informative_ticket)
            TransactionType.SEARCH_BY_NAME.type -> context.getString(R.string.search_by_name)
            TransactionType.INFORMATIVE_TICKET.type -> context.getString(R.string.compendious_advertising)
            TransactionType.INF_ADVERTISING.type -> context.getString(R.string.vehicular_search_info_adv)
            else -> ""
        }
        val color = when (type) {
            TransactionType.ADVERTISING.type -> R.color.teal_blue
            TransactionType.SEARCH_BY_NAME.type -> R.color.history_grey
            else -> R.color.history_orange
        }
        val buttonText = when (type) {
            TransactionType.ADVERTISING.type -> context.getString(R.string.view_ticket)
            TransactionType.INFORMATIVE_TICKET.type -> context.getString(R.string.view_request)
            else -> ""
        }
        return Transaction(
            title,
            id,
            serviceLabel,
            getNroPubAndYear(),
            date, key,
            ammount,
            type,
            color,
            buttonText
        )
    }
}

enum class TransactionType(val type: String) {
    ADVERTISING("1"),
    SEARCH_BY_NAME("2"),
    INFORMATIVE_TICKET("3"),
    SEARCH_DATA_BY_NAME("4"),
    INF_ADVERTISING("5")

}