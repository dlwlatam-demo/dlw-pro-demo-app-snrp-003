package pe.gob.sunarp.appandroid.ui.alerta.home

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.ActivityAlertaHomeBinding


@AndroidEntryPoint
class AlertaHomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAlertaHomeBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModel: AlertaHomeViewModel by viewModels()
    private lateinit var navController: NavController
    private lateinit var navHostFragment: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAlertaHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupViewModel()
        setToolbarTitle("Alerta Registral")
    }

    private fun setupViewModel() {
        with(viewModel) {
            setupNavigators()
            logOutSuccess.observe(this@AlertaHomeActivity) {
                navController.navigate(R.id.action_to_HomeApp)
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }

    private val listener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
                supportActionBar?.hide()
                binding.backToolbar.title = destination.label
                binding.appBackLayout.show()
        }

    fun setToolbarTitle(title: String) {
        binding.toolbar.title = title
        setTitle(title)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun setupNavigators() {
        val navView: BottomNavigationView = binding.navView

        navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.navController

        appBarConfiguration = AppBarConfiguration(navController.graph)

        binding.btnBack.setOnClickListener {
            validate(navController)
            navController.navigateUp()
        }

        navView.setupWithNavController(navController)
        navView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.nav_logout -> {
                    viewModel.logOut()
                    navController.navigate(R.id.action_to_HomeApp)
                    finish()
                    true
                }
                R.id.nav_alerta_home -> {
                    navController.navigate(R.id.action_alert_to_home)
                    true
                }
                else -> true
            }
        }
    }

    private fun validate(navcontroller: NavController) {
        if (navcontroller.currentDestination?.id == R.id.nav_alerta_home) {
            viewModel.logOut()
            navController.navigate(R.id.action_to_HomeApp)
            finish()
        }
    }
}