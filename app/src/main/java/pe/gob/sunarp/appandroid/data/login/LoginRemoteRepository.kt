package pe.gob.sunarp.appandroid.data.login

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.authorization
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.User
import pe.gob.sunarp.appandroid.core.toBitmap
import pe.gob.sunarp.appandroid.data.login.passrecovery.PassCodeValidationRequest
import pe.gob.sunarp.appandroid.data.login.passrecovery.PassRecoveryRequest
import pe.gob.sunarp.appandroid.data.login.passrecovery.UpdatePasswordRequest
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject
import android.util.Log


class LoginRemoteRepository @Inject constructor(
    private val dataSource: LoginDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    suspend fun login(email: String, password: String): DataResult<User> = withContext(dispatcher) {
        when (val result = dataSource.login(
            email, password, "password",
            authorization(BuildConfig.SUNARP_AUTH_USER, BuildConfig.SUNARP_AUTH_PASS),
            getHost(BuildConfig.SUNARP_OAUTH_HOST)
        )) {
            is DataResult.Success -> {
                preferences.saveUserData(result.data)
                DataResult.Success(result.data)
            }
            is DataResult.Failure -> {
                DataResult.Failure(Exception(result.exception.message ?: ""))
            }
        }
    }

    fun getUserPhoto() = preferences.userPhoto.toBitmap()
    fun getUserName() = with(preferences){"$userName $userFirstLastname $userSecondLastName"}
    fun getUserDoc() = preferences.userDocNumber

    suspend fun logout(): DataResult<Boolean> = withContext(dispatcher) {
        when (val result =
            dataSource.logout(preferences.jti, getHost(BuildConfig.SUNARP_OAUTH_HOST))) {
            is DataResult.Success -> {
                DataResult.Success(true)
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun requestRecoveryCode(email: String): DataResult<String?> = withContext(dispatcher) {
        when (val result = dataSource.requestRecoveryCode(
            PassRecoveryRequest(email),
            getHost(BuildConfig.SUNARP_MTO_HOST)
        )) {
            is DataResult.Success -> {
                if (result.data.responseId == "1") {
                    result.data.guid?.let { preferences.guid = it }
                    DataResult.Success(result.data.description)
                } else {
                    DataResult.Failure(Exception(result.data.description ?: ""))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun validateRecoveryCode(code: String): DataResult<String?> = withContext(dispatcher) {
        when (val result =
            dataSource.validateRecoveryCode(
                PassCodeValidationRequest(preferences.guid, code),
                getHost(BuildConfig.SUNARP_MTO_HOST)
            )) {
            is DataResult.Success -> {
                if (result.data.responseId == "1") {
                    result.data.guid?.let { preferences.guid = it }
                    DataResult.Success(result.data.description)
                } else {
                    DataResult.Failure(Exception(result.data.description))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun updatePassword(password: String): DataResult<String?> = withContext(dispatcher) {
        when (val result =
            dataSource.updatePassword(
                UpdatePasswordRequest(preferences.guid, password),
                getHost(BuildConfig.SUNARP_MTO_HOST)
            )) {
            is DataResult.Success -> {
                if (result.data.responseId == "1") {
                    DataResult.Success(result.data.description)
                } else {
                    DataResult.Failure(Exception(result.data.description))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun getLastVersionApp(): DataResult<Boolean> = withContext(dispatcher) {
        val timestamp = System.currentTimeMillis()
        Log.e("getLastVersionApp", BuildConfig.FLAVOR)
        when (val result =
            if(BuildConfig.FLAVOR == "gms")
                dataSource.getLastVersionAppGms(getHost(BuildConfig.SUNARP_OAUTH_HOST),timestamp)
            else
                dataSource.getLastVersionAppHms(getHost(BuildConfig.SUNARP_OAUTH_HOST),timestamp) ) {
            is DataResult.Success -> {
                Log.i("getLastVersionAppValMS", result.data.response)
                DataResult.Success(result.data.response.contains(BuildConfig.VERSION_NAME))
            }
            is DataResult.Failure -> {
                Log.e("getLastVersionApp-Err", result.exception.message.toString())
                DataResult.Failure(result.exception)
            }
        }
    }



}