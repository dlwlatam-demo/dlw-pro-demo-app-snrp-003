package pe.gob.sunarp.appandroid.core.custom.ui.otp.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.getCompatColor

internal class SUNDigitTextView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : AppCompatTextView(context, attrs, defStyleAttr) {

    init {
        gravity = Gravity.CENTER
        TextViewCompat.setTextAppearance(this, R.style.sun_otp_text_cell)
        editableState()
    }

    fun editableState() {
        setTextColor(context.getCompatColor(R.color.charcoal))
        background = ContextCompat.getDrawable(context, R.drawable.shape_sun_otp_enabled)
    }

    fun validatedState() {
        setTextColor(context.getCompatColor(R.color.brown_grey))
        background = ContextCompat.getDrawable(context, R.drawable.shape_sun_otp_enabled)
    }

    fun errorState() {
        setTextColor(context.getCompatColor(R.color.charcoal))
        background = ContextCompat.getDrawable(context, R.drawable.shape_sun_otp_error)
    }
}