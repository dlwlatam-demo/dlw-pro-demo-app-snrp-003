package pe.gob.sunarp.appandroid.data.payment

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PaymentRequest(
    @field:Json(name = "instancia")
    val instance: String,
    @field:Json(name = "accessAppKey")
    val accessKey: String,
)

@JsonClass(generateAdapter = true)
data class SavePaymentRequest(
    @field:Json(name = "solicitudId")
    val requestId: Int,
    @field:Json(name = "usuario")
    val user: String,
    @field:Json(name = "detalle")
    val detail: String,
    @field:Json(name = "monto")
    val amount: String,
    @field:Json(name = "visanetResponse")
    val niubizData: NiubizRequest,
)

@JsonClass(generateAdapter = true)
data class NiubizRequest(
    @field:Json(name = "codAccion")
    val codAccion: String,
    @field:Json(name = "codAutoriza")
    val codAutoriza: String,
    @field:Json(name = "codtienda")
    val codtienda: String,
    @field:Json(name = "concepto")
    val concepto: String,
    @field:Json(name = "decisionCs")
    val decisionCs: String,
    @field:Json(name = "dscCodAccion")
    val dscCodAccion: String,
    @field:Json(name = "dscEci")
    val dscEci: String,
    @field:Json(name = "eci")
    val eci: String,
    @field:Json(name = "estado")
    val estado: String,
    @field:Json(name = "eticket")
    val eticket: String,
    @field:Json(name = "fechaYhoraTx")
    val fechaYhoraTx: String,
    @field:Json(name = "idUnico")
    val idUnico: String,
    @field:Json(name = "idUser")
    val idUser: String,
    @field:Json(name = "impAutorizado")
    val impAutorizado: String,
    @field:Json(name = "nomEmisor")
    val nomEmisor: String,
    @field:Json(name = "numOrden")
    val numOrden: String,
    @field:Json(name = "numReferencia")
    val numReferencia: String,
    @field:Json(name = "oriTarjeta")
    val oriTarjeta: String,
    @field:Json(name = "pan")
    val pan: String,
    @field:Json(name = "resCvv2")
    val resCvv2: String,
    @field:Json(name = "respuesta")
    val response: String,
    @field:Json(name = "reviewTransaction")
    val reviewTransaction: String,
    @field:Json(name = "transId")
    val transId: String,
)