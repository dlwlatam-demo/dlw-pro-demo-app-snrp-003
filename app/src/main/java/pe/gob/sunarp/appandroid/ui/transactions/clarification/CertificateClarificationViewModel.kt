package pe.gob.sunarp.appandroid.ui.transactions.clarification

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateClarificationViewModel @Inject constructor(
    private val repository: TransactionRemoteRepository
) : BaseViewModel() {

    val onComplete = SingleLiveEvent<Boolean>()
    val loading = SingleLiveEvent<Boolean>()

    fun sendClarification(key: String, description: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.sendClarification(key, description)) {
                is DataResult.Success -> onSuccess()
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun sendRetrieval(key: String, description: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.sendRetrieval(key, description)) {
                is DataResult.Success -> onSuccess()
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onSuccess() {
        loading.value = false
        onComplete.value = true
    }

}