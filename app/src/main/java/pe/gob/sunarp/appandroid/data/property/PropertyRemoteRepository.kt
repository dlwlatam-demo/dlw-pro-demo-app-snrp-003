package pe.gob.sunarp.appandroid.data.property

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.DataModel
import pe.gob.sunarp.appandroid.core.model.UserInformation
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject

class PropertyRemoteRepository @Inject constructor(
    private val dataSource: PropertyDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getPropertiesList(
        user: UserInformation
    ): DataResult<List<DataModel>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getPropertiesInformation(
                    user.toRequest(),
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(validateList(result.data.toModel()))
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }

        }

    private fun validateList(items: List<DataModel>): List<DataModel> {
        val newList = arrayListOf<DataModel>(DataModel.NoDataAvailable)
        return items.ifEmpty { newList }
    }
}