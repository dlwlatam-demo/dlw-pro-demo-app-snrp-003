package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AddPartidaRequest(

	@Json(name="nuPart")
	val nuPart: String? = null,

	@Json(name="coArea")
	val coArea: String? = null,

	@Json(name="coRegi")
	val coRegi: String? = null,

	@Json(name="nuPartSarp")
	val nuPartSarp: String? = null,

	@Json(name="numPlaca")
	val numPlaca: String? = null,

	@Json(name="coLibr")
	val coLibr: String? = null,

	@Json(name="coSede")
	val coSede: String? = null
)
