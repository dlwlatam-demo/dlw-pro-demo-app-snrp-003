package pe.gob.sunarp.appandroid.ui.transactions.status

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.UserInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.transaction.dto.Flags
import pe.gob.sunarp.appandroid.data.transaction.dto.Status
import pe.gob.sunarp.appandroid.databinding.FragmentRequestStatusBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.DropProcessDialog
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.status.model.TransactionStatus

@AndroidEntryPoint
class TransactionStatusFragment :
    BaseFragment<FragmentRequestStatusBinding>(FragmentRequestStatusBinding::inflate) {
    private val viewModel: TransactionStatusViewModel by viewModels()
    private var transId: Int? = null
    private var transType: String? = null
    private val certId = "6970717273"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        binding.apply {
            retrieval.tvOption.text = getString(R.string.retrieval)
            retrieval.ivIcon.setBackgroundResource(R.drawable.ic_retrieval)
            retrieval.itemContainer.setOnClickListener {
                findNavController().navigate(
                    R.id.action_transactionStatusFragment_to_certificateClarificationFragment,
                    bundleOf("transId" to transId, "type" to "SUB")
                )
            }

            certClarification.tvOption.text = getString(R.string.certificate_clarification)
            certClarification.ivIcon.setBackgroundResource(R.drawable.ic_certificate)
            certClarification.itemContainer.setOnClickListener {
                findNavController().navigate(
                    R.id.action_transactionStatusFragment_to_certificateClarificationFragment,
                    bundleOf("transId" to transId, "type" to "ACL")
                )
            }

            seeDocument.tvOption.text = getString(R.string.view_document)
            seeDocument.ivIcon.setBackgroundResource(R.drawable.ic_document)
            seeDocument.itemContainer.setOnClickListener {
                findNavController().navigate(
                    R.id.action_transactionStatusFragment_to_pdfViewerFragment,
                    bundleOf("transId" to transId, "id" to "DOC")
                )
            }

            giveUpProcess.tvOption.text = getString(R.string.giveup_process)
            giveUpProcess.ivIcon.setBackgroundResource(R.drawable.ic_giveup_process)
            giveUpProcess.itemContainer.setOnClickListener {
                activity?.let {
                    val dropProcessDilog = DropProcessDialog()
                    dropProcessDilog.apply {
                        onCancelListener {
                            this.dismiss()
                        }
                        onDropListener {
                            this.dismiss()
                            viewModel.dropProcess("$transId")
                        }
                        show(it.supportFragmentManager, "drop")
                    }

                }
            }

            payment.tvOption.text = getString(R.string.payment)
            payment.ivIcon.setBackgroundResource(R.drawable.ic_dolar)

        }
    }

    private fun setTransactionData(data: TransactionStatus) {
        binding.apply {
            statusInformation.setStatus(data.status)
            statusInformation.setData(data.information)
            if (data.solicitudId != "") {
                transId = data.solicitudId?.toInt()
            }
            if (!data.userKeyId.isNullOrEmpty()) {
                if (data.userKeyId == viewModel.userKeyId) {

                    retrieval.itemContainer.visibility = retrievalVisibility(data.flags)
                    certClarification.itemContainer.visibility =
                        certificationVisibility(data.certificateId, data.statusData, data.flags)
                    seeDocument.itemContainer.visibility = seeDocumentVisibility(data.statusData)
                    giveUpProcess.itemContainer.visibility =
                        giveUpProcessVisibility(data.statusData, data.flags)
                    payment.itemContainer.visibility =
                        paymentVisibility(data.ammountToPay, data.flags)
                    payment.itemContainer.setOnClickListener {
                        findNavController().navigate(
                            R.id.action_transactionStatusFragment_to_paymentFragment,
                            bundleOf(
                                "transId" to transId,
                                "transType" to transType,
                                "transDesc" to data.certificateType,
                                "paymentAmount" to data.ammountToPay,
                                "page" to Page.TRANSACTIONS
                            )
                        )
                    }
                }
            }

        }
    }

    private fun retrievalVisibility(flags: Flags?): Int {
        val visibility =
            flags?.flgObs.equals("1") && (flags?.flgDesis.equals("0") || flags?.flgDesis == null)
        return if (visibility) View.VISIBLE else View.GONE
    }

    private fun certificationVisibility(
        certificateId: String?,
        status: Status?,
        flags: Flags?
    ): Int {
        val visibility = !certId.contains(certificateId ?: "") &&
                status?.statusCode.equals("D") && (flags?.flgAban.equals("0") || flags?.flgAban == null)

        return if (visibility) View.VISIBLE else View.GONE
    }

    private fun seeDocumentVisibility(status: Status?): Int {
        val visibility =
            status?.statusCode.equals("D") || status?.statusCode.equals("L")

        return if (visibility) View.VISIBLE else View.GONE
    }

    private fun giveUpProcessVisibility(status: Status?, flags: Flags?): Int {
        val visibility =
            status?.statusCode.equals("C") && !flags?.flgAban.equals("1") && !flags?.flgDesis.equals(
                "1"
            )

        return if (visibility) View.VISIBLE else View.GONE
    }

    private fun paymentVisibility(ammount: String, flags: Flags?): Int {
        val ammountToTransfer = if (ammount.isNotEmpty()) ammount.toDouble() else 0.0
        val visibility =
            flags?.flgLiq.equals("1") && flags?.flgStatus.equals("1")
                    || ammountToTransfer > 0 && (flags?.flgDesis.equals("0") || flags?.flgAban == null)

        return if (visibility) View.VISIBLE else View.GONE
    }

    private fun initViewModels() {
        transId = arguments?.getInt("transId")
        transType = arguments?.getString("type")
        val year = arguments?.getString("year").orEmpty()
        with(viewModel) {
            initViewModelsErrorToken()
            getTransactionStatus("$transId",year)
            onChange(transaction) { item -> setTransactionData(item) }
            onChange(loading) { show -> showLoading(show) }
            onChange(onDropComplete) { onDropEnded() }
        }
    }

    private fun onDropEnded() {
        toast("La solicitud se desistió correctamente")
        findNavController().navigateUp()
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}