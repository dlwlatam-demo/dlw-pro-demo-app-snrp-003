package pe.gob.sunarp.appandroid.ui.welcome.onboarding

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import javax.inject.Inject

@HiltViewModel
class OnBoardingViewModel @Inject constructor(
    private val repository: SharedPreferenceRepository
) : ViewModel() {

    val onBoardingFinished = SingleLiveEvent<Boolean>()

    fun skipOnBoarding() {
        viewModelScope.launch {
            repository.setOnboardingPreference(true)
            onBoardingFinished.value = true
        }
    }
}