package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TiveDocument(
    val documento: String
): Parcelable {
    override fun toString(): String {
        return documento
    }
}