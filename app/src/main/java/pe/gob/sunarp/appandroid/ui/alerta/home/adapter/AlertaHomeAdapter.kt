package pe.gob.sunarp.appandroid.ui.alerta.home.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaHomeItemBinding

class AlertaHomeAdapter(
    private val items: List<AlertaHomeItem>
) : RecyclerView.Adapter<AlertaHomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutAlertaHomeItemBinding =
            LayoutAlertaHomeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutAlertaHomeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AlertaHomeItem) = with(binding) {
            with(item) {
                homeItem.setCardBackgroundColor(Color.parseColor(color))
                imageViewItemIcon.setImageResource(icon)
                tvTitle.text = title
                tvSubtitle.text = subtitle
                homeItem.setOnClickListener {
                    item.navigation?.let {
                        itemView.findNavController().navigate(it)
                    }
                }
            }
        }
    }
}