package pe.gob.sunarp.appandroid.core.model

data class RegisterArea(
    val id: String,
    val name: String,
    val price: Float
) {
    override fun toString(): String {
        return name
    }
}