package pe.gob.sunarp.appandroid.ui.alerta.mandatos.detail

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.DetailMandatoResponse
import pe.gob.sunarp.appandroid.data.alerta.modelservices.MandatoItemResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaMandatoDetailViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val dataCorreo = SingleLiveEvent<List<DetailMandatoResponse>>()
    val dataSMS = SingleLiveEvent<List<DetailMandatoResponse>>()


    fun getData(mandato: MandatoItemResponse) {
        getDataServicesCorreo(mandato)
    }

    private fun getDataServicesCorreo(mandato: MandatoItemResponse) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getNotificationDetail(mandato, "EML", "MAN")) {
                is DataResult.Success -> onGetDataServicesCorreoSuccess(result.data, mandato)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onGetDataServicesCorreoSuccess(
        data: List<DetailMandatoResponse>,
        mandato: MandatoItemResponse
    ) {
        dataCorreo.value = data
        getDataServicesSMS(mandato)
    }

    private fun getDataServicesSMS(mandato: MandatoItemResponse) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getNotificationDetail(mandato, "SMS", "MAN")) {
                is DataResult.Success -> onGetDataServicesSMSSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onGetDataServicesSMSSuccess(data: List<DetailMandatoResponse>) {
        dataSMS.value = data
        loading.value = false
    }


    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}

