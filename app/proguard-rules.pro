# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keep class com.huawei.hianalytics.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}
-keepclassmembers class pe.gob.sunarp.appandroid.data.maps.request.** {
  <init>(...);
  <fields>;
}
-dontwarn org.jetbrains.annotations.**
-keep class kotlin.Metadata { *; }
-keep class com.cardinalcommerce.cardinalmobilesdk.**{*;}
-keep class com.lexisnexisrisk.threatmetrix.tmxprofilingmodule.**{*;}
-keep class com.lexisnexisrisk.threatmetrix.tmxprofilingconnections.**{*;}
-keep class lib.visanet.com.pe.visanetlib.**{*;}
-keepnames class androidx.navigation.fragment.NavHostFragment
-keepnames class androidx.fragment.app.FragmentContainerView
-keep class * extends androidx.fragment.app.Fragment{}

-keepnames class pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
-keepnames class pe.gob.sunarp.appandroid.core.model.ZoneRegisterInformation
-keepnames class pe.gob.sunarp.appandroid.core.model.CertificateType