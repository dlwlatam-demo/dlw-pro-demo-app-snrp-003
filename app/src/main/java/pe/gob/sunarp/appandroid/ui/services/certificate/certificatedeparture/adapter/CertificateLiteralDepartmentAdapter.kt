package pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.LiteralCertificate
import pe.gob.sunarp.appandroid.databinding.LayoutLiteralCertificateItemBinding

class CertificateLiteralDepartmentAdapter(
    val items: List<LiteralCertificate>
): RecyclerView.Adapter<CertificateLiteralDepartmentAdapter.ViewHolder>() {

    private var onSeatsPressed: ((id: LiteralCertificate) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutLiteralCertificateItemBinding =
            LayoutLiteralCertificateItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setOnSeatsPressed(listener: (id: LiteralCertificate) -> Unit) {
        this.onSeatsPressed = listener
    }

    inner class ViewHolder(private val binding: LayoutLiteralCertificateItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LiteralCertificate) = with(binding) {
            with(item) {
                registerOffice.tvTitle.text = "Oficina registral:"
                registerOffice.tvDescription.text = officeName
                certificate.tvTitle.text = "Partida:"
                certificate.tvDescription.text = certNumber
                ficha.tvTitle.text = "Ficha:"
                ficha.tvDescription.text = fichaId
                tomo.tvTitle.text = "Tomo:"
                tomo.tvDescription.text = tomoId
                folio.tvTitle.text = "Folio:"
                folio.tvDescription.text = fojaId
                addressItem.tvTitle.text = "Dirección:"
                addressItem.tvDescription.text = address
                registerArea.tvTitle.text = "Área registral:"
                registerArea.tvDescription.text = areaRegisDescription
                registerTo.tvTitle.text = "Registro de:"
                registerTo.tvDescription.text = libroDescription

                btnSeats.setOnClickListener {
                    onSeatsPressed?.invoke(item)
                }
            }
        }
    }

}