package pe.gob.sunarp.appandroid.data.home

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject

class HomeRemoteRepository @Inject constructor(
    private val dataSource: HomeDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    suspend fun sendComment(comment: String, image: String): DataResult<String?> =
        withContext(dispatcher) {
            when (val result = dataSource.sendComment(
                CommentRequest(comment, image, BuildConfig.SUNARP_APP_ID),
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
            )) {
                is DataResult.Success -> DataResult.Success(result.data.description)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }
}