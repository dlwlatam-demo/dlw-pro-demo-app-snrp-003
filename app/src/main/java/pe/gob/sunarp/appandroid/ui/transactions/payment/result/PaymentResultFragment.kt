package pe.gob.sunarp.appandroid.ui.transactions.payment.result

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.CertificateType
import pe.gob.sunarp.appandroid.databinding.FragmentPaymentResultBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.adapter.PaymentResultAdapter
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class PaymentResultFragment :
    BaseFragment<FragmentPaymentResultBinding>(FragmentPaymentResultBinding::inflate) {

    private var certificate: CertificateType? = null
    var data: PaymentResult? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }
    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }
    private fun initViews() {
        arguments?.let { arg ->
            data = PaymentResultFragmentArgs.fromBundle(arg).paymentResult
            val transId: String = PaymentResultFragmentArgs.fromBundle(arg).transId.orEmpty()
            val userInformation = PaymentResultFragmentArgs.fromBundle(arg).information
            val ok = PaymentResultFragmentArgs.fromBundle(arg).ok
            certificate = PaymentResultFragmentArgs.fromBundle(arg).certificate
            setTittle()

            val adapter = PaymentResultAdapter(data?.items ?: emptyList())
            binding.apply {
                if (ok) {
                    titleContainer.setBackgroundResource(R.color.booger)
                    ivIcon.setImageResource(R.drawable.ic_payment_success)
                    tvTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.booger))
                    tvDescription.hide()
                    btnSend.text = getString(R.string.finish_label)
                    btnSend.backgroundTintList = ContextCompat.getColorStateList(
                        requireContext(), R.color.primary_tint
                    )
                } else {
                    titleContainer.setBackgroundResource(R.color.red)
                    ivIcon.setImageResource(R.drawable.ic_payment_failed)
                    tvTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.red))
                    tvDescription.show()
                    btnSend.text = getString(R.string.back_label)
                    btnSend.backgroundTintList = ContextCompat.getColorStateList(
                        requireContext(), R.color.failed_tint
                    )
                }

                tvTitle.text = data?.title.orEmpty()
                tvDescription.text = data?.description.orEmpty()

                if (data?.sourcePage == Page.TRANSACTIONS) {
                    btnSend.text =
                        if (ok) getString(R.string.finish_label) else getString(R.string.back_label)
                    btnSend.setOnClickListener {
                        findNavController().navigate(R.id.action_paymentResultFragment_to_transactionsHistoryFragment)
                    }
                } else if (data?.sourcePage == Page.VEHICLE_INFORMATION) {
                    if (ok) {
                        btnSend.text = getString(R.string.vehicular_search_see_receipt)
                        btnBack.show()
                        btnBack.text = getString(R.string.back_label)
                        btnSend.setOnClickListener {
                            findNavController().navigate(
                                R.id.action_paymentResultFragment_to_pdfViewerFragment2,
                                bundleOf("id" to "NW", "transId" to transId.toIntOrNull())
                            )
                        }
                        btnBack.setOnClickListener { findNavController().navigateUp() }
                    } else {
                        btnSend.text = getString(R.string.back_label)
                        btnSend.setOnClickListener { findNavController().navigateUp() }
                    }
                } else {
                    if (ok) {
                        btnSend.text = getString(R.string.search_by_name_see_date)
                        btnSend.setOnClickListener {
                            findNavController().navigate(
                                R.id.action_paymentResultFragment_to_certificateResultFragment,
                                bundleOf("userInformation" to userInformation)
                            )
                        }
                    } else {
                        btnSend.text = getString(R.string.back_label)
                        btnSend.setOnClickListener { findNavController().navigateUp() }
                    }

                }

                certificate?.let{
                    btnSend.text = getString(R.string.finish_label)
                    btnSend.setOnClickListener {
                        findNavController().navigate(R.id.action_paymentResultFragment_to_transactionsHistoryFragment)
                    }
                }

                rvContent.apply {
                    this.adapter = adapter
                    layoutManager = LinearLayoutManager(context)
                }

            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}