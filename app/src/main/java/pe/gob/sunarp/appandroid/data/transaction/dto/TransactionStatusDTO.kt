package pe.gob.sunarp.appandroid.data.transaction.dto

import android.content.Context
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group.StatusInfoContent
import pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group.StatusInfoGroup
import pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group.StatusInfoItem
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.ui.transactions.status.model.TransactionStatus

@JsonClass(generateAdapter = true)
data class TransactionStatusDTO(
    @field:Json(name = "solicitudId")
    val requestId: String?,
    @field:Json(name = "userKeyId")
    val userKeyId: String?,
    @field:Json(name = "nsolMonLiq")
    val ammountToTransfer: String?,
    @field:Json(name = "flags")
    val flags: Flags?,
    @field:Json(name = "estado")
    val status: Status?,
    @field:Json(name = "certificado")
    val certificate: CertificateDTO?,
    @field:Json(name = "solicitante")
    val applicant: ApplicantDTO?,
    @field:Json(name = "destinatario")
    val destination: DestinationDTO?,
    @field:Json(name = "pago")
    val payment: PaymentDTO?,

    ) : BaseModel() {
    fun toModel(context: Context): TransactionStatus {
        with(context) {
            val statusContent = ArrayList<StatusInfoItem>()
            statusContent.add(StatusInfoItem(getString(R.string.request_number), requestId))
            statusContent.add(
                StatusInfoItem(
                    getString(R.string.pub_number),
                    "${certificate?.pubYear.orEmpty()}-${certificate?.pubNumber.orEmpty()}"
                )
            )
            statusContent.add(
                StatusInfoItem(
                    getString(R.string.request_date),
                    payment?.date.orEmpty()
                )
            )
            statusContent.add(
                StatusInfoItem(
                    getString(R.string.certificate_type),
                    certificate?.type
                )
            )
            statusContent.add(
                StatusInfoItem(
                    getString(R.string.register_type),
                    certificate?.regisArea
                )
            )
            statusContent.add(StatusInfoItem(getString(R.string.office_label), certificate?.office))
            statusContent.add(StatusInfoItem(getString(R.string.seat_number), certificate?.seat))
            statusContent.add(
                StatusInfoItem(
                    getString(R.string.page_number),
                    certificate?.numPages
                )
            )
            val statusInformation =
                StatusInfoContent(getString(R.string.request_label), statusContent)

            val userInfo = ArrayList<StatusInfoItem>()
            userInfo.add(StatusInfoItem(getString(R.string.user_name_label), applicant?.name))
            userInfo.add(StatusInfoItem(getString(R.string.document_type), applicant?.docType))
            userInfo.add(StatusInfoItem(getString(R.string.document_number), applicant?.docNumber))
            val userInformation = StatusInfoContent(getString(R.string.applicant_number), userInfo)


            val destInfo = ArrayList<StatusInfoItem>()
            destInfo.add(StatusInfoItem(getString(R.string.user_name_label), destination?.name))
            destInfo.add(StatusInfoItem(getString(R.string.address_label), destination?.address))
            destInfo.add(StatusInfoItem(getString(R.string.description_label), destination?.type))
            val destInformation = StatusInfoContent(getString(R.string.destination_data), destInfo)

            val paymentInfo = ArrayList<StatusInfoItem>()
            paymentInfo.add(
                StatusInfoItem(
                    getString(R.string.ammount_label),
                    "S/ ${payment?.ammountQuantity.orEmpty()}"
                )
            )
            paymentInfo.add(StatusInfoItem(getString(R.string.date_label), payment?.date.orEmpty()))
            paymentInfo.add(
                StatusInfoItem(
                    getString(R.string.payment_label),
                    "S/ ${payment?.payment.orEmpty()}"
                )
            )
            paymentInfo.add(
                StatusInfoItem(
                    getString(R.string.payment_type_label),
                    payment?.type.orEmpty()
                )
            )
            val paymentInformation =
                StatusInfoContent(getString(R.string.payment_data), paymentInfo)

            val information = StatusInfoGroup(
                statusInformation,
                userInformation,
                destInformation,
                paymentInformation
            )

            return TransactionStatus(
                getString(R.string.status_label, status?.status),
                information,
                userKeyId,
                certificate?.id,
                ammountToTransfer ?: "0",
                flags,
                status,
                certificate?.type,
                requestId
            )
        }
    }
}

@JsonClass(generateAdapter = true)
data class Flags(
    @field:Json(name = "flgObs")
    val flgObs: String?,
    @field:Json(name = "flgEstado")
    val flgStatus: String?,
    @field:Json(name = "flgLiq")
    val flgLiq: String?,
    @field:Json(name = "flgTipoLiq")
    val flgLiqType: String?,
    @field:Json(name = "flgDesis")
    val flgDesis: String?,
    @field:Json(name = "flgDeneg")
    val flgDeneg: String?,
    @field:Json(name = "flgAban")
    val flgAban: String?,
    @field:Json(name = "flgAclar")
    val flgAclar: String?,
) : BaseModel()

@JsonClass(generateAdapter = true)
data class Status(
    @field:Json(name = "estado")
    val status: String?,
    @field:Json(name = "codEstado")
    val statusCode: String?
) : BaseModel()

@JsonClass(generateAdapter = true)
data class CertificateDTO(
    @field:Json(name = "certificadoId")
    val id: String?,
    @field:Json(name = "tpoCertificado")
    val type: String?,
    @field:Json(name = "aaPubl")
    val pubYear: String?,
    @field:Json(name = "nuPubl")
    val pubNumber: String?,
    @field:Json(name = "oficina")
    val office: String?,
    @field:Json(name = "codOficina")
    val officeCode: String?,
    @field:Json(name = "codZona")
    val zoneCode: String?,
    @field:Json(name = "numPartida")
    val number: String?,
    @field:Json(name = "asiento")
    val seat: String?,
    @field:Json(name = "annoTitulo")
    val year: String?,
    @field:Json(name = "numTitulo")
    val deedNumber: String?,
    @field:Json(name = "numPaginas")
    val numPages: String?,
    @field:Json(name = "acto")
    val act: String?,
    @field:Json(name = "tpoPers")
    val personType: String?,
    @field:Json(name = "nombrePers")
    val personName: String?,
    @field:Json(name = "areaRegistral")
    val regisArea: String?,
) : BaseModel()

@JsonClass(generateAdapter = true)
data class ApplicantDTO(
    @field:Json(name = "numDoc")
    val docNumber: String?,
    @field:Json(name = "tpoDoc")
    val docType: String?,
    @field:Json(name = "nombre")
    val name: String?
) : BaseModel()

@JsonClass(generateAdapter = true)
data class DestinationDTO(
    @field:Json(name = "tpoEnvioDesc")
    val type: String?,
    @field:Json(name = "oficina")
    val office: String?,
    @field:Json(name = "direccion")
    val address: String?,
    @field:Json(name = "nombre")
    val name: String?
) : BaseModel()

@JsonClass(generateAdapter = true)
data class PaymentDTO(
    @field:Json(name = "monto")
    val ammountQuantity: String?,
    @field:Json(name = "fecha")
    val date: String?,
    @field:Json(name = "mayorDerecho")
    val payment: String?,
    @field:Json(name = "tpoPago")
    val type: String?
) : BaseModel()