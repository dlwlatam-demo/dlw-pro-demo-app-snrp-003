package pe.gob.sunarp.appandroid.ui.alerta.account

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.filterMaxLengthCel
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.core.toEditable
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaCuentaDetalleBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class AlertaAccountDetailFragment :
    BaseFragment<FragmentAlertaCuentaDetalleBinding>(FragmentAlertaCuentaDetalleBinding::inflate){

    private val viewModel: AlertaAccountDetailViewModel by viewModels()
    private var flEnvioSms: String = "N"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (binding.btnGrabarCuenta.isVisible) {
                    cleanEditView()
                    binding.btnAccountCancel.text = "Cancelar cuenta"
                    binding.checkSendsms.disable()
                    binding.tilOperadorTelefonico.disable()
                    binding.etNroCelular.disable()
                    if (flEnvioSms == "S") {
                        binding.checkSendsms.isChecked = true
                        binding.tilOperadorTelefonico.visibility = View.VISIBLE
                        binding.etNroCelular.visibility = View.VISIBLE
                        obtenerDatosUser()
                    } else {
                        binding.checkSendsms.isChecked = false
                    }
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            initViewModels()
            viewModel.getInfoUser(result = true)
            tilOperadorTelefonico.disable()
            etNroCelular.disable()
            if (flEnvioSms == "S") {
                checkSendsms.isChecked = true
                tilOperadorTelefonico.visibility = View.VISIBLE
                etNroCelular.visibility = View.VISIBLE
            } else {
                checkSendsms.isChecked = false
            }
            checkSendsms.disable()
            obtenerDatosUser()
            viewModel.envioSmsSelect.value = false
            boxEditCel.visibility = View.VISIBLE
            checkSendsms.onChanged {
                viewModel.envioSmsSelect.value = it
                if (it) {
                    tilOperadorTelefonico.visibility = View.VISIBLE
                    etNroCelular.visibility = View.VISIBLE
                } else {
                    tilOperadorTelefonico.visibility = View.GONE
                    etNroCelular.visibility = View.GONE
                }
            }
            validateForm()

            etNroCelular.onChanged { validateForm() }
            etDocumento.onChanged { validateForm() }
            etNombre.onChanged { validateForm() }
            etNombre.filterMayus()
            etPrimerApellido.onChanged { validateForm() }
            etPrimerApellido.filterMayus()
            etSegundoApellido.filterMayus()
            etSegundoApellido.onChanged { validateForm() }
            etDireccion.onChanged { validateForm() }
            btnAccountCancel.setOnClickListener {
                if (binding.btnGrabarCuenta.isVisible) {
                    cleanEditView()
                    (it as Button).text = "Cancelar cuenta"
                } else {
                    activity?.let {
                        val alertaCancelAccountDialog = AlertaCancelAccountDialog()
                        alertaCancelAccountDialog.apply {
                            onCancelListener {
                                this.dismiss()
                            }
                            onAcceptListener {
                                this.dismiss()
                                viewModel.cancelAccount()
                            }
                            show(it.supportFragmentManager, "drop")
                        }
                    }
                }
            }
            btnEditarCuenta.setOnClickListener {
                showEditViews()
                btnAccountCancel.text = "Cancelar"
            }
            btnGrabarCuenta.setOnClickListener {
                tilOperadorTelefonico.disable()
                etNroCelular.disable()
                checkSendsms.disable()
                viewModel.updateAccount(
                    etPrimerApellido.text.toString(),
                    etSegundoApellido.text.toString(),
                    etNombre.text.toString(),
                    etDocumento.text.toString(),
                    etDireccion.text.toString(),
                    etNroCelular.text.toString(),
                )
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(operadores) { setOperadores(it) }
            onChange(tipoDocumentos) { setTipoDocumentos(it) }
            onChange(operadorSelect) { validateForm() }
            onChange(tipoDocumentoSelect) { validateForm() }
            onChange(successCancelAccount) { onSuccessCancelAccount(it) }
            onChange(successUpdateAccount) { onSuccessUpdateAccount(it) }
            obtenerDatosUser()
            getParameters()
        }
    }

    private fun obtenerDatosUser() {
        viewModel.getInfoUser().let {
            with(binding) {
                viewModel.operadores.value
                    ?.find { item -> item.id == it.idOperTele }
                    ?.let {
                        etOperadorTelefonico.setText(it.name, false)
                        etOperadorTelefonico.tag = it.id
                        viewModel.setSelectOperadorTelefonico(it)
                    }

                etNroCelular.text = it.cnumCel.toEditable()
                tvTypeDocument.text = it.nameTiDocu
                tvDocument.text = it.noDocu
                tvName.text = it.nombre
                tvLastFirstName.text = it.apPate
                tvLastSecondName.text = it.apMate
                tvDireccion.text = it.direccion
                tvEmail.text = it.email
                flEnvioSms = it.flEnvioSms.toString()

            }
        }
    }
    private fun setValuesCelular() {
        viewModel.getInfoUser().let {
            with(binding) {
                etNroCelular.filterMaxLengthCel()
                viewModel.operadores.value
                    ?.find { item -> item.id == it.idOperTele }
                    ?.let {
                        etOperadorTelefonico.setText(it.name, false)
                        etOperadorTelefonico.tag = it.id
                        viewModel.setSelectOperadorTelefonico(it)
                    }

                etNroCelular.text = it.cnumCel.toEditable()
            }
        }
    }

    private fun validateForm() {
        with(binding) {
            with(viewModel) {
                if (
                    (if(viewModel.envioSmsSelect.value == true) {
                        (operadorSelect.value != null) &&
                        etNroCelular.text.toString().isNotEmpty()
                    } else true) &&
                    (tipoDocumentoSelect.value != null) &&
                    etDocumento.text.toString().isNotEmpty() &&
                    etNombre.text.toString().isNotEmpty() &&
                    etPrimerApellido.text.toString().isNotEmpty() &&
                    etDireccion.text.toString().isNotEmpty()
                ) {
                    btnGrabarCuenta.enable()
                } else {
                    btnGrabarCuenta.disable()
                }
            }
        }
    }

    private fun onSuccessCancelAccount(result: Boolean) {
        if (result) {
            findNavController().navigate(R.id.action_to_HomeApp)
        }
    }

    private fun onSuccessUpdateAccount(result: Boolean) {
        if (result) {
            cleanEditView()
            obtenerDatosUser()
        }
    }

    private fun cleanEditView() {
        hideEditViews()
        binding.btnAccountCancel.text = "Cancelar cuenta"
        binding.etOperadorTelefonico.clean()
        binding.etTipoDocumento.clean()
        viewModel.setSelectOperadorTelefonico(null)
        viewModel.setSelectTipoDocumento(null)
    }

    private fun setOperadores(items: List<ItemSpinner>) {
        with(binding) {
            etOperadorTelefonico.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    viewModel.setSelectOperadorTelefonico(item)
                }
            }
        }
        setValuesCelular()
    }

    private fun setTipoDocumentos(items: List<ItemSpinner>) {
        with(binding) {
            etTipoDocumento.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    viewModel.setSelectTipoDocumento(item)
                }
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    private fun showEditViews() {
        viewModel.getInfoUser().let {
            with(binding) {
                checkSendsms.enable()
                tilOperadorTelefonico.enable()
                etNroCelular.enable()
                viewModel.envioSmsSelect.value = false
                boxEditCel.visibility = View.VISIBLE
                boxEditDatosPersonales.visibility = View.VISIBLE
                btnGrabarCuenta.visibility = View.VISIBLE

                btnEditarCuenta.visibility = View.GONE
                boxViewPersonas.visibility = View.GONE

                viewModel.operadores.value
                    ?.find { item -> item.id == it.idOperTele }
                    ?.let {
                        etOperadorTelefonico.setText(it.name, false)
                        etOperadorTelefonico.tag = it.id
                        viewModel.setSelectOperadorTelefonico(it)
                    }

                viewModel.tipoDocumentos.value
                    ?.find { item -> item.id == it.tiDocu }
                    ?.let {
                        etTipoDocumento.setText(it.name, false)
                        etTipoDocumento.tag = it.id
                        viewModel.setSelectTipoDocumento(it)
                    }


                etNroCelular.setText(it.cnumCel)
                etDocumento.setText(it.noDocu)
                etNombre.setText(it.nombre)
                etPrimerApellido.setText(it.apPate)
                etSegundoApellido.setText(it.apMate)
                etDireccion.setText(it.direccion)
            }
        }
    }

    private fun hideEditViews() {
        with(binding) {
            //boxEditCel.visibility = View.GONE
            boxEditDatosPersonales.visibility = View.GONE
            btnGrabarCuenta.visibility = View.GONE

            btnEditarCuenta.visibility = View.VISIBLE
            boxViewPersonas.visibility = View.VISIBLE
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}