package pe.gob.sunarp.appandroid.ui.alerta.account

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.UpdateAccountRequest
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaAccountDetailViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val successCancelAccount = SingleLiveEvent<Boolean>()
    val successUpdateAccount = SingleLiveEvent<Boolean>()
    var operadores = SingleLiveEvent<List<ItemSpinner>>()
    var operadorSelect = SingleLiveEvent<ItemSpinner?>()
    var tipoDocumentos = SingleLiveEvent<List<ItemSpinner>>()
    var tipoDocumentoSelect = SingleLiveEvent<ItemSpinner?>()
    var envioSmsSelect = SingleLiveEvent<Boolean>()

    init {
        envioSmsSelect.value = false
    }

    fun getParameters() {
        getOperadores()
        getTipoDocumento()
    }

    fun cancelAccount() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.cancelAccount()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun getOperadores() {
        viewModelScope.launch {
            operadores.value = listOf(
                ItemSpinner("1", "Claro"),
                ItemSpinner("2", "Movistar"),
                ItemSpinner("3", "Entel"),
                ItemSpinner("4", "Bitel"),
            )
        }
    }

    fun setSelectOperadorTelefonico(data: ItemSpinner?) {
        operadorSelect.value = data
    }

    private fun getTipoDocumento() {
        viewModelScope.launch {
            tipoDocumentos.value = SunarpSharedPreferences.Tipo_Documents
        }
    }

    fun setSelectTipoDocumento(data: ItemSpinner?) {
        tipoDocumentoSelect.value = data
    }


    fun updateAccount(
        apPate: String,
        apMate: String,
        nombre: String,
        nroDocu: String,
        direccion: String,
        nroCel: String,
    ) {
        viewModelScope.launch {
            loading.value = true
            val data = UpdateAccountRequest(
                preferences.alertIdRgst,
                apPate,
                apMate,
                nombre,
                tipoDocumentoSelect.value?.id ?: "",
                nroDocu,
                direccion,
                nroCel,
                operadorSelect.value?.id ?: "",
                if(envioSmsSelect.value == true) "S" else "N"
            )
            when (val result = repository.updateUser(data)) {
                is DataResult.Success -> onUpdateAccountSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    private fun onUpdateAccountSuccess(result: Boolean) {
        loading.value = false
        getInfoUser(result)
    }

    fun getInfoUser(result: Boolean) {
        viewModelScope.launch {
            loading.value = true
            when (val resultrelogin = repository.getInfoUSer()) {
                is DataResult.Success -> onReLoginSuccess(result)
                is DataResult.Failure -> onError(resultrelogin.exception)
            }
        }
    }

    fun getInfoUser() = preferences.getUserDataAlerta()


    private fun onRegistrationOfficeSuccess(result: Boolean) {
        loading.value = false
        if (result) {
            preferences.logoutAlert()
        }
        successCancelAccount.value = result
    }
    private fun onReLoginSuccess(result: Boolean) {
        loading.value = false
        successUpdateAccount.value = result
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}