package pe.gob.sunarp.appandroid.data.alerta.modelservices

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class MandatoItemResponse(

	@Json(name="idUsuaModi")
	val idUsuaModi: String? = null,

	@Json(name="nuDocu")
	val nuDocu: String? = null,

	@Json(name="feBajaCont")
	val feBajaCont: String? = null,

	@Json(name="idRgst")
	val idRgst: String? = null,

	@Json(name="tsUsuaCrea")
	val tsUsuaCrea: String? = null,

	@Json(name="tsUsuaModi")
	val tsUsuaModi: String? = null,

	@Json(name="tiDocuIden")
	val tiDocuIden: String? = null,

	@Json(name="inElim")
	val inElim: String? = null,

	@Json(name="aaCont")
	override val aaCont: String,

	@Json(name="deObs")
	val deObs: String? = null,

	@Json(name="feAltaCont")
	val feAltaCont: String? = null,

	@Json(name="apMatePersNatu")
	val apMatePersNatu: String? = null,

	@Json(name="coNoti")
	val coNoti: String? = null,

	@Json(name="feElim")
	val feElim: String? = null,

	@Json(name="feNoti")
	val feNoti: String? = null,

	@Json(name="idUsuaCrea")
	val idUsuaCrea: String? = null,

	@Json(name="noPersNatu")
	val noPersNatu: String? = null,

	@Json(name="inNoti")
	val inNoti: String? = null,

	@Json(name="nuCont")
	override val nuCont: String,

	@Json(name="apPatePersNatu")
	val apPatePersNatu: String? = null,

	@Json(name="inEstd")
	val inEstd: String? = null
): NotificationResponse, Parcelable
