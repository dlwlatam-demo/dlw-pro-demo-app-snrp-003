package pe.gob.sunarp.appandroid.data.alerta

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.alerta.modelservices.*
import retrofit2.http.*

interface AlertaDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/alerta/registro/loadUserByEmailAndPass")
    suspend fun loginAlerta(
        @Body request: UserRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<LoginResponse>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/loadUser")
    suspend fun loadUserByEmailAndPass(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<LoginResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/loadUserByEmail/{email}")
    suspend fun loadUserByEmail(
        @Path("email") email: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/loadUserByEmailChnPwd/{email}")
    suspend fun loadUserByEmailChnPwd(
        @Path("email") email: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/alerta/registro/createUser")
    suspend fun createUserAlerta(
        @Body request: UserRegisterAlertRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/sendValidationCodeByMail/{guid}")
    suspend fun sendValidationCodeByMail(
        @Path("guid") guid: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/activateUserById/{guid}")
    suspend fun activateUserById(
        @Path("guid") guid: String,
        @Query("coNoti") coNoti: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta/registro/setPassword/{guid}")
    suspend fun restorePassAlerta(
        @Path("guid") guid: String,
        @Query("passw") pass: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/alerta/registro/cancelUser")
    suspend fun cancelAccount(
        @Body request: CancelAccountRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/alerta/registro/updateUser")
    suspend fun updateUser(
        @Body request: UpdateAccountRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/alerta-mandato/addMandato")
    suspend fun addMandato(
        @Body request: AddMandatoRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta-mandato/selectAllMandatosFromUser")
    suspend fun selectAllMandatos(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<MandatoItemResponse>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/alerta-mandato/deleteMandato")
    suspend fun deleteMandato(
        @Body request: DeleteMandatoRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta-partida/selectAllPartidasFromUser")
    suspend fun selectAllPartidas(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<PartidaItemResponse>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/alerta-partida/deletePartida")
    suspend fun deletePartida(
        @Body request: DeleteMandatoRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/alerta-partida/searchPartida")
    suspend fun searchPartida(
        @Body request: BuscarPartidaRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<BuscarPartidaItemResponse>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/alerta-partida/addPartida")
    suspend fun addPartida(
        @Body request: AddPartidaRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<GenericResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/alerta-partida/getAlertasPorContrato")
    suspend fun geteDetailMandato(
        @Query("aaCont") aaCont: String,
        @Query("nuCont") nuCont: String,
        @Query("tipo") tipo: String,
        @Query("source") source: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<DetailMandatoResponse>>

}