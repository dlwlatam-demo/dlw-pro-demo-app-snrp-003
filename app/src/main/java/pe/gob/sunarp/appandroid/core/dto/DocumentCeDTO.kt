package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.DocumentCE

@JsonClass(generateAdapter = true)
data class DocumentCeDTO(
    @field:Json(name = "strCalidadMigratoria")
    val migratoryStatus: String,
    @field:Json(name = "strNombres")
    val names: String,
    @field:Json(name = "strNumRespuesta")
    val number: String,
    @field:Json(name = "strPrimerApellido")
    val firstLastName: String,
    @field:Json(name = "strSegundoApellido")
    val secondLastName: String,
) : BaseModel() {
    constructor() : this("", "", "", "", "")

    fun toModel() =
        DocumentCE(
            migratoryStatus = migratoryStatus,
            names = names,
            number = number,
            firstLastName = firstLastName,
            secondLastName = secondLastName
        )
}