package pe.gob.sunarp.appandroid.ui.services.applicationstatus

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.status.model.TransactionStatus
import javax.inject.Inject

@HiltViewModel
class ApplicationStatusViewModel @Inject constructor(
    private val repository: TransactionRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val transaction = SingleLiveEvent<TransactionStatus>()

    fun getTransactionStatus(transId: String) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.getTransactionStatus(transId)) {
                is DataResult.Success -> onTransactionSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onTransactionSuccess(data: TransactionStatus) {
        loading.value = false
        transaction.value = data
    }

}