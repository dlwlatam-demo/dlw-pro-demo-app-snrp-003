package pe.gob.sunarp.appandroid.ui.contact

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.databinding.FragmentContactSendBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.AlertType
import pe.gob.sunarp.appandroid.ui.common.AlertaCommonDialog
import java.io.IOException

@AndroidEntryPoint
class ContactSendFragment :
    BaseFragment<FragmentContactSendBinding>(FragmentContactSendBinding::inflate) {

    private val viewModel: ContactSendViewModel by viewModels()
    private var uri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setObservers()
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            onSuccess.observe(viewLifecycleOwner) { goToView(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
            onFormError.observe(viewLifecycleOwner) { showError(it) }
        }
    }

    private fun setListeners() {
        binding.actionContactSend.setOnClickListener {
            hideKeyboard()
            var base64 = ""
            uri?.let { base64 = it.getBase64(requireActivity()) }
            viewModel.sendComment(
                binding.textInputComment.editText?.text.toString(),
                base64
            )
//            if (BuildConfig.DEBUG)
//                requireContext().writeToFile(base64)
        }
        binding.actionContactAttach.setOnClickListener { validatePermission { gallery() } }
        binding.actionContactPhoto.setOnClickListener { validatePermission { camera() } }
    }

    private fun validatePermission(action: () -> Unit) {
        with(requireContext()) {
            if (cameraPermissionGranted()) {
                action.invoke()
            } else {
                requestCameraPermission(requireActivity())
            }
        }
    }

    private var resultGalleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result?.data?.data?.let { data ->
                    uri = data
                    binding.imagePreviewView.setImageURI(uri)

                    // Opcional: Puedes redimensionar la imagen para que quepa en tu ImageView
                    val resizedBitmap =
                        uri?.decodeSampledBitmapFromUri(requireContext().contentResolver, 80, 80)
                    binding.imagePreviewView.setImageBitmap(resizedBitmap)
                    lifecycleScope.launch(Dispatchers.IO) {
                        uri = data.createImage(
                            requireActivity(),
                            AUTHORITY, requireContext().createImageFile(),
                            IMAGE_MAX_SIZE,
                            IMAGE_QUALITY
                        )
                    }
                    binding.actionContactPhoto.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_camera,
                        0,
                        0,
                        0
                    )
                    binding.actionContactAttach.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_attach,
                        0,
                        R.drawable.ic_dot_orange,
                        0
                    )
                }
            }
        }

    private var resultCameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                uri?.let {
                    binding.imagePreviewView.setImageURI(it)

                    // Opcional: Puedes redimensionar la imagen para que quepa en tu ImageView
                    val resizedBitmap = it.decodeSampledBitmapFromUri(requireContext().contentResolver, 80, 80)
                    binding.imagePreviewView.setImageBitmap(resizedBitmap)
                    lifecycleScope.launch(Dispatchers.IO) {
                        uri = it.createImage(
                            requireActivity(),
                            AUTHORITY,
                            requireContext().createImageFile(),
                            IMAGE_MAX_SIZE,
                            IMAGE_QUALITY
                        )
                    }
                    binding.actionContactAttach.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_attach,
                        0,
                        0,
                        0
                    )
                    binding.actionContactPhoto.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_camera,
                        0,
                        R.drawable.ic_dot_orange,
                        0
                    )
                }
            }
        }

    private fun gallery() {
        try {
            val intent = createImageChooserIntent(getString(R.string.image_chooser_title))
            intent?.let { resultGalleryLauncher.launch(it) }
        } catch (ex: Exception) {
            showError(ex.message.orEmpty())
        }
    }

    private fun camera() {
        try {
            createImage()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            resultCameraLauncher.launch(intent)
        } catch (ex: Exception) {
            showError(ex.message.orEmpty())
        }
    }

    @Throws(IOException::class)
    private fun createImage() {
        uri = FileProvider.getUriForFile(
            requireContext(),
            AUTHORITY,
            requireContext().createImageFile()
        )
    }

    private fun goToView(message: String?) {
/*        AlertaCommonDialog().apply {
            onCancelListener {
                this.dismiss()
                findNavController().navigate(R.id.action_ContactSendFragment_ContactInfoFragment)
            }
            setAlertType(type = AlertType.SUCCESS)
            setData(title = "Envío Satisfactorio", detalle = message)
            show(activity!!.supportFragmentManager, "error")
        }*/
        context?.let {
            activity?.let {
                AlertaCommonDialog().apply {
                    onCancelListener {
                        this.dismiss()
                        findNavController().navigate(R.id.action_ContactSendFragment_ContactInfoFragment)}
                    setData(title = "Envío Satisfactorio", detalle = message)

                    show(it.supportFragmentManager, "error")
                    setAlertType(type = AlertType.SUCCESS)
                }
            }
        }


        //binding.container.snack(message)

    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    companion object {
        const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.FileProvider"
        const val IMAGE_MAX_SIZE = 1024
        const val IMAGE_QUALITY = 80
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}