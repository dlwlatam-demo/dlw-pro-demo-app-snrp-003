package pe.gob.sunarp.appandroid.ui.services.tive.search

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.model.TiveDocument
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.tive.TiveRemoteRepository
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SearchTiveViewModel @Inject constructor(
    private val repository: TiveRemoteRepository,
    private val parameterRepository: CertificateRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val compleateData = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()

    fun getSearch(body: TiveSearchRequest) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getSearch(body)) {
                is DataResult.Success -> onConsultImageSuccess(result.data, body)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun getParameters() {
        viewModelScope.launch {
            loading.value = true
            when (val result = parameterRepository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    private fun onConsultImageSuccess(data: TiveDocument, body: TiveSearchRequest) {
        saveHistory(body)
    }

    override fun onError(err: Exception) {
        loading.value = false
        val error = Exception(
            if (err.message.toString().contains("003")){
            "No existe coincidencia, por favor verifique los datos ingresados."
            } else err.message
        )

        super.onError(error)
    }

    private fun OnErrorSaveHistory(message: String?) {
        loading.value = false
    }

    private fun saveHistory(body: TiveSearchRequest) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.saveHistory(body)) {
                is DataResult.Success -> onSaveHistorySuccess()
                is DataResult.Failure -> {
                    if (result.exception.message == "Ya se encuentra registrada la consulta TIVe. ") {
                        onSaveHistorySuccess()
                    } else {
                        OnErrorSaveHistory(result.exception.message)
                    }
                }
            }
        }
    }

    private fun onSaveHistorySuccess() {
        loading.value = false
        compleateData.value = true
    }

    fun getZoneCode(office: RegistrationOffice?) = office?.regPubId ?: ""

    fun getCodeOffice(office: RegistrationOffice?) = office?.oficRegId ?: ""
}