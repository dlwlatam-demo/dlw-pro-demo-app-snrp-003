package pe.gob.sunarp.appandroid.ui.services.vehicle.payment

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.formatEndText
import pe.gob.sunarp.appandroid.core.formatMoneySunarp
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.core.model.VehiclePayment
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.databinding.FragmentPaymentBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.common.AlertTerminosDialog
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult


@AndroidEntryPoint
class VehiclePaymentFragment :
    BaseFragment<FragmentPaymentBinding>(FragmentPaymentBinding::inflate), PaymentInterface {

    private val viewModel: VehiclePaymentViewModel by viewModels()
    private var paymentToPay = "0.0"
    private val transId = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        with(binding) {
            tvDescription.show()
            tvTitle.text = getString(R.string.vehicular_search_receipt)
            enablePersonFields()
            btnSend.isEnabled = false;
            checkBoxTerms.setOnCheckedChangeListener { _, isChecked ->
                btnSend.isEnabled = isChecked
            }
            btnSend.setOnClickListener { openPaymentScreen() }
            etNames.filterMayus()
            etFirstLastname.filterMayus()
            etSecondLastname.filterMayus()
            tvTerminos.paintFlags = tvTerminos.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            val fm = activity!!.supportFragmentManager
            tvTerminos.setOnClickListener {
                AlertTerminosDialog().apply {
                    setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert)
                    show(fm, this.javaClass.name)
                }
            }
        }

    }

    override fun sendPaymentConfirmation(data: VisaNetResult) {
        with(binding) {
            val information: VehicleInformation? = arguments?.getParcelable("information")

            val username =
                "${etNames.text.toString()} ${etFirstLastname.text.toString()} ${etSecondLastname.text.toString()}"

            val payment = VehiclePayment(
                paymentToPay,
                information?.regPubId.orEmpty(),
                information?.oficRegId.orEmpty(),
                information?.plate.orEmpty(),
                username,
                viewModel.getNiubizInformation(data)
            )
            viewModel.generateReceiptAsync(payment, data)
        }

    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber, Page.VEHICLE_INFORMATION)
        )
    }

    private fun openPaymentScreen() {
        with(binding) {
            viewModel.create(
                payment = PaymentItem(
                    etNames.text.toString(),
                    etFirstLastname.text.toString(),
                    etSecondLastname.text.toString(),
                    etEmail.text.toString(),
                    paymentToPay,
                    ""
                ), terms = checkBoxTerms.isChecked,
                transId = transId
            )
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            setArgumentsData(arguments!!)
            onChange(onVisaConfiguration) { showVisaNet(it) }
            onChange(onReceipt) { goToNavigate(it) }
            onChange(loading) { setLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(dataCosto) { showDataCosto(it) }
            obtenerTarifa()
        }
    }

    private fun showDataCosto(costo: String) {
        with(binding) {
            paymentToPay = costo
            tvTotalCost.text =
                getString(R.string.total_cost_label, costo.formatMoneySunarp()).formatEndText()
        }
    }

    private fun goToNavigate(paymentResult: PaymentModel) {
        navigateToResult(true, paymentResult.result, paymentResult.transId)
    }

    private fun showVisaNet(item: VisaNetItem) {
        try {
            VisaNet.authorization(activity, item.data, item.custom)
        } catch (e: Exception) {
            Log.i("ERROR", "onClick: " + e.message)
        }
    }

    private fun setLoading(it: Boolean) {
        binding.loadingContainer.apply {
            if (it) loading.show() else loading.hide()
        }
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = VehiclePaymentFragmentDirections
            .actionVehiclePaymentFragmentToPaymentResultFragment(
                transId= transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

    private fun enablePersonFields() {
        with(binding) {
            val userInformation = viewModel.getPersonData()
            etFirstLastname.setText(userInformation.firstLastname.trim())
            etSecondLastname.setText(userInformation.secondLastname.trim())
            etNames.setText(userInformation.name)
            etEmail.setText(userInformation.email)
            etEmail.isEnabled = false
        }
    }
}