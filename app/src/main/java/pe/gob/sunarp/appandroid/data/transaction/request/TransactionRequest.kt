package pe.gob.sunarp.appandroid.data.transaction.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionRequest(
    @field:Json(name = "solicitudId")
    val transKey: String?,
    @field:Json(name = "usuario")
    val userId: String?,
    @field:Json(name = "detalle")
    val detail: String? = null,
)