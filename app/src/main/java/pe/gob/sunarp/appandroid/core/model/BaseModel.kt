package pe.gob.sunarp.appandroid.core.model

abstract class BaseModel {
    @JvmField
    var error: String? = null
}