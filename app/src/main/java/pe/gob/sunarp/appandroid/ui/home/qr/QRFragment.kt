package pe.gob.sunarp.appandroid.ui.home.qr

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import io.github.g00fy2.quickie.QRResult
import io.github.g00fy2.quickie.ScanQRCode
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.FragmentQrBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


class QRFragment : BaseFragment<FragmentQrBinding>(FragmentQrBinding::inflate) {

    private val scanQrCodeLauncher = registerForActivityResult(ScanQRCode(), ::processQRCode)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnScan.setOnClickListener {
            scanQrCodeLauncher.launch(null)
        }
    }

    private fun processQRCode (result: QRResult) {
        when(result) {
            is QRResult.QRUserCanceled -> {
                toast("Se ha cancelado el scaneo")
            }
            is QRResult.QRSuccess -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(result.content.rawValue))
                startActivity(browserIntent)
            }
            is QRResult.QRMissingPermission -> {
                toast("No se tienen permisos de Cámara")
            }
            is QRResult.QRError -> {
                toast("Ocurrio un Error")
            }
        }
    }

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}