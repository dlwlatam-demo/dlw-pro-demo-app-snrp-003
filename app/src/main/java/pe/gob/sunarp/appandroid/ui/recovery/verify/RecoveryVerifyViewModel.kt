package pe.gob.sunarp.appandroid.ui.recovery.verify

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RecoveryVerifyViewModel @Inject constructor(private val repository: LoginRemoteRepository) :
    BaseViewModel() {

    val onError = SingleLiveEvent<String>()
    val isVerified = SingleLiveEvent<String?>()
    val isResend = SingleLiveEvent<String?>()
    val onErrorResend = SingleLiveEvent<String>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun verify(code: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.validateRecoveryCode(code)) {
                is DataResult.Success -> {
                    isVerified.value = result.data
                    showLoading.value = false
                }
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    override fun onError(err: Exception) {
        showLoading.value = false
        onError.value = err.message
    }

    fun resend(email: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.requestRecoveryCode(email)) {
                is DataResult.Success -> {
                    isResend.value = result.data
                    showLoading.value = false
                }
                is DataResult.Failure -> {
                    onErrorResend.value = result.exception.message
                    showLoading.value = false
                }
            }
        }
    }
}