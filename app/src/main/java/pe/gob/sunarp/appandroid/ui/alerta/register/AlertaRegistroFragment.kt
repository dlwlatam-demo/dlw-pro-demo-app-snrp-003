package pe.gob.sunarp.appandroid.ui.alerta.register

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaRegistroBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaRegistroFragment : BaseFragment<FragmentAlertaRegistroBinding>(FragmentAlertaRegistroBinding::inflate) {

    private lateinit var title: String
    private var recovery: Boolean = false
    private val viewModel: AlertaRegistroViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        title = arguments?.getString("title", "")?: ""
        recovery = arguments?.getBoolean("recovery", false)?: false

        with(binding) {
            initViewModelsErrorToken()
            viewModel.showLoading.observe(viewLifecycleOwner) {
                if (it) loadingContainer.loading.show() else loadingContainer.loading.hide()
            }

            viewModel.isAuthenticated.observe(viewLifecycleOwner) {
                it?.let { _ ->
                    findNavController().navigate(
                        R.id.action_alertaregistrofragment_to_alertavalidatecodefragment,
                        bundleOf("guid" to it)
                    )
                }
            }
            textInputEmail.editText?.onChanged { validateFields() }

            textViewTitle.text = title

            actionEnter.setOnClickListener {
                viewModel.register(
                    textInputEmail.editText?.text.toString(),recovery
                )
            }

        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (textInputEmail.editText?.text ?: "").toString().isValidString(Constants.REGEX_EMAIL_ADDRESS)
            ) {
                actionEnter.enable()
            } else {
                actionEnter.disable()
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
