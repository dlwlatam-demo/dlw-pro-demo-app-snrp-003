package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.validityofpower


import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoVigenciaPoderBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.ListAdapterItems
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PersonItem
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputLayout



@AndroidEntryPoint
class CertifiedValidityOfpowerFragment :
    BaseFragment<FragmentCertificadoVigenciaPoderBinding>(
        FragmentCertificadoVigenciaPoderBinding::inflate
    ) {


    private val viewModel: CertifiedValidityOfpowerViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var saveProcess: SaveProcess? = null
    private var book: BooksByOffice? = null
    private var typePerson: String = Constants.PERSON_NATURAL
    var interviniente: PersonItem? = null
    var intervinientePoderdante: PersonItem? = null
    var intervinienteApoderado: PersonItem? = null

    var tipoSolicitud: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun onResume() {
        super.onResume()
        setupAdapter()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()

        with(binding) {

            val itemsList = resources.getStringArray(R.array.conservator_types_request)
            etRequest.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, itemsList))
            tilNamesApoderado.editText?.filterMayus()
            tilFirstLastnameApoderado.editText?.filterMayus()
            tilSecondLastnameApoderado.editText?.filterMayus()
            tilNamesPoderante.editText?.filterMayus()
            tilFirstLastnamePoderante.editText?.filterMayus()
            tilSecondLastnamePoderante.editText?.filterMayus()
            val adapterPoderanteItems = ListAdapterItems(arrayListOf()) { validateFields() }
            val adapterApoderadoItems = ListAdapterItems(arrayListOf()) { validateFields() }

            etNumero.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }

            tilNroAsiento.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilFirstLastnameApoderado.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilSecondLastnameApoderado.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilFirstLastnamePoderante.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilSecondLastnamePoderante.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilNamesPoderante.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tilNamesApoderado.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etNroAsiento.filterMaxLengthAsiento()
            etNumero.filterOnlyMayus()
            etNumero.onChanged {
                validateFields()
                val maxLength = 20 // Límite máximo de caracteres
                val filterArray = arrayOf<InputFilter>(InputFilter.LengthFilter(maxLength))
                etNumero.filters = filterArray
            }



            rvApoderado.apply {
                adapter = adapterApoderadoItems
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }

            rvGrantors.apply {
                adapter = adapterPoderanteItems
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }


            btnPoderante.setOnClickListener {
                hideKeyboard()
                var lastname =
                    (tilFirstLastnamePoderante.editText?.text ?: Constants.EMPTY).toString()
                var lastname2 =
                    (tilSecondLastnamePoderante.editText?.text ?: Constants.EMPTY).toString()
                var name = (tilNamesPoderante.editText?.text ?: Constants.EMPTY).toString()

                if (name.isNotEmpty() && lastname.isNotEmpty()) {

                    var interviniente = PersonItem(5, 1, name, lastname, lastname2, Constants.EMPTY)
                    adapterPoderanteItems.addItem(interviniente)

                    tilFirstLastnamePoderante.editText?.clean()
                    tilSecondLastnamePoderante.editText?.clean()
                    tilNamesPoderante.editText?.clean()
                    validateFields()
                } else {
                    container.snack("Para agregar un Curador ingrese sus nombres completos")
                    validateFields()
                }

            }


            btnApoderado.setOnClickListener {
                hideKeyboard()
                var lastname =
                    (tilFirstLastnameApoderado.editText?.text ?: Constants.EMPTY).toString()
                var lastname2 =
                    (tilSecondLastnameApoderado.editText?.text ?: Constants.EMPTY).toString()
                var name = (tilNamesApoderado.editText?.text ?: Constants.EMPTY).toString()
                var ruc = (tilRucApoderado.editText?.text ?: Constants.EMPTY).toString()

                if ((typePerson == Constants.PERSON_NATURAL && name.isNotEmpty() && lastname.isNotEmpty()) || (typePerson == Constants.PERSON_LEGAL && ruc.isNotEmpty())) {

                    var type = 1
                    if (typePerson == Constants.PERSON_LEGAL) {
                        type = 2
                    }

                    var interviniente =
                        PersonItem(6, type, name, lastname, lastname2, ruc)
                    adapterApoderadoItems.addItem(interviniente)

                    tilFirstLastnameApoderado.editText?.clean()
                    tilSecondLastnameApoderado.editText?.clean()
                    tilNamesApoderado.editText?.clean()
                    tilRucApoderado.editText?.clean()
                    validateFields()
                } else {
                    if (typePerson == Constants.PERSON_NATURAL){
                        container.snack("Para agregar un Apoderado ingrese sus nombres completos")
                        validateFields()
                    } else {
                        container.snack("Ingrese la Razón Social")
                        validateFields()
                    }
                }
            }

            rgTypePerson.setOnCheckedChangeListener { _, checkedId ->

                if (checkedId == R.id.rb_person) {
                    typePerson = Constants.PERSON_NATURAL
                    enableNaturalFields()
                } else {
                    typePerson = Constants.PERSON_LEGAL
                    disableNaturalFields()
                }
            }


            btnApply.setOnClickListener {
                hideKeyboard()
                var nroPartida = binding.etNumero.text.toString()
                /*
                val nameInterdictoPoderdante = tilNamesPoderante.editText?.text.toString()
                val lastnameInterdictoPoderdante = tilFirstLastnamePoderante.editText?.text.toString()
                val lastname2InterdictoPoderdante = tilSecondLastnamePoderante.editText?.text.toString()

                val nameInterdictoApoderado = tilNamesPoderante.editText?.text.toString()
                val lastnameInterdictoApoderado = tilFirstLastnamePoderante.editText?.text.toString()
                val lastname2InterdictoApoderado = tilSecondLastnamePoderante.editText?.text.toString()

                intervinientePoderdante = PersonItem(5, 1, nameInterdictoPoderdante, lastnameInterdictoPoderdante, lastname2InterdictoPoderdante, Constants.EMPTY)
                intervinienteApoderado = PersonItem(6, 1, nameInterdictoApoderado, lastnameInterdictoApoderado, lastname2InterdictoApoderado, Constants.EMPTY)
                */
                certificate?.let {
                    etRequest.tag?.let { tipoPartida ->
                        if (tipoPartida == 0) {
                            tipoSolicitud = 0
                            viewModel.validatePartidaVigencia(
                                nroPartida,
                                legalRecord,
                                certificate,
                                office
                            )
                        } else {

                            if (tipoPartida == 1) {
                                tipoSolicitud = 1
                                viewModel.validatePartidaTomo(
                                    nroPartida,
                                    legalRecord,
                                    certificate,
                                    office,
                                    Constants.EMPTY,
                                    tipoPartida.toString(),
                                    Constants.EMPTY,
                                    Constants.EMPTY
                                )
                            } else {
                                tipoSolicitud = 2
                                var folio =
                                    (etFolio?.text ?: Constants.EMPTY).toString()
                                var tomo =
                                    (etTomo?.text ?: Constants.EMPTY).toString()

                                viewModel.validatePartidaTomo(
                                    nroPartida,
                                    legalRecord,
                                    certificate,
                                    office,
                                    etBook.tag.toString(),
                                    tipoPartida.toString(),
                                    folio,
                                    tomo
                                )
                            }
                        }
                    }
                }
            }


            etRegisterOffice.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etBook.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etRequest.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
        }
    }

    private fun setupAdapter() {
        val items = resources.getStringArray(R.array.conservator_types_request)
        binding.etRequest.apply {
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                tag = position
                if (position == 2) {
                    showBook()
                } else {
                    hideBook()
                }
            }
        }
    }
    private fun getListPoderdante(): ArrayList<PersonItem> {
        return (binding.rvGrantors.adapter as ListAdapterItems).getData()
    }
    private fun getListApoderado(): ArrayList<PersonItem> {
        return (binding.rvApoderado.adapter as ListAdapterItems).getData()
    }

    private fun validatePersonItems(personItems: ArrayList<PersonItem>): Boolean {
        for (personItem in personItems) {
            if (personItem.nombres.isEmpty() || personItem.apPaterno.isEmpty()) {
                return false
            }
        }
        return true
    }

    private fun enableNaturalFields() {
        with(binding) {
            initViewModelsErrorToken()
            tilFirstLastnameApoderado.editText?.clean()
            tilSecondLastnameApoderado.editText?.clean()
            tilNamesApoderado.editText?.clean()
            tilRucApoderado.editText?.clean()

            tilFirstLastnameApoderado.show()
            tilSecondLastnameApoderado.show()
            tilNamesApoderado.show()
            tilRucApoderado.hide()
        }
    }

    private fun disableNaturalFields() {
        with(binding) {
            tilFirstLastnameApoderado.editText?.clean()
            tilSecondLastnameApoderado.editText?.clean()
            tilNamesApoderado.editText?.clean()
            tilRucApoderado.editText?.clean()

            tilFirstLastnameApoderado.hide()
            tilSecondLastnameApoderado.hide()
            tilNamesApoderado.hide()
            tilRucApoderado.show()
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            getSpinners()
            certificate?.areaId?.let { viewModel.getBooks(it) }
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(onSaveProcess) { saveOnSaveProcess(it) }
            onChange(books) { fillBooks(it) }
            onChange(onPartidaProcess) { savePartida(it) }
            onChange(onPartidaProcessTomo) { savePartidaTomo(it) }
        }
    }


    private fun savePartida(partida: Partida) {
        Log.e("save partida", "save partida")
        var nroPartida = binding.etNumero.text.toString()
        var nroAsientos = binding.tilNroAsiento.editText?.text.toString()

        if (partida.estado == 0) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.businessName = Constants.EMPTY
            paymentDataItem.typePerson = typePerson
            paymentDataItem.asientos = nroAsientos
            paymentDataItem.partida = partida
            if (tipoSolicitud == 1) { //  Ficha
                paymentDataItem.partida!!.numFicha = nroPartida
            } else if (tipoSolicitud == 0) {
                paymentDataItem.partida!!.numPartida = nroPartida
            }
            paymentDataItem.lstPoderdantes = getListPoderdante()
            paymentDataItem.lstApoderados = getListApoderado()
            /*
            intervinientePoderdante?.let {
                paymentDataItem.lstPoderdantes = getListPoderdante().apply { add (it) }
            }

            intervinienteApoderado?.let {
                paymentDataItem.lstApoderados = getListApoderado().apply { add (it) }
            }
            */

            findNavController().navigate(
                R.id.action_certifiedValidityOfpowerFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to office,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )


        } else {
            binding.container.snack(partida.msj)
        }
    }

    private fun savePartidaTomo(partidas: List<String>) {
        Log.e("save partida tomo", "save partida tomo")
        if (partidas.isNotEmpty()) {
            var numpart = partidas.get(0)
            var nroPartida = "" // binding.etNumero.text.toString()
            var nroAsientos = binding.tilNroAsiento.editText?.text.toString()
            var tomo = binding.etTomo.text.toString()
            var folio = binding.etFolio.text.toString()

            certificate?.let {
                val partida = Partida(
                    0,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    numpart,
                    Constants.EMPTY
                )

                var paymentDataItem =
                    PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
                paymentDataItem.documentNumebr = Constants.EMPTY
                paymentDataItem.businessName = Constants.EMPTY
                paymentDataItem.typePerson = typePerson
                paymentDataItem.asientos = nroAsientos
                paymentDataItem.partida = partida
                paymentDataItem.partida!!.numPartida = numpart
                paymentDataItem.tomo = tomo
                paymentDataItem.folio = folio
                paymentDataItem.lstPoderdantes = getListPoderdante()
                paymentDataItem.lstApoderados = getListApoderado()
                /*intervinientePoderdante?.let {
                    paymentDataItem.lstPoderdantes = getListPoderdante().apply { add (it) }
                }

                intervinienteApoderado?.let {
                    paymentDataItem.lstApoderados = getListApoderado().apply { add (it) }
                }*/

                Log.e("lista poderdantes", paymentDataItem.lstPoderdantes.toString())
                Log.e("lista apoderados", paymentDataItem.lstApoderados.toString())
                paymentDataItem.partida!!.codigoLibro = binding.etBook.tag.toString()

                findNavController().navigate(
                    R.id.action_certifiedValidityOfpowerFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to null,
                        "ammount" to certificate?.preOffice!!.toDouble()
                    )
                )
            }
        } else {
            binding.container.snack("Lo sentimos, ninguna partida cumple con los criterios de búsqueda especificados.")
        }
    }


    fun validateFields() {
        with(binding) {

            var nroPartida = binding.tilRequestNumber.editText?.text.toString()
            var nroAsiento =
                (tilNroAsiento.editText?.text ?: Constants.EMPTY).toString()

            etRequest.tag?.let { type ->
                etRegisterOffice.tag?.let { offi ->
                    if (type == 2) {
                        etBook.tag.let {
                            var folio =
                                (etFolio?.text ?: Constants.EMPTY).toString()
                            var tomo =
                                (etTomo?.text ?: Constants.EMPTY).toString()
                            if (validatePersonItems(getListPoderdante()) && getListPoderdante().isNotEmpty() && getListApoderado().isNotEmpty() && folio.isNotEmpty() && tomo.isNotEmpty()) {
                                btnApply.enable()
                            } else {
                                btnApply.disable()
                            }
                        }
                    } else {
                        if (nroPartida.isNotEmpty() && validatePersonItems(getListPoderdante()) && getListPoderdante().isNotEmpty() && getListApoderado().isNotEmpty()) {
                            btnApply.enable()
                        } else {
                            btnApply.disable()
                        }
                    }
                }
            }
        }
    }

    private fun showBook() {
        with(binding) {
            etNumero.hide()
            clBook.show()
            tilBookFolio.editText?.clean()
            tilBookTomo.editText?.clean()
        }
    }

    private fun hideBook() {
        with(binding) {
            etNumero.show()
            clBook.hide()
        }
    }

    private fun fillBooks(items: List<BooksByOffice>) {
        binding.etBook.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as BooksByOffice)
                book = officeSelected
                tag = officeSelected.codLibro
                validateFields()
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
        }
    }

    private fun saveOnSaveProcess(process: SaveProcess) {
        saveProcess = process
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}
