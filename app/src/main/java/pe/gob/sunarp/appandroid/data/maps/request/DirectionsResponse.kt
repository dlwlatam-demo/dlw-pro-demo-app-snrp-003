package pe.gob.sunarp.appandroid.data.maps.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DirectionsResponse(

	@Json(name="routes")
	val routes: List<RoutesItem> = arrayListOf(),

	@Json(name="geocoded_waypoints")
	val geocodedWaypoints: List<GeocodedWaypointsItem> = arrayListOf(),

	@Json(name="status")
	val status: String = ""
)

@JsonClass(generateAdapter = true)
data class LegsItem(

	@Json(name="duration")
	val duration: Duration,

	@Json(name="start_location")
	val startLocation: StartLocation,

	@Json(name="distance")
	val distance: Distance,

	@Json(name="start_address")
	val startAddress: String,

	@Json(name="end_location")
	val endLocation: EndLocation,

	@Json(name="end_address")
	val endAddress: String,

	@Json(name="via_waypoint")
	val viaWaypoint: List<Any>,

	@Json(name="steps")
	val steps: List<StepsItem>,

	@Json(name="traffic_speed_entry")
	val trafficSpeedEntry: List<Any>
)

@JsonClass(generateAdapter = true)
data class Duration(

	@Json(name="text")
	val text: String,

	@Json(name="value")
	val value: Int
)

@JsonClass(generateAdapter = true)
data class GeocodedWaypointsItem(

	@Json(name="types")
	val types: List<String>,

	@Json(name="geocoder_status")
	val geocoderStatus: String,

	@Json(name="place_id")
	val placeId: String
)

@JsonClass(generateAdapter = true)
data class StepsItem(

	@Json(name="duration")
	val duration: Duration?,

	@Json(name="start_location")
	val startLocation: StartLocation?,

	@Json(name="distance")
	val distance: Distance?,

	@Json(name="travel_mode")
	val travelMode: String?,

	@Json(name="html_instructions")
	val htmlInstructions: String?,

	@Json(name="end_location")
	val endLocation: EndLocation?,

	@Json(name="maneuver")
	val maneuver: String?,

	@Json(name="polyline")
	val polyline: Polyline?
)

@JsonClass(generateAdapter = true)
data class OverviewPolyline(

	@Json(name="points")
	val points: String?
)

@JsonClass(generateAdapter = true)
data class Polyline(

	@Json(name="points")
	val points: String?
)

@JsonClass(generateAdapter = true)
data class RoutesItem(

	@Json(name="summary")
	val summary: String?,

	@Json(name="copyrights")
	val copyrights: String?,

	@Json(name="legs")
	val legs: List<LegsItem>?,

	@Json(name="warnings")
	val warnings: List<String>?,

	@Json(name="bounds")
	val bounds: Bounds?,

	@Json(name="overview_polyline")
	val overviewPolyline: OverviewPolyline?,

	@Json(name="waypoint_order")
	val waypointOrder: List<Any>?
)

@JsonClass(generateAdapter = true)
data class Southwest(

	@Json(name="lng")
	val lng: Double?,

	@Json(name="lat")
	val lat: Double?
)

@JsonClass(generateAdapter = true)
data class Bounds(

	@Json(name="southwest")
	val southwest: Southwest,

	@Json(name="northeast")
	val northeast: Northeast
)

@JsonClass(generateAdapter = true)
data class Northeast(

	@Json(name="lng")
	val lng: Double?,

	@Json(name="lat")
	val lat: Double?
)

@JsonClass(generateAdapter = true)
data class EndLocation(

	@Json(name="lng")
	val lng: Double?,

	@Json(name="lat")
	val lat: Double?
)

@JsonClass(generateAdapter = true)
data class StartLocation(

	@Json(name="lng")
	val lng: Double?,

	@Json(name="lat")
	val lat: Double?
)

@JsonClass(generateAdapter = true)
data class Distance(

	@Json(name="text")
	val text: String,

	@Json(name="value")
	val value: Int
)
