package pe.gob.sunarp.appandroid.data.registration.sendSMS

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class SMSRequest(
    @field:Json(name = "numcelular")
    val mobilePhone: String
): BaseModel() {
    constructor() : this("")
}