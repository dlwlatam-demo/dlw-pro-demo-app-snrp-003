package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.DocumentType

@JsonClass(generateAdapter = true)
data class DocumentTypeDTO(
    @field:Json(name = "tipoDocId")
    val docTypeId: String,
    @field:Json(name = "nombreAbrev")
    val label: String,
    @field:Json(name = "tipoPer")
    val personId: String,
    @field:Json(name = "descripcion")
    val description: String,
) : BaseModel() {
    constructor() : this("", "", "", "")
    fun toModel() =
        DocumentType(docTypeId, label, description, personId)
}