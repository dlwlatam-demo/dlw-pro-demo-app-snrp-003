package pe.gob.sunarp.appandroid.ui.services.tive.history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import pe.gob.sunarp.appandroid.databinding.LayoutTiveHistoryItemBinding

class TiveHistoryAdapter(
    private val items: List<TiveSearchRequest>
) : RecyclerView.Adapter<TiveHistoryAdapter.ViewHolder>() {

    private var onOpenDocument: ((query: TiveSearchRequest) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutTiveHistoryItemBinding =
            LayoutTiveHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setOpenDocument(listener: (query: TiveSearchRequest) -> Unit) {
        this.onOpenDocument = listener
    }

    inner class ViewHolder(private val binding: LayoutTiveHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TiveSearchRequest) = with(binding) {
            with(item) {
                tvTitleYear.text = if(tipo.equals("T")) "Año del título" else "Año de la publicidad"
                tvTitleNro.text = if(tipo.equals("T")) "N° de título" else "N° de publicidad"
                tvYear.text = anioTitulo
                tvOficina.text = oficina
                tvNroTitle.text = numeroTitulo
                tvPlaca.text = numeroPlaca
                tvCodVer.text = codigoVerificacion
                btnOpenDocument.setOnClickListener {
                    onOpenDocument?.invoke(item)
                }
            }
        }
    }


}