package pe.gob.sunarp.appandroid.data.certificate.request

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ValidarPartidaCargadaDesgravamenRequest(
    val regPubId: String,
    val oficRegId: String,
    val areaRegId: String,
    val numPart: String,
    val codGrupo: String,
    val codigoLibro: String,
    val tipoBusqueda: String,
)