package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PaymentInformation (
    val names:String,
    val firstLastname:String,
    val secondLastname:String,
    val email:String,
) : Parcelable