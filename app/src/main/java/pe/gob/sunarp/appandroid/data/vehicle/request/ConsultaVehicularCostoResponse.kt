package pe.gob.sunarp.appandroid.data.vehicle.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ConsultaVehicularCostoResponse(

	@Json(name="codCerti")
	val codCerti: String,

	@Json(name="monto")
	val monto: String
)
