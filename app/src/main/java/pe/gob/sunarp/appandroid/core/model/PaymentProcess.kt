package pe.gob.sunarp.appandroid.core.model

import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.emptyOrHyphen
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.ArrayList

data class PaymentProcess(
    val solicitudId: Int,
    val descripcion: String,
    val tsCrea: String,
    val total: String,
    val email: String,
    val solicitante: String,
    val tpoPago: String,
    val pagoSolicitudId: Int,
    val numeroRecibo: String,
    val numeroPublicidad: String,
    val codVerificacion: String,
    val secReciDetaAtenNac: String,
    val codCertificado: String,
    val bgr: Boolean,
    val codDescarga: String,
    val codLibroOpc: String,
){
    var visaNetResult : VisaNetResult? = null
}

fun PaymentProcess.toPaymentResultEx(
    name: String,
    description: String,
    paymentProcess: PaymentProcess,
): PaymentResult {
    val dateTime = LocalDateTime.now()
        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))
    val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()

    items.add(Pair2(PaymentResultItem("Servicio:", "Solicitud de certificado"), null))
    items.add(Pair2(PaymentResultItem("Resultado de la operación:", "Aprobado y completado con éxito"), null))
    //items.add(Pair2(PaymentResultItem("Número de la tarjeta:", ""), null))
    items.add(Pair2(PaymentResultItem("Nombre / Razón social:", name), null))
    items.add(Pair2(PaymentResultItem("Descripción:", description), null))
    items.add(
        Pair2(
            PaymentResultItem("Número Publicidad:", paymentProcess.numeroPublicidad.emptyOrHyphen()),
            PaymentResultItem("Número Recibo:", paymentProcess.numeroRecibo.emptyOrHyphen())
        )
    )
    items.add(
        Pair2(
            PaymentResultItem("Medio de Pago:", "App Sunarp"),
            PaymentResultItem("Solicitud N°:", paymentProcess.solicitudId.toString().emptyOrHyphen())
        )
    )
    items.add(
        Pair2(
            PaymentResultItem("Fecha / Hora:", dateTime),
            PaymentResultItem("Monto pagado:","S/0.00")
        )
    )
    items.add(
        Pair2(
            PaymentResultItem("Tipo de moneda:", "PEN"),
            PaymentResultItem("Tipo de pago", "EXONERADO")
        )
    )
    items.add(Pair2(PaymentResultItem("Cod. Verificación:", paymentProcess.codVerificacion.emptyOrHyphen()), null))

    return PaymentResult("PAGO EXITOSO", "", Page.SEARCH_BY_NAME, items)
}