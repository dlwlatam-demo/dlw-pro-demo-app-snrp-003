package pe.gob.sunarp.appandroid.data.registration.register

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class RegisterDTO(
    @field:Json(name = "codResult")
    val resultCode: String?,
    @field:Json(name = "msgResult")
    val resultMessage: String?
) : BaseModel() {
    constructor() : this("", "")
}