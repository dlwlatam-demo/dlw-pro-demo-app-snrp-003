package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.CertificateService

@JsonClass(generateAdapter = true)
class CertificateServiceDTO(
    @field:Json(name = "coServ")
    val serviceId: String?,
    @field:Json(name = "coTipoRgst")
    val registrationTypeId: String?,
    @field:Json(name = "deServRgst")
    val description: String?,
) : BaseModel() {
    fun toModel() =
        CertificateService(serviceId.orEmpty(), registrationTypeId.orEmpty(), description.orEmpty())

}