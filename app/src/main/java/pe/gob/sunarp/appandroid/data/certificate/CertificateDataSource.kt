package pe.gob.sunarp.appandroid.data.certificate

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.*
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.certificate.request.PaymentProcessRequest
import pe.gob.sunarp.appandroid.data.certificate.request.SaveProcessRequest
import retrofit2.http.*
import retrofit2.http.Header

interface CertificateDataSource {
    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/registros/juridicos")
    suspend fun getLegalRecords(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<List<LegalRecordDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/tipoCertificado/{type}")
    suspend fun getCertificateTypes(
        @Path("type") guid: String,
        @Query("areaRegId") areaRegId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<CertificateTypeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/oficinaRegistral")
    suspend fun getRegistrationOffices(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<RegistrationOfficeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/servicioScunac/{areaRegId}")
    suspend fun getCertificateServices(
        @Path("areaRegId") areaRegId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<CertificateServiceDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/validaPartidaLiteral/{regPubId}/{oficRegId}/{areaRegId}/{codGrupo}/{tipoPartidaFicha}/{numPart}/{coServ}/{coTipoRgst}")
    suspend fun validateLiteralCertificate(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("codGrupo") codGrupo: String,
        @Path("tipoPartidaFicha") tipoPartidaFicha: String,
        @Path("numPart") numPart: String,
        @Path("coServ") coServ: String,
        @Path("coTipoRgst") coTipoRgst: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<LiteralCertificateDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/getDetalleAsientos/{codZona}/{codOficina}/{codLibro}/{numPartida}/{fichaId}/{tomoId}/{fojaId}/{ofiSARP}/{coServicio}/{coTipoRegis}")
    suspend fun getDetalleAsientos(
        @Path("codZona") codZona: String,
        @Path("codOficina") codOficina: String,
        @Path("codLibro") codLibro: String,
        @Path("numPartida") numPartida: String,
        @Path("fichaId") fichaId: String,
        @Path("tomoId") tomoId: String,
        @Path("fojaId") fojaId: String,
        @Path("ofiSARP") ofiSARP: String,
        @Path("coServicio") coServicio: String,
        @Path("coTipoRegis") coTipoRegis: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<SeatDetailDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/getDetalleAsientosVeh/{codZona}/{codOficina}/{numPartida}/{numPlaca}")
    suspend fun getDetalleAsientosVeh(
        @Path("codZona") codZona: String,
        @Path("codOficina") codOficina: String,
        @Path("numPartida") numPartida: String,
        @Path("numPlaca") numPlaca: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<SeatDetailDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/getDetalleAsientosRMC/{codZona}/{codOficina}/{numPartida}")
    suspend fun getDetalleAsientosRMC(
        @Path("codZona") codZona: String,
        @Path("codOficina") codOficina: String,
        @Path("numPartida") numPartida: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<SeatDetailDTO>>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/calcularPago/{coServicio}/{cantPaginas}")
    suspend fun getPaymentValue(
        @Path("coServicio") coServicio: String,
        @Path("cantPaginas") cantPaginas: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<String>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/guardarSolicitud")
    suspend fun saveProcess(
        @Body request: SaveProcessRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<SaveProcessDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/pagarSolicitud")
    suspend fun paymentProcess(
        @Body request: PaymentProcessRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<PaymentProcessDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL +"/parametros/tipoDocumentos")
    suspend fun getDocumentTypes(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String,
        @Query("tipoPer") tipoPer: String,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<List<DocumentTypeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCRI/{regPubId}/{oficRegId}/{areaRegId}/{numPart}/{tipoCRI}/{codGrupo}/{tipoPartidaFicha}")
    suspend fun validarCri(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("numPart") numPart: String,
        @Path("tipoCRI") tipoCRI: String,
        @Path("codGrupo") codGrupo: String?,
        @Path("tipoPartidaFicha") tipoPartidaFicha: String? = "",
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/guardarSolicitud")
    suspend fun guardarSolicitudCertificate(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Body tipoPartidaFicha: SaveProcessRequest,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<SaveProcessDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/obtenerLibros/{areaRegId}")
    suspend fun getLibrosRegistrales(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Path("areaRegId") areaRegId: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<LibroRegistralDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCargaGravamen/{regPubId}/{oficRegId}/{areaRegId}/{numPart}/{codGrupo}/{codigoLibro}/{tipoBusqueda}")
    suspend fun validarPartidaCargadaDesgravamen(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("numPart") numPart: String,
        @Path("codGrupo") codGrupo: String?,
        @Path("codigoLibro") codigoLibro: String?,
        @Path("tipoBusqueda") tipoBusqueda: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaApoyo/{regPubId}/{oficRegId}/{areaRegId}/{libroArea}/{numPart}/{numPartMP}")
    suspend fun validatePartidaApoyo(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("libroArea") libroArea: String?,
        @Path("numPart") numPart: String,
        @Path("numPartMP") numPartMP: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaxRefNumPart/{regPubId}/{oficRegId}/{areaRegId}/{numPart}")
    suspend fun validaPartidaxRefNumPart(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("numPart") numPart: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/obtenerNumPartida/{tipo}/{numero}/{regPubId}/{oficRegId}/{areaRegId}")
    suspend fun obtenerNumPartida(
        @Path("tipo") tipo: String,
        @Path("numero") numero: String,
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<String>>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/obtenerLibros/{areaRegId}")
    suspend fun getBooks(
        @Path("areaRegId") areaRegId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<BooksByOfficeDTO>>



    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCurador/{regPubId}/{oficRegId}/{areaRegId}/{libroArea}/{numPart}/{tipoPartidaFicha}")
    suspend fun validaPartidaCurador(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("libroArea") libroArea: String,
        @Path("numPart") numPart: String,
        @Path("tipoPartidaFicha") tipoPartidaFicha: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaxRefNumPart/{regPubId}/{oficRegId}/{areaRegId}/{numPart}")
    suspend fun validaPartidaVigenciaDePoder(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("numPart") numPart: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCGV/{regPubId}/{oficRegId}/{areaRegId}/{tipoNumero}/{numPartida}")
    suspend fun validaPartidaCGV(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("tipoNumero") tipoNumero: String,
        @Path("numPartida") numPartida: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCGA/{regPubId}/{oficRegId}/{areaRegId}/{tipoNumero}/{numPartida}")
    suspend fun validaPartidaCGA(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("tipoNumero") tipoNumero: String,
        @Path("numPartida") numPartida: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/publicidad/validaPartidaCGEP/{regPubId}/{oficRegId}/{areaRegId}/{tipoNumero}/{numPartida}")
    suspend fun validaPartidaCGEP(
        @Path("regPubId") regPubId: String,
        @Path("oficRegId") oficRegId: String,
        @Path("areaRegId") areaRegId: String,
        @Path("tipoNumero") tipoNumero: String,
        @Path("numPartida") numPartida: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<PartidaDTO>



}