package pe.gob.sunarp.appandroid.data.login.passrecovery

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PassRecoveryRequest(
    @field:Json(name = "correoDestino")
    val email: String
)