package pe.gob.sunarp.appandroid.data

import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.model.Gender
import pe.gob.sunarp.appandroid.core.model.SimpleAdapterItem
import pe.gob.sunarp.appandroid.ui.alerta.home.adapter.AlertaHomeItem
import pe.gob.sunarp.appandroid.ui.home.menu.adapter.HomeItem
import pe.gob.sunarp.appandroid.ui.services.home.ServiceItem

class Utils {

    companion object {

        fun getHomeItems(): ArrayList<HomeItem> {
            val items = ArrayList<HomeItem>()
            items.add(HomeItem(R.drawable.ic_services, "#e3f1ea", "Servicios",R.id.nav_services))
            items.add(HomeItem(R.drawable.ic_alert, "#efe3e1", "Alerta Registral", R.id.nav_alerta_login))
            items.add(HomeItem(R.drawable.ic_offices, "#efffff", "Nuestras oficinas",  R.id.nav_office))
            items.add(HomeItem(R.drawable.ic_contact, "#fff4db", "Contáctenos", R.id.nav_contact))
            return items
        }

        fun getAlertaHomeItems(): ArrayList<AlertaHomeItem> {
            val items = ArrayList<AlertaHomeItem>()
            items.add(AlertaHomeItem(R.drawable.ic_alerta_retrato, "#D1D1D1", "Cuenta","Usuario de Alerta Registral", R.id.nav_alerta_cuenta))
//            items.add(AlertaHomeItem(R.drawable.ic_alerta_notificacion, "#EFE4E2", "Notificaciones","Alarmas de Alerta Registral"))
            items.add(AlertaHomeItem(R.drawable.ic_alerta_partida, "#efffff", "Partidas", "Alertas de Partidas Registrales", R.id.nav_alerta_partida))
            items.add(AlertaHomeItem(R.drawable.ic_alerta_mandato, "#fff4db", "Mandatos", "Alertas de Mandatos Registrales", R.id.nav_alerta_mandato))
            return items
        }

        fun getServicesItems(): ArrayList<ServiceItem> {
            val items = ArrayList<ServiceItem>()
            items.add(ServiceItem(ServiceOption.CERT_LIT_ADV,"Certificado Literal de Partida", "(Copia literal)", R.drawable.ic_certified_advertising))
            items.add(ServiceItem(ServiceOption.CERT_ADV,"Publicidad Certificada", "(Solicita tu certificado)", R.drawable.ic_certified_advertising))
            items.add(ServiceItem(ServiceOption.VEH_CONS,"Consulta Vehicular", "(Consulta los datos de tu vehiculo)", R.drawable.ic_vehicule_consultation))
            items.add(ServiceItem(ServiceOption.PROP_INQ,"Consulta de Propiedad", "(Conoce el número de la partida registral de tu casa)", R.drawable.ic_property_inquiry))
            items.add(ServiceItem(ServiceOption.FOLLOW_UP,"Síguelo", "(Tu procedimiento de inscripción al detalle)", R.drawable.ic_follow_up))
            items.add(ServiceItem(ServiceOption.APP_STATUS,"Estado de Solicitud de Publicidad", "(Conoce el proceso de la solicitud de información registral)", R.drawable.ic_advertising_application_status))
            items.add(ServiceItem(ServiceOption.LEG_PERS_SEARCH,"Búsqueda de Personas Jurídicas", "", R.drawable.ic_legal_person_search))
            items.add(ServiceItem(ServiceOption.SEARCH_BY_NAME,"Búsqueda por Nombre", "", R.drawable.ic_search_name))
            items.add(ServiceItem(ServiceOption.CONSULT_TIVE,"Consulta TIVe", "(Tarjeta Identificación Vehicular Electrónica)", R.drawable.ic_search_name))
            items.add(ServiceItem(ServiceOption.ALERTA_NORMATIVA,"Alerta Normativa Registral", "", R.drawable.ic_bell_ring))
            return items
        }

        fun getGender(): List<Gender> {
            val genders = ArrayList<Gender>()
            genders.add(Gender("M", "Masculino"))
            genders.add(Gender("F", "Femenino"))
            genders.add(Gender("-", "Prefiero no declararlo"))
            return genders
        }

        fun getCertificateLiteralInformation(): List<SimpleAdapterItem> {
            val items = ArrayList<SimpleAdapterItem>()
            items.add(SimpleAdapterItem("Oficina registral:","LIMA"))
            items.add(SimpleAdapterItem("Partida:","4125415215"))
            items.add(SimpleAdapterItem("Ficha:",""))
            items.add(SimpleAdapterItem("Tomo:",""))
            items.add(SimpleAdapterItem("Folio:",""))
            items.add(SimpleAdapterItem("Dirección:","MIRAFLORES - AV ARMENDARIZ NRO 40"))
            items.add(SimpleAdapterItem("Área registral:","INMUEBLE"))
            items.add(SimpleAdapterItem("Registro de:","Registro de predio"))
            return items
        }

    }
}

enum class ServiceOption {
    CERT_LIT_ADV,
    CERT_ADV,
    VEH_CONS,
    PROP_INQ,
    FOLLOW_UP,
    APP_STATUS,
    LEG_PERS_SEARCH,
    SEARCH_BY_NAME,
    CONSULT_TIVE,
    ALERTA_NORMATIVA,
}