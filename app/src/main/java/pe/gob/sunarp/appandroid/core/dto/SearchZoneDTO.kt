package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.SearchZone

@JsonClass(generateAdapter = true)
data class SearchZoneDTO(
    @field:Json(name = "regPubId")
    val id: String?,
    @field:Json(name = "nombre")
    val name: String?,
) : BaseModel() {
    fun toModel() = SearchZone(id.orEmpty(), name.orEmpty())
}