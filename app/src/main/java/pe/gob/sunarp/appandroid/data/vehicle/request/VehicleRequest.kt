package pe.gob.sunarp.appandroid.data.vehicle.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VehicleRequest(
    @field:Json(name = "nuPlac")
    val plateNumber: String,
    @field:Json(name = "codigoRegi")
    val regCode: String,
    @field:Json(name = "codigoSede")
    val siteCode: String,
    @field:Json(name = "ipAddress")
    val ipAddress: String? = "127.0.0.1",
    @field:Json(name = "idUser")
    val idUser: String? = "APPSNRPANDRO",
    @field:Json(name = "appVersion")
    val appVersion: String? = "3.0",
)