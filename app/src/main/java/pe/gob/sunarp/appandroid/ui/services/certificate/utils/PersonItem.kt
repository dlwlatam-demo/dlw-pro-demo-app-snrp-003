package pe.gob.sunarp.appandroid.ui.services.certificate.utils


data class PersonItem(
    var tipoInter: Int,
    var tipoPart: Int,
    var nombres: String,
    var apPaterno: String,
    var apMaterno: String,
    var razSocial: String
)