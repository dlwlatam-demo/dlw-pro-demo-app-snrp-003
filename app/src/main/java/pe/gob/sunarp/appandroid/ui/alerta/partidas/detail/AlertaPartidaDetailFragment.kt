package pe.gob.sunarp.appandroid.ui.alerta.partidas.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.data.alerta.modelservices.DetailMandatoResponse
import pe.gob.sunarp.appandroid.data.alerta.modelservices.PartidaItemResponse
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaMandatoDetailBinding
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaMandatoDetailItemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class AlertaPartidaDetailFragment :
    BaseFragment<FragmentAlertaMandatoDetailBinding>(FragmentAlertaMandatoDetailBinding::inflate){

    private lateinit var adapterCorreo: ItemDetailMandatoAdapter
    private lateinit var adapterSMS: ItemDetailMandatoAdapter
    private lateinit var partida: PartidaItemResponse
    private val viewModel: AlertaPartidaDetailViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        partida = arguments?.getParcelable("partida")!!
        with(binding) {
            initViewModels()
            adapterCorreo = ItemDetailMandatoAdapter(arrayListOf())
            rvListAlertaCorreo.adapter = adapterCorreo
            rvListAlertaCorreo.isNestedScrollingEnabled = false
            rvListAlertaCorreo.layoutManager = LinearLayoutManager(requireContext())

            adapterSMS = ItemDetailMandatoAdapter(arrayListOf())
            rvListAlertaSms.adapter = adapterSMS
            rvListAlertaSms.isNestedScrollingEnabled = false
            rvListAlertaSms.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(dataCorreo) { setDataCorreo(it) }
            onChange(dataSMS) { setDataSMS(it) }
            getData(partida)
        }
    }

    private fun setDataSMS(data: List<DetailMandatoResponse>) {
        binding.boxListSms.visibility = View.VISIBLE
        adapterSMS.items.addAll(data)
        adapterSMS.notifyDataSetChanged()
    }

    private fun setDataCorreo(data: List<DetailMandatoResponse>) {
        binding.boxListCorreo.visibility = View.VISIBLE
        adapterCorreo.items.addAll(data)
        adapterCorreo.notifyDataSetChanged()
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItemDetailMandatoAdapter(
        var items: MutableList<DetailMandatoResponse> = arrayListOf(),
    ) : RecyclerView.Adapter<ItemDetailMandatoAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutAlertaMandatoDetailItemBinding =
                LayoutAlertaMandatoDetailItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            return ViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item)
        }

        override fun getItemCount() = items.size

        inner class ViewHolder(private val binding: LayoutAlertaMandatoDetailItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: DetailMandatoResponse) = with(binding) {
                tvFecha.text = item.feNoti
                tvDetail.text = item.message
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}