package pe.gob.sunarp.appandroid.core.custom.ui.otp.view

import android.content.Context
import android.os.CountDownTimer
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.onChanged
import pe.gob.sunarp.appandroid.core.custom.ui.otp.handler.SUNOTPHandler
import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.SUNOTPListener
import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.SUNOTPViewInterface
import pe.gob.sunarp.appandroid.core.custom.ui.otp.model.SUNOTPInputType
import pe.gob.sunarp.appandroid.core.custom.ui.otp.model.SUNOTPState
import pe.gob.sunarp.appandroid.databinding.ViewSunOtpBinding

private const val DEFAULT_SECONDS = 30 // Seconds
private const val COUNT_DOWN_INTERVAL = 1000L // Count down interval
private const val MILLIS_FACTOR = 1000L // Count down interval
private const val RESET_SECONDS = 0 // seconds reset
private const val DEFAULT_LENGTH = 4 // Number of entries
private const val DEFAULT_INPUT_TYPE = 0 // 0 = NUMERIC, 1 ALPHANUMERIC

class SUNOTP @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), SUNOTPViewInterface {
    private val binding: ViewSunOtpBinding =
        ViewSunOtpBinding.inflate(LayoutInflater.from(context), this)
    private var length: Int = DEFAULT_LENGTH
    private var inputType: Int = DEFAULT_INPUT_TYPE
    private val handler = SUNOTPHandler(this)
    internal var timer: CountDownTimer? = null
    internal var digits = mutableListOf<SUNDigitTextView>()
    var listener: SUNOTPListener? = null
    var seconds: Int = DEFAULT_SECONDS
        set(value) {
            field = value
            handler.setState(state)
        }
    var state: SUNOTPState = SUNOTPState.DEFAULT
        set(value) {
            field = value
            handler.setState(value)
        }

    init {
        context.withStyledAttributes(attrs, R.styleable.SUNOTP) {
            getInt(R.styleable.SUNOTP_view_length, DEFAULT_LENGTH).let { length = it }
            getInt(R.styleable.SUNOTP_view_seconds, DEFAULT_SECONDS).let { seconds = it }
            getInt(R.styleable.SUNOTP_view_inputType, DEFAULT_INPUT_TYPE).let { inputType = it }
        }

        setupView()
    }

    private fun setupView() {
        setupEditText()
        setupDigits()
        state = SUNOTPState.DEFAULT
    }

    private fun setupEditText() {
        binding.sunOTPEditText.inputType = SUNOTPInputType.get(inputType).inputType
        binding.sunOTPEditText.setOnClickListener {
            if (state == SUNOTPState.ERROR) {
                handler.setState(SUNOTPState.DEFAULT)
                listener?.onResendCode()
            }
        }
        binding.sunOTPEditText.filters = arrayOf(SUNOTPInputType.get(inputType).filter, InputFilter.LengthFilter(length))
        binding.sunOTPEditText.onChanged { s ->
            state = SUNOTPState.FOCUSED
            setDigit(s)

            if (s.length == length) {
                state = SUNOTPState.VALIDATING
                listener?.onComplete(s)
            }
        }
    }

    private fun setupDigits() {
        val params = LinearLayout.LayoutParams(
            resources.getDimensionPixelSize(R.dimen.sun_otp_digit_width),
            resources.getDimensionPixelSize(R.dimen.sun_otp_digit_height)
        )
        val space = resources.getDimensionPixelSize(R.dimen.sun_otp_space_between_digits)
        params.setMargins(space, space, space, space)

        for (index in 0 until length) {
            val sunDigitTextView = SUNDigitTextView(context)
            binding.containerSUNDigitsTextViewLinearLayout.addView(sunDigitTextView, index, params)
            digits.add(sunDigitTextView)
        }
    }

    internal fun playCountDownTimer() {
        timer?.cancel()
        timer = object : CountDownTimer(seconds.toLong() * MILLIS_FACTOR, COUNT_DOWN_INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                val timeCountDown = (millisUntilFinished / MILLIS_FACTOR).toInt()
                handler.updateDefaultText(timeCountDown)
                val timeFormatted = "${(timeCountDown / 60).toString().padStart(2, '0')}:${(timeCountDown % 60).toString().padStart(2, '0')}"
                listener?.onShowCount(timeFormatted)
            }

            override fun onFinish() {
                handler.updateDefaultText(RESET_SECONDS)
            }
        }
        timer?.start()
    }

    private fun setDigit(otp: CharSequence) {
        digits.forEachIndexed { index, digit ->
            digit.text = if (index < otp.length)
                otp[index].toString()
            else
                ""
        }
    }

    fun setOTP(otp: String) {
        binding.sunOTPEditText.setText(otp)
    }

    fun cancel() {
        timer?.cancel()
    }

    override fun onDetachedFromWindow() {
        cancel()
        super.onDetachedFromWindow()
    }

    override fun getComponentView(): SUNOTP = this

    override fun getView(type: Int): ViewSunOtpBinding = binding
}