package pe.gob.sunarp.appandroid.ui.alerta.validatecode

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaValidateCodeBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaValidateCodeFragment : BaseFragment<FragmentAlertaValidateCodeBinding>(FragmentAlertaValidateCodeBinding::inflate) {

    private var countdown_timer: CountDownTimer? = null
    var START_MILLI_SECONDS = 30000L
    var guid: String = ""
    var currentTime: Long = 0L
    private val viewModel: AlertaValidateCodeViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        guid = arguments?.getString("guid", "")?: ""
        initView()
    }

    private fun initView() {
        with(binding) {
            initViewmodel()
            btnResend.setOnClickListener {viewModel.resendCode(guid)}
            startTimer(START_MILLI_SECONDS)
            viewModel.showLoading.observe(viewLifecycleOwner) {
                if (it) loadingContainer.loading.show() else loadingContainer.loading.hide()
            }
            viewModel.isValidateCode.observe(viewLifecycleOwner) {
                it?.let { _ ->
                    findNavController().navigate(
                        R.id.action_alertavalidatecodefragment_to_alertarestablecerpassfragment,
                        bundleOf("guid" to it)
                    )
                }
            }
            etCode.editText?.onChanged { validateFields() }

            actionEnter.setOnClickListener { viewModel.validateCode(etCode.editText?.text.toString(), guid) }

        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (etCode.editText?.text ?: "").toString().length == 8
            ) {
                actionEnter.enable()
            } else {
                actionEnter.disable()
            }
        }
    }

    private fun initViewmodel() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(newGuid) {
                guid = it
                startTimer(START_MILLI_SECONDS)
            }
        }
    }

    private fun startTimer(time_in_seconds: Long) {
        try {
            countdown_timer = object : CountDownTimer(time_in_seconds, 1000) {
                override fun onFinish() {
                    try {
                        if (binding != null && binding.btnResend != null) {
                            binding.btnResend.enable()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onTick(p0: Long) {
                    try {
                        currentTime = p0
                        val minute = (p0 / 1000) / 60
                        val seconds = (p0 / 1000) % 60

                        if (binding != null && binding.tvTimmer != null) {
                            binding.tvTimmer.text = "$minute:$seconds"
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            countdown_timer?.start()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        if (countdown_timer != null) {
            countdown_timer!!.cancel()
        }
        super.onPause()
    }

    override fun onStart() {
        if (currentTime != 0L)
            startTimer(currentTime)
        super.onStart()
    }

    override fun onDestroy() {
        if (countdown_timer != null) {
            countdown_timer!!.cancel()
        }
        super.onDestroy()
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
