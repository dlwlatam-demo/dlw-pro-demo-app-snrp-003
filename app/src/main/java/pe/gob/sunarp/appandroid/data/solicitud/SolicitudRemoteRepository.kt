package pe.gob.sunarp.appandroid.data.solicitud

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.solicitud.model.SolicitudNotiItemResponse
import javax.inject.Inject


class SolicitudRemoteRepository @Inject constructor(
    private val dataSource: SolicitudDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getNotificationes(): DataResult<List<SolicitudNotiItemResponse>> = withContext(dispatcher) {
        when (val result = dataSource.getNotificaciones(
            preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_REQUEST_HOST))) {
            is DataResult.Success -> DataResult.Success(result.data)
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

}