package pe.gob.sunarp.appandroid.core.dto

import android.content.Context
import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter

import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.emptyOrHyphen
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.request.PaymentProcessRequest
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest
import pe.gob.sunarp.appandroid.data.payment.SavePaymentRequest
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem
import java.text.SimpleDateFormat
import java.util.*

@JsonClass(generateAdapter = true)
data class VisaNetResult(
    @field:Json(name = "header")
    val header: Header,
    @field:Json(name = "order")
    val order: Order,
    @field:Json(name = "dataMap")
    val data: Data,
) {
    val format = SimpleDateFormat("yyMMddHHmmSS")
    val currentFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

    fun toNiubisInformation(userId: String): NiubizInformation {
        val newDate: Date? = format.parse(data.transactionDate.orEmpty())

        return NiubizInformation(
            codAccion = data.actionCode.orEmpty(),
            codAutoriza = data.authorizationCode.orEmpty(),
            codtienda = data.merchant.orEmpty(),
            concepto = "",
            decisionCs = "",
            dscCodAccion = data.actionDescription.orEmpty(),
            dscEci = data.eciDescription.orEmpty(),
            eci = data.eci.orEmpty(),
            estado = data.status.orEmpty(),
            eticket = data.uniqueId.orEmpty(),
            fechaYhoraTx = currentFormat.format(newDate ?: Date()),
            idUnico = data.uniqueId.orEmpty(),
            idUser = userId,
            impAutorizado = order.authorizedAmount.toString(),
            nomEmisor = data.brand.orEmpty(),
            numOrden = order.purchaseNumber.orEmpty(),
            numReferencia = data.traceNumber.orEmpty(),
            oriTarjeta = data.cardType.orEmpty(),
            pan = data.card.orEmpty(),
            resCvv2 = data.cvv2.orEmpty(),
            response = "",
            reviewTransaction = "",
            transId = data.transactionId.orEmpty()
        )
    }

    fun toNiubisRequest(userId: String): NiubizRequest {
        val newDate: Date? = format.parse(data.transactionDate.orEmpty())

        return NiubizRequest(
            codAccion = data.actionCode.orEmpty(),
            codAutoriza = data.authorizationCode.orEmpty(),
            codtienda = data.merchant.orEmpty(),
            concepto = "",
            decisionCs = "",
            dscCodAccion = data.actionDescription.orEmpty(),
            dscEci = data.eciDescription.orEmpty(),
            eci = data.eci.orEmpty(),
            estado = data.status.orEmpty(),
            eticket = data.uniqueId.orEmpty(),
            fechaYhoraTx = currentFormat.format(newDate ?: Date()),
            idUnico = data.uniqueId.orEmpty(),
            idUser = userId,
            impAutorizado = order.authorizedAmount.toString(),
            nomEmisor = data.brand.orEmpty(),
            numOrden = order.purchaseNumber.orEmpty(),
            numReferencia = data.traceNumber.orEmpty(),
            oriTarjeta = data.cardType.orEmpty(),
            pan = data.card.orEmpty(),
            resCvv2 = data.cvv2.orEmpty(),
            response = "",
            reviewTransaction = "",
            transId = data.transactionId.orEmpty()
        )
    }

    fun toRequest(requestId: Int, userId: String) = SavePaymentRequest(
        requestId = requestId,
        user = "APPSNRPANDRO",
        detail = data.actionDescription.orEmpty(),
        amount = data.amount.orEmpty(),
        niubizData = this.toNiubisRequest(userId)
    )


    fun toProcessPaymentRequest(
        requestId: Int,
        userId: String,
        costoTotal: String,
        saveProcess: SaveProcess,
        certificateService: CertificateService,
        nuAsieSelect: String,
        imPagiSIR: String,
        nuSecuSIR: String,
        totalPaginasPartidaFicha: Int,
        cantPaginas: Int,
        cantPaginasExon: Int,
        paginasSolicitadas: Int,
        codigoGla: String,
        codLibro: String,
        codCerti: String,
    ): PaymentProcessRequest {
        val newDate: Date? = format.parse(data.transactionDate.orEmpty())

        return PaymentProcessRequest(
            solicitudId = "$requestId",
            costoTotal = costoTotal,
            ip = "",
            usrId = userId,
            codigoGla = codigoGla,
            idUser = userId,
            idUnico = data.uniqueId,
            pan = data.card,
            dscCodAccion = data.actionDescription,
            codAutoriza = data.authorizationCode,
            codtienda = data.merchant,
            numOrden = order.purchaseNumber,
            codAccion = data.actionCode,
            fechaYhoraTx = currentFormat.format(newDate ?: Date()),
            nomEmisor = data.brand,
            oriTarjeta = data.cardType,
            respuesta = "",
            usrKeyId = "",
            transId = data.transactionId,
            eticket = data.uniqueId,
            concepto = "",
            codCerti = codCerti,
            numeroRecibo = saveProcess.numeroRecibo,
            numeroPublicidad = saveProcess.numeroPublicidad,
            codVerificacion = saveProcess.codVerificacion,
            codLibroOpc = saveProcess.codLibroOpc,
            cantPaginas = cantPaginas.toString(),
            cantPaginasExon = cantPaginasExon.toString(),
            paginasSolicitadas = paginasSolicitadas.toString(),
            totalPaginasPartidaFicha = totalPaginasPartidaFicha,
            nuAsieSelectSARP = nuAsieSelect,
            imPagiSIR = imPagiSIR,
            nuSecuSIR = nuSecuSIR,
            codLibro = if (codLibro == "null") {
                ""
            } else { codLibro },
            coServ = certificateService.id,
            coTipoRgst = certificateService.registrationTypeId,
            niubizData = this.toNiubisRequest(userId)
        )
    }

    fun toPaymentResult(
        name: String,
        description: String,
        paymentProcess: PaymentProcess,
    ): PaymentResult {
        val newDate: Date? = format.parse(data.transactionDate.orEmpty())
        val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()

        items.add(Pair2(PaymentResultItem("Servicio:", "Solicitud de certificado"), null))
        items.add(Pair2(PaymentResultItem("Resultado de la operación:", data.actionDescription.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Número de la tarjeta:", data.card.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Nombre / Razón social:", name), null))
        items.add(Pair2(PaymentResultItem("Descripción:", description), null))
        items.add(Pair2(
            PaymentResultItem("Número Publicidad:", paymentProcess.numeroPublicidad.emptyOrHyphen()),
            PaymentResultItem("Número Recibo:", paymentProcess.numeroRecibo.emptyOrHyphen())
        ))
        items.add(Pair2(
            PaymentResultItem("Medio de Pago:", "App Sunarp"),
            PaymentResultItem("Solicitud N°:", paymentProcess.solicitudId.toString().emptyOrHyphen())
        ))
        items.add(Pair2(
            PaymentResultItem("Fecha / Hora:", currentFormat.format(newDate ?: Date())),
            PaymentResultItem("Monto pagado:","S/ ${data.amount}")
        ))
        items.add(Pair2(
            PaymentResultItem("Tipo de moneda:", order.currency.orEmpty()),
            PaymentResultItem("Tipo de pago", data.brand.orEmpty().replace("Tarjeta", "Tarjeta de Crédito"))
        ))
        items.add(Pair2(
            PaymentResultItem("Cod. Verificación:", paymentProcess.codVerificacion.emptyOrHyphen()),
            PaymentResultItem("Pedido N°", data.purchaseNumber.orEmpty().replace("Tarjeta", "Tarjeta de Crédito"))
        ))

        return PaymentResult("PAGO EXITOSO", "", Page.SEARCH_BY_NAME, items)
    }


    fun toPaymentResult(
        context: Context?,
        description: String,
        userInformation: ZoneRegisterInformation?
    ): PaymentResult {
        val newDate: Date? = format.parse(data.transactionDate.orEmpty())
        val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()
        context?.let {
            items.add(
                Pair2(PaymentResultItem(
                    "Servicio:",
                    context.getString(R.string.search_data_by_name)
                ), null)
            )
        }
        items.add(Pair2(PaymentResultItem("Resultado de la operación:", data.actionDescription.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Número de la tarjeta:", data.card.orEmpty()), null))
        items.add(
                Pair2(PaymentResultItem(
                "Nombre / Razón social:",
                userInformation?.getFullname().orEmpty()
            ), null)
        )
        items.add(Pair2(PaymentResultItem("Descripción:", description), null))
        items.add(Pair2(PaymentResultItem("Fecha / Hora:", currentFormat.format(newDate ?: Date())), null))
        items.add(Pair2(PaymentResultItem("Monto pagado:", "S/ ${data.amount}"), null))
        items.add(Pair2(PaymentResultItem("Tipo de moneda:", order.currency.orEmpty()), null))
        items.add(Pair2(PaymentResultItem("Tipo de pago", data.brand.orEmpty()), null))
        items.add(Pair2(
            PaymentResultItem("Pedido N°", data.purchaseNumber.orEmpty().replace("Tarjeta", "Tarjeta de Crédito")), null
        ))
        return PaymentResult("PAGO EXITOSO", "", Page.SEARCH_BY_NAME, items)
    }

    companion object {
        fun toModel(json: String): VisaNetResult? {
            val moshi: Moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<VisaNetResult> = moshi.adapter(VisaNetResult::class.java)
            return adapter.fromJson(json)
        }
    }
}

@JsonClass(generateAdapter = true)
data class Data(
    @field:Json(name = "TERMINAL")
    val terminal: String?,
    @field:Json(name = "BRAND_ACTION_CODE")
    val brandActionCode: String?,
    @field:Json(name = "BRAND_HOST_DATE_TIME")
    val brandHostDateTime: String?,
    @field:Json(name = "TRACE_NUMBER")
    val traceNumber: String?,
    @field:Json(name = "CARD_TYPE")
    val cardType: String?,
    @field:Json(name = "ECI_DESCRIPTION")
    val eciDescription: String?,
    @field:Json(name = "SIGNATURE")
    val signature: String?,
    @field:Json(name = "CARD")
    val card: String?,
    @field:Json(name = "MERCHANT")
    val merchant: String?,
    @field:Json(name = "STATUS")
    val status: String?,
    @field:Json(name = "ACTION_DESCRIPTION")
    val actionDescription: String?,
    @field:Json(name = "ID_UNICO")
    val uniqueId: String?,
    @field:Json(name = "AMOUNT")
    val amount: String?,
    @field:Json(name = "BRAND_HOST_ID")
    val brandHostId: String?,
    @field:Json(name = "AUTHORIZATION_CODE")
    val authorizationCode: String?,
    @field:Json(name = "YAPE_ID")
    val yapeId: String?,
    @field:Json(name = "CURRENCY")
    val currency: String?,
    @field:Json(name = "TRANSACTION_DATE")
    val transactionDate: String?,
    @field:Json(name = "ACTION_CODE")
    val actionCode: String?,
    @field:Json(name = "ECI")
    val eci: String?,
    @field:Json(name = "ID_RESOLUTOR")
    val resolutorId: String?,
    @field:Json(name = "BRAND")
    val brand: String?,
    @field:Json(name = "ADQUIRENTE")
    val adquirente: String?,
    @field:Json(name = "BRAND_NAME")
    val brandName: String?,
    @field:Json(name = "PROCESS_CODE")
    val processCode: String?,
    @field:Json(name = "TRANSACTION_ID")
    val transactionId: String?,
    @field:Json(name = "CVV2_VALIDATION_RESULT")
    val cvv2: String?,
    @field:Json(name = "PURCHASE_NUMBER")
    var purchaseNumber:  String?,
)

@JsonClass(generateAdapter = true)
data class Header(
    @field:Json(name = "ecoreTransactionDate")
    val ecoreTransactionDate: Long?,
    @field:Json(name = "ecoreTransactionUUID")
    val ecoreTransactionUUID: String?,
    @field:Json(name = "millis")
    val millis: Int?,
)

@JsonClass(generateAdapter = true)
data class Order(
    @field:Json(name = "amount")
    val amount: Double?,
    @field:Json(name = "authorizationCode")
    val authorizationCode: String?,
    @field:Json(name = "authorizedAmount")
    val authorizedAmount: Double?,
    @field:Json(name = "currency")
    val currency: String?,
    @field:Json(name = "externalTransactionId")
    val externalTransactionId: String?,
    @field:Json(name = "installment")
    val installment: Int?,
    @field:Json(name = "purchaseNumber")
    val purchaseNumber: String?,
    @field:Json(name = "traceNumber")
    val traceNumber: String?,
    @field:Json(name = "transactionDate")
    val transactionDate: String?,
    @field:Json(name = "transactionId")
    val transactionId: String?,
)