package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CertificateType(
    val areaId: String,
    val serviceId: String,
    val certificateId: String,
    val name: String,
    val preOffice: String,
    val codGrupoLibroArea: String,
    val type: String,
    val descGrupoLibroArea: String,
) : Parcelable{
    override fun toString(): String {
        return name
    }
}