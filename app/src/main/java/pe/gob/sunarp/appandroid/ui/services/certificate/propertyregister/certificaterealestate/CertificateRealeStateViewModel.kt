package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certificaterealestate

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.Partida
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarCriRequest
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateRealeStateViewModel @Inject constructor(
    private val parameterRepository: CertificateRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val openPayment = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val officeSelect = SingleLiveEvent<RegistrationOffice>()
    val estadoPartida = SingleLiveEvent<Partida>()

    fun getParameters() {
        viewModelScope.launch {
            loading.value = true
            when (val result = parameterRepository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validarPartida(request: ValidarCriRequest) {
        viewModelScope.launch {
            loading.value = true
            when (val result = parameterRepository.validarCri(request)) {
                is DataResult.Success -> onValidarPartidaSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    fun onValidarPartidaSuccess(data: Partida) {
        loading.value = false
        estadoPartida.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun selectOfficeForList(itemSelect: RegistrationOffice) {
        officeSelect.value = offices.value!!.find { item ->
            item.oficRegId == itemSelect.oficRegId &&
                    item.regPubId == itemSelect.regPubId
        }
    }

    fun getZoneCode() = officeSelect.value?.regPubId ?: Constants.EMPTY

    fun getCodeOffice() = officeSelect.value?.oficRegId ?: Constants.EMPTY
}