package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group

import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.GenericViewInterface
import pe.gob.sunarp.appandroid.databinding.LayoutTransactionStatusBinding

internal interface StatusInfoInterface : GenericViewInterface<LayoutTransactionStatusBinding> {
    fun getComponentView(): StatusInformationGroup
}