package pe.gob.sunarp.appandroid.ui.alerta.preregister

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaPreRegistroBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaPreRegistroFragment : BaseFragment<FragmentAlertaPreRegistroBinding>(FragmentAlertaPreRegistroBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {



            actionEnter.setOnClickListener {
                findNavController().navigate(R.id.action_alertapreregistrofragment_to_alertaregistrofragment_to, bundleOf("title" to "Bienvenido"))
            }

            webview.settings.setJavaScriptEnabled(true)

            webview.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    url?.let {
                        view?.loadUrl(it)
                    }
                    return true
                }
            }
            webview.loadUrl("https://www.sunarp.gob.pe/alertaregistral/app_ar_terms.html")


        }
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}
