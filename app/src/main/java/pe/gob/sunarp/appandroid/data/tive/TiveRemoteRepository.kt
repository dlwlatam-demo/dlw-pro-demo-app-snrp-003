package pe.gob.sunarp.appandroid.data.tive

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.TiveDocument
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest

class TiveRemoteRepository(
    private val dataSource: TiveDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getSearch(body: TiveSearchRequest): DataResult<TiveDocument> =
        withContext(dispatcher) {

            when (val result = dataSource.getTiveInformation(
                    body,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_TIVE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun saveHistory(body: TiveSearchRequest): DataResult<ServiceResponse> =
        withContext(dispatcher) {

            when (val result = dataSource.saveHistory(
                    body,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_TIVE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getHistory(): DataResult<List<TiveSearchRequest>> =
        withContext(dispatcher) {

            when (val result = dataSource.getHistory(
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_TIVE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

}