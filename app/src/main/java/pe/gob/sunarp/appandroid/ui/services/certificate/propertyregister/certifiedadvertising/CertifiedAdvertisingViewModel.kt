package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certifiedadvertising

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertifiedAdvertisingViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository,
    private val userRegister: UserRegistrationRemoteRepository
) : BaseViewModel() {

    val docTypes = SingleLiveEvent<List<DocumentType>>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val loading = SingleLiveEvent<Boolean>()
    val onFormError = SingleLiveEvent<String>()

    val isValidDocumentDNI = SingleLiveEvent<DocumentDNI>()
    val isValidDocumentCE = SingleLiveEvent<DocumentCE>()

    fun getSpinners() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getDocumentType(typePer : String){
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getDocTypeList(typePer)) {
                is DataResult.Success -> onDocSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    fun validateDocument(documentType: String, documentNumber: String) {
        viewModelScope.launch {
            loading.value = true
            when (documentType) {
                Constants.DOCUMENT_TYPE_DNI -> {
                    when (val result = userRegister.informationDocumentDNI(documentNumber)) {
                        is DataResult.Success -> {
                            isValidDocumentDNI.value = result.data
                            loading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                Constants.DOCUMENT_TYPE_CE -> {
                    when (val result = userRegister.informationDocumentCE(documentNumber)) {
                        is DataResult.Success -> {
                            isValidDocumentCE.value = result.data
                            loading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
            }
        }
    }

    private fun onDocSuccess(data: List<DocumentType>) {
        loading.value = false
        docTypes.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }
}