package pe.gob.sunarp.appandroid.ui.office.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.huawei.hms.maps.common.util.DistanceCalculator.computeDistanceBetween
import com.huawei.hms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.callNumber
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.databinding.FragmentDetailOfficeBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity


@AndroidEntryPoint
class DetailOfficeSearchFragment :
    BaseFragment<FragmentDetailOfficeBinding>(FragmentDetailOfficeBinding::inflate){

    private var info: ListOfficeResultItem? = null
    lateinit var destiny: LatLng
    var myposition: LatLng? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            arguments?.let {
                putTitleToolbar(DetailOfficeSearchFragmentArgs.fromBundle(it).title)
            }
        }
    }

    private fun initViews() {
        info =DetailOfficeSearchFragmentArgs.fromBundle(arguments!!).info
        myposition =DetailOfficeSearchFragmentArgs.fromBundle(arguments!!).myposition
        destiny =DetailOfficeSearchFragmentArgs.fromBundle(arguments!!).destiny
        var adapterInfo: InforDetailOfficeHistoryAdapter
        info?.let { _info ->
            setTittle()
            val items: MutableList<ItemDetail> = mutableListOf()
            items.add(ItemDetail(_info.office.uppercase(), "${_info.type} a ${_info.distance}"))
            items.add(ItemDetail("Dirección", _info.address))
            val horarios: MutableList<String> = mutableListOf()
            _info.attentions.map { tempitem -> horarios.addAll(tempitem.schedules) }
            horarios
                .mapIndexed { i, s ->  ItemDetail("Horario ${i + 1}", s) }
                .forEach { itemDetail ->  items.add(itemDetail) }

            adapterInfo = InforDetailOfficeHistoryAdapter(items)
            with(binding) {
                rvInfo.apply {
                    adapter = adapterInfo
                    layoutManager = LinearLayoutManager(context)
                }

                boxShare.setOnClickListener {
                    val msg = "${_info.office.uppercase()} - ${_info.type}\nDirección: ${_info.address}\nhttp://maps.google.com/maps?daddr=${_info.lat},${_info.lon}"
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_TEXT, msg)
                    startActivity(intent)
                }
                boxRutas.disable()
                boxRutas.backgroundTintList = context?.resources?.getColorStateList(R.color.gray, context?.theme)
                myposition?.let { mypos ->
                    boxRutas.enable()
                    boxRutas.backgroundTintList = context?.resources?.getColorStateList(R.color.white , context?.theme)
                    boxRutas.setOnClickListener {
                        if (validateMin10KM()) {
                            val action = DetailOfficeSearchFragmentDirections.actionDetailOfficeSearchFragmentToOfficeRouteFragment(
                                myposition = mypos,
                                destiny = destiny
                            )
                            findNavController().navigate(action)
                        } else {
                            Toast.makeText(context, "La distancia es mayor a 10km", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                tvPhones.text = _info.phones.first().phone
                boxPhones.setOnClickListener {
                    callNumber(tvPhones.text.toString())
                }
            }
        }


    }

    private fun validateMin10KM(): Boolean = computeDistanceBetween(myposition, destiny)/1000 <= 10

    

    override fun initViewModelsErrorToken() {}

}