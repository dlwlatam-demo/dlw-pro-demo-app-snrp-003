package pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.search

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.model.SearchLegalPerson
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentLegalPersonSearchBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class SearchByLegalPersonFragment :
    BaseFragment<FragmentLegalPersonSearchBinding>(FragmentLegalPersonSearchBinding::inflate) {

    private val viewModel: SearchByLegalPersonViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        with(binding) {
            etAcronym.filterMayus()
            etLegalPerson.filterMayus()
            etLegalPerson.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etAcronym.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etLegalPerson.onChanged { validateFields() }
            checkAllOffices.setOnClickListener {
                hideKeyboard()
                validateFields()
            }
            tilRegisterOffice.setOnClickListener { hideKeyboard() }
            checkAllOffices.setOnCheckedChangeListener { _, isCheck ->
                tilRegisterOffice.isEnabled = !isCheck
                etRegisterOffice.tag = ""
                validateFields()
                etRegisterOffice.clean()
            }
            btnSearch.setOnClickListener {
                hideKeyboard()
                with(viewModel) {
                    searchLegalPerson(getInformationModel())
                }
            }
        }
    }

    fun getInformationModel() = SearchLegalPerson(
        if (binding.checkAllOffices.isChecked) ""
        else (binding.etRegisterOffice.tag ?: "").toString(),
        binding.etLegalPerson.text.toString().uppercase(),
        binding.etAcronym.text.toString().uppercase()
    )

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getZones()
            onChange(offices) { fillOffices(it) }
            onChange(hasData) { goToResult(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun goToResult(result: Boolean) {
        if(result) {
            findNavController().navigate(
                R.id.action_searchByLegalPersonFragment_to_legalPersonSearchFragment,
                bundleOf("information" to getInformationModel())
            )
        } else {
            Toast.makeText(context, "No se encontraron resultados", Toast.LENGTH_SHORT).show()
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(), R.layout.layout_simple_spinner_item, R.id.tv_title, items))
            setOnItemClickListener { parent, _, position, _ ->
                tag = (parent.getItemAtPosition(position) as RegistrationOffice).name
                validateFields()
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            Log.e("Se eligió una oficina", (etRegisterOffice.tag ?: "").toString())
            if ((checkAllOffices.isChecked || (etRegisterOffice.tag ?: "").toString()
                    .isNotEmpty()) && etLegalPerson.text.toString().length >= 2)
                btnSearch.enable()
            else
                btnSearch.disable()

        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}