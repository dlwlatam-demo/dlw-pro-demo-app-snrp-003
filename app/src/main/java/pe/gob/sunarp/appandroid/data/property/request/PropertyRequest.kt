package pe.gob.sunarp.appandroid.data.property.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PropertyRequest(
    @field:Json(name = "apePaterno")
    val firstLastname: String?,
    @field:Json(name = "apeMaterno")
    val secondLastname: String?,
    @field:Json(name = "nombres")
    val names: String?,
    @field:Json(name = "typeDoc")
    val docType: String?,
    @field:Json(name = "numbDoc")
    val docNumber: String?,
    @field:Json(name = "userApp")
    val userApp: String = "APPSNRPANDRO",
    @field:Json(name = "deviceId")
    val deviceId: String = "",
    @field:Json(name = "userCrea")
    val userCrea: String = "55555",
    @field:Json(name = "tipo")
    val type: String = "N",
    @field:Json(name = "razonSocial")
    val ruc: String = "",
)