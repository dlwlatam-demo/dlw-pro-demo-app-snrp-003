package pe.gob.sunarp.appandroid.ui.office.search

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.huawei.hms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.databinding.FragmentSearchDetailOfficeBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class SearchsItemsFragment :
    BaseFragment<FragmentSearchDetailOfficeBinding>(FragmentSearchDetailOfficeBinding::inflate){


    private lateinit var items: Array<ListOfficeResultItem>
    private lateinit var search: String
    private var myposition: LatLng? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        search =SearchsItemsFragmentArgs.fromBundle(arguments!!).search
        items =SearchsItemsFragmentArgs.fromBundle(arguments!!).items
        myposition =SearchsItemsFragmentArgs.fromBundle(arguments!!).myposition
        with(binding) {
            rvResultSearch.apply {
                adapter = ItemSearchOfficeAdapter(items.toList()) {
                    val action = SearchsItemsFragmentDirections
                        .actionSearchsItemsFragmentToDetailOfficeSearchFragment(
                            title = it.type,
                            info = it,
                            myposition = myposition,
                            destiny = LatLng(it.lat.replace(",", "").toDouble(), it.lon.replace(",", "").toDouble())
                        )
                    findNavController().navigate(action)
                }
                layoutManager = LinearLayoutManager(context)
            }
            etSearch.setText(search)
            (rvResultSearch.adapter as ItemSearchOfficeAdapter).filter.filter(search)
            etSearch.addTextChangedListener {
                (rvResultSearch.adapter as ItemSearchOfficeAdapter).filter.filter(it)
            }
            btnSearch.setOnClickListener {
                val searchText = etSearch.text.toString().trim()
                Log.e("...","...")
                (rvResultSearch.adapter as ItemSearchOfficeAdapter).filter.filter(searchText)
            }
        }


    }

    

    override fun initViewModelsErrorToken() {}

}