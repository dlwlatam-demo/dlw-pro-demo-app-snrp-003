package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.data.profile.information.request.UpdateUserInformationRequest

@Parcelize
data class ProfileInformation(
    val gender: String,
    val documentType: String,
    val documentNumber: String,
    val names: String,
    val firstLastname: String,
    val secondLastname: String,
    val birthday: String,
    val mobilePhone: String,

    ) : Parcelable {
    fun toRequest(
        token: String,
        appVersion: String,
        ipAddress: String,
        lastConnection: String,
        latitude: Int,
        longitude: Int
    ) = UpdateUserInformationRequest(
        documentType,
        documentNumber,
        names,
        firstLastname,
        secondLastname,
        birthday,
        gender,
        mobilePhone,
        token,
        appVersion,
        ipAddress,
        lastConnection,
        latitude,
        longitude
    )
}