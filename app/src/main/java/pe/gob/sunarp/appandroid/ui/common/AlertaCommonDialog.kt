package pe.gob.sunarp.appandroid.ui.common

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaCommonBinding


class AlertaCommonDialog : DialogFragment() {

    private var onCancelListener: (() -> Unit)? = null
    private var onAcceptListener: (() -> Unit)? = null

    private var _binding: LayoutAlertaCommonBinding? = null
    private val binding get() = _binding!!
    private var title: String = ""
    private var detalle: String = ""
    private var type: AlertType? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = LayoutAlertaCommonBinding.inflate(LayoutInflater.from(context))
        binding.btnClose.setOnClickListener {
            onCancelListener?.invoke()
            dismiss()
        }
        binding.tvTitle.text = title
        binding.tvMessage.text = detalle

        if (type != null) {
            when(type){
                AlertType.SUCCESS -> {
                    binding.titleContainer.setBackgroundColor(
                        ContextCompat.getColor(requireContext(),
                            R.color.green_500))
                }
                AlertType.ERROR -> {
                    binding.titleContainer.setBackgroundColor(
                        ContextCompat.getColor(requireContext(),
                            R.color.red_boomer))
                }

                AlertType.INFORMATIVE -> {
                    binding.titleContainer.setBackgroundColor(
                        ContextCompat.getColor(requireContext(),
                            R.color.gray_600))
                }
            }

        }

        val dialog = AlertDialog.Builder(requireActivity(), R.style.MyThemeOverlayAlertDialog)
            .setView(binding.root)
            .setCancelable(false)
            .create()
        dialog.window?.setGravity(Gravity.TOP)
        val layoutParams = dialog.window!!.attributes
        layoutParams.y = 170
        dialog.window?.attributes = layoutParams
        return dialog
    }

    fun onCancelListener(listener: () -> Unit) {
        this.onCancelListener = listener
    }

    fun onAcceptListener(listener: () -> Unit) {
        this.onAcceptListener = listener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun setData(title: String, detalle: String?) {
        this.title = title
        this.detalle = detalle.orEmpty()
    }

    fun setAlertType(type: AlertType){
        this.type = type

    }
}

enum class AlertType{
    SUCCESS, ERROR, INFORMATIVE
}