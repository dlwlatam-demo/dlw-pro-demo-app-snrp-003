package pe.gob.sunarp.appandroid.ui.office

import androidx.lifecycle.viewModelScope
import com.huawei.hms.maps.model.Marker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.maps.MapsRemoteRepository
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class OfficeSearchViewModel @Inject constructor(
    private val repository: MapsRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<ListOfficeResultItem>>()
    private val officesAndMarker: MutableList<Pair<Marker,ListOfficeResultItem>> = arrayListOf()

    init {
        offices.value = arrayListOf()
    }

    fun getOffices() {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.getOffices()) {
                is DataResult.Success -> onGetOfficesSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun addOfficeAndMarker(marker: Marker,office: ListOfficeResultItem) {
        officesAndMarker.add(Pair(marker, office))
    }

    fun getOfficeDataForMarker(marker: Marker): ListOfficeResultItem? {
        return officesAndMarker.find { it.first == marker }?.second

    }

    fun getAllOfficeWithDistance(): List<ListOfficeResultItem> {
        return officesAndMarker.map { it.second }
    }

    fun clearMarkers() {
        officesAndMarker.forEach {
            it.first.remove()
        }
        officesAndMarker.clear()
    }

    private fun onGetOfficesSuccess(data: List<ListOfficeResultItem>) {
        loading.value = false
        offices.value = data
    }

}