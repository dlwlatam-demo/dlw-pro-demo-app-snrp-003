package pe.gob.sunarp.appandroid.ui.alerta.home

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject

@HiltViewModel
class AlertaHomeViewModel @Inject constructor(
    private val preferences: SunarpSharedPreferences,
) : ViewModel() {

    val logOutSuccess = SingleLiveEvent<Boolean>()


    fun logOut() {
        preferences.logoutAlert()
    }

}