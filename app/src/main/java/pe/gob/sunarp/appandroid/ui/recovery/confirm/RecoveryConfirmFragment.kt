package pe.gob.sunarp.appandroid.ui.recovery.confirm

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRecoveryConfirmBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class RecoveryConfirmFragment :
    BaseFragment<FragmentRecoveryConfirmBinding>(FragmentRecoveryConfirmBinding::inflate) {

    private val viewModel: RecoveryConfirmViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setObservers()
    }

    private fun setListeners() {

        binding.btnBack.setOnClickListener {
            findNavController().navigate(R.id.action_RecoveryConfirmFragment_to_LoginActivity)
        }

        binding.actionRecoveryConfirm.setOnClickListener { actionConfirm() }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.action_RecoveryConfirmFragment_to_RecoverySendFragment)
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            isConfirmed.observe(viewLifecycleOwner) { goToNextView() }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun actionConfirm() {
        with(binding) {
            viewModel.confirm(
                textInputPassword.editText?.text.toString(),
                textInputPasswordConfirm.editText?.text.toString()
            )
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun goToNextView() {
        findNavController().navigate(R.id.action_RecoveryConfirmFragment_to_LoginActivity)
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}