package pe.gob.sunarp.appandroid.ui.services.property.propertylist

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.DataModel
import pe.gob.sunarp.appandroid.core.model.UserInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.FragmentPropertyListBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.property.propertylist.adapter.PropertyListAdapter

@AndroidEntryPoint
class PropertyListFragment :
    BaseFragment<FragmentPropertyListBinding>(FragmentPropertyListBinding::inflate) {

    private val viewModel: PropertyListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
    }

    private fun initViewModels() {
        val user: UserInformation? =
            arguments?.getParcelable("user")
        with(viewModel) {
            initViewModelsErrorToken()
            user?.let { getPropertyList(it) }
            onChange(onSuccessProperties) { initViews(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun initViews(items: List<DataModel>) {
        val transactionsAdapter = PropertyListAdapter(items)
        binding.rvPropertyList.apply {
            adapter = transactionsAdapter
            layoutManager = LinearLayoutManager(context)
        }
        var totalRegistros = items.size // Usar "size" en lugar de "count()"
        if (items.first().toString().substring(0, 6) == "pe.gob") {
            totalRegistros = totalRegistros - 1
        }
        binding.tvItemCount.text = "Total de registros: $totalRegistros"
        Log.e("regUNico", items.first().toString())
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}