package pe.gob.sunarp.appandroid.data.search.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest

@JsonClass(generateAdapter = true)
data class SearchRequest(
    @field:Json(name = "codZonas")
    val zones: List<String>,
    @field:Json(name = "apPaterno")
    val firstLastname: String,
    @field:Json(name = "apMaterno")
    val secondLastname: String,
    @field:Json(name = "nombre")
    val names: String,
    @field:Json(name = "tipoTitular")
    val tipoTitular: String,
    @field:Json(name = "areaRegistral")
    val areaRegistral: String,
    @field:Json(name = "visanetResponse")
    val visanet: NiubizRequest?,
    @field:Json(name = "razonSocial")
    val ruc: String = "",
    @field:Json(name = "userId")
    val userApp: String = "APPSNRPANDRO",
    @field:Json(name = "derecho")
    val derecho: String? = "",
    @field:Json(name = "sociedad")
    val sociedad: String? = "",
    @field:Json(name = "deviceId")
    val deviceId: String = "Android",
    @field:Json(name = "userCrea")
    val userCrea: String = "55555"

)