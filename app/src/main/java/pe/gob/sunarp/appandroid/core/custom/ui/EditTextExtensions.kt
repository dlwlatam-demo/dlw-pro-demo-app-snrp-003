package pe.gob.sunarp.appandroid.core.custom.ui

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun EditText.hideKeyboard(): Boolean = (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)

fun EditText.onChanged(onChanged: (CharSequence) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            //not implementation needed
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //not implementation needed
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            onChanged(s)
        }
    })
}

fun EditText.clean() {
    this.setText("")
}

fun EditText.disable() {
    focusable = View.NOT_FOCUSABLE
    isEnabled = false
    isClickable = false
    isCursorVisible = false
    isFocusableInTouchMode = false
}

fun EditText.enable() {
    focusable = View.FOCUSABLE
    isEnabled = true
    isClickable = true
    isCursorVisible = true
    isFocusableInTouchMode = true
}