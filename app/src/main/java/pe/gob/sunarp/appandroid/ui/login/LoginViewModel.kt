package pe.gob.sunarp.appandroid.ui.login

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.data.profile.information.ProfileRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: LoginRemoteRepository,
    private val profileRemoteRepository: ProfileRemoteRepository
) :
    BaseViewModel() {
    val isAuthenticated = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun login(username: String, password: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.login(username, password)) {
                is DataResult.Success -> sendData()
                is DataResult.Failure -> sendError(result.exception)
            }
        }
    }

    private fun sendError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private suspend fun sendData() {
        when (val result = profileRemoteRepository.getUserInformation()) {
            is DataResult.Success -> sendProfileData()
            is DataResult.Failure -> sendError(result.exception)
        }
    }

    private fun sendProfileData() {
        isAuthenticated.value = true
        showLoading.value = false
    }
}
