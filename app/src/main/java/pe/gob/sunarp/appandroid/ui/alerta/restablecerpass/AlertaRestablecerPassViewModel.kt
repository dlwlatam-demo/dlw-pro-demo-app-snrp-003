package pe.gob.sunarp.appandroid.ui.alerta.restablecerpass

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaRestablecerPassViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) :
    BaseViewModel() {
    val isConfirmed = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun confirm(password: String, password2: String, guid: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val status = validateForm(password, password2)) {
                is FormStatus.Ok -> {
                    when (val result = repository.restorePassAlerta(guid, password)) {
                        is DataResult.Success -> {
                            isConfirmed.value = result.data
                            showLoading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> onError(Exception(status.error))
            }
        }
    }

    private fun validateForm(password: String, passwordConfirm: String): FormStatus {

        if (!password.isValidString(Constants.REGEX_PASS)) return FormStatus.Ko("No cumple con los requisitos de seguridad: 1 mayúscula, 1 minúscula, 1 número, mínimo 8 caracteres")
        if (password.isEmpty()) return FormStatus.Ko("Olvidó el campo contraseña")
        if (passwordConfirm.isEmpty()) return FormStatus.Ko("Olvidó el campo confirmar contraseña")
        if (passwordConfirm != password) return FormStatus.Ko("Las contraseñas no coinciden")

        return FormStatus.Ok
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }
}
