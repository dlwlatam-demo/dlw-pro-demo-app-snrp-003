package pe.gob.sunarp.appandroid.core.model

import java.io.Serializable

data class DocumentCE(
    val migratoryStatus: String,
    val names: String,
    val number: String,
    val firstLastName: String,
    val secondLastName: String
) : Serializable