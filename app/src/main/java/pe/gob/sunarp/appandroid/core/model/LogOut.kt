package pe.gob.sunarp.appandroid.core.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LogOut (
    @field:Json(name = "resultado")
    val response:String
): BaseModel() {
    constructor() : this("")
}