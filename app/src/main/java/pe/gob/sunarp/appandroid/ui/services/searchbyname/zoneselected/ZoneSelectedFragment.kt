package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselected

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.model.PersonType
import pe.gob.sunarp.appandroid.core.model.RegisterArea
import pe.gob.sunarp.appandroid.core.model.ZoneRegisterInformation
import pe.gob.sunarp.appandroid.core.model.ZonesSelected
import pe.gob.sunarp.appandroid.databinding.FragmentSelectedRegisterZoneBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.common.AlertTerminosDialog
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselected.adapter.ZoneSelectedAdapter
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class ZoneSelectedFragment :
    BaseFragment<FragmentSelectedRegisterZoneBinding>(FragmentSelectedRegisterZoneBinding::inflate),
    PaymentInterface {

    private val viewModel: ZoneSelectedViewModel by viewModels()
    private var ammountToPay: Float = 0.0f
    private var areaSelection: RegisterArea? = null
    private var userInformation: ZoneRegisterInformation? = null
    private var countZones: Int? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        val zones: ZonesSelected? = arguments?.getParcelable("zones")
        with(binding) {
            btnSend.disable()
            etFirstLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                    validateFields()
                }
            }
            etFirstLastname.filterMayus()
            etSecondLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                    validateFields()
                }
            }
            etSecondLastname.filterMayus()
            etNames.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                    validateFields()
                }
            }
            etNames.filterMayus()
            etNames.addTextChangedListener() {
                validateFields()
            }
            etSecondLastname.addTextChangedListener() {
                validateFields()
            }
            etFirstLastname.addTextChangedListener() {
                validateFields()
            }
            checkBoxTerms.setOnCheckedChangeListener { buttonView, isChecked ->
                validateFields()
            }

            tvTerminos.paintFlags = tvTerminos.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            val fm = activity!!.supportFragmentManager
            tvTerminos.setOnClickListener {
                AlertTerminosDialog().apply {
                    setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert)
                    show(fm, this.javaClass.name)
                }
                validateFields()
            }
            rvZones.apply {
                val zonesAdapter = ZoneSelectedAdapter(zones?.items ?: mutableListOf())
                adapter = zonesAdapter
                val manager = LinearLayoutManager(context)
                layoutManager = manager
                countZones = zonesAdapter.itemCount
            }

            btnSend.setOnClickListener {
                val adapter = (rvZones.adapter as ZoneSelectedAdapter)
                val personalType = (etParticipantSearch.tag ?: "").toString()
                val registerId = (etRegisterArea.tag ?: "").toString()
                Log.e("zoneRegister", "")
                Log.e(binding.etParticipantSearch.tag.toString(), "")
                Log.e(personalType, "")
                viewModel.create(
                    information = ZoneRegisterInformation(
                        adapter.getZones(),
                        etFirstLastname.text.toString(),
                        etSecondLastname.text.toString(),
                        etNames.text.toString(),
                        personalType,
                        registerId,
                        areaSelection?.name.orEmpty(),
                        isLegalPerson = binding.etParticipantSearch.tag == Constants.PERSON_LEGAL
                    ),
                    terms = checkBoxTerms.isChecked,
                    ammountToPay = ammountToPay
                )
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getRegisterArea()
            onChange(areas) { fillAreas(it) }
            onChange(personType) { fillPersonType(it) }
            onChange(onVisaConfiguration) { showVisaNet(it) }
            onChange(onFormSuccess) { saveFormSuccess(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun saveFormSuccess(information: ZoneRegisterInformation) {
        userInformation = information
    }

    private fun showVisaNet(item: VisaNetItem) {
        try {
            VisaNet.authorization(activity, item.data, item.custom)
        } catch (e: Exception) {
            Log.i("ERROR", "onClick: " + e.message)
        }
    }

    override fun sendPaymentConfirmation(data: VisaNetResult) {
        val description = "${getString(R.string.search_by_name)} - ${areaSelection?.name}"

        with(binding) {
            val adapter = (rvZones.adapter as ZoneSelectedAdapter)
            val personalType = (etParticipantSearch.tag ?: "").toString()
            val registerId = (etRegisterArea.tag ?: "").toString()
            val niubiz = viewModel.getNiubizInformation(data)
            val useInfo = ZoneRegisterInformation(
                adapter.getZones(),
                etFirstLastname.text.toString(),
                etSecondLastname.text.toString(),
                etNames.text.toString(),
                personalType,
                registerId,
                areaSelection?.name.orEmpty(),
                niubiz
            )
            navigateToResult(true, data.toPaymentResult(context, description, useInfo), useInfo)
        }
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(ok, result.toPayment(result.data.purchaseNumber, Page.SEARCH_BY_NAME), userInformation)
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        zoneRegister: ZoneRegisterInformation?
    ) {
        val action = ZoneSelectedFragmentDirections
            .actionZoneSelectedFragmentToPaymentResultFragment(
                paymentResult = paymentResult,
                information = zoneRegister,
                ok = ok
            )
        findNavController().navigate(action)
    }

    private fun fillPersonType(list: List<PersonType>) {
        binding.etParticipantSearch.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, list))

            setOnItemClickListener { parent, _, position, _ ->
                tag = (parent.getItemAtPosition(position) as PersonType).id
                when (tag) {
                    Constants.PERSON_NATURAL -> {
                        enableNaturalPerson()
                    }

                    Constants.DERECHO_MINERO -> {
                        enableJuridicPerson()
                        binding.etNames.hint = "Derecho Minero"
                    }

                    Constants.SOCIEDAD_MINERA -> {
                        enableJuridicPerson()
                        binding.etNames.hint = "Sociedad Minera"
                    }

                    else -> {
                        enableJuridicPerson()
                    }
                }
                validateFields()
            }
        }
    }

    private fun enableNaturalPerson() {
        with(binding) {
            tilFirstLastname.show()
            tilSecondLastname.show()
            etFirstLastname.enable()
            etFirstLastname.clean()
            etSecondLastname.clean()
            etSecondLastname.enable()
            etNames.hint = getString(R.string.label_names)
            etNames.clean()
        }
    }

    private fun enableJuridicPerson() {
        with(binding) {
            tilFirstLastname.hide()
            etFirstLastname.enable()
            etFirstLastname.clean()
            tilSecondLastname.hide()
            etSecondLastname.clean()
            etSecondLastname.enable()
            etNames.hint = getString(R.string.label_legal_person)
            etNames.clean()
        }
    }

    private fun fillAreas(areas: List<RegisterArea>) {
        with(binding) {
            etRegisterArea.apply {
                clean()
                etParticipantSearch.clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        areas
                    )
                )
                setOnItemClickListener { parent, _, position, _ ->
                    val areaSelected = parent.getItemAtPosition(position) as RegisterArea
                    areaSelection = areaSelected
                    tag = areaSelected.id
                    etNames.clean()
                    etFirstLastname.clean()
                    etSecondLastname.clean()
                    Log.e("contador de zonas", countZones.toString())
                    ammountToPay = areaSelected.price * countZones!!
                    if (etRegisterArea.text.toString() == "Registro Mobiliario de Contratos") {
                        tvTotalCost.text = getString(
                            R.string.total_cost_label,
                            areaSelected.price.formatMoneySunarpNew()
                        ).formatEndText()
                    } else {
                        tvTotalCost.text = getString(
                            R.string.total_cost_label,
                            (areaSelected.price * countZones!!).formatMoneySunarpNew()
                        ).formatEndText()
                    }

                    viewModel.getPersonTypes(areaSelected.id)
                    validateFields()
                }
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }
    private fun validateFields() {
        with(binding) {
            if (etParticipantSearch.tag == Constants.PERSON_NATURAL) {
                if (etNames.text.toString().isNotEmpty() && etFirstLastname.text.toString().isNotEmpty() && etRegisterArea.text.toString().isNotEmpty() && etParticipantSearch.text.toString().isNotEmpty() && checkBoxTerms.isChecked )
                    btnSend.enable()
                else
                    btnSend.disable()
            } else {
                if (etNames.text.toString().isNotEmpty() && etRegisterArea.text.toString().isNotEmpty() && etParticipantSearch.text.toString().isNotEmpty() && checkBoxTerms.isChecked )
                    btnSend.enable()
                else
                    btnSend.disable()
            }


        }
    }
    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}