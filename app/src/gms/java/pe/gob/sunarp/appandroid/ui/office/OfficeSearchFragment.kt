package pe.gob.sunarp.appandroid.ui.office

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.material.card.MaterialCardView
import com.google.maps.android.SphericalUtil
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.databinding.FragmentOfficeSearchBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class OfficeSearchFragment :
    BaseFragment<FragmentOfficeSearchBinding>(FragmentOfficeSearchBinding::inflate),
    OnMapReadyCallback {
    private var myMarker: Marker? = null
    private var isFirstLocation = true
    private var lasLocation: Location? = null
    private val viewModel: OfficeSearchViewModel by viewModels()
    protected var mypopupWindow: PopupWindow? = null
    lateinit var btn_ofi_registral: MaterialCardView
    lateinit var btn_ofi_receptora: MaterialCardView
    lateinit var btn_sede_central: MaterialCardView
    lateinit var btn_tribunal_registral: MaterialCardView
    lateinit var btn_zona_registral: MaterialCardView
    lateinit var btn_oficina_otros: MaterialCardView
    lateinit var btn_todos: MaterialCardView

    private var map: GoogleMap? = null
    private var fusedLocationProvider: FusedLocationProviderClient? = null
    private val locationRequest: LocationRequest = LocationRequest.create().apply {
        interval = 10000
        fastestInterval = 2000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        maxWaitTime = 60
    }

    var isGPS = false

    override fun setTittle() {
        putTitleToolbar("Nuestras Oficinas")
    }

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val locationList = locationResult.locations
            if (locationList.isNotEmpty()) {
                //The last location in the list is the newest
                lasLocation = locationList.last()
                goLocation(lasLocation, isFirstLocation)
                if (isFirstLocation) {
                    viewModel.getOffices()
                }
                gpsMapOption(true)
                isFirstLocation = false
                setLoading(false)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        with(binding) {
            boxDetail.hide()
        }
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationProvider?.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
            with(binding) {
                etSearch.setText("")
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            gpsMapOption(false)
            fusedLocationProvider?.removeLocationUpdates(locationCallback)
        }
    }

    private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                AlertDialog.Builder(requireContext())
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission, please accept to use location functionality")
                    .setPositiveButton(
                        "OK"
                    ) { _, _ ->
                        requestLocationPermission()
                    }
                    .create()
                    .show()
            } else {
                requestLocationPermission()
            }
        }
        /*
        else {
            checkBackgroundLocation()
        }
         */
    }
/*
    private fun checkBackgroundLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestBackgroundLocationPermission()
        }
    }
*/
    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
            ),
            MY_PERMISSIONS_REQUEST_LOCATION
        )
    }

    private fun requestBackgroundLocationPermission() {
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ),
                MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION
            )
        } else {
            */
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_LOCATION
            )
        //}
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        fusedLocationProvider?.requestLocationUpdates(
                            locationRequest,
                            locationCallback,
                            Looper.getMainLooper()
                        )
                        // checkBackgroundLocation()
                    }
                } else {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION
                        )
                    ) {
                        startActivity(
                            Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", requireActivity().packageName, null),
                            ),
                        )
                    }
                }
                return
            }
            /*
            MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        fusedLocationProvider?.requestLocationUpdates(
                            locationRequest,
                            locationCallback,
                            Looper.getMainLooper()
                        )
                    }
                }
                return
            }
            */
        }
    }

    companion object {
        private const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        private const val MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION = 66
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        validateGps()
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setLoading(true)
        initViews()
        initViewModels()

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(requireActivity())
        checkLocationPermission()
        activity?.registerReceiver(GPSCheck(object : GPSCheck.LocationCallBack {
            override fun turnedOn() {
                Log.e("GpsTurn", "turnedOn: ON")
                isGPS = true
            }

            override fun turnedOff() {
                Log.e("GpsTurn", "turnedOn: OFF")
                isGPS = false
                GpsTurnOn()
            }
        }), IntentFilter(LocationManager.MODE_CHANGED_ACTION))
    }

    private fun validateGps() {
        context?.let {
            val mLocationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if(!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                GpsTurnOn()
            } else {
                Log.e("validateGps.GpsTurn", "turnedOn: OFF")
            }
        }
    }

    fun GpsTurnOn() {
        activity?.let { act ->
            val locationRequest: LocationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = 10 * 1000
            locationRequest.fastestInterval = 10 * 1000 / 2

            val locationSettingsRequestBuilder = LocationSettingsRequest.Builder()
            locationSettingsRequestBuilder.addLocationRequest(locationRequest)
            locationSettingsRequestBuilder.setAlwaysShow(true)

            val settingsClient: SettingsClient = LocationServices.getSettingsClient(act)
            val task: Task<LocationSettingsResponse> = settingsClient.checkLocationSettings(( locationSettingsRequestBuilder.build()))
            task.addOnSuccessListener(act) {
                isGPS = true
            }
            task.addOnFailureListener(act) { e ->
                isGPS = false
                if (e is ResolvableApiException) {
                    try {
                        val res: ResolvableApiException = e
                        res.startResolutionForResult(act, 666)
                    } catch (sendIntentException: IntentSender.SendIntentException) {
                        sendIntentException.printStackTrace()
                    }
                }
            }
        }
    }

    fun isActiveGps() {
        with (binding) {
            boxDetail.enableRoutes(true)
            btnPosition.enable()
        }
    }

    fun isFailActiveGps() {
        viewModel.getOffices()
        with (binding) {
            boxDetail.enableRoutes(false)
            btnPosition.disable()
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        gpsMapOption(isGPS)
        map?.setOnMarkerClickListener { marker ->
            viewModel.getOfficeDataForMarker(marker)?.let {
                showDetailInMap(it, marker.position)
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 16F))
            }
            true
        }
        map?.isIndoorEnabled = false
        if (isFirstLocation) {
            val peruBounds = LatLngBounds(
                LatLng(-18.724136, -82.606662),
                LatLng(1.641182, -67.837554),
            )
            map?.moveCamera(CameraUpdateFactory.newLatLngBounds(peruBounds, 0))
        }
    }

    private fun gpsMapOption(isGPS: Boolean) {
        map?.apply {
            //isMyLocationEnabled = isGPS
            uiSettings.isMyLocationButtonEnabled = isGPS
        }
    }

    private fun showDetailInMap(officeDataForMarker: ListOfficeResultItem, position: LatLng) {
        with(binding) {
            myMarker?.let {
                val distance = SphericalUtil.computeDistanceBetween(it.position, position)
                if (distance > 999.99) {
                    officeDataForMarker.distance = String.format("%.2f", distance/1000) + "km"
                } else {
                    officeDataForMarker.distance = String.format("%.2f", distance) + "m"
                }
            }
            boxDetail.setInfo(officeDataForMarker, {
                val action = OfficeSearchFragmentDirections
                    .actionOfficeSearchFragmentToDetailOfficeSearchFragment(
                        title = officeDataForMarker.type,
                        info = officeDataForMarker,
                        myposition = myMarker?.position,
                        destiny = position
                    )
                findNavController().navigate(action)
            }, {
                val msg = "${it.office.uppercase()} - ${it.type}\nDirección: ${it.address}\nhttp://maps.google.com/maps?daddr=${it.lat},${it.lon}"
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, msg)
                startActivity(intent)
            } , {
                myMarker?.let {
                    if (validateMin10KM(position)) {
                        val action =
                            OfficeSearchFragmentDirections.actionOfficeSearchFragmentToOfficeRouteFragment(
                                myposition = it.position,
                                destiny = position
                            )
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(context, "La distancia es mayor a 10km", Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    private fun validateMin10KM(position: LatLng): Boolean {
        var result = false
        myMarker?.let {
            val distance = SphericalUtil.computeDistanceBetween(it.position, position)/1000
            result = distance <= 10
        }
        return result
    }

    private fun goLocation(location: Location?, moveMarker: Boolean = false) {
        myMarker?.remove()
        val mylocation = LatLng(location!!.latitude, location.longitude)
        val mark = MarkerOptions()
            .position(mylocation)
            .icon(getBitmapFromVector(R.drawable.my_marker))
        myMarker = map?.addMarker(mark)
        if (moveMarker)
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 16F))
    }

    private fun initViews() {
        setPopUpWindow()
        with(binding) {
            btnPosition.setOnClickListener {
                lasLocation?.let {
                    goLocation(it, moveMarker = true)
                }
            }
            btnFilter.setOnClickListener {
                mypopupWindow!!.showAsDropDown(it, -it.width*3, -(it.height*6.3).toInt())
            }
            btnSearch.setOnClickListener {
                val action = OfficeSearchFragmentDirections
                    .actionOfficeSearchFragmentToSearchsItemsFragment(
                        search = etSearch.text.toString().trim(),
                        items = viewModel.getAllOfficeWithDistance().toTypedArray(),
                        myposition = myMarker?.position,
                    )
                findNavController().navigate(action)
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { setLoading(it) }
            onChange(offices) { showOffices(it) }
        }
    }

    private fun showOffices(dataList: List<ListOfficeResultItem>) {
        map?.let { mapok ->
            viewModel.clearMarkers()
            dataList.forEach {
                val position = LatLng(it.lat.replace(",", "").toDouble(), it.lon.replace(",", "").toDouble())
                mapok.addMarker(
                    MarkerOptions()
                        .position(position)
                        .icon(when(it.type) {
                            Constants.MAP_OFFICE_TYPE_OFICINA_REGISTRAL -> getBitmapFromVector(R.drawable.ic_office_type_of_registral)
                            Constants.MAP_OFFICE_TYPE_OFICINA_RECEPTORA -> getBitmapFromVector(R.drawable.ic_office_type_of_receptora)
                            Constants.MAP_OFFICE_TYPE_SEDE_CENTRAL -> getBitmapFromVector(R.drawable.ic_office_type_sede_central)
                            Constants.MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL -> getBitmapFromVector(R.drawable.ic_office_type_tribunal_registral)
                            Constants.MAP_OFFICE_TYPE_ZONA_REGISTRAL -> getBitmapFromVector(R.drawable.ic_office_type_zona_registral)
                            Constants.MAP_OFFICE_TYPE_OFICINA_OTRAS -> getBitmapFromVector(R.drawable.ic_office_type_oficina_otras)
                            else -> getBitmapFromVector(R.drawable.ic_office_type_oficina_otras)
                        })
                )?.apply {
                    if(myMarker != null) {
                        val distance = SphericalUtil.computeDistanceBetween(this.position, myMarker?.position)
                        if (distance > 999.99) {
                            it.distance = String.format("%.2f", distance/1000) + "km"
                        } else {
                            it.distance = String.format("%.2f", distance) + "m"
                        }
                    } else {
                        it.distance = null
                    }
                    viewModel.addOfficeAndMarker(this, it)
                }
            }
        }
    }

    private fun getBitmapFromVector(@DrawableRes vectorResourceId: Int): BitmapDescriptor {
        val vectorDrawable = ResourcesCompat.getDrawable(resources, vectorResourceId, null)
            ?: return BitmapDescriptorFactory.defaultMarker()
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun setLoading(it: Boolean) {
        binding.loadingContainer.apply {
            if (it) loading.show() else loading.hide()
        }
    }

    private fun setPopUpWindow() {
        val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.layout_menu_filter_map, null)
        btn_ofi_registral = view.findViewById(R.id.btn_ofi_registral)
        btn_ofi_receptora = view.findViewById(R.id.btn_ofi_receptora)
        btn_sede_central = view.findViewById(R.id.btn_sede_central)
        btn_tribunal_registral = view.findViewById(R.id.btn_tribunal_registral)
        btn_zona_registral = view.findViewById(R.id.btn_zona_registral)
        btn_oficina_otros = view.findViewById(R.id.btn_oficina_otros)
        btn_todos = view.findViewById(R.id.btn_todos)
        btn_ofi_registral.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_OFICINA_REGISTRAL)
            filterOffice(Constants.MAP_OFFICE_TYPE_OFICINA_REGISTRAL)
        }
        btn_ofi_receptora.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_OFICINA_RECEPTORA)
            filterOffice(Constants.MAP_OFFICE_TYPE_OFICINA_RECEPTORA)
        }
        btn_sede_central.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_SEDE_CENTRAL)
            filterOffice(Constants.MAP_OFFICE_TYPE_SEDE_CENTRAL)
        }
        btn_tribunal_registral.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL)
            filterOffice(Constants.MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL)
        }
        btn_zona_registral.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_ZONA_REGISTRAL)
            filterOffice(Constants.MAP_OFFICE_TYPE_ZONA_REGISTRAL)
        }
        btn_oficina_otros.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_OFICINA_OTRAS)
            filterOffice(Constants.MAP_OFFICE_TYPE_OFICINA_OTRAS)
        }
        btn_todos.setOnClickListener {
            selectCard(Constants.MAP_OFFICE_TYPE_ALL)
            filterOffice(Constants.MAP_OFFICE_TYPE_ALL)
        }
        btn_todos.setCardBackgroundColor(resources.getColor(R.color.gray,null))
        mypopupWindow = PopupWindow(view,
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            true)
    }

    private fun selectCard(type: String) {
        btn_ofi_registral.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_ofi_receptora.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_sede_central.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_tribunal_registral.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_zona_registral.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_oficina_otros.setCardBackgroundColor(resources.getColor(R.color.white,null))
        btn_todos.setCardBackgroundColor(resources.getColor(R.color.white,null))
        when(type) {
            Constants.MAP_OFFICE_TYPE_OFICINA_REGISTRAL -> btn_ofi_registral.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            Constants.MAP_OFFICE_TYPE_OFICINA_RECEPTORA -> btn_ofi_receptora.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            Constants.MAP_OFFICE_TYPE_SEDE_CENTRAL -> btn_sede_central.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            Constants.MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL -> btn_tribunal_registral.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            Constants.MAP_OFFICE_TYPE_ZONA_REGISTRAL -> btn_zona_registral.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            Constants.MAP_OFFICE_TYPE_OFICINA_OTRAS -> btn_oficina_otros.setCardBackgroundColor(resources.getColor(R.color.gray,null))
            else -> btn_todos.setCardBackgroundColor(resources.getColor(R.color.gray,null))
        }
    }

    private fun filterOffice(typeOffice: String) {
        mypopupWindow?.dismiss()
        viewModel.offices.value?.let {list ->
            showOffices(
                if (typeOffice != Constants.MAP_OFFICE_TYPE_ALL) list.filter { it.type == typeOffice }
                else list
            )
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}