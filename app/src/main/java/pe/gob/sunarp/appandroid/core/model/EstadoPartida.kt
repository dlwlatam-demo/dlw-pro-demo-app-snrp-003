package pe.gob.sunarp.appandroid.core.model

data class EstadoPartida(
    val estado: Int?,
    val msj: String?,
    val refNumPart: String?,
    val refNumPartMP: String?,
    val codigoGla: String?,
    val codigoLibro: String?,
    val numPartida: String?,
    val numPlaca: String?,
)