package pe.gob.sunarp.appandroid.ui.welcome.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import javax.inject.Inject
import android.util.Log

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val repository: SharedPreferenceRepository,
    private val repositoryLogin: LoginRemoteRepository
) : ViewModel() {

    val skipBoarding = SingleLiveEvent<Boolean>()
    val okValidateVersion = SingleLiveEvent<Boolean>()

    fun skipOnBoarding() {
        viewModelScope.launch {
            skipBoarding.value = repository.getOnboardingPreference()
        }
    }

    fun validateVersion() {
        viewModelScope.launch {
            when (val result = repositoryLogin.getLastVersionApp()) {
                is DataResult.Success -> {
                    if (result == null) {
                        okValidateVersion.value = false
                    }
                    else {
                        okValidateVersion.value = result.data
                    }
                }
                is DataResult.Failure -> okValidateVersion.value = false
            }
        }

    }
}