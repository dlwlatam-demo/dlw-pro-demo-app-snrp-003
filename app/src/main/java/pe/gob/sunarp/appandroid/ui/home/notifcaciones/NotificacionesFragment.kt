package pe.gob.sunarp.appandroid.ui.home.notifcaciones

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.data.solicitud.model.SolicitudNotiItemResponse
import pe.gob.sunarp.appandroid.databinding.FragmentNotificacionesBinding
import pe.gob.sunarp.appandroid.databinding.LayoutNotificacionItemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class NotificacionesFragment :
    BaseFragment<FragmentNotificacionesBinding>(FragmentNotificacionesBinding::inflate){

    private lateinit var adapter: ItenNotificacionAdapter
    private val viewModel: NotificacionesViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        with(binding) {
            adapter = ItenNotificacionAdapter( arrayListOf()) {
                findNavController().navigate(R.id.action_notificacionesFragment_to_transactionStatusFragment2,
                    bundleOf("transId" to it.solicitudId))
            }
            rvNotificationList.adapter = adapter
            rvNotificationList.isNestedScrollingEnabled = false
            rvNotificationList.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(listData) { setDataList(it) }
            getListNotificaciones()
        }
    }

    private fun setDataList(it: List<SolicitudNotiItemResponse>) {
        adapter.items.addAll(it)
        adapter.notifyDataSetChanged()
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItenNotificacionAdapter(var items: MutableList<SolicitudNotiItemResponse> = arrayListOf(), private val clickDetail: (item: SolicitudNotiItemResponse) -> Unit) :
        RecyclerView.Adapter<ItenNotificacionAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutNotificacionItemBinding =
                LayoutNotificacionItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item, clickDetail)
        }

        override fun getItemCount() = items.size

        inner class ViewHolder(private val binding: LayoutNotificacionItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: SolicitudNotiItemResponse, clickFuntion: (item: SolicitudNotiItemResponse) -> Unit) = with(binding) {
                tvBusqueda.text = item.strBusq
                tvFecha.text = item.fecHor
                tvEstado.text = item.estado
                container.setOnClickListener { clickFuntion(item) }
            }
        }


    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}