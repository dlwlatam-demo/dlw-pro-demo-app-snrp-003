package pe.gob.sunarp.appandroid.ui.home.menu.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.databinding.LayoutHomeItemBinding

class HomeAdapter(
    private val items: List<HomeItem>
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutHomeItemBinding =
            LayoutHomeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutHomeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HomeItem) = with(binding) {
            with(item) {
                viewItemBackground.setBackgroundColor(Color.parseColor(color))
                imageViewItemIcon.setImageResource(icon)
                textViewItemTitle.text = title
                homeItem.setOnClickListener {
                    item.navigation?.let { itemView.findNavController().navigate(it) }
                }
            }
        }
    }
}