package pe.gob.sunarp.appandroid.ui.transactions.payment.result.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PaymentResult(
    val title: String,
    val description: String,
    val sourcePage: Page,
    val items: List<Pair2<PaymentResultItem, PaymentResultItem?>>
) : Parcelable

@Parcelize
data class PaymentResultItem(
    val label: String,
    val description: String,
) : Parcelable

enum class Page {
    TRANSACTIONS,
    SEARCH_BY_NAME,
    VEHICLE_INFORMATION,
    CERTIFICATE,
}

data class Pair2<out A : Parcelable, out B : Parcelable?>(
    val first: A,
    val second: B
) : Parcelable {
    private val ff: A = first
    private val ss: B = second
    constructor(parcel: Parcel) : this(
        parcel.readValue(Parcelable::class.java.classLoader) as A,
        parcel.readValue(Parcelable::class.java.classLoader) as B
    )
    /**
     * Returns string representation of the [Pair] including its [first] and [second] values.
     */
    public override fun toString(): String = "($first, $second)"
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(ff)
        dest.writeValue(ss)
    }
    override fun describeContents(): Int {
        return 0
    }
    companion object CREATOR : Parcelable.Creator<Pair2<Parcelable, Parcelable>> {
        override fun createFromParcel(parcel: Parcel): Pair2<Parcelable, Parcelable> {
            return Pair2(parcel)
        }
        override fun newArray(size: Int): Array<Pair2<Parcelable, Parcelable>?> {
            return arrayOfNulls(size)
        }
    }
}