package pe.gob.sunarp.appandroid.data.certificate.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest

@JsonClass(generateAdapter = true)
class PaymentProcessRequest(
    @field:Json(name = "solicitudId")
    val solicitudId: String?,
    @field:Json(name = "costoTotal")
    val costoTotal: String?,
    @field:Json(name = "ip")
    val ip: String?,
    @field:Json(name = "usrId")
    val usrId: String?,
    @field:Json(name = "codigoGla")
    val codigoGla: String?,
    @field:Json(name = "idUser")
    val idUser: String?,
    @field:Json(name = "idUnico")
    val idUnico: String?,
    @field:Json(name = "pan")
    val pan: String?,
    @field:Json(name = "dscCodAccion")
    val dscCodAccion: String?,
    @field:Json(name = "codAutoriza")
    val codAutoriza: String?,
    @field:Json(name = "codtienda")
    val codtienda: String?,
    @field:Json(name = "numOrden")
    val numOrden: String?,
    @field:Json(name = "codAccion")
    val codAccion: String?,
    @field:Json(name = "fechaYhoraTx")
    val fechaYhoraTx: String?,
    @field:Json(name = "nomEmisor")
    val nomEmisor: String?,
    @field:Json(name = "oriTarjeta")
    val oriTarjeta: String?,
    @field:Json(name = "respuesta")
    val respuesta: String?,
    @field:Json(name = "usrKeyId")
    val usrKeyId: String?,
    @field:Json(name = "transId")
    val transId: String?,
    @field:Json(name = "eticket")
    val eticket: String?,
    @field:Json(name = "concepto")
    val concepto: String?,
    @field:Json(name = "codCerti")
    val codCerti: String?,
    @field:Json(name = "numeroRecibo")
    val numeroRecibo: String?,
    @field:Json(name = "numeroPublicidad")
    val numeroPublicidad: String?,
    @field:Json(name = "codVerificacion")
    val codVerificacion: String?,
    @field:Json(name = "codLibroOpc")
    val codLibroOpc: String?,
    @field:Json(name = "cantPaginas")
    val cantPaginas: String?,
    @field:Json(name = "cantPaginasExon")
    val cantPaginasExon: String?,
    @field:Json(name = "paginasSolicitadas")
    val paginasSolicitadas: String?,
    @field:Json(name = "totalPaginasPartidaFicha")
    val totalPaginasPartidaFicha: Int,
    @field:Json(name = "nuAsieSelectSARP")
    val nuAsieSelectSARP: String,
    @field:Json(name = "imPagiSIR")
    val imPagiSIR: String,
    @field:Json(name = "nuSecuSIR")
    val nuSecuSIR: String,
    @field:Json(name = "codLibro")
    val codLibro: String?,
    @field:Json(name = "coServ")
    val coServ: String?,
    @field:Json(name = "coTipoRgst")
    val coTipoRgst: String?,
    @field:Json(name = "visanetResponse")
    val niubizData: NiubizRequest
){

    @field:Json(name = "lstOtorgantes")
    var lstOtorgantes: String = Constants.EMPTY

    @field:Json(name = "lstApoyos")
    var lstApoyos: String = Constants.EMPTY

    @field:Json(name = "lstCuradores")
    var lstCuradores: String = Constants.EMPTY

    @field:Json(name = "lstPoderdantes")
    var lstPoderdantes: String = Constants.EMPTY

    @field:Json(name = "lstApoderados")
    var lstApoderados: String = Constants.EMPTY
}