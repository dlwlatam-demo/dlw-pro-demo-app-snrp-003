package pe.gob.sunarp.appandroid.ui.alerta.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.data.Utils
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaHomeBinding
import pe.gob.sunarp.appandroid.ui.alerta.home.adapter.AlertaHomeAdapter
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaHomeFragment : BaseFragment<FragmentAlertaHomeBinding>(FragmentAlertaHomeBinding::inflate) {

    private val viewModel: AlertaHomeFragmentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = AlertaHomeAdapter(Utils.getAlertaHomeItems())
        binding.rvHome.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }
    }

    override fun onDestroyView() {
        viewModel.logOut()
        super.onDestroyView()
    }

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }

}