package pe.gob.sunarp.appandroid.data.profile.information.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class ProfileDTO(
    @field:Json(name = "usuarioLogin")
    val userData: UserDTO?,
) : BaseModel() {
    constructor() : this(UserDTO("", "", "", "", "", "", "", "", "", "", "", "", ""))
}

