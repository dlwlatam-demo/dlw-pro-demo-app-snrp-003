package pe.gob.sunarp.appandroid.data.payment

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.PaymentKeys
import pe.gob.sunarp.appandroid.data.payment.dto.PaymentDataDTO
import pe.gob.sunarp.appandroid.data.payment.dto.PaymentResultDTO
import retrofit2.http.*
import pe.gob.sunarp.appandroid.data.Constants
interface PaymentDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/parametros/visaKeys")
    suspend fun getPaymentKeys(
        @Body request: PaymentRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
    ): DataResult<PaymentDataDTO>


    @Headers("Content-Type: text/plain")
    @GET
    suspend fun getVisanetToken(
        @Url tokenUrl: String,
        @Header("Authorization") authorization: String
    ): DataResult<String?>


    @Headers("Content-Type: application/json")
    @POST
    suspend fun getVisanetPinHash(
        @Url pinHashUrl: String,
        @Header("Authorization") authorization: String
    ): DataResult<PaymentDataDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/solicitud/nextCount")
    suspend fun getTransactionID (
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<String>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/solicitud/pagar/liquidacion")
    suspend fun savePayment(
        @Body request: SavePaymentRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<PaymentResultDTO>


}