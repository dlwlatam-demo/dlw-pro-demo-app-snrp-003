package pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.CertificateService
import pe.gob.sunarp.appandroid.core.model.LiteralCertificate
import pe.gob.sunarp.appandroid.core.model.LiteralCertificateRequest
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateLiteralDepartmentViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val services = SingleLiveEvent<List<CertificateService>>()
    val literalCertificate = SingleLiveEvent<List<LiteralCertificate>>()

    fun getSpinners(type: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
            when (val result = repository.getCertificateService(type)) {
                is DataResult.Success -> onCertificateServicesSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validateLiteralCertificate(request: LiteralCertificateRequest) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.validateLiteralCertificate(request)) {
                is DataResult.Success -> onLiteralCertificateSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onLiteralCertificateSuccess(data: List<LiteralCertificate>) {
        loading.value = false
        literalCertificate.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    private fun onCertificateServicesSuccess(data: List<CertificateService>) {
        loading.value = false
        services.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}