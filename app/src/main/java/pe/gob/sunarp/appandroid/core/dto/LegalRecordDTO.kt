package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.LegalRecord

@JsonClass(generateAdapter = true)
data class LegalRecordDTO(
    @field:Json(name = "areaRegId")
    val id: String?,
    @field:Json(name = "nombre")
    val name: String?,
) : BaseModel() {
    fun toModel() = LegalRecord(id.orEmpty(),name.orEmpty())
}