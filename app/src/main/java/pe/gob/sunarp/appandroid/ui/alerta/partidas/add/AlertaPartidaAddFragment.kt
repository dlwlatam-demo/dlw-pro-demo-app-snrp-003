package pe.gob.sunarp.appandroid.ui.alerta.partidas.add

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.filterOnlyMayus
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.data.alerta.modelservices.BuscarPartidaItemResponse
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaPartidaAddBinding
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaPartidaEncontradaItemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment


@AndroidEntryPoint
class AlertaPartidaAddFragment :
    BaseFragment<FragmentAlertaPartidaAddBinding>(FragmentAlertaPartidaAddBinding::inflate){

    private val viewModel: AlertaPartidaAddViewModel by viewModels()
    private lateinit var adapterPartidas: ItemPartidaEncontradaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (binding.rvListPartidas.isVisible) {
                    binding.rvListPartidas.visibility = View.GONE
                    binding.searchContainer.visibility = View.VISIBLE
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            initViewModels()
            etPlaca.filterOnlyMayus()
            etDocumento.onChanged { validateForm() }
            etFolioFoja.onChanged { validateForm() }
            etPlaca.onChanged { validateForm() }
            btnAccept.setOnClickListener {
                etDocumento.clearFocus()
                val numeroText = etDocumento.text.toString()

                if (numeroText.matches(Regex("[0-9]+"))) {
                    etDocumento.setText(numeroText.padStart(8, '0'))
                } else {
                }
                hideKeyboard()
                viewModel.searchPartida(
                    etDocumento.text.toString(),
                    etFolioFoja.text.toString(),
                    etPlaca.text.toString().replace("-", "").toUpperCase()
                )
            }
            adapterPartidas =
                ItemPartidaEncontradaAdapter(arrayListOf()) { partidaItem ->
                    viewModel.addPartida( partidaItem, etDocumento.text.toString() )
                }
            rvListPartidas.adapter = adapterPartidas
            rvListPartidas.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(offices) { setListenerOffice(it) }
            onChange(tipoPartidas) { setTipoPartidas(it) }
            onChange(tipoBusquedas) { setTipoBusquedas(it) }
            onChange(partidasBuscadas) { showPartidasEncontradas(it) }
            onChange(addPartidaSuccess) { addPartidaSuccess(it) }
            getParameters()
        }
    }

    private fun addPartidaSuccess(result: Boolean) {
        if (result) {
            findNavController().navigateUp()
        }
    }

    private fun showPartidasEncontradas(listadata: List<BuscarPartidaItemResponse>) {
        binding.rvListPartidas.visibility = View.VISIBLE
        binding.searchContainer.visibility = View.GONE
        adapterPartidas.addItems(listadata)
    }

    private fun setTipoPartidas(items: List<ItemSpinner>) {
        with(binding) {
            etTipo.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    hideKeyboard()
                    etPlaca.clean()
                    etDocumento.clean()
                    etFolioFoja.clean()
                    viewModel.setSelectTipoPartida(item)
                    viewModel.getTipoBusqueda()
                    if(item.id == "24000") {
                        boxVehicular.visibility = View.VISIBLE
                        boxNormal.visibility = View.GONE
                    } else {
                        boxVehicular.visibility = View.GONE
                        boxNormal.visibility = View.VISIBLE
                    }
                    validateForm()
                }
            }
        }
    }

    private fun setTipoBusquedas(items: List<ItemSpinner>) {
        with(binding) {
            etSubtype.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    viewModel.setSelectTipoBusqueda(item)
                    hideKeyboard()
                    etDocumento.clean()
                    etFolioFoja.clean()
                    etDocumento.hint = item.name
                    etDocumento.visibility= View.VISIBLE
                    etFolioFoja.visibility = if (item.id == "T") View.VISIBLE else View.GONE
                    etDocumento.inputType = if (item.id == "S") InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD else InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                    validateForm()
                }
            }
        }
    }

    private fun setListenerOffice(items: List<RegistrationOffice>) {
        with(binding) {
            etAgencia.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as RegistrationOffice)
                    this.tag = item.regPubId + item.oficRegId
                    viewModel.setSelectOficinaRegistral(item)
                    validateForm()
                }
            }
        }
    }

    private fun validateForm() {
        with(binding) {
            with(viewModel) {
                if (
                    (etAgencia.tag ?: "").toString().isNotEmpty() &&
                    (
                        if (tipoPartidaSelect.value != null && tipoPartidaSelect.value?.id == "24000")
                            etPlaca.text.toString().isNotEmpty()
                        else etDocumento.text.toString().isNotEmpty()
                    ) &&
                    (
                        if (tipoBusquedaSelect.value != null && tipoBusquedaSelect.value?.id == "T")
                            etFolioFoja.text.toString().isNotEmpty()
                        else true
                    )
                ) {
                    btnAccept.enable()
                } else {
                    btnAccept.disable()
                }
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItemPartidaEncontradaAdapter(var items: MutableList<BuscarPartidaItemResponse> = arrayListOf(), val clickToAdd: (item: BuscarPartidaItemResponse) -> Unit) :
        RecyclerView.Adapter<ItemPartidaEncontradaAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutAlertaPartidaEncontradaItemBinding =
                LayoutAlertaPartidaEncontradaItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item, clickToAdd)
        }

        override fun getItemCount() = items.size

        fun addItems(listadata: List<BuscarPartidaItemResponse>) {
            items.clear()
            items.addAll(listadata)
            notifyDataSetChanged()
        }

        inner class ViewHolder(private val binding: LayoutAlertaPartidaEncontradaItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: BuscarPartidaItemResponse, addFunction: (item: BuscarPartidaItemResponse) -> Unit) = with(binding) {
                tvTitle.text = item.descripcion
                tvPartida.text = item.numPartida
                imgAdd.setOnClickListener { addFunction(item) }
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}