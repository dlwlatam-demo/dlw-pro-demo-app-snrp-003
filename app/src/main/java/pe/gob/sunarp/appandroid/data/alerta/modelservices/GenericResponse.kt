package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GenericResponse(

	@Json(name="codResult")
	val codResult: String? = null,

	@Json(name="msgResult")
	val msgResult: String? = null,

	@Json(name="guid")
	val guid: String? = "",

)
