package pe.gob.sunarp.appandroid.ui.recovery.send

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RecoverySendViewModel @Inject constructor(private val repository: LoginRemoteRepository) :
    BaseViewModel() {

    val isSent = SingleLiveEvent<Boolean>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun send(email: String) {
        viewModelScope.launch {
            when (val status = validateForm(email)) {
                is FormStatus.Ok -> {
                    showLoading.value = true
                    when (val result = repository.requestRecoveryCode(email)) {
                        is DataResult.Success -> {
                            isSent.value = true
                            //onError(Exception(result.data))
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> {
                    onError(Exception(status.error))
                }
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private fun validateForm(password: String): FormStatus {
        if (password.isEmpty()) return FormStatus.Ko("Olvidó el campo contraseña")
        return FormStatus.Ok
    }
}