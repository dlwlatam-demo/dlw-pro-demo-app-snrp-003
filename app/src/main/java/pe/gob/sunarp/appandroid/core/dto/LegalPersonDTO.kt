package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.LegalPerson

@JsonClass(generateAdapter = true)
data class LegalPersonDTO(
    @field:Json(name = "razon")
    val legalPerson: String?,
    @field:Json(name = "siglas")
    val acronym: String?,
    @field:Json(name = "oficina")
    val office: String?,
    @field:Json(name = "partida")
    val request: String?,
    @field:Json(name = "codigoRegi")
    val registerCode: String?,
    @field:Json(name = "codigoSede")
    val siteCode: String?,
) : BaseModel() {
    fun toModel() = LegalPerson(
        legalPerson.orEmpty(),
        acronym.orEmpty(),
        office.orEmpty(),
        request.orEmpty(),
        registerCode.orEmpty(),
        siteCode.orEmpty()
    )
}