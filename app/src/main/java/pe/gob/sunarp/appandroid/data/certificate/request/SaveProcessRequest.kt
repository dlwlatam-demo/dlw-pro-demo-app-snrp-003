package pe.gob.sunarp.appandroid.data.certificate.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SaveProcessRequest(
    @field:Json(name = "codCerti")
    val codCerti: String?,
    @field:Json(name = "codArea")
    val codArea: String?,
    @field:Json(name = "codLibro")
    var codLibro: String?,
    @field:Json(name = "oficinaOrigen")
    val oficinaOrigen: String?,
    @field:Json(name = "partida")
    val partida: String?,
    @field:Json(name = "ficha")
    val ficha: String?,
    @field:Json(name = "placa")
    val placa: String?,
    @field:Json(name = "tpoPersona")
    val tpoPersona: String?,
    @field:Json(name = "apePaterno")
    val apePaterno: String?,
    @field:Json(name = "apeMaterno")
    val apeMaterno: String?,
    @field:Json(name = "nombre")
    val nombre: String?,
    @field:Json(name = "razSoc")
    val razSoc: String?,
    @field:Json(name = "tpoDoc")
    val tpoDoc: String?,
    @field:Json(name = "numDoc")
    val numDoc: String?,
    @field:Json(name = "email")
    val email: String?,
    @field:Json(name = "asiento")
    var asiento: String?,
    @field:Json(name = "costoServicio")
    val costoServicio: String?,
    @field:Json(name = "cantPaginas")
    val cantPaginas: String?,
    @field:Json(name = "cantPaginasExon")
    val cantPaginasExon: String?,
    @field:Json(name = "paginasSolicitadas")
    val paginasSolicitadas: String?,
    @field:Json(name = "ipRemote")
    val ipRemote: String?,
    @field:Json(name = "sessionId")
    val sessionId: String?,
    @field:Json(name = "usrId")
    val usrId: String = "APPSNRPANDRO",
) {
    @field:Json(name = "tipPerPN")
    var tipPerPN: String? = ""

    @field:Json(name = "apePatPN")
    var apePatPN: String? = ""

    @field:Json(name = "apeMatPN")
    var apeMatPN: String? = ""

    @field:Json(name = "nombPN")
    var nombPN: String? = ""

    @field:Json(name = "razSocPN")
    var razSocPN: String? = ""

    @field:Json(name = "tipoDocPN")
    var tipoDocPN: String? = ""

    @field:Json(name = "numDocPN")
    var numDocPN: String? = ""

    @field:Json(name = "refNumPart")
    var refNumPart: String? = ""

    @field:Json(name = "tomo")
    var tomo: String? = ""

    @field:Json(name = "folio")
    var folio: String? = ""

    @field:Json(name = "refNumPartMP")
    var refNumPartMP: String? = ""

    @field:Json(name = "numAsientoMP")
    var numAsientoMP: String? = ""

    @field:Json(name = "numPartidaMP")
    var numPartidaMP: String? = ""

    @field:Json(name = "tipPerVP")
    var tipPerVP: String? = ""

    @field:Json(name = "apePateVP")
    var apePateVP: String? = ""

    @field:Json(name = "apeMateVP")
    var apeMateVP: String? = ""

    @field:Json(name = "nombVP")
    var nombVP: String? = ""

    @field:Json(name = "razSocVP")
    var razSocVP: String? = ""

    @field:Json(name = "cargoApoderado")
    var cargoApoderado: String? = ""

    @field:Json(name = "datoAdic")
    var datoAdic: String? = ""

    @field:Json(name = "formaEnvio")
    var formaEnvio: String? = "V"

    @field:Json(name = "expediente")
    var expediente: String? = ""

    @field:Json(name = "nomEmbarcacion")
    var nomEmbarcacion: String? = ""
}