package pe.gob.sunarp.appandroid.core

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.icu.text.NumberFormat
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.text.*
import android.text.style.StyleSpan
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.ui.common.DatePickerFragment
import pe.gob.sunarp.appandroid.ui.login.LoginActivity
import java.io.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Pattern
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import android.text.InputFilter
import android.text.InputType

fun <T> Fragment.viewLifecycle(): ReadWriteProperty<Fragment, T> =
    object : ReadWriteProperty<Fragment, T>, DefaultLifecycleObserver {

        private var binding: T? = null
        private var viewLifecycleOwner: LifecycleOwner? = null

        init {
            this@viewLifecycle
                .viewLifecycleOwnerLiveData
                .observe(this@viewLifecycle) { newLifecycleOwner ->
                    viewLifecycleOwner?.lifecycle?.removeObserver(this)
                    viewLifecycleOwner = newLifecycleOwner.also {
                        it.lifecycle.addObserver(this)
                    }
                }
        }

        override fun onDestroy(owner: LifecycleOwner) {
            super.onDestroy(owner)
            binding = null
        }

        override fun getValue(
            thisRef: Fragment,
            property: KProperty<*>
        ): T {
            return this.binding!!
        }

        override fun setValue(
            thisRef: Fragment,
            property: KProperty<*>,
            value: T
        ) {
            this.binding = value
        }
    }

fun Fragment.navigateTo(id: Int) = this.findNavController().navigate(id)
fun Fragment.navigateUp(): Boolean = this.findNavController().navigateUp()

fun Context.startActivity(cls: Class<*>?) {
    cls?.let {
        try {
            val intent = Intent(this, cls)
            startActivity(intent)
        } catch (ignored: ClassNotFoundException) {
            error("Class not found: $packageName.${cls.simpleName}")
        }
    }
}

fun <T> Fragment.onChange(event: SingleLiveEvent<T>, action: (T) -> Unit) {
    event.observe(this) { action.invoke(it) }
}

fun Fragment.startActivity(cls: Class<*>?) = requireContext().startActivity(cls)
fun Context.toast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
fun View.snack(text: String?, indefinite: Boolean = false) = text?.let {
    Snackbar.make(
        this,
        it,
        if (indefinite) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_SHORT
    ).show()
}

fun Fragment.toast(text: String) = requireContext().toast(text)
fun Fragment.finishActivity() = requireActivity().finish()

fun SharedPreferences.getBoolean(key: String): Boolean {
    return getBoolean(key, false)
}

fun SharedPreferences.putBoolean(key: String, value: Boolean) {
    edit().putBoolean(key, value).apply()
}

fun SharedPreferences.getString(key: String): String? {
    return getString(key, null)
}

fun SharedPreferences.putString(key: String, value: String?) {
    edit().putString(key, value).apply()
}

fun String.formatText(): CharSequence {
    if (!TextUtils.isEmpty(this)) {
        val index = this.indexOf(':')

        if (index > 0) {
            val sb = SpannableStringBuilder(this)
            sb.setSpan(
                StyleSpan(Typeface.BOLD),
                0,
                index + 1,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )
            return SpannableString(sb)
        }
    }
    return ""
}

fun String.orHyphen(): String {
    return this.ifEmpty { "-" }
}

fun String?.emptyOrHyphen(): String {
    return if (this != null && this.isNotEmpty()) this else "-"
}

fun Fragment.callNumber(phone: String) {
    val phoneCorrect = phone.replace("(", "").replace(")", "").replace(" ", "")
    val uri = "tel:$phoneCorrect"
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse(uri)
    startActivity(intent)
}

fun String.formatEndText(): CharSequence {
    if (!TextUtils.isEmpty(this)) {
        val index = this.indexOf(':')

        if (index > 0) {
            val sb = SpannableStringBuilder(this)
            sb.setSpan(
                StyleSpan(Typeface.BOLD),
                index + 1,
                this.length,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE
            )
            return SpannableString(sb)
        }
    }
    return ""
}
fun EditText.filterOnlyMayus() {
    this.filters += InputFilter.AllCaps()
    this.filters += InputFilter { source, start, end, dest, dstart, dend ->

        //val result = source.toList().filter {!this.tag.toString().contains("" + it) }
        val allowedCharacters = this.tag.toString() + "-" // Agregar el carácter "-" a los caracteres permitidos
        val result = source.toList().filter { !allowedCharacters.contains("" + it) }
        if (result.isNotEmpty()) {
            ""
        } else null
    }
    this.setOnFocusChangeListener { v, hasFocus ->
        if(!hasFocus) {
            try {
                if(text.length <= 8 && isNumeric(text.toString()) && text.isNotEmpty()) {
                    val padded = ("00000000$text").substring(text.length)
                    this.setText(padded)
                }
            }catch (ex: Exception) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
}

fun EditText.filterMayus() {
    this.filters += InputFilter.AllCaps()

    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
}

fun EditText.filterOnlyPartida() {
    val maxLength = 8

    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            try {
                if (text.length <= 8 && isNumericNew(text.toString()) && text.isNotEmpty()) {
                    val padded = text.toString().padStart(8, '0')
                    setText(padded)
                }
            } catch (ex: Exception) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
}
fun EditText.filterOnlyBusinessName() {
    val maxLength = 8

    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            try {
                if (text.length <= 8 && isNumericNew(text.toString()) && text.isNotEmpty()) {
                    val padded = text.toString().padStart(8, '0')
                    setText(padded)
                }
            } catch (ex: Exception) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
}
fun EditText.filterOnlyMayusLiteral() {
    val maxLength = 10

    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            try {
                if (text.length <= 8 && isNumericNew(text.toString()) && text.isNotEmpty()) {
                    val padded = text.toString().padStart(8, '0')
                    setText(padded)
                }
            } catch (ex: Exception) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
}

fun EditText.filterOnlyMayusLiteralPlaca() {
    val maxLength = 7

    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            try {
                if (text.length <= 8 && isNumericNew(text.toString()) && text.isNotEmpty()) {
                    val padded = text.toString().padStart(8, '0')
                    setText(padded)
                }
            } catch (ex: Exception) {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
}

fun EditText.filterMaxLengthAsiento() {
    val maxLength = 5 // Definimos la longitud máxima como 5 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    // Configuramos el tipo de entrada del EditText
    this.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
}

fun EditText.filterMaxLengthCargoDatoAdicional() {
    val maxLength = 200 // Definimos la longitud máxima como 200 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter.AllCaps()

    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
}
fun EditText.filterMaxLengthDNI() {
    val maxLength = 8 // Definimos la longitud máxima como 8 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }
}

fun EditText.filterMaxLengthRUC() {
    val maxLength = 11 // Definimos la longitud máxima como 11 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    // Configuramos un listener para cuando el EditText pierde el foco
    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) { // Si pierde el foco
            //Toast.makeText(context, "error", Toast.LENGTH_SHORT).show() // Mostramos un mensaje de error
        }
    }
}

fun EditText.filterMaxLengthCel() {
    val maxLength = 9 // Definimos la longitud máxima como 9 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))

    // Configuramos un listener para cuando el EditText pierde el foco
    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) { // Si pierde el foco
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show() // Mostramos un mensaje de error
        }
    }
}
fun EditText.filterMaxLengthCE() {
    val maxLength = 9 // Definimos la longitud máxima como 9 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    // Configuramos un listener para cuando el EditText pierde el foco
    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) { // Si pierde el foco
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show() // Mostramos un mensaje de error
        }
    }
}

fun EditText.filterMaxLengthOthers() {
    val maxLength = 20 // Definimos la longitud máxima como 20 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    // Configuramos un listener para cuando el EditText pierde el foco
    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) { // Si pierde el foco
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show() // Mostramos un mensaje de error
        }
    }
}

fun EditText.filterMaxLengthEleven() {
    val maxLength = 11 // Definimos la longitud máxima como 11 caracteres

    // Agregamos filtros al EditText para que cumpla con ciertas condiciones
    this.filters += arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(maxLength))
    this.filters += InputFilter { source, _, _, _, _, _ ->
        val allowedCharacters = "[A-Za-z0-9]+"
        if (source.matches(allowedCharacters.toRegex())) {
            null
        } else {
            ""
        }
    }

    // Configuramos un listener para cuando el EditText pierde el foco
    this.setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) { // Si pierde el foco
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show() // Mostramos un mensaje de error
        }
    }
    this.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
}

fun EditText.clearFiltersAndListeners() {
    filters = emptyArray() // Elimina todos los filtros
    setOnFocusChangeListener(null) // Elimina el listener onFocusChangeListener
}

fun isNumericNew(text: String): Boolean {
    return text.matches("-?\\d+".toRegex())
}

fun isNumeric(toCheck: String): Boolean {
    return toCheck.all { char -> char.isDigit() }
}

fun getHost(host: String): String? {
    return if (BuildConfig.BUILD_TYPE == "mock") null else host
}

fun String.toTitle(): String {
    return "$this:"
}

fun String.toBearer(): String {
    return "Bearer $this"
}

fun String.isValidString(pattern: String): Boolean =
    Pattern.compile(pattern).matcher(this).matches()

fun Context.requestPermission(requestCode: Int = 0, vararg permission: String) {
    val activity = this as? Activity?
    activity?.let { requestPermissions(it, permission, requestCode) }
}

fun Context.checkCallPermission(): Boolean {
    return isPermissionGranted(Manifest.permission.CALL_PHONE)
}

fun Context.requestCallPermission(requestCode: Int = 0) {
    requestPermission(requestCode, Manifest.permission.CALL_PHONE)
}

fun Context.checkWritePermission(): Boolean {
    return when (Build.VERSION.SDK_INT) {
        in 1..32 -> isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        else -> isPermissionGranted(Manifest.permission.READ_MEDIA_IMAGES)
    }
}

fun Context.requestWritePermission(requestCode: Int = 0) {
    when (Build.VERSION.SDK_INT) {
        in 1..32 ->
            requestPermission(requestCode, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        else ->
            requestPermission(requestCode, Manifest.permission.READ_MEDIA_IMAGES)
    }
}

fun Activity.shouldShowRationale(permission: String) =
    ActivityCompat.shouldShowRequestPermissionRationale(this, permission)

fun Context.isPermissionGranted(permission: String): Boolean {
    return ContextCompat
        .checkSelfPermission(this, permission) ==
        PackageManager.PERMISSION_GRANTED
}

fun Context.cameraPermissionGranted(): Boolean {
    return when (Build.VERSION.SDK_INT) {
        in 1..32 -> isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                isPermissionGranted(Manifest.permission.CAMERA)

        else -> isPermissionGranted(Manifest.permission.CAMERA)
    }
}

fun Context.requestCameraPermission(activity: Activity, requestCode: Int = 0) {
    return when (Build.VERSION.SDK_INT) {
        in 1..32 ->
        requestPermissions(
            activity,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
            requestCode
        )
        else ->
        requestPermissions(
            activity,
            arrayOf(Manifest.permission.CAMERA),
            requestCode
        )
    }

    //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
}

fun Context.requestPhonePermission(activity: Activity, start: () -> Unit, requestCode: Int = 0) {
    val permission = Manifest.permission.CALL_PHONE
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
        start.invoke()
        return
    }
    if (isPermissionGranted(permission)) {
        start.invoke()
    } else {
        requestPermissions(
            activity,
            arrayOf(permission),
            requestCode
        )
    }
}

fun createImageChooserIntent(title: String): Intent? {
    return try {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"
        val pickIntent =
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.setDataAndType(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            "image/*"
        )
        val chooserIntent = Intent.createChooser(getIntent, title)
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))
        chooserIntent
    } catch (e: Exception) {
        Log.e("ERROR", e.toString())
        null
    }
}

@Throws(IOException::class)
fun Context.createImageFile(): File {
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "Sunarp_$timeStamp"
    val storageDir = File(this.filesDir, "images")
    if (!storageDir.exists()) storageDir.mkdirs()
    return File.createTempFile(imageFileName, ".jpeg", storageDir)
}

fun Uri.createImage(
    activity: Activity,
    authority: String,
    imageFile: File,
    imageMaxSize: Int,
    imageQuality: Int = 100,
    fileMaxSize: Int = 1000000
): Uri? {
    try {
        val bitmap: Bitmap = this.getBitmap(activity)
        var width: Int = bitmap.width
        var height: Int = bitmap.height
        if (width > imageMaxSize || height > imageMaxSize) {
            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = imageMaxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = imageMaxSize
                width = (height * bitmapRatio).toInt()
            }
        }
        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, false)
        val out = FileOutputStream(imageFile)
        val sampleImageQuality = if (scaledBitmap.byteCount > fileMaxSize) imageQuality else 100
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, sampleImageQuality, out)
        out.flush()
        out.close()
        bitmap.recycle()
        scaledBitmap.recycle()
        return FileProvider.getUriForFile(activity, authority, imageFile)
    } catch (e: Exception) {
        e.printStackTrace()
        return null
    }
}
fun Uri.decodeSampledBitmapFromUri(contentResolver: ContentResolver, reqWidth: Int, reqHeight: Int): Bitmap? {
    return try {
        // Abrimos un flujo de entrada desde la URI
        val inputStream: InputStream? = contentResolver.openInputStream(this)

        // Si el flujo de entrada es válido, continuamos
        if (inputStream != null) {
            // Primero, obtenemos las dimensiones reales de la imagen sin cargarla en memoria
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeStream(inputStream, null, options)

            // Calculamos inSampleSize para redimensionar la imagen
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

            // Cerramos el flujo de entrada y lo volvemos a abrir para decodificar la imagen
            inputStream.close()
            val newInputStream: InputStream = contentResolver.openInputStream(this)!!

            // Decodificamos la imagen con inSampleSize configurado
            options.inJustDecodeBounds = false
            BitmapFactory.decodeStream(newInputStream, null, options)
        } else {
            null
        }
    } catch (e: Exception) {
        Log.e("ImageUtils", "Error al decodificar la imagen: ${e.message}")
        null
    }
}

private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {
        val halfHeight = height / 2
        val halfWidth = width / 2

        while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}
fun Uri.getBitmap(activity: Activity): Bitmap =
    BitmapFactory.decodeStream(activity.contentResolver?.openInputStream(this))

fun Uri.getBase64(activity: Activity) = this.getBitmap(activity).encodeToString()

fun String.toBitmap(): Bitmap? {
    return if (this.isNotEmpty()) {
        val decodedString: ByteArray = Base64.decode(this, Base64.DEFAULT)
        BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    } else null
}

fun String.encodeToString(): String =
    Base64.encodeToString(this.toByteArray(), Base64.NO_WRAP)

fun Bitmap.encodeToString(quality: Int = 100): String {
    val byteArrayOutputStream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream)
    val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(byteArray, Base64.NO_WRAP)
}

fun String.decodeToBytes(): ByteArray = Base64.decode(this, Base64.DEFAULT)

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.writeToFile(data: String) {
    try {
        val outputStreamWriter =
            OutputStreamWriter(this.openFileOutput("base64.txt", Context.MODE_PRIVATE))
        outputStreamWriter.write(data)
        outputStreamWriter.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

// TODO: validate accuracy
fun String.isValidAge(age: Int): Boolean {
    val birthdayLocalDate =
        SimpleDateFormat(Constants.PATTERN_DATE).parse(this)?.toInstant()?.atZone(
            ZoneId.systemDefault()
        )?.toLocalDate()
    val currentLocalDate = LocalDate.now(ZoneId.systemDefault())
    val years = Period.between(birthdayLocalDate, currentLocalDate).years
    return years >= age
}

fun Fragment.showDatePicker(date: String?) {
    val calendar = Calendar.getInstance()
    date?.let {
        val dateItems = date.split("-")
        if (dateItems.size >= 3) {
            calendar[Calendar.MONTH] = dateItems[1].toInt() - 1
            calendar[Calendar.DAY_OF_MONTH] = dateItems[2].toInt()
            calendar[Calendar.YEAR] = dateItems[0].toInt()
        }
    }
    DatePickerFragment(
        this,
        calendar
    ).show(
        requireActivity().supportFragmentManager,
        this.tag
    )
}
fun Fragment.closeSession() {
    activity?.getSharedPreferences("SunarpApp", Context.MODE_PRIVATE)?.edit()
        ?.clear()?.apply()
    startActivity(LoginActivity::class.java)
    activity?.finishAffinity()
}

fun authorization(username: String, password: String): String =
    "Basic " + "$username:$password".encodeToString()

fun Double?.formatMoneySunarp(): String {
    return String.format("S/.%.2f", this)
}
fun Float?.formatMoneySunarp(): String {
    return String.format("S/.%.2f", this)
}
fun Double?.formatMoneySunarpNew(): String {
    return String.format("S/%.2f", this)
}
fun Float?.formatMoneySunarpNew(): String {
    return String.format("S/%.2f", this)
}
fun String?.formatMoneySunarp(): String {
    val parsed = this.orEmpty().toDouble()
    return NumberFormat.getCurrencyInstance(Locale("ES", "PE")).format(parsed)
}
fun String.formatGender(): String = if (this == "M") { "Masculino"
} else if (this == "F") {
    "Femenino"
} else ""

fun String.formatDate(): String =
    try {
        val firstApiFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val date = LocalDate.parse(this, firstApiFormat)

        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        date.format(formatter)
    } catch (e: Exception) {
        ""
    }
fun String.formatDateGetYear(): String =
    try {
        val firstApiFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        LocalDate.parse(this, firstApiFormat).year.toString()
    } catch (e: Exception) {
        ""
    }

fun String.formatDateToSendServer(): String =
    try {
        val firstApiFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val date = LocalDate.parse(this, firstApiFormat)

        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        date.format(formatter)
    } catch (e: Exception) {
        ""
    }

fun EditText.validFormNotEmpty(): Boolean = this.text.toString().isNotEmpty()
fun EditText.validFormTagEmpty(): Boolean = (this.tag ?: "").toString().isNotEmpty()

fun String?.toEditable(): Editable? {
    return this?.let { Editable.Factory.getInstance().newEditable(this) }
}