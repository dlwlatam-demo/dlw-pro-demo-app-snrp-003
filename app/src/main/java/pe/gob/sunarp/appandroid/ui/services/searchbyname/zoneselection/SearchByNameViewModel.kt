package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselection

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.SearchZone
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SearchByNameViewModel @Inject constructor(
    private val repository: SearchRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val zones = SingleLiveEvent<List<SearchZone>>()

    fun getZones() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getZones()) {
                is DataResult.Success -> onZonesSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    private fun onZonesSuccess(data: List<SearchZone>) {
        loading.value = false
        zones.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}