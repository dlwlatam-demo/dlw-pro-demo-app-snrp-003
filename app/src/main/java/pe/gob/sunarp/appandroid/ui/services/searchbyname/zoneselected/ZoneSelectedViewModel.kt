package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselected

import android.util.Log
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.model.PersonType
import pe.gob.sunarp.appandroid.core.model.RegisterArea
import pe.gob.sunarp.appandroid.core.model.ZoneRegisterInformation
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.BasicPaymentData
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import javax.inject.Inject

@HiltViewModel
class ZoneSelectedViewModel @Inject constructor(
    private val repository: SearchRemoteRepository,
    private val paymentRepository: PaymentRemoteRepository,
    private val preference: SharedPreferenceRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val areas = SingleLiveEvent<List<RegisterArea>>()
    val personType = SingleLiveEvent<List<PersonType>>()
    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onFormSuccess = SingleLiveEvent<ZoneRegisterInformation>()

    fun create(
        information: ZoneRegisterInformation,
        terms: Boolean,
        ammountToPay: Float
    ) {
        viewModelScope.launch {
            when (val status = validateForm(
                information,
                terms
            )) {
                is FormStatus.Ok -> openPayment(information, ammountToPay)
                is FormStatus.Ko -> onError(Exception(status.error))
            }
        }
    }

    private fun openPayment(information: ZoneRegisterInformation, ammountToPay: Float) {
        getPaymentKeys(information, ammountToPay, 1)
    }

    private fun getPaymentKeys(
        information: ZoneRegisterInformation,
        ammountToPay: Float,
        transId: Int
    ) {
        loading.value = true
        viewModelScope.launch {
            val data = paymentRepository.getPaymentData("")

            when (val result = paymentRepository.getPaymentKeys(ammountToPay.toString(),
                transId,
                BasicPaymentData(data.name, data.firstLastname, data.secondLastname, data.email))) {
                is DataResult.Success -> onPaymentKeySuccess(result.data, information)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getRegisterArea() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegisterAreas()) {
                is DataResult.Success -> onRegisterAreas(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getPersonTypes(areaRegId: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getPersonTypes(areaRegId)) {
                is DataResult.Success -> onPersonType(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getNiubizInformation(
        visaNetResult: VisaNetResult
    ) = visaNetResult.toNiubisInformation(preference.getDoc())

    private fun onPersonType(data: List<PersonType>) {
        loading.value = false
        personType.value = data
    }


    private fun onRegisterAreas(data: List<RegisterArea>) {
        loading.value = false
        areas.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onPaymentKeySuccess(item: VisaNetItem, information: ZoneRegisterInformation) {
        loading.value = false
        onVisaConfiguration.value = item
        onFormSuccess.value = information
    }

    private fun validateForm(
        information: ZoneRegisterInformation,
        terms: Boolean
    ): FormStatus {

        if (information.tipoTitular.isEmpty()) return FormStatus.Ko("Olvidó seleccionar el titular")
        if (information.areaRegistral.isEmpty()) return FormStatus.Ko("Olvidó seleccionar el Área registral")
        if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        if (information.isLegalPerson) {
            if (information.names.isEmpty()) return FormStatus.Ko("Olvidó el campo Razón Social")
        } else if (information.tipoTitular.equals("D")) { // Derecho Minero
            if (information.names.isEmpty()) return FormStatus.Ko("Olvidó el campo Derecho Minero")
        } else if (information.tipoTitular.equals("S")) { // Sociedad Minera
            if (information.names.isEmpty()) return FormStatus.Ko("Olvidó el campo Sociedad Minera")
        }
        else {
            if (information.names.isEmpty()) return FormStatus.Ko("Olvidó el campo Nombres")
            if (information.firstLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido Paterno")
        }

        return FormStatus.Ok
    }
    fun getPersonData() = paymentRepository.getPaymentData("")
}