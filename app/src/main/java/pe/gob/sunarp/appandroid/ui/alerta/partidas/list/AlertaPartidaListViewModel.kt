package pe.gob.sunarp.appandroid.ui.alerta.partidas.list

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.DeleteMandatoRequest
import pe.gob.sunarp.appandroid.data.alerta.modelservices.PartidaItemResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaPartidaListViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) :
    BaseViewModel() {
    val loading = SingleLiveEvent<Boolean>()
    val listData = SingleLiveEvent<List<PartidaItemResponse>>()

    fun getListData() =
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.selectAllPartidas()) {
                is DataResult.Success -> onGetListDataSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    private fun onGetListDataSuccess(data: List<PartidaItemResponse>) {
        listData.value = data
        loading.value = false
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun cancelPartida(mandatoitem: PartidaItemResponse) = viewModelScope.launch {
        loading.value = true
        when (val result = repository.deletePartida(DeleteMandatoRequest(mandatoitem.nuCont, mandatoitem.aaCont))) {
            is DataResult.Success -> onCancelMandatoSuccess(result.data)
            is DataResult.Failure -> onError(result.exception)
        }
    }

    private fun onCancelMandatoSuccess(result: Boolean) {
        getListData()
    }
}
