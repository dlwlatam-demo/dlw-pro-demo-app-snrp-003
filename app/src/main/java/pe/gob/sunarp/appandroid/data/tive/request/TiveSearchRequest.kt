package pe.gob.sunarp.appandroid.data.tive.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TiveSearchRequest(
    @field:Json(name = "codigoZona")
    val codigoZona: String,
    @field:Json(name = "codigoOficina")
    val codigoOficina: String,
    @field:Json(name = "anioTitulo")
    val anioTitulo: String,
    @field:Json(name = "numeroTitulo")
    val numeroTitulo: String,
    @field:Json(name = "numeroPlaca")
    val numeroPlaca: String,
    @field:Json(name = "codigoVerificacion")
    val codigoVerificacion: String,
    @field:Json(name = "tipo")
    val tipo: String,
    @field:Json(name = "oficina")
    val oficina: String = "",

)