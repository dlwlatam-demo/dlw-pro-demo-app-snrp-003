package pe.gob.sunarp.appandroid.ui.home.menu.adapter

data class HomeItem(
    val icon: Int,
    val color: String,
    val title: String,
    val navigation: Int? = null
)