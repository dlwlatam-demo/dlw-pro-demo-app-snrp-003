package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certifiedadvertising


import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoPublicidadBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertifiedAdvertisingFragment :
    BaseFragment<FragmentCertificadoPublicidadBinding>(FragmentCertificadoPublicidadBinding::inflate),
    PaymentInterface {


    private val viewModel: CertifiedAdvertisingViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var documentType: DocumentType? = null
    private var isSelectedDocumentType: Boolean = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }
    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()
        with(binding) {

            tvDocumentType.tag = "09"
            isSelectedDocumentType = true

            clearInputValues()
            rgTypePerson.setOnCheckedChangeListener { _, checkedId ->
                var type = Constants.PERSON_NATURAL
                if (checkedId == R.id.rb_person) {
                    enableFieldsPerson()
                    tvDocumentType.tag = "09"
                    isSelectedDocumentType = true
                } else {
                    type = Constants.PERSON_LEGAL
                    enableFieldsLegal()
                    isSelectedDocumentType = false
                }

                clearInputValues()
                viewModel.getDocumentType(type)
            }

            tvDocumentType.setOnItemClickListener { parent, _, position, _ ->
                documentType =
                    (parent.getItemAtPosition(position) as DocumentType)
                tvDocumentType.tag = documentType?.id
                clearInputValues()
            }
            etBusinessName.inputType = InputType.TYPE_CLASS_TEXT
            etDocumentNumber.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etBusinessName.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etFirstLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etSecondLastname.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            etNames.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tvDocumentType.setOnFocusChangeListener { view, hasFocus ->
                if(!hasFocus) {
                    hideKeyboard()
                }
            }

            tvDocumentType.setOnItemClickListener { parent, _, position, _ ->
                documentType =
                    (parent.getItemAtPosition(position) as DocumentType)
                tvDocumentType.tag = documentType?.id
                Log.e("tvDocumentType", tvDocumentType.tag.toString())
                isSelectedDocumentType = true
                clearInputValues()
            }

            etDocumentNumber.onChanged { validateFields() }
            etBusinessName.onChanged { validateFields() }
            etBusinessName.filterMayus()
            etFirstLastname.onChanged { validateFields() }
            etFirstLastname.filterMayus()
            etSecondLastname.onChanged { validateFields() }
            etSecondLastname.filterMayus()
            etNames.onChanged { validateFields() }
            etNames.filterMayus()

            etDocumentNumber.onChanged {

                tvDocumentType.tag?.let{ type ->
                    val maxLength = 16 // Número máximo de caracteres permitidos

                    val inputFilters = arrayOf<InputFilter>(InputFilter.LengthFilter(maxLength))

                    etDocumentNumber.filters = inputFilters
                    evaluateDocumentNumber(
                        type.toString(),
                        it.length,
                        it.toString()
                    )
                }
            }

            btnApply.setOnClickListener {
                var documentNumber = etDocumentNumber.text.toString()
                var documentTypePN = tvDocumentType.tag.toString()
                var firstLastname = etFirstLastname.text.toString()
                var secondLastname = etSecondLastname.text.toString()
                var names = etNames.text.toString()
                var businessName = etBusinessName.text.toString()
                var typePerson = Constants.PERSON_NATURAL
                if (rgTypePerson.checkedRadioButtonId != R.id.rb_person) {
                    typePerson = Constants.PERSON_LEGAL
                }

                var paymentDataItem =
                    PaymentDataItem(names, firstLastname, secondLastname)
                paymentDataItem.documentNumebr = documentNumber
                paymentDataItem.businessName = businessName
                paymentDataItem.typePerson = typePerson
                paymentDataItem.documentType = documentTypePN

                findNavController().navigate(
                    R.id.action_certifiedAdvertisingFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to documentType,
                        "ammount" to certificate?.preOffice!!.toDouble()
                    )
                )
            }
        }
    }
    private fun selectDNIDocument(docs: List<DocumentType>) {
        val selectedDoc = docs.find { it.name == "DNI" } // Encuentra el elemento "DNI"

        selectedDoc?.let {
            val selectedIndex = docs.indexOf(it)
            Log.e("Index",selectedIndex.toString())
            binding.tvDocumentType.setText(it.name, false)
            binding.tvDocumentType.setSelection(3)
        }
    }

    private fun evaluateDocumentNumber(
        documentType: String,
        length: Int,
        text: String,
    ) {
        when (documentType) {
            Constants.DOCUMENT_TYPE_DNI -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_DNI
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text,
                    )
                    validateFields()
                }
            }
            Constants.DOCUMENT_TYPE_CE -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_CE
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text
                    )
                }
            }
            Constants.DOCUMENT_TYPE_RUC -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_RUC
                ) {
                    hideKeyboard()
                }
            }
        }
    }

    private fun enableFieldsPerson() {
        with(binding) {
            tilBusinessName.hide()
            tilFirstLastname.show()
            tilSecondLastname.show()
            tilNames.show()
        }
    }


    private fun enableFieldsLegal() {
        with(binding) {
            tilBusinessName.show()
            tilFirstLastname.hide()
            tilSecondLastname.hide()
            tilNames.hide()
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            viewModel.getDocumentType("N")
            onChange(offices) { fillOffices(it) }
            onChange(docTypes) {
                fillDocType(it)
                selectDNIDocument(it)
            }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(isValidDocumentDNI) { validDocumentDNI(it) }
            onChange(isValidDocumentCE) { validDocumentCE(it) }
        }
    }


    private fun validDocumentDNI(documentDNI: DocumentDNI) {
        with(binding) {
            if (rgTypePerson.checkedRadioButtonId == R.id.rb_person) {
                tilNames.editText?.disable()
                tilFirstLastname.editText?.disable()
                tilSecondLastname.editText?.disable()

                tilNames.editText?.setText(documentDNI.names)
                tilFirstLastname.editText?.setText(documentDNI.paternal)
                tilSecondLastname.editText?.setText(documentDNI.maternal)
            }
        }
    }


    private fun validDocumentCE(documentCE: DocumentCE) {
        with(binding) {
            if (rgTypePerson.checkedRadioButtonId == R.id.rb_person) {
                tilNames.editText?.disable()
                tilFirstLastname.editText?.disable()
                tilSecondLastname.editText?.disable()

                tilNames.editText?.setText(documentCE.names)
                tilFirstLastname.editText?.setText(documentCE.firstLastName)
                tilSecondLastname.editText?.setText(documentCE.secondLastName)
            }
        }
    }

    private fun clearInputValues() {
        with(binding) {

            etDocumentNumber?.clean()
            etNames?.clean()
            etFirstLastname?.clean()
            etSecondLastname?.clean()
            etDocumentNumber?.enable()

            tvDocumentType.tag?.let { type ->
                when (type) {
                    Constants.DOCUMENT_TYPE_DNI -> {
                        etFirstLastname?.disable()
                        etSecondLastname?.disable()
                        etNames?.disable()
                    }
                    Constants.DOCUMENT_TYPE_CE -> {
                        etFirstLastname?.disable()
                        etSecondLastname?.disable()
                        etNames?.disable()
                    }
                    else -> {
                        etFirstLastname?.enable()
                        etSecondLastname?.enable()
                        etNames?.enable()
                    }
                }
            }
        }
    }

    fun validateFields() {
        with(binding) {
            etRegisterOffice.tag?.let {
                if (it.toString().isNotEmpty() &&
                    (rgTypePerson.checkedRadioButtonId == R.id.rb_person
                            && etDocumentNumber.text.toString().isNotEmpty() && etDocumentNumber.text?.length!! >= 8
                            && etFirstLastname.text.toString().isNotEmpty()
                            && etNames.text.toString().isNotEmpty()
                            && isSelectedDocumentType)
                            || (rgTypePerson.checkedRadioButtonId == R.id.rb_legal
                            && etDocumentNumber.text.toString().isNotEmpty()
                            && etBusinessName.text.toString().isNotEmpty()
                            && isSelectedDocumentType)
                    )
                 {
                    btnApply.enable()
                } else {
                    btnApply.disable()
                }
            }
        }
    }

    private fun fillDocType(docs: List<DocumentType>) {
        binding.tvDocumentType.clean()
        binding.tvDocumentType.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                docs
            )
        )
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
        }
    }


    override fun sendPaymentConfirmation(data: VisaNetResult) {
        /*
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificate?.let {
                saveProcess?.let { it1 ->
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, null
                    )
                }
            }
        }
        */
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber, Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) { //4551708161768059
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()
        val action = CertifiedAdvertisingFragmentDirections
            .actionCertifiedAdvertisingFragmentToPaymentResultFragment(
                transId = transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}