package pe.gob.sunarp.appandroid.data.home

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface HomeDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/servicios/contacto/comentario")
    suspend fun sendComment(
        @Body request: CommentRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>
}