package pe.gob.sunarp.appandroid.core.api

import org.jetbrains.annotations.NotNull

sealed class DataResult<out T> {
    data class Success<T>(@NotNull val data: T) : DataResult<T>()
    data class Failure(val exception: Exception) : DataResult<Nothing>()
}