package pe.gob.sunarp.appandroid.ui.services.tive.history

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.TiveDocument
import pe.gob.sunarp.appandroid.data.tive.TiveRemoteRepository
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TiveHistoryViewModel @Inject constructor(
    private val repository: TiveRemoteRepository
) : BaseViewModel() {

    val queries = SingleLiveEvent<List<TiveSearchRequest>>()
    val loading = SingleLiveEvent<Boolean>()
    val selectimage = SingleLiveEvent<String>()

    fun getHistory() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getHistory()) {
                is DataResult.Success -> onSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getSearch(body: TiveSearchRequest) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getSearch(body)) {
                is DataResult.Success -> onConsultImageSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onSuccess(data: List<TiveSearchRequest>) {
        loading.value = false
        queries.value = data
    }

    private fun onConsultImageSuccess(data: TiveDocument) {
        loading.value = false
        selectimage.value = data.documento
    }
}