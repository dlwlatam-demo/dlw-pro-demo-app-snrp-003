package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.data.property.request.PropertyRequest

@Parcelize
class UserInformation(
    val names: String,
    val firstLastname: String,
    val secondLastname: String,
    val docType: String,
    val docNumber: String
) : Parcelable {
    fun toRequest(): PropertyRequest {
        return PropertyRequest(
            names = names,
            firstLastname = firstLastname,
            secondLastname = secondLastname,
            docType = docType,
            docNumber = docNumber
        )
    }
}