package pe.gob.sunarp.appandroid.ui.transactions.history

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.FragmentTransactionsHistoryBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.transactions.history.adapter.TransactionHistoryAdapter
import pe.gob.sunarp.appandroid.ui.transactions.history.model.Transaction

@AndroidEntryPoint
class TransactionHistoryFragment :
    BaseFragment<FragmentTransactionsHistoryBinding>(FragmentTransactionsHistoryBinding::inflate) {

    private val viewModel: TransactionHistoryViewModel by viewModels()
    private val transactionsAdapter by lazy { TransactionHistoryAdapter(requireContext())  }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getTransactions()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.transactions_history)
        initViews()
        initViewModels()
    }

    private fun initViews() {

        transactionsAdapter.setOpenTransactions { transId,type ->
            findNavController().navigate(
                R.id.action_transactionsHistoryFragment_to_transactionStatusFragment,
                bundleOf("transId" to transId,"type" to type)
            )
        }
        transactionsAdapter.setOpenDocument {transId ->
            findNavController().navigate(
                R.id.action_transactionsHistoryFragment_to_pdfViewerFragment,
                bundleOf("transId" to transId,"id" to "NW")
            )

        }
        binding.rvTransactions.adapter = transactionsAdapter
        binding.rvTransactions.layoutManager = LinearLayoutManager(context)
        binding.tvItemCount.text = "Total de registros: " + transactionsAdapter.itemCount
    }

    private fun updateItems(items: List<Transaction>) {
        transactionsAdapter.setItems(items)
    }

    private fun initViewModels() {
        with(viewModel){
            initViewModelsErrorToken()
            onChange(transactions){
                updateItems(it)
                binding.tvItemCount.text = "Total de registros: " + transactionsAdapter.itemCount
            }
            onChange(loading){showLoading(it)}
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}