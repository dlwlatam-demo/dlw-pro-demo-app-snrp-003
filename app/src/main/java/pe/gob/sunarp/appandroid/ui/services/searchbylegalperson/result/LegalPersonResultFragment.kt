package pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.result

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.onChanged
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.core.model.LegalPerson
import pe.gob.sunarp.appandroid.core.model.SearchLegalPerson
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentLegalPersonResultBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.result.adapter.LegalPersonSearchResultAdapter

@AndroidEntryPoint
class LegalPersonResultFragment :
    BaseFragment<FragmentLegalPersonResultBinding>(FragmentLegalPersonResultBinding::inflate) {

    private val viewModel: LegalPersonSearchResultViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        with(binding) {
            etSearch.setOnFocusChangeListener { _, hasFocus -> if (!hasFocus) hideKeyboard() }
            etSearch.onChanged { s ->
                val adapter = rvLegalSearch.adapter as? LegalPersonSearchResultAdapter

                if (adapter != null) {
                    adapter.filter = s.toString()
                }
            }
            rvLegalSearch.setOnTouchListener { v, event ->
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> hideKeyboard()
                }
                v?.onTouchEvent(event) ?: true
            }
        }
    }

    private fun initViewModels() {
        val searchParams: SearchLegalPerson? = arguments?.getParcelable("information")
        with(viewModel) {
            initViewModelsErrorToken()
            searchParams?.let { searchLegalPerson(it) }
            onChange(legalPersons) {
                fillLegalPerson(it)
                binding.tvItemCount.text = "Total de registros: " + it.count()
            }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun fillLegalPerson(items: List<LegalPerson>) {

        binding.rvLegalSearch.apply {
            val legalPersonAdapter = LegalPersonSearchResultAdapter()
            legalPersonAdapter.legalPerson = items
            adapter = legalPersonAdapter
            val manager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
            addItemDecoration(dividerItemDecoration)
            layoutManager = manager
        }
    }

    private fun showLoading(show: Boolean) {
        with(binding) {
            loadingContainer.apply {
                if (show) {
                    loading.show()
                    statusContainer.hide()
                } else {
                    loading.hide()
                    statusContainer.show()
                }
            }
        }
    }

    override fun showError(error: String?) {
        Log.e("ERROR", "$error")

        with(binding) {
            statusContainer.hide()
            tvError.text = error
            tvError.show()
            super.showError(error)
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}