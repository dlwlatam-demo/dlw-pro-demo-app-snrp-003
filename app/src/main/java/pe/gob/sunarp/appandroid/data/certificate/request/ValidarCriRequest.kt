package pe.gob.sunarp.appandroid.data.certificate.request

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ValidarCriRequest(
    val regPubId: String,
    val oficRegId: String,
    val areaRegId: String,
    val numPart: String,
    val tipoCRI: String,
    val codGrupo: String,
    val typePartida: String,
)