package pe.gob.sunarp.appandroid.ui.recovery

import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.ActivityRecoveryBinding
import pe.gob.sunarp.appandroid.ui.base.BaseActivity

@AndroidEntryPoint
class RecoveryActivity : BaseActivity<ActivityRecoveryBinding>() {

    override fun viewBinding(): ActivityRecoveryBinding = ActivityRecoveryBinding.inflate(layoutInflater)

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

}