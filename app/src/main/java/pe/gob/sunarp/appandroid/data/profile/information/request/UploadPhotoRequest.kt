package pe.gob.sunarp.appandroid.data.profile.information.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UploadPhotoRequest(
    @field:Json(name = "userPhoto")
    val userPhoto: String,
)