package pe.gob.sunarp.appandroid.core.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Document(
    @field:Json(name = "documento")
    val document: String?,
    @field:Json(name = "filename")
    val filename: String?,
)