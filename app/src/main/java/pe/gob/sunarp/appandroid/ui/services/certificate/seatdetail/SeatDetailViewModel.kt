package pe.gob.sunarp.appandroid.ui.services.certificate.seatdetail

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.SeatDetail
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SeatDetailViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val seatDetail = SingleLiveEvent<List<SeatDetail>>()
    val onPaymentValue = SingleLiveEvent<Double>()

    fun getSeatsDetail(
        codZona: String,
        codOficina: String,
        codLibro: String,
        numPartida: String,
        fichaId: String,
        tomoId: String,
        fojaId: String,
        ofiSARP: String,
        coServicio: String,
        coTipoRegis: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getDetalleAsientos(
                codZona,
                codOficina,
                codLibro,
                numPartida,
                fichaId,
                tomoId,
                fojaId,
                ofiSARP,
                coServicio,
                coTipoRegis
            )) {
                is DataResult.Success -> onSeatDetail(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getSeatsDetailVEH(
        zoneCode: String,
        officeCode: String,
        certificateNumber: String,
        plate: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result =
                repository.getDetalleAsientosVeh(zoneCode, officeCode, certificateNumber, plate)) {
                is DataResult.Success -> onSeatDetail(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getSeatsDetailRMC(
        zoneCode: String,
        officeCode: String,
        certificateNumber: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result =
                repository.getDetalleAsientosRMC(zoneCode, officeCode, certificateNumber)) {
                is DataResult.Success -> onSeatDetail(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getPaymentValue(
        coServicio: String,
        cantPaginas: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getPaymentValue(coServicio, cantPaginas)) {
                is DataResult.Success -> onPaymentValueSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onPaymentValueSuccess(data: Double) {
        loading.value = false
        onPaymentValue.value = data
    }

    private fun onSeatDetail(data: List<SeatDetail>) {
        loading.value = false
        seatDetail.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}