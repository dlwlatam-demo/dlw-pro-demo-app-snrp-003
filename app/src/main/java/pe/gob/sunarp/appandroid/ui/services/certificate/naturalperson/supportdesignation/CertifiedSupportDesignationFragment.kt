package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.supportdesignation


import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoVigenciaDesignacionBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.ListAdapterItems
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PersonItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page

@AndroidEntryPoint
class CertifiedSupportDesignationFragment :
    BaseFragment<FragmentCertificadoVigenciaDesignacionBinding>(
        FragmentCertificadoVigenciaDesignacionBinding::inflate
    ) {


    private val viewModel: CertifiedSupportDesignationViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var saveProcess: SaveProcess? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()
        with(binding) {
            tilFirstLastnameOtorgante.editText?.filterMayus()
            tilSecondLastnameOtorgante.editText?.filterMayus()
            tilNamesOtorgante.editText?.filterMayus()
            tilFirstLastnameSupport.editText?.filterMayus()
            tilSecondLastnameSupport.editText?.filterMayus()
            tilNamesSupport.editText?.filterMayus()

            etBusinessName.filterOnlyBusinessName()
            etCommandRecord.filterOnlyMayus()
            etNroAsientoRecord.filterMaxLengthAsiento()
            etNroAsiento.filterMaxLengthAsiento()
            val adapterGrantorsListItems = ListAdapterItems(arrayListOf()) { validateFields() }
            val adapterSupportsItems = ListAdapterItems(arrayListOf()) { validateFields() }

            rvGrantors.apply {
                adapter = adapterGrantorsListItems
                setHasFixedSize(true)
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }

            rvSupport.apply {
                adapter = adapterSupportsItems
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }

            checkBoxTerms.setOnCheckedChangeListener { _, isChecked ->
                hideKeyboard()
                if (isChecked) {
                    enabledPartidaAsiento()
                } else {
                    disabledPartidaAsiento()
                }
                validateFields()
            }

            btnOtorgante.setOnClickListener {
                hideKeyboard()
                var lastname =
                    (tilFirstLastnameOtorgante.editText?.text ?: Constants.EMPTY).toString()
                var lastname2 =
                    (tilSecondLastnameOtorgante.editText?.text ?: Constants.EMPTY).toString()
                var name = (tilNamesOtorgante.editText?.text ?: Constants.EMPTY).toString()

                if (name.isNotEmpty() && lastname.isNotEmpty()) {

                    var interviniente = PersonItem(1, 1, name, lastname, lastname2, Constants.EMPTY)
                    adapterGrantorsListItems.addItem(interviniente)
                    tilFirstLastnameOtorgante.editText?.clean()
                    tilSecondLastnameOtorgante.editText?.clean()
                    tilNamesOtorgante.editText?.clean()
                    validateFields()
                } else {
                    container.snack("Para agregar un Otorgante ingrese sus nombres completos")
                }
            }

            btnSupport.setOnClickListener {
                hideKeyboard()
                var lastname =
                    (tilFirstLastnameSupport.editText?.text ?: Constants.EMPTY).toString()
                var lastname2 =
                    (tilSecondLastnameSupport.editText?.text ?: Constants.EMPTY).toString()
                var name = (tilNamesSupport.editText?.text ?: Constants.EMPTY).toString()

                if (name.isNotEmpty() && lastname.isNotEmpty()) {

                    var interviniente = PersonItem(2, 1, name, lastname, lastname2, Constants.EMPTY)
                    adapterSupportsItems.addItem(interviniente)

                    tilFirstLastnameSupport.editText?.clean()
                    tilSecondLastnameSupport.editText?.clean()
                    tilNamesSupport.editText?.clean()
                    validateFields()
                } else {
                    container.snack("Para agregar un Apoyo ingrese sus nombres completos")
                }
            }

            btnApply.setOnClickListener {
                hideKeyboard()
                var nroPartida = binding.etBusinessName.text.toString()
                certificate?.let {
                    viewModel.validatePartida(
                        nroPartida,
                        legalRecord,
                        certificate,
                        office
                    )
                }
            }

            etCommandRecord.onChanged { validateFields() }
            etBusinessName.onChanged { validateFields() }
            etRegisterOffice
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(onSaveProcess) { saveOnSaveProcess(it) }
            onChange(onPartidaProcess) { savePartida(it) }
        }
    }

    private fun savePartida(partida: Partida) {

        var nroAsientos = binding.etNroAsiento.text.toString()

        if (partida.estado == 0) {
            certificate?.let { certificate ->

                var paymentDataItem =
                    PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
                paymentDataItem.documentNumebr = Constants.EMPTY
                paymentDataItem.businessName = Constants.EMPTY
                paymentDataItem.typePerson = Constants.PERSON_NATURAL
                paymentDataItem.asientos = nroAsientos
                paymentDataItem.partida = partida
                paymentDataItem.lstApoyos = getListSupports()
                paymentDataItem.lstOtorgantes = getListGrantors()

                findNavController().navigate(
                    R.id.action_certifiedSupportDesignationFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to null,
                        "ammount" to certificate.preOffice.toDouble()
                    )
                )
            }
        } else {
            binding.container.snack(partida.msj)
        }
    }


    fun validateFields() {
        with(binding) {
            if (etRegisterOffice.tag != null) {
                if (etRegisterOffice.tag.toString().isNotEmpty() &&
                    (etBusinessName.text.toString().isNotEmpty()) &&
                    getListGrantors().isNotEmpty() && getListSupports().isNotEmpty() &&
                    ( if(checkBoxTerms.isChecked ) !etCommandRecord.text.isNullOrBlank() else true)
                ) {
                    btnApply.enable()
                } else {
                    btnApply.disable()
                }
            } else {
                btnApply.disable()
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
        }
    }


    private fun getListGrantors(): ArrayList<PersonItem> {
        return (binding.rvGrantors.adapter as ListAdapterItems).getData()
    }

    private fun getListSupports(): ArrayList<PersonItem> {
        return (binding.rvSupport.adapter as ListAdapterItems).getData()
    }

    private fun enabledPartidaAsiento() {
        binding.clCommandRecord.visibility = View.VISIBLE
        binding.tilCommandRecord.editText?.text?.clear()
        binding.tilNroAsientoRecord.editText?.text?.clear()
    }

    private fun disabledPartidaAsiento() {
        binding.clCommandRecord.visibility = View.GONE
        binding.tilCommandRecord.editText?.text?.clear()
        binding.tilNroAsientoRecord.editText?.text?.clear()
    }

    private fun saveOnSaveProcess(process: SaveProcess) {
        saveProcess = process
    }


    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}