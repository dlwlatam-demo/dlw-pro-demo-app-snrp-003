package pe.gob.sunarp.appandroid.ui.services.certificate.seatdetail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.model.SeatDetail
import pe.gob.sunarp.appandroid.data.alerta.modelservices.MandatoItemResponse
import pe.gob.sunarp.appandroid.databinding.LayoutSeatItemBinding
import pe.gob.sunarp.appandroid.ui.services.certificate.seatdetail.SeatDetailFragment

class SeatDetailAdapter(
    val items: MutableList<SeatDetail>,
    val isBienesMuebles: Boolean,
    val isRegistroPropiedad: Boolean,
    var isEnableCheckBox: Boolean
) : RecyclerView.Adapter<SeatDetailAdapter.ViewHolder>() {

    private var onSeatsMarked: ((value: Boolean, pos: Int) -> Unit)? = null
    private var selectedCheckboxCount: Int = 0
    private var seatCheckedChangeListener: OnSeatCheckedChangeListener? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SeatDetailAdapter.ViewHolder {
        val itemBinding: LayoutSeatItemBinding =
            LayoutSeatItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SeatDetailAdapter.ViewHolder, position: Int) {
        holder.bind(items[position], position, isBienesMuebles, isRegistroPropiedad)
    }

    fun setOnSeatsPressed(listener: (value: Boolean, pos: Int) -> Unit) {
        this.onSeatsMarked = listener
    }
    fun getSelectedCheckboxCount(): Int {
        return selectedCheckboxCount
    }

    fun getNumSeats(): Int {
        return items.filter { it.enable }
            .sumOf { it.nuPaginasCantidad.toIntOrNull() ?: 1 }
    }

    fun clearItems(enable: Boolean){
        items.forEach {
            it.enable = enable
        }
        notifyDataSetChanged()
    }

    interface OnSeatCheckedChangeListener {
        fun onSeatCheckedChanged(position: Int, isChecked: Boolean)
    }
    fun setOnSeatsPressed(listener: OnSeatCheckedChangeListener) {
        this.seatCheckedChangeListener = listener
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(val binding: LayoutSeatItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SeatDetail, position: Int, isBienesMuebles: Boolean, isRegistroPropiedad: Boolean) = with(binding) {
            with(item) {
                checkBoxPages.text = "${position + 1}/${items.size}"
                checkBoxPages.setOnCheckedChangeListener(null)
                checkBoxPages.isChecked = enable
                checkBoxPages.isEnabled = isEnableCheckBox
                checkBoxPages.setOnCheckedChangeListener { _, value ->
                    item.enable = value
                    onSeatsMarked?.invoke(value, position)
                    seatCheckedChangeListener?.onSeatCheckedChanged(adapterPosition, value)
                    updateSelectedCheckboxCount()
                }
                if (isBienesMuebles) {
                    boxNoBienes.visibility = View.GONE
                    boxSiBienes.visibility = View.VISIBLE

                    conceptoItem.tvTitle.text = "Concepto:"
                    conceptoItem.tvDescription.text = descripcionConcepto
                    tituloItem.tvTitle.text = "Título:"
                    tituloItem.tvDescription.text = titulo
                    asientoItem.tvTitle.text = "Nro. Asiento:"
                    asientoItem.tvDescription.text = seatNumber
                    exoneradoItem2.tvTitle.text = "Exonerado:"
                    exoneradoItem2.tvDescription.text = exonerado2
                    cantPgItem.tvTitle.text = "Cant. Páginas:"
                    cantPgItem.tvDescription.text = pageNumber

                } else {
                    boxNoBienes.visibility = View.VISIBLE
                    boxSiBienes.visibility = View.GONE
                    actoItem.tvTitle.text = "Acto:"
                    seatTypes.tvTitle.text = "Tipo de asiento:"
                    seatsNumberItem.tvTitle.text = "Nro de asiento:"
                    exoneradoItem.tvTitle.text = "Exonerado:"
                    pageNumberItem.tvTitle.text = "Cant. páginas:"
                    coverPagesItem.tvTitle.text = "Pág. portadas:"
//                    if (isRegistroPropiedad) {
                        actoItem.tvDescription.text = desActo
                        seatTypes.tvDescription.text = desAsiento
                        seatsNumberItem.tvDescription.text = detalle
                        exoneradoItem.tvDescription.text = flagExonerado
                        pageNumberItem.tvDescription.text = nuPaginasCantidad
                        coverPagesItem.tvDescription.text = paginas
//                    } else {
//                        actoItem.tvDescription.text = acto
//                        seatTypes.tvDescription.text = seatType
//                        seatsNumberItem.tvDescription.text = seatNumber
//                        exoneradoItem.tvDescription.text = exonerado
//                        pageNumberItem.tvDescription.text = pageNumber
//                        coverPagesItem.tvDescription.text = coverPages
//                    }
                }
            }
        }

        private fun updateSelectedCheckboxCount() {
            selectedCheckboxCount = items.count { it.enable }
        }
    }


}