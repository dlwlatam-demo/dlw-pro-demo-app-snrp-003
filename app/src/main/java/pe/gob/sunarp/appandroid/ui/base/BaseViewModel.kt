package pe.gob.sunarp.appandroid.ui.base

import androidx.lifecycle.ViewModel
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.NotControlledException
import pe.gob.sunarp.appandroid.core.api.TimeOutException
import pe.gob.sunarp.appandroid.core.api.TokenException

open class BaseViewModel: ViewModel() {

    val onErrorData = SingleLiveEvent<ErrorModel>()

    protected open fun onError(err: Exception) {
        onErrorData.value =
            when (err) {
                is TokenException -> {
                    ErrorModel(type = ErrorType.ERROR_TYPE_TOKEN, message = err.message)
                }
                is NotControlledException -> {
                    ErrorModel(type = ErrorType.ERROR_TYPE_NOT_CONTROLLED, message = err.message)
                }
                is TimeOutException -> {
                    ErrorModel(type = ErrorType.ERROR_TYPE_TIMEOUT, message = err.message)
                }
                else -> {
                    ErrorModel(type = ErrorType.ERROR_TYPE_GENERIC, message = err.message)
                }
            }
    }
}

enum class ErrorType {
    ERROR_TYPE_TOKEN, ERROR_TYPE_GENERIC, ERROR_TYPE_NOT_CONTROLLED, ERROR_TYPE_VERSION_APP, ERROR_TYPE_TIMEOUT
}

data class ErrorModel (val type: ErrorType, val message: String?)