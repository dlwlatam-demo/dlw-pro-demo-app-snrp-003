package pe.gob.sunarp.appandroid.data.vehicle

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipAsyncDTO
import pe.gob.sunarp.appandroid.core.dto.VehicleDataDTO
import pe.gob.sunarp.appandroid.core.dto.VehicleReceiptAsyncDTO
import pe.gob.sunarp.appandroid.core.dto.VehicleReceiptDTO
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.search.request.SearchRequest
import pe.gob.sunarp.appandroid.data.vehicle.request.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

interface VehicleDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-vehicular/vehiculo/data")
    suspend fun getVehicleData(
        @Body request: VehicleRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<VehicleDataDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-vehicular/boleta/validar")
    suspend fun validateReceipt(
        @Body request: VehicleReceiptRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-vehicular/boleta/generar")
    suspend fun generateReceipt(
        @Body request: VehicleGenerateReceiptRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<VehicleReceiptDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-vehicular/boleta/generarAsync")
    suspend fun generateReceiptAsync(
        @Body request: VehicleGenerateReceiptRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
        //Timeout expanded
    ): DataResult<VehicleReceiptAsyncDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consulta-vehicular/async/status/generarBoleta/{guid}")
    suspend fun generateReceiptAsyncStatus(
        @Path("guid") areaId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
        //Timeout expanded
    ): DataResult<VehicleReceiptAsyncDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/parametros/tarifa")
    suspend fun obtenerTarifa(
        @Body request: ConsultaVehicularCostoRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<ConsultaVehicularCostoResponse>

}