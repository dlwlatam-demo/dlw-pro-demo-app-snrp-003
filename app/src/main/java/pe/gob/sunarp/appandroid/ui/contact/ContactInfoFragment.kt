package pe.gob.sunarp.appandroid.ui.contact

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.requestPhonePermission
import pe.gob.sunarp.appandroid.databinding.FragmentContactInfoBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class ContactInfoFragment :
    BaseFragment<FragmentContactInfoBinding>(FragmentContactInfoBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        with(binding) {
            cardItemLayoutPhone.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse(cardItemLayoutPhone.tag.toString())
                startActivity(intent)
                /*
                context?.let {
                    it.requestPhonePermission(requireActivity(), {
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse(cardItemLayoutPhone.tag.toString())
                        startActivity(intent)
                    })
                }
                */
            }
            cardItemLayoutPhoneFree.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse(cardItemLayoutPhoneFree.tag.toString())
                startActivity(intent)
                /*
                context?.let {
                    it.requestPhonePermission(requireActivity(), {
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse(cardItemLayoutPhoneFree.tag.toString())
                        startActivity(intent)
                    })
                } */
            }
            cardItemLayoutEmail.setOnClickListener {
                findNavController().navigate(R.id.action_ContactInfoFragment_ContactSendFragment)
            }
            cardItemLayoutWeb.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(cardItemLayoutWeb.tag.toString())
                startActivity(intent)
            }
            imageViewIconSocialFacebook.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(imageViewIconSocialFacebook.tag.toString())
                startActivity(intent)
            }
            imageViewIconSocialYoutube.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(imageViewIconSocialYoutube.tag.toString())
                startActivity(intent)
            }
            imageViewIconSocialTwitter.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(imageViewIconSocialTwitter.tag.toString())
                startActivity(intent)
            }
            imageViewIconSocialLinkedin.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(imageViewIconSocialLinkedin.tag.toString())
                startActivity(intent)
            }
            imageViewIconSocialInstagram.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(imageViewIconSocialInstagram.tag.toString())
                startActivity(intent)
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}