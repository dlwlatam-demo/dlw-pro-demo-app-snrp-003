package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json

interface NotificationResponse {

    @Json(name="aaCont")
    val aaCont: String

    @Json(name="nuCont")
    val nuCont: String
}

