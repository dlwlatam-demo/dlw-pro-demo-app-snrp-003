package pe.gob.sunarp.appandroid.core.custom.ui.otp.model

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern

class SUNOTPInputFilter(private val pattern: String) : InputFilter {
    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        for (i in start until end) {
            if (!Pattern.compile(pattern).matcher(source[i].toString()).matches()) {
                return ""
            }
        }
        return null
    }
}