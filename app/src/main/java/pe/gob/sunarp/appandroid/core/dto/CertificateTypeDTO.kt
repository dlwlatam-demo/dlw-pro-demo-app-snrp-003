package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.CertificateType

@JsonClass(generateAdapter = true)
data class CertificateTypeDTO(
    @field:Json(name = "areaRegId")
    val areaId: String?,
    @field:Json(name = "servicioId")
    val serviceId: String?,
    @field:Json(name = "certificadoId")
    val certificateId: String?,
    @field:Json(name = "nombre")
    val name: String?,
    @field:Json(name = "precOfic")
    val preOffice: String?,
    @field:Json(name = "codGrupoLibroArea")
    val codGrupoLibroArea: String?,
    @field:Json(name = "tpoCertificado")
    val type: String?,
    @field:Json(name = "descGrupoLibroArea")
    val descGrupoLibroArea: String?,
) : BaseModel() {
    fun toModel() = CertificateType(
        areaId.orEmpty(),
        serviceId.orEmpty(),
        certificateId.orEmpty(),
        name.orEmpty(),
        preOffice.orEmpty(),
        codGrupoLibroArea.orEmpty(),
        type.orEmpty(),
        descGrupoLibroArea.orEmpty()
    )

}