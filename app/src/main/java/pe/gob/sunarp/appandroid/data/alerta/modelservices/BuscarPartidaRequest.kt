package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BuscarPartidaRequest(

	@Json(name="ofic")
	val ofic: String? = null,

	@Json(name="region")
	val region: String? = null,

	@Json(name="area")
	val area: String? = null,

	@Json(name="tipo")
	val tipo: String? = null,


	@Json(name="partida")
	val partida: String? = null,

	@Json(name="ficha")
	val ficha: String? = null,

	@Json(name="tomo")
	val tomo: String? = null,

	@Json(name="foja")
	val foja: String? = null,

	@Json(name="partiSarp")
	val partiSarp: String? = null,

	@Json(name="placa")
	val placa: String? = null
)
