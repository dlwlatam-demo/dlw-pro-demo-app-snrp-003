package pe.gob.sunarp.appandroid.ui.services.vehicle.searchplate

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.vehicle.VehicleRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SearchPlateViewModel @Inject constructor(
    private val certificateRepository: CertificateRemoteRepository,
    private val repository: VehicleRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val plateSuccess = SingleLiveEvent<String>()

    fun getZones() {
        viewModelScope.launch {
            loading.value = true
            when (val result = certificateRepository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getVehicleInformation(information: VehicleInformation, callback: (Boolean) -> Unit) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getVehicleData(information)) {
                is DataResult.Success -> {
                    onSuccess(result.data)
                    // Llama a la devolución de llamada con el resultado (true en este caso)
                    callback(true)
                }
                is DataResult.Failure -> {
                    onError(result.exception)
                    // Llama a la devolución de llamada con el resultado (false en caso de error)
                    callback(false)
                }
            }
        }
    }


    private fun onSuccess(image: String) {
        loading.value = false
        plateSuccess.value = image
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}