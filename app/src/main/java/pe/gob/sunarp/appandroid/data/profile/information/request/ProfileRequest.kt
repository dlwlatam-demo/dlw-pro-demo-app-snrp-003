package pe.gob.sunarp.appandroid.data.profile.information.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProfileRequest(
    @field:Json(name = "deviceLoginRequest")
    val deviceRequest: DeviceRequest,
)

@JsonClass(generateAdapter = true)
data class DeviceRequest(
    @field:Json(name = "appId")
    val appId: String,
    @field:Json(name = "deviceToken")
    val deviceToken: String,
    @field:Json(name = "deviceType")
    val deviceType: String,
    @field:Json(name = "status")
    val status: String,
    @field:Json(name = "deviceId")
    val deviceId: String,
    @field:Json(name = "appVersion")
    val appVersion: String,
    @field:Json(name = "ipAddress")
    val ipAddress: String,
    @field:Json(name = "deviceLogged")
    val deviceLogged: String,
    @field:Json(name = "receiveNotifications")
    val receiveNotifications: String,
    @field:Json(name = "deviceOs")
    val deviceOs: String,
)