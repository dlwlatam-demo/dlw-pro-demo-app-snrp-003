package pe.gob.sunarp.appandroid.core.model

data class PersonType(
    val id: String,
    val name: String,
){
    override fun toString(): String {
        return name
    }
}