package pe.gob.sunarp.appandroid.data.profile.information

object UserProfile {
    var email: String = ""
    var sexo: String = ""
    var tipoDoc: String = ""
    var nroDoc: String = ""
    var nombres: String = ""
    var priApe: String = ""
    var segApe: String = ""
}