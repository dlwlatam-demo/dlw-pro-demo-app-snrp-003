package pe.gob.sunarp.appandroid.core.model

class CertificateOwnership(
    val numPartida: String,
    val regPubSiglas: String,
    val officeName: String,
    val fichaId: String,
    val tomoId: String,
    val fojaId: String,
    val regDescription: String,
    val libroDescripcion: String,
    val participanteDesc: String,
    val tipoDocumPartic: String,
    val numDocumPartic: String,
    val direccionPredio: String,
    val estadoInd: String,
)