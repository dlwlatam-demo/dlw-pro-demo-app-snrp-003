package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.DocumentDNI

@JsonClass(generateAdapter = true)
data class DocumentDniDTO(
    @field:Json(name = "type")
    val documentType: String,
    @field:Json(name = "dni")
    val dni: String,
    @field:Json(name = "apellidoPaterno")
    val paternal: String,
    @field:Json(name = "apellidoMaterno")
    val maternal: String,
    @field:Json(name = "nombres")
    val names: String,
    @field:Json(name = "fechaNacimiento")
    val birthdayDate: String,
    @field:Json(name = "fechaEmision")
    val issueDate: String,
) : BaseModel() {
    constructor() : this("", "", "", "", "", "", "")

    fun toModel() =
        DocumentDNI(
            documentType = documentType,
            dni = dni,
            paternal = paternal,
            maternal = maternal,
            names = names,
            birthdayDate = birthdayDate,
            issueDate = issueDate
        )
}