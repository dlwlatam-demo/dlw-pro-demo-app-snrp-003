package pe.gob.sunarp.appandroid.core.api

class TimeOutException : Exception {
    constructor() : super("Revise su conexión a internet, volver a intentar...") {}
    constructor(message: String?) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
    constructor(cause: Throwable?) : super(cause) {}
}