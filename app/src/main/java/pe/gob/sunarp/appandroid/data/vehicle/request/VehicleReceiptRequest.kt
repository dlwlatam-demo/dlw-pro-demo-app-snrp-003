package pe.gob.sunarp.appandroid.data.vehicle.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VehicleReceiptRequest(
    @field:Json(name = "placa")
    val plateNumber: String,
    @field:Json(name = "codZona")
    val zoneCode: String,
    @field:Json(name = "codOficina")
    val officeCode: String,
    @field:Json(name = "ipAddress")
    val ipAddress: String? = "127.0.0.1",
    @field:Json(name = "idUser")
    val idUser: String? = "APPSNRPANDRO",
    @field:Json(name = "appVersion")
    val appVersion: String? = "3.0",
)