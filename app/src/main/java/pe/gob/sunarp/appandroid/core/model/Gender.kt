package pe.gob.sunarp.appandroid.core.model

data class Gender (
    val id: String,
    val name: String
) {
    override fun toString(): String = name
}