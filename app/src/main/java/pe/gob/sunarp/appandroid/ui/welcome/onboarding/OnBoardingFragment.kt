package pe.gob.sunarp.appandroid.ui.welcome.onboarding

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.FragmentOnboardingBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.welcome.onboarding.adapter.OnBoardingAdapter
import pe.gob.sunarp.appandroid.ui.welcome.onboarding.adapter.OnBoardingViewPageItem

@AndroidEntryPoint
class OnBoardingFragment :
    BaseFragment<FragmentOnboardingBinding>(FragmentOnboardingBinding::inflate) {

    private val viewModel: OnBoardingViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initObservers() {
        viewModel.onBoardingFinished.observe(viewLifecycleOwner) { _ ->
            findNavController().navigate(R.id.action_onBoardingFragment_to_preSignInFragment)
        }
    }

    private fun initViews() {
        val items = ArrayList<OnBoardingViewPageItem>()
        items.add(
            OnBoardingViewPageItem(
                R.drawable.ic_tutorial,
                getString(R.string.certificate_request),
                getString(R.string.certificate_description)
            )
        )
        items.add(
            OnBoardingViewPageItem(
                R.drawable.ic_tutorial,
                getString(R.string.certificate_request),
                getString(R.string.certificate_description)
            )
        )
        items.add(
            OnBoardingViewPageItem(
                R.drawable.ic_tutorial,
                getString(R.string.certificate_request),
                getString(R.string.certificate_description)
            )
        )

        activity?.let {
            val adapter = OnBoardingAdapter(it, items)
            binding.viewpager.adapter = adapter
            TabLayoutMediator(binding.intoTabLayout, binding.viewpager)
            { _, _ -> }.attach()
        }

        binding.btnSkip.setOnClickListener {
            //findNavController().navigate(R.id.action_onBoardingFragment_to_preSignInFragment)
            viewModel.skipOnBoarding()
        }
    }

    

    override fun initViewModelsErrorToken() {
    }


}