package pe.gob.sunarp.appandroid.ui.base

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import pe.gob.sunarp.appandroid.core.closeSession
import pe.gob.sunarp.appandroid.core.finishActivity
import pe.gob.sunarp.appandroid.ui.common.AlertType
import pe.gob.sunarp.appandroid.ui.common.AlertaCommonDialog
import pe.gob.sunarp.appandroid.ui.home.HomeActivity

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding>(
    private val inflate: Inflate<VB>
) : Fragment() {

    private lateinit var _binding: VB
    val binding get() = _binding

    private lateinit var contentView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //if (!this::_binding.isInitialized) {
            _binding = inflate.invoke(inflater, container, false)
        //}
        //if (!::contentView.isInitialized) {
            contentView = binding.root
        //}

        return contentView
    }

    protected open fun validateError(errorModel: ErrorModel) {
        when (errorModel.type) {
            ErrorType.ERROR_TYPE_TOKEN -> {
                context?.let {
                    AlertDialog.Builder(it).apply {
                        setTitle("Tiempo de Sesión expirada")
                        setMessage(("Su sesión ha vencido, o ha ingresado datos incorrectos. Por favor inicie sesión nuevamente"))
                        setCancelable(false)
                        setPositiveButton("OK") { _, _ -> closeSession() }
                        create().show()
                    }
                }
            }
            ErrorType.ERROR_TYPE_NOT_CONTROLLED -> {
                context?.let {
                    activity?.let {
                        AlertaCommonDialog().apply {
                            onCancelListener { closeSession() }
                            setData(title = "Ocurrio un Error", detalle = errorModel.message)
                            show(it.supportFragmentManager, "error")
                            isCancelable = false
                        }
                    }
                }
            }
            ErrorType.ERROR_TYPE_GENERIC -> {
                activity?.let {
                    AlertaCommonDialog().apply {
                        onCancelListener {
                            this.dismiss()
                        }
                        setData(title = "Alerta del Sistema", detalle = errorModel.message)
                        show(it.supportFragmentManager, "error")
                        setAlertType(type = AlertType.INFORMATIVE)
                        isCancelable = false
                    }
                }
            }
            ErrorType.ERROR_TYPE_TIMEOUT -> {
                activity?.let {
                    AlertaCommonDialog().apply {
                        onCancelListener { closeSession() }
                        setData(title = "Ocurrió un Error", detalle = errorModel.message)
                        show(it.supportFragmentManager, "error")
                        isCancelable = false
                    }
                }
            }
            ErrorType.ERROR_TYPE_VERSION_APP -> {
                activity?.let {
                    AlertaCommonDialog().apply {
                        onCancelListener {
                            this.dismiss()
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=pe.gob.sunarp.appandroid")
                                )
                            )
                            finishActivity()
                        }
                        setData(title = "Actualizar Aplicación", detalle = errorModel.message)
                        show(it.supportFragmentManager, "error")
                        isCancelable = false
                    }
                }
            }
        }
    }

    protected open fun showError(error: String?) = validateError(ErrorModel(ErrorType.ERROR_TYPE_GENERIC, error))

    abstract fun initViewModelsErrorToken()

    override fun onResume() {
        super.onResume()
        setTittle()
    }

    protected open fun setTittle() {

    }

    fun putTitleToolbar(tittle: String) {
        (activity as HomeActivity).setToolbarTitle(tittle)
    }
}
