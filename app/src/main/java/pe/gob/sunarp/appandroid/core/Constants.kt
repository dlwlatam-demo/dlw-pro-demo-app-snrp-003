package pe.gob.sunarp.appandroid.core

class Constants {
    companion object {
        const val EMPTY = ""
        const val DOCUMENT_TYPE_DNI = "09"
        const val DOCUMENT_TYPE_CE = "03"
        const val DOCUMENT_TYPE_RUC = "05"
        const val DOCUMENT_TYPE_PS = "08"
        const val DOCUMENT_TYPE_CAN = "50"
        const val DOCUMENT_TYPE_PTP = "51"

        const val REAL_STATE_AREA_ID = "24000"

        const val CERTIFICATE_TYPE_ID = "1"
        const val FILE_TYPE_ID = "2"
        const val PLATE_TYPE_ID = "4"


        const val SERVICE_VEH_CODE = "28"
        const val SERVICE_VEH_REG_TYPE = "005"

        const val SERVICE_RMC_CODE = "37"
        const val SERVICE_RMC_REG_TYPE = "008"

        const val DOCUMENT_LENGTH_DNI = 8
        const val DOCUMENT_LENGTH_CE = 9
        const val DOCUMENT_LENGTH_ANY = 20
        const val DOCUMENT_LENGTH_RUC = 13

        const val LEGAL_AGE = 18

        const val PERSON_NATURAL = "N"
        const val PERSON_LEGAL = "J"
        const val DERECHO_MINERO = "D"
        const val SOCIEDAD_MINERA = "S"

        val DOCUMENT_TYPES_ALLOWED = listOf(
            DOCUMENT_TYPE_DNI,
            DOCUMENT_TYPE_CE,
            DOCUMENT_TYPE_PS,
            DOCUMENT_TYPE_CAN,
            DOCUMENT_TYPE_PTP
        )

        const val KEY_BUNDLE_REGISTER_INFORMATION = "registerInformation"

        const val PATTERN_DATE = "yyyy-MM-dd"

        const val REGEX_EMAIL_ADDRESS =
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"

        const val REGEX_PASS = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z0-9 -\\/:-@\\[-\\`{-~]{8,}$"


        const val REGISTRO_DE_PROPIEDAD = "21000"
        const val CERTIFICADO_NEGATIVO_PREDIOS = "61"
        const val CERTIFICADO_NEGATIVO_PREDIOS_B = "93"
        const val CERTIFICADO_POSITIVO_PREDIOS = "60"
        const val CERTIFICADO_POSITIVO_PREDIOS_B = "92"

        const val REGISTRO_DE_PERSONAS_JURIDICAS = "22000"
        const val CERTIFICADO_NEGATIVO_DE_PERSONA_JURIDICA = "101"
        const val CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA = "100"
        const val CERTIFICADO_POSITIVO_DE_PERSONA_JURIDICA_B = "104"
        const val CERTIFICADO_POSITIVO_NEGATIVO_DE_PERSONA_JURIDICA = "4"

        const val REGISTRO_DE_PERSONAS_NATURALES = "23000"
        const val CERTIFICADO_VIGENCIA_DESIGNACION_APOYO = "77"
        const val CERTIFICADO_VIGENCIA_NOMBRAMIENTO_CURADOR = "78"
        const val CERTIFICADO_VIGENCIA_PODER = "50"
        const val CERTIFICADO_VIGENCIA_PODER_B = "91"
        const val CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA = "57"
        const val CERTIFICADO_NEGATIVO_DE_SUCESION_INTESTADA_B = "83"
        const val CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA = "56"
        const val CERTIFICADO_POSITIVO_DE_SUCESION_INTESTADA_B = "89"
        const val CERTIFICADO_NEGATIVO_UNION_DE_HECHO = "86"
        const val CERTIFICADO_POSITIVO_UNION_DE_HECHO = "87"
        const val CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL = "66"
        const val CERTIFICADO_NEGATIVO_DE_REGISTRO_PERSONAL_B = "99"
        const val CERTIFICADO_NEGATIVO_DE_TESTAMENTOS = "63"
        const val CERTIFICADO_NEGATIVO_DE_TESTAMENTOS_B = "103"
        const val CERTIFICADO_POSITIVO_DE_TESTAMENTOS = "62"
        const val CERTIFICADO_POSITIVO_DE_TESTAMENTOS__B = "102"
        const val CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_A = "74"
        const val CERTIFICADO_REGISTRAL_BUSQUEDA_CATASTRAL = "74"
        const val CERTIFICADO_REGISTRAL_INMOBILIARIO_ENTREGA_PAPEL = "44"
        const val CERTIFICADO_REGISTRAL_INMOBILIARIO_CON_FIRMA_ELECTRONICA_B = "75"
        const val CERTIFICADO_DE_CARGAS_Y_GRAVAMENES = "85"
        const val CERTIFICADO_DE_VIGENCIA_DE_PODER_PERS_JURIDICA = "76"
        const val CERTIFICADO_DE_VIGENCIA_DE_PODER_PJ = "84"
        const val CERTIFICADO_DE_VIGENCIA_DE_ORGANO_DIRECTIVO = "88"

        const val REGISTRO_DE_BIENES_INMUEBLES = "24000"
        const val REGISTRO_DE_BIENES_MUEBLES = "24000"
        const val CERTIFICADO_REGISTRAL_VEHICULAR = "45"
        const val CERTIFICADO_REGISTRAL_VEHICULAR_B = "98"
        const val CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES = "46"
        const val CERTIFICADO_CARGAS_GRAVAMENES_AERONAVES_B = "96"
        const val CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES = "47"
        const val CERTIFICADO_CARGAS_GRAVAMENES_EMBARCACIONES_B = "97"
        const val CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR = "58"
        const val CERTIFICADO_POSITIVO_DE_PROPIEDAD_VEHICULAR_B = "94"
        const val CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR = "59"
        const val CERTIFICADO_NEGATIVO_DE_PROPIEDAD_VEHICULAR_B = "95"

        const val MAP_OFFICE_TYPE_OFICINA_REGISTRAL = "Oficina Registral"
        const val MAP_OFFICE_TYPE_OFICINA_RECEPTORA = "Oficina Receptora"
        const val MAP_OFFICE_TYPE_SEDE_CENTRAL = "Sede Central"
        const val MAP_OFFICE_TYPE_TRIBUNAL_REGISTRAL = "Tribunal Registral"
        const val MAP_OFFICE_TYPE_ZONA_REGISTRAL = "Zona Registral"
        const val MAP_OFFICE_TYPE_OFICINA_OTRAS = "Oficina Otras"
        const val MAP_OFFICE_TYPE_ALL = ""

    }
}