package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.status

import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.GenericViewInterface
import pe.gob.sunarp.appandroid.databinding.LayoutRequestStatusBinding

internal interface StatusInformationInterface : GenericViewInterface<LayoutRequestStatusBinding> {
    fun getComponentView(): StatusInformation
}