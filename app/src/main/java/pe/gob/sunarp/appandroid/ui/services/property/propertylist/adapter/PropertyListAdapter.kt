package pe.gob.sunarp.appandroid.ui.services.property.propertylist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.DataModel
import pe.gob.sunarp.appandroid.databinding.LayoutNoItemsBinding
import pe.gob.sunarp.appandroid.databinding.LayoutPropertyItemBinding

class PropertyListAdapter(
    private val items: List<DataModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_DATA = 1
        const val VIEW_TYPE_ERROR = 2
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_DATA -> getDataViewHolder(parent)
            else -> getNoDataViewHolder(parent)
        }
    }

    private fun getDataViewHolder(parent: ViewGroup): ViewHolder {
        val itemBinding: LayoutPropertyItemBinding =
            LayoutPropertyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    private fun getNoDataViewHolder(parent: ViewGroup): NoDataViewHolder {
        val itemBinding: LayoutNoItemsBinding =
            LayoutNoItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoDataViewHolder(itemBinding)
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is DataModel.NoDataAvailable -> VIEW_TYPE_ERROR
            is DataModel.PropertyInformation -> VIEW_TYPE_DATA
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return when (val item = items[position]) {
            is DataModel.PropertyInformation -> (holder as ViewHolder).bind(item)
            is DataModel.NoDataAvailable -> Unit
        }

    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutPropertyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DataModel.PropertyInformation) = with(binding) {
            with(item) {
                tvDocumentNumber.text = documentNumber
                tvOwner.text = owner
                tvRegister.text = request
                tvZone.text = zone
                tvOffice.text = office
            }
        }
    }

    inner class NoDataViewHolder(binding: LayoutNoItemsBinding) :
        RecyclerView.ViewHolder(binding.root)


}
