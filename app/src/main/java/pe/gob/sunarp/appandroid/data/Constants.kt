package pe.gob.sunarp.appandroid.data

class Constants {
    companion object {
        const val CONTEXTO_MS = "/appsunarp/api/v1"
        // const val CONTEXTO_API = "/sunarp/app-sunarp/appsunarp_servicios"
        const val CONTEXTO_API = "sunarp-desarrollo/app-sunarpp/appsunarp_servicios"
        //const val HEADER_API_GATEWAY = "X-IBM-Client-Id: d559d4ee5ea8a1ca7be9033c598bab21"
        const val HEADER_API_GATEWAY = "X-IBM-Client-Id: 658b4813f125bcff05778ac0db25face"
        const val CONTEXTO_URL = CONTEXTO_API
        const val CONTEXTO_OAUTH_URL = CONTEXTO_API // Para Api gateway: CONTEXTO_API // Para MS == ""
    }

}