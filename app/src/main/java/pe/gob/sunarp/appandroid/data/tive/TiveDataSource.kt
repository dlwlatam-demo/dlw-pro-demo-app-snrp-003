package pe.gob.sunarp.appandroid.data.tive

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.*
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.tive.request.TiveSearchRequest
import retrofit2.http.*
import retrofit2.http.Header

interface TiveDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-tive/busqueda")
    suspend fun getTiveInformation(
        @Body request: TiveSearchRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<TiveDocumentDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consulta-tive/historial/insertar")
    suspend fun saveHistory(
        @Body request: TiveSearchRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<ServiceResponse>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consulta-tive/historial/busqueda")
    suspend fun getHistory(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<List<TiveSearchRequest>>


}