package pe.gob.sunarp.appandroid.ui.services.certificate.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.model.LiteralCertificate


class ListAdapterItems(private val dataSet: ArrayList<PersonItem>, private val listenerRemove: (()-> Unit)?) :
    RecyclerView.Adapter<ListAdapterItems.ViewHolder>() {

    private var mClickListener: ItemClickListener? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView
        val btnDelete: ImageButton

        init {
            // Define click listener for the ViewHolder's View.
            name = view.findViewById(R.id.tv_title)
            btnDelete = view.findViewById(R.id.img_delete)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_ssupport_item_and_delete, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if(dataSet[position].tipoPart == 1){
            viewHolder.name.text = "${dataSet[position].apPaterno} ${dataSet[position].apMaterno} ${dataSet[position].nombres}, Natural"
        }else {
            viewHolder.name.text = "${dataSet[position].razSocial}, Jurídica"
        }
        viewHolder.btnDelete.setOnClickListener {
            dataSet.removeAt(position)
            notifyDataSetChanged()
            listenerRemove?.invoke()
        }
    }

    override fun getItemCount() = dataSet.size

    fun addItem(item: PersonItem) {
        dataSet.add(item)
        notifyDataSetChanged()
    }

    fun getData(): ArrayList<PersonItem> {
        return dataSet.map { it }.toTypedArray().toCollection(ArrayList())
    }

    fun removeItem(position: Int) {

    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}
