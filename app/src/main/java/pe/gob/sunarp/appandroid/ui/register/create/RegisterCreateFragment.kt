package pe.gob.sunarp.appandroid.ui.register.create

import android.app.DatePickerDialog
import android.graphics.Paint
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_LENGTH_ANY
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_LENGTH_CE
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_LENGTH_DNI
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_CE
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_DNI
import pe.gob.sunarp.appandroid.core.Constants.Companion.KEY_BUNDLE_REGISTER_INFORMATION
import pe.gob.sunarp.appandroid.core.Constants.Companion.PATTERN_DATE
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentRegisterCreateBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.AlertTerminosDialog
import java.text.SimpleDateFormat
import java.util.*
import android.R as androidR

@AndroidEntryPoint
class RegisterCreateFragment :
    BaseFragment<FragmentRegisterCreateBinding>(FragmentRegisterCreateBinding::inflate),
    DatePickerDialog.OnDateSetListener {

    private val viewModel: RegisterCreateViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
        setObservers()
    }

    private fun initViews() {
        with(binding) {
            autoCompleteDocumentType.tag = ""
            autoCompleteGenderType.tag = ""
            tvTerminos.paintFlags = tvTerminos.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            val fm = activity!!.supportFragmentManager
            tvTerminos.setOnClickListener {
                AlertTerminosDialog().apply {
                    setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert)
                    show(fm, this.javaClass.name)
                }
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(inDocTypes) { fillDocumentType(it) }
            onChange(inGenderType) { fillGenderType(it) }
            onChange(isValidDocumentDNI) { validDocumentDNI(it) }
            onChange(isValidDocumentCE) { validDocumentCE(it) }
            onChange(isCreated) { goToNextView(it) }
            onChange(onError) { showError(it) }
            onChange(onFormError) { showError(it) }
            onChange(showLoading) { showLoading(it) }
        }
    }

    private fun fillGenderType(list: List<Gender>) {
        binding.autoCompleteGenderType.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                list
            )
        )
    }

    private fun fillDocumentType(list: List<DocumentType>) {
        binding.autoCompleteDocumentType.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                list
            )
        )
    }

    private fun validDocumentDNI(documentDNI: DocumentDNI) {
        with(binding) {
            textInputFullName.editText?.disable()
            textInputPaternal.editText?.disable()
            textInputMaternal.editText?.disable()
            textInputFullName.editText?.setText(documentDNI.names)
            textInputPaternal.editText?.setText(documentDNI.paternal)
            textInputMaternal.editText?.setText(documentDNI.maternal)
            textInputDate.editText?.tag = documentDNI.birthdayDate
        }
    }

    private fun validDocumentCE(documentCE: DocumentCE) {
        with(binding) {
            textInputFullName.editText?.disable()
            textInputPaternal.editText?.disable()
            textInputMaternal.editText?.disable()
            textInputFullName.editText?.setText(documentCE.names)
            textInputPaternal.editText?.setText(documentCE.firstLastName)
            textInputMaternal.editText?.setText(documentCE.secondLastName)
        }
    }

    override fun showError(error: String?) {
        with(binding) {
            actionRegisterCreate.enable()
        }
    }

    private fun setListeners() {
        with(binding) {
            textInputDatePicker.setOnClickListener { showDatePicker( textInputDate.editText?.text.toString()) }
            textInputDate.editText?.onChanged {
                evaluateDocumentNumber(
                    autoCompleteDocumentType.tag.toString(),
                    textInputDocumentNumber.editText?.text.toString().length,
                    textInputDocumentNumber.editText?.text.toString(),
                    textInputDate.editText?.text.toString()
                )
            }
            textInputDocumentNumber.editText?.onChanged {
                evaluateDocumentNumber(
                    autoCompleteDocumentType.tag.toString(),
                    it.length,
                    it.toString(),
                    textInputDate.editText?.text.toString()
                )
            }
            autoCompleteGenderType.setOnItemClickListener { parent, _, position, _ ->
                autoCompleteGenderType.tag = (parent.getItemAtPosition(position) as Gender).id
            }
            autoCompleteDocumentType.setOnItemClickListener { parent, _, position, _ ->
                autoCompleteDocumentType.tag =
                    (parent.getItemAtPosition(position) as DocumentType).id
                clearInputValues()
                updateHintInputDate(autoCompleteDocumentType.tag.toString())
                updateDocumentMaxLength(autoCompleteDocumentType.tag.toString())
                toggleFields(autoCompleteDocumentType.tag.toString())
            }
            actionRegisterCreate.setOnClickListener { doCreate() }
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
                finishActivity()
            }
        }
    }

    private fun doCreate() {
        with(binding) {
            viewModel.create(
                registerInformation = RegisterInformation(
                    email = textInputEmail.editText?.text.toString(),
                    gender = autoCompleteGenderType.tag.toString(),
                    documentType = autoCompleteDocumentType.tag.toString(),
                    documentNumber = textInputDocumentNumber.editText?.text.toString(),
                    names = textInputFullName.editText?.text.toString(),
                    firstLastname = textInputPaternal.editText?.text.toString(),
                    secondLastname = textInputMaternal.editText?.text.toString(),
                    date = textInputDate.editText?.tag.toString(),
                    numberPhone = requireArguments().getString("numberPhone", ""),
                    password = ""
                ),
                terms = checkBoxTerms.isChecked
            )
        }
    }

    private fun clearInputValues() {
        with(binding) {
            textInputDocumentNumber.editText?.clean()
            textInputDate.editText?.clean()
            textInputFullName.editText?.clean()
            textInputPaternal.editText?.clean()
            textInputMaternal.editText?.clean()
        }
    }

    private fun updateHintInputDate(documentType: String) {
        with(binding) {
            when (documentType) {
                DOCUMENT_TYPE_DNI ->
                    textInputDate.hint = getString(R.string.register_label_issuance_date)
                else ->
                    textInputDate.hint = getString(R.string.register_label_birthday)
            }.exhaustive
        }
    }

    private fun evaluateDocumentNumber(
        documentType: String,
        length: Int,
        text: String,
        date: String
    ) {
        when (documentType) {
            DOCUMENT_TYPE_DNI -> {
                if (documentType.isNotEmpty() &&
                    length == DOCUMENT_LENGTH_DNI &&
                    date.isNotEmpty()
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text,
                        issueDate = date
                    )
                }
            }
            DOCUMENT_TYPE_CE -> {
                if (documentType.isNotEmpty() &&
                    length == DOCUMENT_LENGTH_CE
                ) {
                    hideKeyboard()
                    viewModel.validateDocument(
                        documentType = documentType,
                        documentNumber = text
                    )
                }
            }
        }
    }

    private fun updateDocumentMaxLength(documentType: String) {
        with(binding) {
            when (documentType) {
                DOCUMENT_TYPE_DNI -> textInputDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(DOCUMENT_LENGTH_DNI)
                )
                DOCUMENT_TYPE_CE -> textInputDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(DOCUMENT_LENGTH_CE)
                )
                else -> textInputDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(DOCUMENT_LENGTH_ANY)
                )
            }.exhaustive
        }
    }

    private fun toggleFields(documentType: String) {
        with(binding) {
            when (documentType) {
                DOCUMENT_TYPE_DNI, DOCUMENT_TYPE_CE -> {
                    textInputFullName.editText?.disable()
                    textInputPaternal.editText?.disable()
                    textInputMaternal.editText?.disable()
                }
                else -> {
                    textInputFullName.editText?.enable()
                    textInputPaternal.editText?.enable()
                    textInputMaternal.editText?.enable()
                }
            }
        }
    }

    private fun goToNextView(registerInformation: RegisterInformation) {
        findNavController().navigate(
            R.id.action_RegisterCreateFragment_to_RegisterConfirmFragment,
            bundleOf(KEY_BUNDLE_REGISTER_INFORMATION to registerInformation)
        )
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    override fun onDateSet(
        view: DatePicker,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val selectedDate: String = SimpleDateFormat(PATTERN_DATE).format(calendar.time)
        binding.textInputDatePicker.setText(selectedDate)
        binding.textInputDatePicker.tag = selectedDate
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}