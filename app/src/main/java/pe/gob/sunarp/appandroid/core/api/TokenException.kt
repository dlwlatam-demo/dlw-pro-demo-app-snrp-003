package pe.gob.sunarp.appandroid.core.api

class TokenException : Exception {
    constructor() : super("Su sesión ha vencido, o ha ingresado datos incorrectos. Por favor inicie sesión nuevamente") {}
    constructor(message: String?) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
    constructor(cause: Throwable?) : super(cause) {}
}