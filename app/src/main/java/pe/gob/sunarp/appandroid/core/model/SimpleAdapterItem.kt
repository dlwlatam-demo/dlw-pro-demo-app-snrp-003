package pe.gob.sunarp.appandroid.core.model

data class SimpleAdapterItem(
    val title: String,
    val description: String
)