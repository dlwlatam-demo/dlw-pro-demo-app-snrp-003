package pe.gob.sunarp.appandroid.ui.services.certificate.naturalperson.conservator

import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoConservadorBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.ListAdapterItems
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PersonItem

@AndroidEntryPoint
class CertifiedConservatorFragment :
    BaseFragment<FragmentCertificadoConservadorBinding>(
        FragmentCertificadoConservadorBinding::inflate
    ) {

    private val viewModel: CertifiedConservatorViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var book: BooksByOffice? = null
    private var saveProcess: SaveProcess? = null
    var interviniente: PersonItem? = null
    var tipoSolicitud: Int = 0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun onResume() {
        super.onResume()
        setupAdapter()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()

        with(binding) {

            val itemsList = resources.getStringArray(R.array.conservator_types_request)
            etRequest.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, itemsList))


            val adapterSupportsItems = ListAdapterItems(arrayListOf()) {
                validateFields()
                hideKeyboard()
            }
            etNumero.onChanged { validateFields() }
            etFolio.onChanged { validateFields() }
            etTomo.onChanged { validateFields() }
            etApePaternoInterdicto.onChanged { validateFields() }
            etApePaternoInterdicto.filterMayus()
            tilSecondLastnameInjunction.editText?.filterMayus()
            etNameInterdicto.onChanged { validateFields() }
            etNameInterdicto.filterMayus()
            tilFirstLastnameConservator.editText?.onChanged { validateFields() }
            tilFirstLastnameConservator.editText?.filterMayus()
            tilSecondLastnameConservator.editText?.onChanged { validateFields() }
            tilSecondLastnameConservator.editText?.filterMayus()
            tilNamesConservator.editText?.onChanged { validateFields() }
            tilNamesConservator.editText?.filterMayus()
            //etFolio.filterOnlyMayus()
            //etTomo.filterOnlyMayus()
            etNroAsiento.filterMaxLengthAsiento()
            etNumero.filterOnlyPartida()
            rvCurador.apply {
                adapter = adapterSupportsItems
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }



            btnCurador.setOnClickListener {
                hideKeyboard()
                var lastname =
                    (tilFirstLastnameConservator.editText?.text ?: Constants.EMPTY).toString()
                var lastname2 =
                    (tilSecondLastnameConservator.editText?.text ?: Constants.EMPTY).toString()
                var name = (tilNamesConservator.editText?.text ?: Constants.EMPTY).toString()

                if (name.isNotEmpty() && lastname.isNotEmpty()) {
                    val interviniente = PersonItem(4, 1, name, lastname, lastname2, Constants.EMPTY)
                    adapterSupportsItems.addItem(interviniente)

                    tilFirstLastnameConservator.editText?.clean()
                    tilSecondLastnameConservator.editText?.clean()
                    tilNamesConservator.editText?.clean()
                    validateFields()
                } else {
                    container.snack("Para agregar un Curador ingrese sus nombres completos")
                }
            }

            btnApply.setOnClickListener {
                hideKeyboard()
                val nroPartida = etNumero.text.toString()
                val nameInterdicto = etNameInterdicto.text.toString()
                val lastnameInterdicto = tilFirstLastnameInjunction.editText?.text.toString()
                val lastname2Interdicto = tilSecondLastnameInjunction.editText?.text.toString()

                interviniente = PersonItem(3, 1, nameInterdicto, lastnameInterdicto, lastname2Interdicto, Constants.EMPTY)
                // adapterSupportsItems.addItem(interviniente) // TODO aca se añade y por eso se vizualiza en la lista de curadores, valdiar poqrue se hace esto

                certificate?.let {
                    etRequest.tag?.let { tipoPartida ->
                        if (tipoPartida == 2) {
                            tipoSolicitud = 2
                            val folio = etFolio.text.toString()
                            val tomo = etTomo.text.toString()

                            viewModel.validatePartidaTomo(
                                "$tomo-$folio",
                                legalRecord,
                                certificate,
                                office,
                                etBook.tag.toString(),
                                tipoPartida.toString()
                            )
                        } else {
                            tipoSolicitud = (tipoPartida as Int)
                            val tp = (tipoPartida as Int) + 1
                            viewModel.validatePartidaCurador(
                                nroPartida,
                                legalRecord,
                                certificate,
                                office,
                                tp.toString()
                            )
                        }
                    }
                }
            }


            etBook.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
        }
    }

    private fun setupAdapter() {
        val items = resources.getStringArray(R.array.conservator_types_request)
        binding.etRequest.apply {
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                tag = position
                if (position == 2) {
                    showBook()
                } else {
                    hideBook()
                }
                validateFields()
            }
        }
    }

    private fun getListCuradores(): ArrayList<PersonItem> {
        return (binding.rvCurador.adapter as ListAdapterItems).getData()
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            certificate?.areaId?.let { viewModel.getBooks(it) }
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
            onChange(onSaveProcess) { saveOnSaveProcess(it) }
            onChange(books) { fillBooks(it) }
            onChange(onPartidaProcess) { savePartida(it) }
            onChange(onPartidaProcessTomo) { savePartidaTomo(it) }
        }
    }

    private fun isVisbleBook(): Boolean = binding.clBook.isVisible

    private fun showBook() {
        with(binding) {
            clBook.show()
            tilBookFolio.editText?.clean()
            tilBookTomo.editText?.clean()
            tilRequestNumber.hide()
        }
    }

    private fun hideBook() {
        with(binding) {
            clBook.hide()
            tilRequestNumber.show()
            tilRequestNumber.editText?.clean()
        }
    }

    private fun savePartidaTomo(partidas: List<String>) {
        if (partidas.count() > 0) {
            var numpart = partidas.get(0)
            var nroPartida = binding.etNumero.text.toString()
            var nroAsientos = binding.tilNroAsiento.editText?.text.toString()
            var tomo = binding.etTomo.text.toString()
            var folio = binding.etFolio.text.toString()

            certificate?.let {
                val partida = Partida(
                    0,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    Constants.EMPTY,
                    numpart,
                    Constants.EMPTY
                )

                val paymentDataItem =
                    PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
                paymentDataItem.documentNumebr = Constants.EMPTY
                paymentDataItem.businessName = Constants.EMPTY
                paymentDataItem.typePerson = Constants.PERSON_NATURAL
                paymentDataItem.asientos = nroAsientos
                paymentDataItem.partida = partida
                paymentDataItem.tomo = tomo
                paymentDataItem.folio = folio
                interviniente?.let {
                    paymentDataItem.lstCuradores = getListCuradores().apply { add(it) }
                }
                Log.e("CuradoresEPAV", paymentDataItem.lstCuradores.toString())
                Log.e("Codigo Libro", binding.etBook.tag.toString())
                paymentDataItem.partida!!.codigoLibro = binding.etBook.tag.toString()

                findNavController().navigate(
                    R.id.action_certifiedConservatorFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to null,
                        "ammount" to certificate?.preOffice!!.toDouble()
                    )
                )
            }
        } else {
            binding.container.snack("Lo sentimos, ninguna partida cumple con los criterios de búsqueda especificados.")
        }
    }

    private fun savePartida(partida: Partida) {
        var nroPartida = binding.etNumero.text.toString()
        var nroAsientos = binding.tilNroAsiento.editText?.text.toString()

        if (partida.estado == 0) {
            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.businessName = Constants.EMPTY
            paymentDataItem.typePerson = Constants.PERSON_NATURAL
            paymentDataItem.asientos = nroAsientos
            paymentDataItem.partida = partida
            if (tipoSolicitud == 1) { //  Ficha
                paymentDataItem.partida!!.numFicha = nroPartida
            } else if (tipoSolicitud == 0) {
                paymentDataItem.partida!!.numPartida = nroPartida
            }
            interviniente?.let {
                paymentDataItem.lstCuradores = getListCuradores().apply { add(it) }
            }
            Log.e("CuradoresEPAV", paymentDataItem.lstCuradores.toString())

            findNavController().navigate(
                R.id.action_certifiedConservatorFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to office,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )
        } else {
            binding.container.snack(partida.msj)
        }
    }

    fun validateFields() {
        with(binding) {
            val nroRequest = etNumero.toString()
            val nameInterdicto = etNameInterdicto.toString()
            val lastnameInterdicto = etApePaternoInterdicto.toString()

            etRequest.tag?.let { type ->
                etRegisterOffice.tag?.let { offi ->
                    val folio = etFolio.text.toString()
                    val tomo = etTomo.text.toString()

                    if (nroRequest.isNotEmpty() && nameInterdicto.isNotEmpty() && lastnameInterdicto.isNotEmpty() &&
                        getListCuradores().isNotEmpty() && if (isVisbleBook()) (folio.isNotEmpty() && tomo.isNotEmpty()) else true
                    ) {
                        btnApply.enable()
                    } else {
                        btnApply.disable()
                    }
                }
            }
        }
    }

    private fun fillBooks(items: List<BooksByOffice>) {
        binding.etBook.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as BooksByOffice)
                book = officeSelected
                tag = officeSelected.codLibro
                validateFields()
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
            }
        }
    }

    private fun saveOnSaveProcess(process: SaveProcess) {
        saveProcess = process
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it) }
        }
    }
}
