package pe.gob.sunarp.appandroid.ui.contact

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.home.HomeRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class ContactSendViewModel @Inject constructor(
    private val repository: HomeRemoteRepository
) : BaseViewModel() {

    val onFormError = SingleLiveEvent<String>()
    val onSuccess = SingleLiveEvent<String?>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun sendComment(comment: String, image: String) {
        showLoading.value = true
        viewModelScope.launch {
            when (val status = validateForm(comment)) {
                is FormStatus.Ok -> {
                    when (val result = repository.sendComment(comment, image)) {
                        is DataResult.Success -> onSuccess(result.data)
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> {
                    onFormError.value = status.error
                    showLoading.value = false
                }
            }
        }
    }

    private fun validateForm(
        comment: String
    ): FormStatus {
        if (comment.isEmpty()) return FormStatus.Ko("Ingrese el mensaje a enviar")
        return FormStatus.Ok
    }

    private fun onSuccess(data: String?) {
        onSuccess.value = data
        showLoading.value = false
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }
}