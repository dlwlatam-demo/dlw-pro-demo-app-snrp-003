package pe.gob.sunarp.appandroid.ui.services.certificate.legalperson.certificatevigenciapoderpersjur

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificadoVigenciaPoderPersJuridicaViewModel @Inject constructor(
    private val parameterRepository: CertificateRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    var selectOficinaRegistral = SingleLiveEvent<RegistrationOffice>()
    var appliesTypes = SingleLiveEvent<List<ItemSpinner>>()
    var selectAppliesTypes = SingleLiveEvent<ItemSpinner>()
    var librosRegistrales = SingleLiveEvent<List<LibroRegistral>>()
    var selectLibroRegistral = SingleLiveEvent<LibroRegistral>()

    val estadoPartida = SingleLiveEvent<Partida>()
    val estadoPartidaBool = SingleLiveEvent<Boolean>()
    val estadoPartidaReal = SingleLiveEvent<List<String>?>()

    fun getParameters() {
        getOficinasRegistrales()
        getAppliesTypes()
    }

    private fun getOficinasRegistrales() {
        viewModelScope.launch {
            loading.value = true
            when (val result = parameterRepository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun getAppliesTypes() {
        viewModelScope.launch {
            appliesTypes.value = listOf(
                ItemSpinner("00", "Número de partida"),
                ItemSpinner("01", "Ficha"),
                ItemSpinner("02", "Tomo/Folio")
            )
        }
    }

    fun isTomoFolio() = selectAppliesTypes.value?.id == "02"
    fun isPartida() = selectAppliesTypes.value?.id == "00"

    fun isFicha() = selectAppliesTypes.value?.id == "01"

    fun getLibrosRegistrales(certificate: CertificateType?) {
        viewModelScope.launch {
            loading.value = true
            when (val result =
                parameterRepository.getLibrosRegistrales(certificate?.areaId.orEmpty())) {
                is DataResult.Success -> OnGetLibrosRegistrales(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    fun solicitar(
        certificate: CertificateType?,
        numero: String
    ) {
        viewModelScope.launch {
            loading.value = true
            if (isPartida()) {
                when (val result = parameterRepository.validaPartidaxRefNumPart(
                    selectOficinaRegistral.value?.regPubId.orEmpty(),
                    selectOficinaRegistral.value?.oficRegId.orEmpty(),
                    certificate?.areaId.orEmpty(),
                    numero
                )) {
                    is DataResult.Success -> onValidarPartidaSuccess(result.data)
                    is DataResult.Failure -> onError(result.exception)
                }
            } else {
                when (val result = parameterRepository.obtenerNumPartidaReal(
                    if (selectAppliesTypes.value?.id == "01") "1" else "2",
                    numero,
                    selectOficinaRegistral.value?.regPubId.orEmpty(),
                    selectOficinaRegistral.value?.oficRegId.orEmpty(),
                    certificate?.areaId.orEmpty()
                )) {
                    is DataResult.Success -> onValidarPartidaRealSuccess(result.data)
                    is DataResult.Failure -> onError(result.exception)
                }
            }
        }
    }

    fun onValidarPartidaBoolSuccess(data: Boolean) {
        loading.value = false
        estadoPartidaBool.value = data
    }

    fun onValidarPartidaSuccess(data: Partida) {
        loading.value = false
        estadoPartida.value = data
    }

    fun onValidarPartidaRealSuccess(data: List<String>?) {
        loading.value = false
        estadoPartidaReal.value = data
    }

    fun selectLibroRegistral(data: LibroRegistral) {
        selectLibroRegistral.value = data
    }


    private fun OnGetLibrosRegistrales(data: List<LibroRegistral>) {
        loading.value = false
        librosRegistrales.value = data
    }


    fun setSelectOficinaRegistral(data: RegistrationOffice) {
        selectOficinaRegistral.value = data
    }

    fun getSelectOficinaRegistral(): RegistrationOffice? {
        return selectOficinaRegistral?.value
    }

    fun setSelectApplyTypes(data: ItemSpinner) {
        selectAppliesTypes.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}