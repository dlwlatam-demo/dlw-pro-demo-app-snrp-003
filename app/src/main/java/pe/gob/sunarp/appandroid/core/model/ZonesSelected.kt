package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ZonesSelected(
    val items: MutableList<SearchZone>
): Parcelable
