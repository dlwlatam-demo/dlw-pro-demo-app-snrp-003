package pe.gob.sunarp.appandroid.ui.services.certificate.property.vehicleregistration

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertifiedVehicleRegistrationViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository,
    private val paymentRepository: PaymentRemoteRepository,
    private val userRegister: UserRegistrationRemoteRepository
) : BaseViewModel() {

    val docTypes = SingleLiveEvent<List<DocumentType>>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    val loading = SingleLiveEvent<Boolean>()
    val onFormError = SingleLiveEvent<String>()

    val isValidDocumentDNI = SingleLiveEvent<DocumentDNI>()
    val isValidDocumentCE = SingleLiveEvent<DocumentCE>()

    val onPartidaProcess = SingleLiveEvent<Partida>()
    val documentTypeSaved = MutableLiveData<String>()
    val documentSaved = MutableLiveData<String>()

    fun getPersonData(ammount: String) = paymentRepository.getPaymentData(ammount)

    fun getSpinners() {
        viewModelScope.launch { loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

  /*  fun getDocumentsSpinners(context: Context) {
        viewModelScope.launch { loading.value = true
            when (val result = context.resources.getString(R.string.arr)) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }*/


    fun validatePartidaCGV(
        nroPartida: String,
        tipoNumero: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.validatePartidaCGV(
                    nroPartida,
                    tipoNumero,
                    legalRecord,
                    certificate,
                    office
                )
            ) {
                is DataResult.Success -> validatePartidaProcess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    fun validatePartidaProcess(data: Partida) {
        loading.value = false
        onPartidaProcess.value = data
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }
}