package pe.gob.sunarp.appandroid.ui.services.certificate.utils


import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.model.Partida
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarCriRequest
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarPartidaCargadaDesgravamenRequest

@Parcelize
data class PaymentDataItem(var name: String, var lastname: String, var lastname2: String) :
    Parcelable {

    var documentNumebr: String = Constants.EMPTY
    var businessName: String = Constants.EMPTY
    var typePerson: String = Constants.EMPTY
    var partida: Partida? = null
    var zoneCode: String? = Constants.EMPTY
    var zoneCodeOffice: String? = Constants.EMPTY
    var request: ValidarCriRequest? = null
    var folio: String? = Constants.EMPTY
    var tomo: String? = Constants.EMPTY
    var asientos: String? = Constants.EMPTY
    var etApePaterno: String? = Constants.EMPTY
    var etApeMaterno: String? = Constants.EMPTY
    var etNombres: String? = Constants.EMPTY
    var etRazonSocial: String? = Constants.EMPTY
    var etCargo: String? = Constants.EMPTY
    var etDato: String? = Constants.EMPTY
    var requestPCD: ValidarPartidaCargadaDesgravamenRequest? = null
    var numero: String? = Constants.EMPTY
    var apoderado: String? = Constants.EMPTY
    var lstOtorgantes: ArrayList<PersonItem>? = null
    var lstApoyos: ArrayList<PersonItem>? = null
    var lstCuradores: ArrayList<PersonItem>? = null
    var lstPoderdantes: ArrayList<PersonItem>? = null
    var lstApoderados: ArrayList<PersonItem>? = null
    var expediente: String? = Constants.EMPTY
    var nomEmbarcacion: String? = Constants.EMPTY
    var documentType: String = Constants.EMPTY

    override fun toString(): String {
        return "${name} ${lastname}  ${lastname2}"
    }
}
