package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.status

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group.StatusInfoGroup
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.custom.ui.toggleVisibility
import pe.gob.sunarp.appandroid.databinding.LayoutRequestStatusBinding

class StatusInformation @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), StatusInformationInterface {

    private val binding: LayoutRequestStatusBinding =
        LayoutRequestStatusBinding.inflate(LayoutInflater.from(context), this)
    private var status: String = ""

    init {
        context.withStyledAttributes(attrs, R.styleable.STATUSINFO) {
            getString(R.styleable.REQSTATUS_title).let { status = it ?: "" }
        }
        configViews()

    }

    private fun configViews() {
        binding.apply {
            tvStatus.text = status
            btnSeeDetail.setOnClickListener {
                userGroup.toggleVisibility()
                //destinationGroup.toggleVisibility()
                destinationGroup.hide()
                paymentGroup.toggleVisibility()
                val text =
                    if (userGroup.isVisible)
                        context.getString(R.string.hide_detail)
                    else
                        context.getString(R.string.see_detail)
                btnSeeDetail.text = text
            }

        }
    }

    fun setStatus(status: String) {
        binding.tvStatus.text = status
    }

    fun setData(data: StatusInfoGroup) {
        binding.apply {
            statusGroup.setTitle(data.requestData.title)
            statusGroup.setAdapter(data.requestData.data)

            userGroup.setTitle(data.userData.title)
            userGroup.setAdapter(data.userData.data)

            destinationGroup.setTitle(data.destinationData.title)
            destinationGroup.setAdapter(data.destinationData.data)

            if( data.destinationData.data[0].description.toString().isEmpty()) {
                destinationGroup.hide()
            }
            else {
                destinationGroup.toggleVisibility()
            }
            paymentGroup.setTitle(data.paymentData.title)
            paymentGroup.setAdapter(data.paymentData.data)
        }
    }



    override fun getComponentView(): StatusInformation = this

    override fun getView(type: Int): LayoutRequestStatusBinding = binding

}