package pe.gob.sunarp.appandroid.ui.services.certificate.certificatefilter

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.CertificateType
import pe.gob.sunarp.appandroid.core.model.LegalRecord
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateFilterViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val certificateTypes = SingleLiveEvent<List<CertificateType>>()
    val legalRecords = SingleLiveEvent<List<LegalRecord>>()


    fun getLegalRecords() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getLegalRecords()) {
                is DataResult.Success -> onLegalRecordsSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getCertificateTypes(type:String,areaRegId:String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getCertificateTypes(type,areaRegId)) {
                is DataResult.Success -> onCertificateTypesSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    private fun isCertificateType(data: List<LegalRecord>) {
        loading.value = false
        legalRecords.value = data
    }


    private fun onLegalRecordsSuccess(data: List<LegalRecord>) {
        loading.value = false
        legalRecords.value = data
    }

    private fun onCertificateTypesSuccess(data: List<CertificateType>) {
        loading.value = false
        certificateTypes.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}