package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LiteralCertificate(
    val officeCode: String,
    val officeName: String,
    val zoneCode: String,
    val certNumber: String,
    val plateNumber: String,
    val fichaId: String,
    val tomoId: String,
    val fojaId: String,
    val address: String,
    val areaRegisDescription: String,
    val codLibro: String,
    val libroDescription: String,
    val regPubSiglas: String,
    val baja: String,
    val codigoGla: String,
    val enable: Boolean = true
) : Parcelable