package pe.gob.sunarp.appandroid.data.login.passrecovery

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PassCodeValidationRequest(
    @field:Json(name = "guid")
    val guid: String?,
    @field:Json(name = "codigoValidacion")
    val validationCode: String,
)