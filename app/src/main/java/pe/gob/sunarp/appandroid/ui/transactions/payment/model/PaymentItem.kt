package pe.gob.sunarp.appandroid.ui.transactions.payment.model

data class PaymentItem(
    val name: String,
    val firstLastname: String,
    val secondLastname: String,
    val email: String,
    val ammount: String,
    val documentNumber: String,
    val type: TypePersonPayment = TypePersonPayment.PERSON_NATURAL,
) {

    fun isLegalPerson() = type == TypePersonPayment.PERSON_LEGAL
}

data class BasicPaymentData(
    val name: String,
    val firstLastname: String,
    val secondLastname: String,
    val email: String
) {

    fun getLastName(): String {
        return "$firstLastname $secondLastname"
    }
}

enum class TypePersonPayment{
    PERSON_NATURAL, PERSON_LEGAL
}