package pe.gob.sunarp.appandroid.ui.welcome.onboarding.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import pe.gob.sunarp.appandroid.ui.welcome.onboarding.items.OnBoardingItemFragment
import java.io.Serializable

class OnBoardingAdapter(fa: FragmentActivity, private val items: List<OnBoardingViewPageItem>) :
    FragmentStateAdapter(fa) {

    override fun getItemCount() = items.size
    override fun createFragment(position: Int) = OnBoardingItemFragment.newInstance(items[position])

}

data class OnBoardingViewPageItem(
    val image: Int,
    val title: String,
    val description: String
) : Serializable

