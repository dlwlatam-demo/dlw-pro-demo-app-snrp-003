package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CertificateService (
    val id: String,
    val registrationTypeId: String,
    val description: String,
): Parcelable{
    override fun toString(): String {
        return description
    }
}