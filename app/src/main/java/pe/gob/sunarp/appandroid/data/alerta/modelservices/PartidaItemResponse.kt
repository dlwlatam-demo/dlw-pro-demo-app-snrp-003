package pe.gob.sunarp.appandroid.data.alerta.modelservices

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class PartidaItemResponse(

	@Json(name="area")
	val area: String? = null,

	@Json(name="nuPart")
	val nuPart: String? = null,

	@Json(name="estado")
	val estado: String? = null,

	@Json(name="idRgst")
	val idRgst: Int? = null,

	@Json(name="sede")
	val sede: String? = null,

	@Json(name="fecReg")
	val fecReg: String? = null,

	@Json(name="region")
	val region: String? = null,

	@Json(name="nuCont")
	override val nuCont: String,

	@Json(name="estadoAP")
	val estadoAP: String? = null,

	@Json(name="aaCont")
	override val aaCont: String,

): NotificationResponse, Parcelable
