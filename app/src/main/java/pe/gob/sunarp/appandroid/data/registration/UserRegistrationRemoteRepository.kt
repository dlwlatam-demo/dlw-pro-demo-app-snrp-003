package pe.gob.sunarp.appandroid.data.registration

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPES_ALLOWED
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.Utils
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.registration.register.CeRequest
import pe.gob.sunarp.appandroid.data.registration.register.DniRequest
import pe.gob.sunarp.appandroid.data.registration.sendSMS.SMSRequest
import pe.gob.sunarp.appandroid.data.registration.validateSMS.ValidationSMSRequest
import javax.inject.Inject

class UserRegistrationRemoteRepository @Inject constructor(
    private val dataSource: UserRegistrationDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun sendSMS(mobilePhone: String): DataResult<String?> = withContext(dispatcher) {
        when (val result =
            dataSource.sendSMSMessage(
                SMSRequest(mobilePhone),
                getHost(BuildConfig.SUNARP_MTO_HOST)
            )) {
            is DataResult.Success -> {
                if (result.data.responseId == "1" || result.data.responseId == "2") {
                    preferences.guid = result.data.guid
                    DataResult.Success(result.data.description)
                } else {
                    DataResult.Failure(Exception(result.data.description))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun validateSMS(code: String): DataResult<String?> = withContext(dispatcher) {
        when (val result = dataSource.validateSMSMessage(
            ValidationSMSRequest(code, preferences.guid ?: ""),
            getHost(BuildConfig.SUNARP_MTO_HOST)
        )) {
            is DataResult.Success -> {
                if (result.data.responseId == "1") {
                    preferences.guid = result.data.guid
                    DataResult.Success(result.data.description)
                } else {
                    DataResult.Failure(Exception(result.data.description))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun getDocTypesList(): DataResult<List<DocumentType>> = withContext(dispatcher) {
        when (val result =
            dataSource.getDocTypes(
                preferences.guid ?: "",
                getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
            )) {
            is DataResult.Success -> {
                DataResult.Success(
                    result.data
                        .filter { type -> DOCUMENT_TYPES_ALLOWED.any { it == type.docTypeId } }
                    .map {
                        it.toModel()
                    })
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun getGenderList(): DataResult<List<Gender>> = withContext(dispatcher) {
        DataResult.Success(Utils.getGender())
    }

    suspend fun registerUser(
        registerInformation: RegisterInformation
    ): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.registerUser(
            registerInformation.toRequest(preferences.guid, BuildConfig.SUNARP_APP_ID),
            getHost(BuildConfig.SUNARP_MTO_HOST)
        )) {
            is DataResult.Success -> {
                if (result.data.responseId !== null) {
                    DataResult.Success(true)
                } else {
                    DataResult.Failure(Exception(result.data.error))
                }
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun validateDocumentDNI(
        documentNumber: String,
        issueDate: String
    ): DataResult<DocumentDNI> = withContext(dispatcher) {
        when (val result = dataSource.validateDocumentDNI(
            DniRequest(issueDate, documentNumber, preferences.guid),
            getHost(BuildConfig.SUNARP_MTO_HOST)
        )) {
            is DataResult.Success -> {
                DataResult.Success(result.data.toModel())
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun validateDocumentCE(
        documentNumber: String
    ): DataResult<DocumentCE> = withContext(dispatcher) {
        when (val result = dataSource.validateDocumentCE(
            CeRequest(documentNumber, preferences.guid),
            getHost(BuildConfig.SUNARP_MTO_HOST)
        )) {
            is DataResult.Success -> {
                DataResult.Success(result.data.toModel())
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }


    suspend fun informationDocumentDNI(
        documentNumber: String
    ): DataResult<DocumentDNI> = withContext(dispatcher) {
        when (val result = dataSource.informationDocumentDNI(
            getHost(BuildConfig.SUNARP_QUERIES_KEY),
            documentNumber,
            preferences.accessToken.toBearer()
        )) {
            is DataResult.Success -> {
                DataResult.Success(result.data.toModel())
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

    suspend fun informationDocumentCE(
        documentNumber: String
    ): DataResult<DocumentCE> = withContext(dispatcher) {
        when (val result = dataSource.informationDocumentCE(
            CeRequest(documentNumber, preferences.guid),
            getHost(BuildConfig.SUNARP_QUERIES_KEY),
            preferences.accessToken.toBearer()
        )) {
            is DataResult.Success -> {
                DataResult.Success(result.data.toModel())
            }
            is DataResult.Failure -> {
                DataResult.Failure(result.exception)
            }
        }
    }

}