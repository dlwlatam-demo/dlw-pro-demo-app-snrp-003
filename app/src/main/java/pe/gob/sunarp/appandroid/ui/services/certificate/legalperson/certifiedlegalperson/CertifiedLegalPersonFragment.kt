package pe.gob.sunarp.appandroid.ui.services.certificate.legalperson.certifiedlegalperson


import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.filterMayus
import pe.gob.sunarp.appandroid.core.model.CertificateType
import pe.gob.sunarp.appandroid.core.model.DocumentType
import pe.gob.sunarp.appandroid.core.model.LegalRecord
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoPersonaLegalBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragmentDirections
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult

@AndroidEntryPoint
class CertifiedLegalPersonFragment :
    BaseFragment<FragmentCertificadoPersonaLegalBinding>(FragmentCertificadoPersonaLegalBinding::inflate),
    PaymentInterface {


    private val viewModel: CertifiedLegalPersonViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var documentType: DocumentType? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()

        with(binding) {

            etBusinessName.onChanged { validateFields() }
            etBusinessName.filterMayus()

            btnApply.setOnClickListener {
                var businessName = etBusinessName.text.toString()

                var paymentDataItem =
                    PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
                paymentDataItem.documentNumebr = Constants.EMPTY
                paymentDataItem.businessName = businessName
                paymentDataItem.typePerson = Constants.PERSON_LEGAL

                findNavController().navigate(
                    R.id.action_certifiedLegalPersonFragment_to_certificatePaymentFragment,
                    bundleOf(
                        "certificate" to certificate,
                        "legalRecord" to legalRecord,
                        "dataItem" to paymentDataItem,
                        "office" to office,
                        "documentType" to documentType,
                        "ammount" to certificate?.preOffice!!.toDouble()
                    )
                )

            }
        }
    }


    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners()
            onChange(offices) { fillOffices(it) }
            onChange(loading) { showLoading(it) }
            onChange(onFormError) { showError(it) }
        }
    }


    fun validateFields() {
        with(binding) {
            etRegisterOffice.tag?.let {
                if (it.toString().isNotEmpty() &&
                    (etBusinessName.text.toString().isNotEmpty())
                ) {
                    btnApply.enable()
                } else {
                    btnApply.disable()
                }
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(ArrayAdapter(requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title, items))
            setOnClickListener { hideKeyboard() }
            setOnItemClickListener { parent, _, position, _ ->
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateFields()
                hideKeyboard()
            }
        }
    }

    override fun sendPaymentConfirmation(data: VisaNetResult) {
        /*
        with(binding) {
            val docNumber = etDocumentNumber.text.toString()
            certificate?.let {
                saveProcess?.let { it1 ->
                    viewModel.paymentProcess(
                        data, docNumber, "$paymentAmmount", it1, null
                    )
                }
            }
        }4551708161768059
        03/2028
        */
    }

    override fun sendPaymentFailed(ok: Boolean, result: VisaNetError) {
        navigateToResult(
            ok,
            result.toPayment(result.data.purchaseNumber, Page.CERTIFICATE)
        )
    }

    private fun navigateToResult(
        ok: Boolean,
        paymentResult: PaymentResult,
        transId: String? = null
    ) {
        val navOptions: NavOptions =
            NavOptions.Builder().setPopUpTo(R.id.servicesFragment, true).build()

        val action = CertifiedLegalPersonFragmentDirections
            .actionCertifiedLegalPersonFragmentToPaymentResultFragment(
                transId =  transId,
                paymentResult = paymentResult,
                ok = ok
            )
        findNavController().navigate(action, navOptions)
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}