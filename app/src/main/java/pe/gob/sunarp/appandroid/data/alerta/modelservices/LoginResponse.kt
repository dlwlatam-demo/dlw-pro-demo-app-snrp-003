package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginResponse(

	@Json(name="apMate")
	val apMate: String? = null,

	@Json(name="ruc")
	val ruc: String? = null,

	@Json(name="idRgst")
	val idRgst: String? = null,

	@Json(name="cnumCel")
	val cnumCel: String? = null,

	@Json(name="apPate")
	val apPate: String? = null,

	@Json(name="tiDocu")
	val tiDocu: String? = null,

	@Json(name="direccion")
	val direccion: String? = null,

	@Json(name="rzSocl")
	val rzSocl: String? = null,

	@Json(name="idOperTele")
	val idOperTele: String? = null,

	@Json(name="nombre")
	val nombre: String? = null,

	@Json(name="noDocu")
	val noDocu: String? = null,

	@Json(name="coNoti")
	val coNoti: String? = null,

	@Json(name="tiPers")
	val tiPers: String? = null,

	@Json(name="inNoti")
	val inNoti: String? = null,

	@Json(name="flEnvioSms")
	val flEnvioSms: String? = null,

	@Json(name="nuTele")
	val nuTele: String? = null,

	@Json(name="email")
	val email: String? = null,

	@Json(name="celNumber")
	val celNumber: String? = null,

	@Json(name="nameTiDocu")
	val nameTiDocu: String? = null
)
