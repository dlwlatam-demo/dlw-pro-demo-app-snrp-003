package pe.gob.sunarp.appandroid.core.model

import pe.gob.sunarp.appandroid.core.emptyOrHyphen
import pe.gob.sunarp.appandroid.data.certificate.request.PaymentProcessRequest
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

data class SaveProcess(
    val solicitudId: Int,
    val descripcion: String,
    val tsCrea: String,
    val total: String,
    val email: String,
    val solicitante: String,
    val tpoPago: String,
    val pagoSolicitudId: Int,
    val numeroRecibo: String,
    val numeroPublicidad: String,
    val codVerificacion: String,
    val secReciDetaAtenNac: Int,
    val codCertificado: String,
    val bgr: Boolean,
    val codDescarga: String,
    val codLibroOpc: String,
)