package pe.gob.sunarp.appandroid.data.transaction

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.NewsletterDTO
import pe.gob.sunarp.appandroid.core.model.Document
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.transaction.dto.TransactionDTO
import pe.gob.sunarp.appandroid.data.transaction.dto.TransactionStatusDTO
import pe.gob.sunarp.appandroid.data.transaction.request.TransactionRequest
import retrofit2.http.*

interface TransactionDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/transaccion")
    suspend fun getTransactions(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Query("timestamp") timestamp: Long
    ): DataResult<List<TransactionDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/{transId}")
    suspend fun getTransaction(
        @Path("transId") transactionId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<TransactionStatusDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/publicidad/{year}/{transId}")
    suspend fun getApplicationTransaction(
        @Path("year") year: String,
        @Path("transId") transactionId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<TransactionStatusDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/solicitud/desistimiento")
    suspend fun dropProcess(
        @Body request: TransactionRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/solicitud/aclaramiento")
    suspend fun certClarification(
        @Body request: TransactionRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/solicitud/subsanacion")
    suspend fun retrieval(
        @Body request: TransactionRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/documento/{transId}")
    suspend fun getDocument(
        @Path("transId") transactionId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<Document>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/boleta/informativa/descarga/{transId}")
    suspend fun getNewsletter(
        @Path("transId") transactionId: String,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<NewsletterDTO>


}