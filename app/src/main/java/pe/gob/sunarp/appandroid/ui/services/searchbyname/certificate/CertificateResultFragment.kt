package pe.gob.sunarp.appandroid.ui.services.searchbyname.certificate

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.CertificateOwnership
import pe.gob.sunarp.appandroid.core.model.ZoneRegisterInformation
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.core.toTitle
import pe.gob.sunarp.appandroid.databinding.FragmentSearchSuccessPaymentBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.searchbyname.certificate.adapter.CertificateResultAdapter

@AndroidEntryPoint
class CertificateResultFragment :
    BaseFragment<FragmentSearchSuccessPaymentBinding>(FragmentSearchSuccessPaymentBinding::inflate) {

    private val viewModel: CertificateResultViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        val userInformation: ZoneRegisterInformation? = arguments?.getParcelable("userInformation")
        with(binding) {
            registerArea.tvTitle.text = getString(R.string.search_by_name_register_area).toTitle()
            registerArea.tvDescription.text = userInformation?.areaRegistralDesc.orEmpty()

            userData.tvTitle.text = getString(R.string.search_by_name_input_area).toTitle()
            userData.tvDescription.text = userInformation?.getFullname()

        }
    }

    private fun initViewModels() {
        val userInformation: ZoneRegisterInformation? = arguments?.getParcelable("userInformation")
        with(viewModel) {
            initViewModelsErrorToken()
             getOwnershipQuery(userInformation)

            onChange(certificates) { showCertificates(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun getOwnershipQuery(userInformation: ZoneRegisterInformation?) {
        with(viewModel) {
            userInformation?.let {
                // getOwnershipQuery(it)
                Log.e("getOwnershipQuery", "inicio")
                getOwnershipQueryAsync(it)
            }
            runBlocking {
                delay(5000)
            }
        }
    }

    private fun showCertificates(list: List<CertificateOwnership>) {
        with(binding) {
            var size = "0"
            if (list != null && list.isNotEmpty()) {
                size = list.size.toString()
            }

            Log.e("getOwnershipQuery - ", size)

            tvTotalRegister.text = getString(R.string.search_by_name_total_registers, size)
            rvCertificates.apply {
                val certificateAdapter = CertificateResultAdapter(list)
                adapter = certificateAdapter
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) {
                binding.statusContainer.hide()
                binding.registerCard.hide()
                loading.show()
            } else {
                binding.statusContainer.show()
                binding.registerCard.show()
                loading.hide()
            }
        }
    }



    override fun showError(error: String?) {
        Log.e("ERROR", "$error")
        with(binding) {
            statusContainer.hide()
            registerCard.hide()
            tvError.text = error.orEmpty()
            tvError.show()
            super.showError(error)
        }

    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}