package pe.gob.sunarp.appandroid.core.model

data class SeatDetail(
    val acto: String,
    val seatType: String,
    val seatNumber: String,
    val exonerado: String,
    val pageNumber: String,
    val coverPages: String,
    val descripcionConcepto: String,
    val titulo: String,
    val exonerado2: String,
    val desActo: String = "",
    val desAsiento: String = "",
    val flagExonerado: String = "",
    val detalle: String = "",
    val nuPaginasCantidad: String = "",
    val paginas: String = "",
    var enable: Boolean = false,
    var nuSecu: String = ""
)