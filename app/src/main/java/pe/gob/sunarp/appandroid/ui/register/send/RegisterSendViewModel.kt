package pe.gob.sunarp.appandroid.ui.register.send

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterSendViewModel @Inject constructor(private val repository: UserRegistrationRemoteRepository) : BaseViewModel() {

    val isSent = SingleLiveEvent<String?>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun send(phone: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val status = validateForm(phone)) {
                is FormStatus.Ok -> {
                    when (val result = repository.sendSMS(phone)) {
                        is DataResult.Success -> {
                            isSent.value = result.data
                            showLoading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                is FormStatus.Ko -> {
                    onError(Exception(status.error))
                }
            }
        }
    }
    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private fun validateForm(
        numberPhone: String
    ): FormStatus {
        if (numberPhone.isEmpty()) return FormStatus.Ko("Olvidó el campo Teléfono")
        return FormStatus.Ok
    }
}