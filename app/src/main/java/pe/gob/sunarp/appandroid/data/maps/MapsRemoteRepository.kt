package pe.gob.sunarp.appandroid.data.maps

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.maps.request.DataDirectionsRequest
import pe.gob.sunarp.appandroid.data.maps.request.DirectionsResponse
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResultItem
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences

class MapsRemoteRepository(
    private val dataSource: MapsDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getOffices(): DataResult<List<ListOfficeResultItem>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getOffices(
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.listOfficeResult)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getMapaApis(dataSend: DataDirectionsRequest): DataResult<DirectionsResponse> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getMapaApis(
                    origin = dataSend.origin,
                    destination = dataSend.destination,
                    key = dataSend.key,
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

}