package pe.gob.sunarp.appandroid.core.model

sealed class DataModel {
    data class PropertyInformation(
        val documentNumber: String,
        val owner: String,
        val request: String,
        val zone: String,
        val office: String,
        val error: Boolean = false
    ) : DataModel()

    object NoDataAvailable : DataModel()
}