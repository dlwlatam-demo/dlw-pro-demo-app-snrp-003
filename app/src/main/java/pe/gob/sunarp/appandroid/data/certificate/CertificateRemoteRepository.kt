package pe.gob.sunarp.appandroid.data.certificate

import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.core.orHyphen
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.certificate.request.PaymentProcessRequest
import pe.gob.sunarp.appandroid.data.certificate.request.SaveProcessRequest
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarCriRequest
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarPartidaCargadaDesgravamenRequest
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.profile.information.UserProfile
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.TypePersonPayment

class CertificateRemoteRepository(
    private val dataSource: CertificateDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getLegalRecords(): DataResult<List<LegalRecord>> =
        withContext(dispatcher) {

            when (val result =
                dataSource.getLegalRecords(
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getCertificateTypes(
        type: String,
        areaRegId: String
    ): DataResult<List<CertificateType>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getCertificateTypes(
                    type, areaRegId,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getRegistrationOffices(): DataResult<List<RegistrationOffice>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getRegistrationOffices(
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getCertificateService(areaRegId: String): DataResult<List<CertificateService>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getCertificateServices(
                    areaRegId,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validateLiteralCertificate(request: LiteralCertificateRequest): DataResult<List<LiteralCertificate>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validateLiteralCertificate(
                    request.regPubId,
                    request.oficRegId,
                    request.areaRegId,
                    request.codGrupo,
                    request.tipoPartidaFicha,
                    request.numPart,
                    request.coServ,
                    request.coTipoRgst,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getDetalleAsientos(
        codZona: String,
        codOficina: String,
        codLibro: String,
        numPartida: String,
        fichaId: String,
        tomoId: String,
        fojaId: String,
        ofiSARP: String,
        coServicio: String,
        coTipoRegis: String,
    ): DataResult<List<SeatDetail>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getDetalleAsientos(
                    codZona,
                    codOficina,
                    codLibro,
                    numPartida,
                    fichaId.orHyphen(),
                    tomoId.orHyphen(),
                    fojaId.orHyphen(),
                    ofiSARP.orHyphen(),
                    coServicio,
                    coTipoRegis,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getDetalleAsientosVeh(
        zoneCode: String,
        officeCode: String,
        certificateNumber: String,
        plate: String
    ): DataResult<List<SeatDetail>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getDetalleAsientosVeh(
                    zoneCode,
                    officeCode,
                    certificateNumber,
                    plate,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getDetalleAsientosRMC(
        zoneCode: String,
        officeCode: String,
        certificateNumber: String,
    ): DataResult<List<SeatDetail>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getDetalleAsientosRMC(
                    zoneCode,
                    officeCode,
                    certificateNumber,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getPaymentValue(
        coServicio: String,
        cantPaginas: String
    ): DataResult<Double> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getPaymentValue(
                    coServicio,
                    cantPaginas,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toDoubleOrNull() ?: 0.0)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validatePartidaTomo(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        libroArea: String?,
        tipoPartidaFicha: String?,
    ): DataResult<List<String>> =
        withContext(dispatcher) {
            when (val result =
                dataSource.obtenerNumPartida(
                    tipoPartidaFicha.toString(),
                    nroPartida,
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.areaId.toString(),
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validatePartidaCurador(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        tipoPartida: String?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validaPartidaCurador(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.areaId.toString(),
                    certificate?.codGrupoLibroArea.toString(),
                    nroPartida,
                    tipoPartida.toString(),
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validatePartidaVigencia(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validaPartidaVigenciaDePoder(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.areaId.toString(),
                    nroPartida,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validatePartidaCGV(
        nroPartida: String,
        tipoNumero: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validaPartidaCGV(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.codGrupoLibroArea.toString(),
                    tipoNumero,
                    nroPartida,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validaPartidaCGA(
        nroPartida: String,
        tipoNumero: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validaPartidaCGA(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.codGrupoLibroArea.toString(),
                    tipoNumero,
                    nroPartida,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validaPartidaCGEP(
        nroPartida: String,
        tipoNumero: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validaPartidaCGEP(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.codGrupoLibroArea.toString(),
                    tipoNumero,
                    nroPartida,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun validatePartida(
        nroPartida: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validatePartidaApoyo(
                    office!!.regPubId,
                    office!!.oficRegId,
                    certificate?.areaId.toString(),
                    certificate?.codGrupoLibroArea,
                    nroPartida,
                    "-",
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun saveProcessPartida(
        partida: Partida,
        dataPayment: PaymentItem,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        nroPartida: String,
        nroAsientos: String,
        expediente: String = "",
        nomEmbarcacion: String = "",
        tpoPersona: String,
        documentType: String
    ): DataResult<SaveProcess> =
        withContext(dispatcher) {
            var sp = SaveProcessRequest(
                certificate?.certificateId,
                office?.regPubId,
                Constants.EMPTY,
                office?.regPubId + office?.oficRegId,
                partida.numPartida,
                Constants.EMPTY,
                partida.numPlaca,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
                dataPayment?.firstLastname,
                dataPayment?.secondLastname,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
                if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
                documentType,
                dataPayment?.documentNumber,
                dataPayment?.email,
                Constants.EMPTY,
                certificate?.preOffice,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )
            sp.refNumPart = partida.refNumPart
            sp.refNumPartMP = partida.refNumPartMP
            sp.asiento = nroAsientos
            sp.numPartidaMP = partida.refNumPartMP
            sp.numAsientoMP = nroAsientos
            sp.codLibro = partida.codigoLibro
            sp.expediente = expediente
            sp.nomEmbarcacion = nomEmbarcacion

            when (val result =
                dataSource.saveProcess(
                    sp,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun saveProcessPartidaPNVigencias(
        partida: Partida,
        dataPayment: PaymentItem,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        nroPartida: String,
        nroAsientos: String,
        tomo: String,
        folio: String,
        nomEmbarcacion: String = "",
        tpoPersona: String,
        tpoDocument:String
    ): DataResult<SaveProcess> =
        withContext(dispatcher) {
            var sp = SaveProcessRequest(
                certificate?.certificateId,
                office?.regPubId,
                partida.codigoLibro,
                office?.regPubId + office?.oficRegId,
                partida.numPartida,
                Constants.EMPTY,
                Constants.EMPTY,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
                dataPayment?.firstLastname,
                dataPayment?.secondLastname,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
                if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
                tpoDocument,
                dataPayment?.documentNumber,
                dataPayment?.email,
                Constants.EMPTY,
                certificate?.preOffice,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )
            sp.refNumPart = partida.refNumPart
            sp.refNumPartMP = partida.refNumPartMP
            sp.asiento = nroAsientos
            sp.numPartidaMP = partida.refNumPartMP
            sp.numAsientoMP = nroAsientos
            sp.codLibro = partida.codigoLibro
            sp.folio = folio
            sp.tomo = tomo
            sp.tipPerVP = typePerson
            sp.nomEmbarcacion = nomEmbarcacion

            Log.e("Sp.tomo",sp.tomo.toString())
            Log.e("Sp.folio",sp.folio.toString())
            when (val result =
                dataSource.saveProcess(
                    sp,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun saveProcessCertifiedAdvertising(
        dataPayment: PaymentItem?,
        documentNumber: String,
        firstLastname: String,
        secondLastname: String,
        names: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        documentType: String,
        businessName: String,
        typeDocumentPoN: String
    ): DataResult<SaveProcess> =
        withContext(dispatcher) {
            var sp = SaveProcessRequest(
                certificate?.certificateId,
                office?.regPubId,
                Constants.EMPTY,
                office?.regPubId + office?.oficRegId,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
                dataPayment?.firstLastname,
                dataPayment?.secondLastname,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
                if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
                documentType,
                dataPayment?.documentNumber,
                dataPayment?.email,
                Constants.EMPTY,
                certificate?.preOffice,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )
            sp.tipPerPN = typePerson
            sp.apePatPN = firstLastname
            sp.apeMatPN = secondLastname
            sp.nombPN = names
            sp.razSocPN = businessName
            sp.tipoDocPN = typeDocumentPoN
            sp.numDocPN = documentNumber

            when (val result =
                dataSource.saveProcess(
                    sp,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun saveProcess(
        certificateId: String,
        literalCertificate: LiteralCertificate,
        apellidoPaterno: String,
        apellidoMaterno: String,
        nombre: String,
        razonSocial: String,
        numDoc: String,
        numSeats: String,
        cantPaginasExon: String,
        requestedPages: String,
        ammount: String,
        office: RegistrationOffice?,
        tpoPersona: String,
        documentType: String
    ): DataResult<SaveProcess> =
        withContext(dispatcher) {
            when (val result =
                dataSource.saveProcess(
                    SaveProcessRequest(
                        certificateId,
                        literalCertificate.areaRegisDescription,
                        literalCertificate.codLibro,
                        office?.regPubId + office?.oficRegId,
                        literalCertificate.certNumber,
                        literalCertificate.fichaId,
                        literalCertificate.plateNumber,
                        tpoPersona,
                        apellidoPaterno,
                        apellidoMaterno,
                        if (tpoPersona == "N") nombre else "",
                        if (tpoPersona == "J") razonSocial else "",
                        documentType,
                        numDoc,
                        "",
                        "",
                        ammount,
                        numSeats,
                        cantPaginasExon = cantPaginasExon,
                        requestedPages,
                        "",
                        ""
                    ),
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )

            )
            {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun paymentProcess(
        request: PaymentProcessRequest,
    ): DataResult<PaymentProcess> =
        withContext(dispatcher) {
            when (val result =
                dataSource.paymentProcess(
                    request,
                    preferences.accessToken.toBearer(),
                    getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun getDocTypeList(typePer: String): DataResult<List<DocumentType>> =
        withContext(dispatcher) {
            when (val result = dataSource.getDocumentTypes(
                preferences.accessToken.toBearer(),
                BuildConfig.SUNARP_PARAMETERS_HOST,
                typePer
            )) {
                is DataResult.Success -> {
                    DataResult.Success(result.data
                        .map {
                            it.toModel()
                        })
                }
                is DataResult.Failure -> {
                    DataResult.Failure(result.exception)
                }
            }
        }

    suspend fun validarCri(request: ValidarCriRequest): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result = dataSource.validarCri(
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY),
                request.regPubId, request.oficRegId,
                request.areaRegId, request.numPart,
                request.tipoCRI, request.codGrupo,
                request.typePartida
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun guardarSolicitudCertificate(
        inforequest : SaveProcessRequest
    ): DataResult<SaveProcess> =
        withContext(dispatcher) {
            Log.e("Tomo y Folio", inforequest.tomo.toString())
            Log.e("Tomo y Folio", inforequest.folio.toString())
            when (val result = dataSource.guardarSolicitudCertificate(
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_PROPERTIES_KEY),
                inforequest
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun getLibrosRegistrales(areaRegId: String): DataResult<List<LibroRegistral>> =
        withContext(dispatcher) {
            when (val result = dataSource.getLibrosRegistrales(
                preferences.accessToken.toBearer(),
                getHost(BuildConfig.SUNARP_PARAMETERS_HOST),
                areaRegId
            )) {
                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validarPartidaCargadaDesgravamen(request: ValidarPartidaCargadaDesgravamenRequest): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result = dataSource.validarPartidaCargadaDesgravamen(
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY),
                request.regPubId, request.oficRegId, request.areaRegId, request.numPart,
                request.codGrupo, request.codigoLibro, request.tipoBusqueda,
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validaPartidaxRefNumPart(
        regPubId: String,
        oficRegId: String,
        areaRegId: String,
        numPart: String
    ): DataResult<Partida> =
        withContext(dispatcher) {
            when (val result = dataSource.validaPartidaxRefNumPart(
                regPubId, oficRegId, areaRegId, numPart,
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
            )) {
                is DataResult.Success -> DataResult.Success(result.data.toModel())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun obtenerNumPartida(
        tipo: String,
        numero: String,
        regPubId: String,
        oficRegId: String,
        areaRegId: String
    ): DataResult<Boolean> =
        withContext(dispatcher) {
            when (val result = dataSource.obtenerNumPartida(
                tipo, numero, regPubId, oficRegId, areaRegId,
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
            )) {
                is DataResult.Success -> {
                    if (result.data.isNotEmpty()) {
                        DataResult.Success(true)
                    } else {
                        DataResult.Success(false)
                    }
                }
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun obtenerNumPartidaReal(
        tipo: String,
        numero: String,
        regPubId: String,
        oficRegId: String,
        areaRegId: String
    ): DataResult<List<String>?> =
        withContext(dispatcher) {
            when (val result = dataSource.obtenerNumPartida(
                tipo, numero, regPubId, oficRegId, areaRegId,
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
            )) {
                is DataResult.Success -> {
                    if (result.data.isNotEmpty()) {
                        DataResult.Success(result.data)
                    } else {
                        DataResult.Success(null)
                    }
                }
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }


    suspend fun getBooks(areaRegId: String): DataResult<List<BooksByOffice>> =
        withContext(dispatcher) {
            when (val result = dataSource.getBooks(
                areaRegId,
                preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
            )) {

                is DataResult.Success -> DataResult.Success(result.data.map { it.toModel() })
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }
}

