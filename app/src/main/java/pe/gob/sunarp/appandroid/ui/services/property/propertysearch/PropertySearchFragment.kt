package pe.gob.sunarp.appandroid.ui.services.property.propertysearch


import android.app.DatePickerDialog
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.DocumentCE
import pe.gob.sunarp.appandroid.core.model.DocumentDNI
import pe.gob.sunarp.appandroid.core.model.DocumentType
import pe.gob.sunarp.appandroid.core.model.UserInformation
import pe.gob.sunarp.appandroid.databinding.FragmentPropertySearchBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.common.DatePickerFragment
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

@AndroidEntryPoint
class PropertySearchFragment :
    BaseFragment<FragmentPropertySearchBinding>(FragmentPropertySearchBinding::inflate),
    DatePickerDialog.OnDateSetListener {

    private val viewModel: PropertySearchViewModel by viewModels()
    private var validateDocument = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        initViews()
    }

    private fun initViews() {
        var dateRight: String = ""
        binding.apply {
            tilIssueDate.tag = ""
            autoCompleteDocumentType.tag = ""
            etIssueDate.setOnClickListener { newShowDatePicker(tilIssueDate.editText?.text.toString()) }

            autoCompleteDocumentType.setOnItemClickListener { parent, _, position, _ ->
                autoCompleteDocumentType.tag =
                    (parent.getItemAtPosition(position) as DocumentType).id
                clearInputValues()
                updateDocumentMaxLength(autoCompleteDocumentType.tag.toString())
                toggleFields(autoCompleteDocumentType.tag.toString())
            }

            tilIssueDate.editText?.onChanged {
                dateRight = tilIssueDate.editText?.text.toString()
                if (dateRight.isNotEmpty()){
                    dateRight = convertirFechaFormato(dateRight)
                }
                evaluateDocumentNumber(
                    autoCompleteDocumentType.tag.toString(),
                    tilDocumentNumber.editText?.text.toString().length,
                    dateRight
                )
            }

            tilDocumentNumber.editText?.onChanged {
                evaluateDocumentNumber(
                    autoCompleteDocumentType.tag.toString(),
                    it.length,
                    dateRight
                )
            }

            btnValidate.setOnClickListener {
                if (validateDocument)
                    viewModel.validateDocument(
                        documentType = autoCompleteDocumentType.tag.toString(),
                        documentNumber = tilDocumentNumber.editText?.text.toString(),
                        issueDate = dateRight
                    )
                else {
                    val user = UserInformation(
                        names = tilNames.editText?.text.toString(),
                        firstLastname = tilFirstLastname.editText?.text.toString(),
                        secondLastname = tilSecondLastname.editText?.text.toString(),
                        docType = autoCompleteDocumentType.tag.toString(),
                        docNumber = tilDocumentNumber.editText?.text.toString()
                    )

                    findNavController().navigate(
                        R.id.action_propertySearchFragment_to_propertyListFragment,
                        bundleOf("user" to user)
                    )
                }
            }
        }
    }

    private fun Fragment.newShowDatePicker(date: String?) {
        val calendar = Calendar.getInstance()
        date?.let {
            val dateItems = date.split("-")
            if (dateItems.size >= 3) {
                calendar[Calendar.MONTH] = dateItems[1].toInt() - 1
                calendar[Calendar.DAY_OF_MONTH] = dateItems[0].toInt()
                calendar[Calendar.YEAR] = dateItems[2].toInt()
            }
        }
        DatePickerFragment(
            this,
            calendar
        ).show(
            requireActivity().supportFragmentManager,
            this.tag
        )
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getDocTypes()
            onChange(docTypes) { fillDocType(it) }
            onChange(isValidDocumentDNI) { validDocumentDNI(it) }
            onChange(isValidDocumentCE) { validDocumentCE(it) }
            onChange(loading) { showLoading(it) }
        }
    }
    private fun convertirFechaFormato(fecha: String): String {
        val formatterInput = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val formatterOutput = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        val fechaLocal = LocalDate.parse(fecha, formatterInput)
        val fechaFormateada = fechaLocal.format(formatterOutput)

        return fechaFormateada
    }

    private fun validDocumentDNI(documentDNI: DocumentDNI) {
        with(binding) {
            userDataContainer.show()
            tilNames.editText?.disable()
            tilFirstLastname.editText?.disable()
            tilSecondLastname.editText?.disable()
            tilNames.editText?.setText(documentDNI.names)
            tilFirstLastname.editText?.setText(documentDNI.paternal)
            tilSecondLastname.editText?.setText(documentDNI.maternal)
            btnValidate.text = getString(R.string.action_begin_search)
            validateDocument = false
        }
    }

    private fun validDocumentCE(documentCE: DocumentCE) {
        with(binding) {
            userDataContainer.show()
            tilNames.editText?.disable()
            tilFirstLastname.editText?.disable()
            tilSecondLastname.editText?.disable()
            tilNames.editText?.setText(documentCE.names)
            tilFirstLastname.editText?.setText(documentCE.firstLastName)
            tilSecondLastname.editText?.setText(documentCE.secondLastName)
            btnValidate.text = getString(R.string.action_begin_search)
            validateDocument = false
        }
    }

    private fun clearInputValues() {
        with(binding) {
            tilIssueDate.editText?.clean()
            tilDocumentNumber.editText?.clean()
            validateDocument = true
            btnValidate.apply {
                disable()
                text = getString(R.string.action_validate_document)
            }
        }
    }

    private fun evaluateDocumentNumber(
        documentType: String,
        length: Int,
        date: String
    ) {
        when (documentType) {
            Constants.DOCUMENT_TYPE_DNI -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_DNI &&
                    date.isNotEmpty()
                ) {
                    hideKeyboard()
                    binding.btnValidate.enable()
                } else {
                    binding.btnValidate.disable()
                }
            }
            Constants.DOCUMENT_TYPE_CE -> {
                if (documentType.isNotEmpty() &&
                    length == Constants.DOCUMENT_LENGTH_CE
                ) {
                    hideKeyboard()
                    binding.btnValidate.enable()
                } else {
                    binding.btnValidate.disable()
                }
            }
        }
    }

    private fun toggleFields(documentType: String) {
        with(binding) {
            when (documentType) {
                Constants.DOCUMENT_TYPE_DNI -> {
                    tilIssueDate.show()
                    userDataContainer.hide()
                    tilDocumentNumber.show()
                    tilNames.editText?.disable()
                    tilFirstLastname.editText?.disable()
                    tilSecondLastname.editText?.disable()
                }
                Constants.DOCUMENT_TYPE_CE -> {
                    tilIssueDate.hide()
                    userDataContainer.hide()
                    tilDocumentNumber.show()
                    tilNames.editText?.disable()
                    tilFirstLastname.editText?.disable()
                    tilSecondLastname.editText?.disable()
                }
                else -> {
                    tilDocumentNumber.show()
                    tilNames.editText?.enable()
                    tilFirstLastname.editText?.enable()
                    tilSecondLastname.editText?.enable()
                }
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    private fun fillDocType(docs: List<DocumentType>) {
        binding.autoCompleteDocumentType.setAdapter(
            ArrayAdapter(
                requireContext(),
                R.layout.layout_simple_spinner_item,
                R.id.tv_title,
                docs
            )
        )
    }

    private fun updateDocumentMaxLength(documentType: String) {
        with(binding) {
            when (documentType) {
                Constants.DOCUMENT_TYPE_DNI -> tilDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(Constants.DOCUMENT_LENGTH_DNI)
                )
                Constants.DOCUMENT_TYPE_CE -> tilDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(Constants.DOCUMENT_LENGTH_CE)
                )
                else -> tilDocumentNumber.editText?.filters = arrayOf(
                    InputFilter.LengthFilter(Constants.DOCUMENT_LENGTH_ANY)
                )
            }.exhaustive
        }
    }

    override fun onDateSet(
        view: DatePicker,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val PATTERN_DATE = "dd/MM/yyyy"
        val selectedDate: String = SimpleDateFormat(PATTERN_DATE).format(calendar.time)
        binding.etIssueDate.setText(selectedDate)
        binding.etIssueDate.tag = selectedDate
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }


}