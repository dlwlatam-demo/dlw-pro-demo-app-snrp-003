package pe.gob.sunarp.appandroid.ui.services.searchbyname.certificate

import android.util.Log
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipAsyncDTO
import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipDTO
import pe.gob.sunarp.appandroid.core.model.CertificateOwnership
import pe.gob.sunarp.appandroid.core.model.ZoneRegisterInformation
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateResultViewModel @Inject constructor(
    private val repository: SearchRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val certificates = SingleLiveEvent<List<CertificateOwnership>>()
    val status = SingleLiveEvent<Boolean>()
    val guid = SingleLiveEvent<String>()

    fun getOwnershipQuery(information: ZoneRegisterInformation) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getOwnershipQuery(information)) {
                is DataResult.Success -> onSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun getOwnershipQueryAsync(information: ZoneRegisterInformation) {
        viewModelScope.launch {
            loading.value = true
            Log.e("getOwnershipQuery", "getOwnershipQueryAsync")

            when (val result = repository.getOwnershipQueryAsync(information)) {
                is DataResult.Success -> onSuccessAsync(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun getOwnershipQueryAsyncStatus (guid: String) {
        viewModelScope.launch {
            Log.e("getOwnershipQuery", "getOwnershipQueryAsyncStatus")

            when (val result = repository.getOwnershipQueryAsyncStatus (guid))  {
                is DataResult.Success -> onSuccessAsync(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onSuccess(data: List<CertificateOwnership>) {
        loading.value = false
        certificates.value = data
    }

    private fun onSuccessAsync(data: CertificateOwnershipAsyncDTO) {
        Log.e("getOwnershipQuery", "onSuccessAsync")

        if (!data.response.isNullOrEmpty() ) {
            Log.e("getOwnershipQuery", "certificates...")
            var lista = data.response!!
            certificates.value = lista.map { it.toModel() }
        }
        else {
            Log.e("getOwnershipQuery", "empty list...")
            certificates.value = emptyList()
        }
        Log.e("getOwnershipQuery: ", data.status.toString())
        if (data.status.equals("1") ) {
            status.value = true
            loading.value = false
            guid.value = data.guid.toString()
        }
        else {
            status.value = false
            guid.value = data.guid.toString()
            Log.e("getOwnershipQuery: ", guid.value.toString())

            getOwnershipQueryAsyncStatus(guid.value.toString())
            runBlocking {
                delay(5000)
            }

        }

    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}