package pe.gob.sunarp.appandroid.ui.recovery.verify

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.custom.ui.otp.listener.SUNOTPListener
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentRecoveryVerifyBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class RecoveryVerifyFragment :
    BaseFragment<FragmentRecoveryVerifyBinding>(FragmentRecoveryVerifyBinding::inflate) {

    private val viewModel: RecoveryVerifyViewModel by viewModels()

    private var otpCode: String = ""
    private var emailAddress: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
        setObservers()
        emailAddress = arguments?.getString("emailAddress") ?: ""
    }

    private fun initViews() {
        binding.textViewCodeResend.hide()
    }

    private fun setListeners() {
        with(binding) {

            binding.btnBack.setOnClickListener {
                findNavController().navigate(R.id.action_recoveryVerifyFragment_to_loginActivity)
            }

            actionRecoveryVerify.setOnClickListener { viewModel.verify(otpCode) }
            textViewCodeResend.setOnClickListener { viewModel.resend(emailAddress) }

            sunOTP.listener = object : SUNOTPListener {
                override fun onShowCount(time: String) {
                    textViewCounterTime.text = time
                }

                override fun onStarCount() {
                    textViewCounterTime.show()
                    textViewCodeResend.hide()
                }

                override fun onStopCount() {
                    textViewCounterTime.hide()
                    textViewCodeResend.show()
                }

                override fun onResendCode() {
                    textViewCounterTime.show()
                }

                override fun onComplete(otp: CharSequence) {
                    otpCode = otp.toString()
                }
            }
        }
    }

    private fun setObservers() {
        with(viewModel) {
            initViewModelsErrorToken()
            onErrorResend.observe(viewLifecycleOwner) { wasNotForwarded(it) }
            onError.observe(viewLifecycleOwner) { anError(it) }
            isVerified.observe(viewLifecycleOwner) { goToNextView(it) }
            isResend.observe(viewLifecycleOwner) { wasForwarded(it) }
            showLoading.observe(viewLifecycleOwner) { showLoading(it) }
        }
    }

    private fun wasNotForwarded(message: String?) {
        binding.sunOTP.default()
        showError(message)
    }

    private fun wasForwarded(message: String?) {
        binding.sunOTP.default()
        showError(message)
    }

    private fun showLoading(show: Boolean) {
        if (show) binding.loadingContainer.loading.show() else binding.loadingContainer.loading.hide()
    }

    private fun anError(message: String?) {
        binding.sunOTP.error()
        showError(message)
    }

    private fun goToNextView(message: String?) {
        binding.sunOTP.validated()
        //showError("hola")
        findNavController().navigate(R.id.action_RecoveryVerifyFragment_to_RecoveryConfirmFragment)
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}