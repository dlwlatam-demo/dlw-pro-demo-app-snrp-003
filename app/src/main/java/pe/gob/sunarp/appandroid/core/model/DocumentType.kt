package pe.gob.sunarp.appandroid.core.model

import java.io.Serializable

data class DocumentType(
    val id: String,
    val name: String,
    val description: String,
    val personId: String
) : Serializable {
    override fun toString(): String {
        return name
    }
}