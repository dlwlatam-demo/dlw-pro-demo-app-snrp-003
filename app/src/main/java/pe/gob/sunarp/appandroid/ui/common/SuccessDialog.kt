package pe.gob.sunarp.appandroid.ui.common

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.hideKeyboard
import pe.gob.sunarp.appandroid.databinding.LayoutSuccessModalBinding

class SuccessDialog : DialogFragment() {

    private var _binding: LayoutSuccessModalBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = LayoutSuccessModalBinding.inflate(LayoutInflater.from(context))
        binding.tvTitle.text = getString(R.string.success_message)
        binding.tvMessage.text = getString(R.string.label_observation_success)
        return AlertDialog.Builder(requireActivity(), R.style.MyThemeOverlayAlertDialog)
            .setView(binding.root)
            .setCancelable(false)
            .create()
    }

    private fun navigateUp() {
        hideKeyboard()
        findNavController().navigateUp()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        navigateUp()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}