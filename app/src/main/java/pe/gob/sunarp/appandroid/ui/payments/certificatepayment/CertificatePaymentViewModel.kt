package pe.gob.sunarp.appandroid.ui.payments.certificatepayment

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonParser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.certificate.request.PaymentProcessRequest
import pe.gob.sunarp.appandroid.data.certificate.request.SaveProcessRequest
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarCriRequest
import pe.gob.sunarp.appandroid.data.payment.NiubizRequest
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.BasicPaymentData
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.TypePersonPayment
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import javax.inject.Inject


@HiltViewModel
class CertificatePaymentViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository,
    private val paymentRepository: PaymentRemoteRepository,
    private val preferences: SunarpSharedPreferences
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onSaveProcess = SingleLiveEvent<SaveProcess>()
    val onPaymentProcess = SingleLiveEvent<PaymentProcess>()
    val sendPaymentExConfirmation = SingleLiveEvent<SaveProcess>()
    val onPaymentProcessEx = SingleLiveEvent<PaymentProcess>()
    val docTypes = SingleLiveEvent<List<DocumentType>>()
    var TAG = "CertificatePaymentViewModel"


    fun getPersonData(ammount: String) = paymentRepository.getPaymentData(ammount)

    fun create(
        payment: PaymentItem, terms: Boolean,
        certificateId: String,
        literalCertificate: LiteralCertificate,
        numSeats: String,
        cantPaginasExon: String,
        requestedPages: String,
        office: RegistrationOffice?,
        tpoPersona: String,
        documentType: String,

    ) {
        viewModelScope.launch {
            when (val status = validateForm(
                payment,
                terms
            )) {
                is FormStatus.Ok -> saveProcess(
                    payment,
                    certificateId,
                    literalCertificate,
                    numSeats,
                    cantPaginasExon,
                    requestedPages,
                    office,
                    tpoPersona,
                    documentType
                )
                is FormStatus.Ko -> onError(Exception(status.error))
            }
        }
    }

    private fun validateForm(
        item: PaymentItem,
        terms: Boolean
    ): FormStatus {
        if (item.type == TypePersonPayment.PERSON_LEGAL) {
            if (item.name.isEmpty()) return FormStatus.Ko("Olvidó el nombre jurídico")
            if (item.documentNumber.isEmpty()) return FormStatus.Ko("Olvidó el campo número de Documento")
            if (item.documentNumber.length != 11) return FormStatus.Ko("El Documento no es válido")
            if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        } else {
            if (item.name.isEmpty()) return FormStatus.Ko("Olvidó el nombre")
            if (item.firstLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido Paterno")
            if (item.secondLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido Materno")
            if (item.documentNumber.isEmpty()) return FormStatus.Ko("Olvidó el campo Documento")
            if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        }

        return FormStatus.Ok
    }

    private fun saveProcess(
        payment: PaymentItem,
        certificateId: String,
        literalCertificate: LiteralCertificate,
        numSeats: String,
        cantPaginasExon: String,
        requestedPages: String,
        office: RegistrationOffice?,
        tpoPersona: String,
        documentType: String
    ) {

        viewModelScope.launch {
            loading.value = true
            when (val result = repository.saveProcess(
                certificateId,
                literalCertificate,
                payment.firstLastname,
                payment.secondLastname,
                payment.name,
                payment.name,
                payment.documentNumber,
                numSeats,
                cantPaginasExon,
                requestedPages,
                payment.ammount,
                office,
                if(payment.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", //tpoPersona,
                documentType
            )) {
                is DataResult.Success -> {
                    if (payment.ammount == "0.0") {
                        successPaymentEx(result.data)
                    }else{
                        getPaymentKeys(payment, result.data)
                    }
                }
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun successPaymentEx(saveProcess: SaveProcess){
       sendPaymentExConfirmation.value = saveProcess
    }

    fun saveSolicitudCertificateSaveProcessRequestOrgano(
        dataItem: PaymentDataItem,
        partida: Partida,
        dataPayment: PaymentItem,
        certificate: CertificateType?,
        typePerson: String,
        documentType: String
    ) {
        loading.value = true

        val inforequest = SaveProcessRequest(
            certificate?.certificateId.orEmpty(),
            dataItem?.requestPCD?.areaRegId,
            partida?.codigoLibro,
            dataItem?.requestPCD?.regPubId + dataItem?.requestPCD?.oficRegId,
            partida?.numPartida,
            partida?.numFicha,
            Constants.EMPTY,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
            dataPayment?.firstLastname,
            dataPayment?.secondLastname,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
            if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
            documentType,
            dataPayment?.documentNumber, //preferences.userDocNumber,
            preferences.email,
            dataItem?.asientos,
            certificate?.preOffice.orEmpty(),
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY
        )
        inforequest.refNumPart = partida?.refNumPart
        inforequest.tomo = dataItem?.tomo
        inforequest.folio = dataItem?.folio
        inforequest.tipPerVP = dataItem?.typePerson
        inforequest.apePateVP = Constants.EMPTY
        inforequest.apeMateVP = Constants.EMPTY
        inforequest.nombVP = Constants.EMPTY
        inforequest.razSocVP = dataItem?.businessName
        inforequest.cargoApoderado = dataItem?.apoderado
        inforequest.datoAdic = dataItem?.etDato

        viewModelScope.launch {
            loading.value = true
            when (val result = repository.guardarSolicitudCertificate(
                inforequest
            )) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveSolicitudCertificateSaveProcessRequestPJ(
        dataItem: PaymentDataItem,
        partida: Partida,
        dataPayment: PaymentItem,
        certificate: CertificateType?,
        typePerson: String,
        documentType: String
    ) {
        loading.value = true

        val inforequest = SaveProcessRequest(
            certificate?.certificateId.orEmpty(),
            dataItem?.requestPCD?.areaRegId,
            partida?.codigoLibro,
            dataItem?.requestPCD?.regPubId + dataItem?.requestPCD?.oficRegId,
            partida?.numPartida,
            partida?.numFicha,
            Constants.EMPTY,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
            dataPayment?.firstLastname,
            dataPayment?.secondLastname,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
            if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
            documentType,
            dataPayment?.documentNumber, //preferences.userDocNumber,
            preferences.email,
            dataItem?.asientos,
            certificate?.preOffice.orEmpty(),
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY
        )
        inforequest.refNumPart = partida?.refNumPart
        inforequest.tomo = dataItem?.tomo
        inforequest.folio = dataItem?.folio
        inforequest.tipPerVP = dataItem.typePerson
        inforequest.apePateVP = Constants.EMPTY
        inforequest.apeMateVP = Constants.EMPTY
        inforequest.nombVP = Constants.EMPTY
        inforequest.razSocVP = dataItem?.businessName
        inforequest.cargoApoderado = dataItem?.apoderado
        inforequest.datoAdic = dataItem?.etDato

        viewModelScope.launch {
            loading.value = true
            when (val result = repository.guardarSolicitudCertificate(
                inforequest
            )) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveSolicitudCertificateSaveProcessRequestCharges(
        dataItem: PaymentDataItem,
        partida: Partida,
        dataPayment: PaymentItem,
        certificate: CertificateType?,
        typePerson: String,
        documentType: String
    ) {
        loading.value = true

        val inforequest = SaveProcessRequest(
            certificate?.certificateId.orEmpty(),
            dataItem?.requestPCD?.areaRegId,
            partida?.codigoLibro,
            dataItem?.requestPCD?.regPubId + dataItem?.requestPCD?.oficRegId,
            partida?.numPartida,
            Constants.EMPTY,
            Constants.EMPTY,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
            dataPayment?.firstLastname,
            dataPayment?.secondLastname,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
            if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
            documentType,
            dataPayment?.documentNumber, //preferences.userDocNumber,
            preferences.email,
            dataItem?.asientos,
            certificate?.preOffice.orEmpty(),
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY
        )
        inforequest.refNumPart = partida?.refNumPart
        inforequest.tomo = dataItem?.tomo
        inforequest.folio = dataItem?.folio
        inforequest.tipPerVP = dataItem?.typePerson
        inforequest.apePateVP = dataItem?.etApePaterno
        inforequest.apeMateVP = dataItem?.etApeMaterno
        inforequest.nombVP = dataItem?.etNombres
        inforequest.razSocVP = dataItem?.etRazonSocial
        inforequest.cargoApoderado = dataItem?.etCargo
        inforequest.datoAdic = dataItem?.etDato



        viewModelScope.launch {
            loading.value = true
            when (val result = repository.guardarSolicitudCertificate(
                inforequest
            )) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveSolicitudCertificateSaveProcessRequest(
        dataItem: PaymentDataItem,
        partida: Partida,
        dataPayment: PaymentItem,
        certificate: CertificateType?,
        typePerson: String,
        documentType: String
    ) {
        loading.value = true

        val inforequest = SaveProcessRequest(
            certificate?.certificateId.orEmpty(),
            dataItem?.requestPCD?.areaRegId,
            partida?.codigoLibro,
            dataItem?.requestPCD?.regPubId + dataItem?.requestPCD?.oficRegId,
            partida.numPartida,
            partida.numFicha,
            Constants.EMPTY,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
            dataPayment?.firstLastname,
            dataPayment?.secondLastname,
            if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
            if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
            documentType,
            dataPayment?.documentNumber, //preferences.userDocNumber,
            preferences.email,
            dataItem?.asientos,
            certificate?.preOffice.orEmpty(),
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY,
            Constants.EMPTY
        )
        inforequest.refNumPart = partida?.refNumPart
        inforequest.tomo = dataItem?.tomo
        inforequest.folio = dataItem?.folio
        inforequest.apePateVP = dataItem?.etApePaterno
        inforequest.apeMateVP = dataItem?.etApeMaterno
        inforequest.nombVP = dataItem?.etNombres
        inforequest.razSocVP = dataItem?.etRazonSocial
        inforequest.datoAdic = dataItem?.etDato
        inforequest.cargoApoderado = dataItem?.etCargo
        inforequest.tipPerVP = dataItem?.typePerson

        viewModelScope.launch {
            loading.value = true
            when (val result = repository.guardarSolicitudCertificate(
                inforequest
            )) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveSolicitudCertificate(
        partida: Partida,
        dataPayment: PaymentItem,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        request: ValidarCriRequest,
        typePerson: String,
        zoneCode: String,
        zoneCodeOffice: String,
        documentType: String,
    ) {
        loading.value = true

        viewModelScope.launch {
            loading.value = true

            val inforequest = SaveProcessRequest(
                certificate?.certificateId.orEmpty(),
                zoneCode,
                partida.codigoLibro,
                zoneCode + zoneCodeOffice,
                if (request.typePartida != "2") request.numPart else Constants.EMPTY,
                if (request.typePartida == "2") request.numPart else Constants.EMPTY,
                Constants.EMPTY,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) "N" else "J", // typePerson,
                dataPayment?.firstLastname,
                dataPayment?.secondLastname,
                if(dataPayment?.type == TypePersonPayment.PERSON_NATURAL) dataPayment?.name else "",
                if(dataPayment?.type == TypePersonPayment.PERSON_LEGAL) dataPayment?.name else "",
                documentType,
                dataPayment?.documentNumber, // preferences.userDocNumber,
                preferences.email,
                Constants.EMPTY,
                certificate?.preOffice.orEmpty(),
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )
            inforequest.refNumPart = partida.refNumPart

            when (val result = repository.guardarSolicitudCertificate(
                inforequest
            )) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveProcessPartida(
        partida: Partida,
        dataPayment: PaymentItem,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        nroAsientos: String,
        expediente: String = "",
        nomEmbarcacion: String = "",
        tpoPersona: String,
        documentType: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.saveProcessPartida(
                    partida,
                    dataPayment,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    Constants.EMPTY,
                    nroAsientos,
                    expediente,
                    nomEmbarcacion,
                    tpoPersona,
                    documentType
                )
            ) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun saveProcessPartidaPNVigencias(
        partida: Partida,
        dataPayment: PaymentItem,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        nroAsientos: String,
        tomo: String,
        folio: String,
        nomEmbarcacion: String = "",
        tpoPersona: String,
        tpoDocument: String,
    ) {
        viewModelScope.launch {
            Log.e("nomEmb en CPvM", nomEmbarcacion)
            loading.value = true
            when (
                val result = repository.saveProcessPartidaPNVigencias(
                    partida,
                    dataPayment,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    Constants.EMPTY,
                    nroAsientos,
                    tomo,
                    folio,
                    nomEmbarcacion,
                    tpoPersona,
                    tpoDocument
                )
            )
            {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun saveProcessVehicleOwnership(
        dataPayment: PaymentItem,
        documentNumber: String,
        firstLastname: String,
        secondLastname: String,
        names: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        documentType: String,
        businessName: String,
        typeDocumentPoN: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.saveProcessCertifiedAdvertising(
                    dataPayment,
                    documentNumber,
                    firstLastname,
                    secondLastname,
                    names,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    documentType,
                    businessName,
                    typeDocumentPoN
                )
            ) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    }

    fun saveProcessUnion(
        dataPayment: PaymentItem,
        documentNumber: String,
        firstLastname: String,
        secondLastname: String,
        names: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        documentType: String,
        businessName: String,
        typeDocumentPoN: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.saveProcessCertifiedAdvertising(
                    dataPayment,
                    documentNumber,
                    firstLastname,
                    secondLastname,
                    names,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    documentType,
                    businessName,
                    typeDocumentPoN
                )
            ) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    }


    fun saveProcessLegalPerson(
        dataPayment: PaymentItem,
        documentNumber: String,
        firstLastname: String,
        secondLastname: String,
        names: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        documentType: String,
        businessName: String,
        documentTypePN: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.saveProcessCertifiedAdvertising(
                    dataPayment,
                    documentNumber,
                    firstLastname,
                    secondLastname,
                    names,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    documentType,
                    businessName,
                    documentTypePN
                )
            ) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    }

    fun saveProcessPrediosPositivosNegativos(
        dataPayment: PaymentItem,
        documentNumber: String,
        firstLastname: String,
        secondLastname: String,
        names: String,
        legalRecord: LegalRecord?,
        certificate: CertificateType?,
        office: RegistrationOffice?,
        typePerson: String,
        documentType: String,
        businessName: String,
        documentTypePN: String
    ) {
        viewModelScope.launch {
            loading.value = true
            when (
                val result = repository.saveProcessCertifiedAdvertising(
                    dataPayment,
                    documentNumber,
                    firstLastname,
                    secondLastname,
                    names,
                    legalRecord,
                    certificate,
                    office,
                    typePerson,
                    documentType,
                    businessName,
                    documentTypePN
                )
            ) {
                is DataResult.Success -> getPaymentKeys(dataPayment, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    //crear paymentProcessEx, quitar visanetResult
    fun paymentProcess(
        payment: VisaNetResult,
        userId: String,
        costoTotal: String,
        saveProcess: SaveProcess,
        certificateService: CertificateService,
        dataItem: PaymentDataItem?,
        nuAsieSelect: String,
        imPagiSIR: String,
        nuSecuSIR: String,
        totalPaginasPartidaFicha: Int,
        cantPaginas: Int,
        cantPaginasExon: Int,
        paginasSolicitadas: Int,
        codigoGla: String,
        codLibro: String,
        codCerti: String
    ) {

        viewModelScope.launch {
            loading.value = true
            //
            var pp = payment.toProcessPaymentRequest(
                saveProcess.solicitudId,
                userId,
                costoTotal,
                saveProcess,
                certificateService,
                nuAsieSelect?:"",
                imPagiSIR?:"",
                nuSecuSIR?:"",
                totalPaginasPartidaFicha?:0,
                cantPaginas?:0,
                cantPaginasExon?:0,
                paginasSolicitadas?:0,
                codigoGla?:"",
                codLibro?:"",
                codCerti?:""
            )
            dataItem?.lstOtorgantes?.let { list ->
                pp.lstOtorgantes = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
            }
            dataItem?.lstApoyos?.let { list ->
                pp.lstApoyos = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
            }
            dataItem?.lstCuradores?.let { list ->
                pp.lstCuradores = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
            }
            dataItem?.lstPoderdantes?.let { list ->
                pp.lstPoderdantes = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
            }
            dataItem?.lstApoderados?.let { list ->
                pp.lstApoderados = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
            }

            when (val result = repository.paymentProcess(pp)) {
                is DataResult.Success -> onPaymentProcessSuccess(payment, result.data)
                is DataResult.Failure -> {
                    onError(result.exception)
                    Log.d(TAG, "error ${result.exception.message}")
                }
            }
        }
    }

    fun paymentProcessEx(
        userId: String,
        costoTotal: String,
        saveProcess: SaveProcess,
        certificateService: CertificateService,
        dataItem: PaymentDataItem?,
        nuAsieSelect: String,
        imPagiSIR: String,
        nuSecuSIR: String,
        totalPaginasPartidaFicha: Int,
        cantPaginas: Int,
        cantPaginasExon: Int,
        paginasSolicitadas: Int,
    ) {
        viewModelScope.launch {
            loading.value = true
            val pp = getPaymentRequestEx(
                saveProcess.solicitudId,
                userId,
                costoTotal,
                saveProcess,
                nuAsieSelect,
                imPagiSIR,
                nuSecuSIR,
                totalPaginasPartidaFicha,
                cantPaginas,
                cantPaginasExon,
                paginasSolicitadas,
                certificateService
            ).apply {
                dataItem?.lstOtorgantes?.let { list ->
                    lstOtorgantes = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
                }
                dataItem?.lstApoyos?.let { list ->
                    lstApoyos = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
                }
                dataItem?.lstCuradores?.let { list ->
                    lstCuradores = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
                }
                dataItem?.lstPoderdantes?.let { list ->
                    lstPoderdantes = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
                }
                dataItem?.lstApoderados?.let { list ->
                    lstApoderados = JsonParser().parse(Gson().toJson(list)).asJsonArray.toString()
                }
            }

            when (val result = repository.paymentProcess(pp)) {
                is DataResult.Success -> onPaymentProcessExSuccess(result.data)
                is DataResult.Failure -> {
                    onError(result.exception)
                    Log.d(TAG, "error ${result.exception.message}")
                }
            }
        }
    }

    private fun getPaymentRequestEx(requestId: Int,
                                    userId: String,
                                    costoTotal: String,
                                    saveProcess: SaveProcess,
                                    nuAsieSelect: String,
                                    imPagiSIR: String,
                                    nuSecuSIR: String,
                                    totalPaginasPartidaFicha: Int,
                                    cantPaginas: Int,
                                    cantPaginasExon: Int,
                                    paginasSolicitadas: Int,
                                    certificateService: CertificateService): PaymentProcessRequest{

        val niubizRequest = NiubizRequest(
            codAccion = "000",
            codAutoriza = "",
            codtienda = "EXONERADO",
            concepto = "",
            decisionCs = "",
            dscCodAccion = "",
            dscEci = "",
            eci = "",
            estado = "",
            eticket = "",
            fechaYhoraTx = "",
            idUnico = "",
            idUser = "",
            impAutorizado = "",
            nomEmisor = "",
            numOrden = "",
            numReferencia = "",
            oriTarjeta = "",
            pan = "",
            resCvv2 = "",
            response = "",
            reviewTransaction = "",
            transId = ""
        )
        return PaymentProcessRequest(
            solicitudId = "$requestId",
            costoTotal = costoTotal,
            ip = "",
            usrId = userId,
            codigoGla = "",
            idUser = userId,
            idUnico = "",
            pan = "",
            dscCodAccion = "",
            codAutoriza = "",
            codtienda = "EXONERADO",
            numOrden = "",
            codAccion = "000",
            fechaYhoraTx = "",
            nomEmisor = "",
            oriTarjeta = "",
            respuesta = "",
            usrKeyId = "",
            transId = "",
            eticket = "",
            concepto = "",
            codCerti = "",
            numeroRecibo = saveProcess.numeroRecibo,
            numeroPublicidad = saveProcess.numeroPublicidad,
            codVerificacion = saveProcess.codVerificacion,
            codLibroOpc = saveProcess.codLibroOpc,
            cantPaginas = cantPaginas.toString(),
            cantPaginasExon = cantPaginasExon.toString(),
            paginasSolicitadas = paginasSolicitadas.toString(),
            totalPaginasPartidaFicha = totalPaginasPartidaFicha.toInt(),
            nuAsieSelectSARP = nuAsieSelect,
            imPagiSIR = imPagiSIR,
            nuSecuSIR = nuSecuSIR,
            codLibro = "",
            coServ = certificateService.id,
            coTipoRgst = certificateService.registrationTypeId,
            niubizData = niubizRequest
        )
    }

    private fun getPaymentKeys(payment: PaymentItem, saveProcess: SaveProcess) {
        Log.d(TAG, "getPaymentKeys")
        loading.value = true
        viewModelScope.launch {
            when (val result =
                paymentRepository.getPaymentKeys(payment.ammount, saveProcess.solicitudId,
                    if (payment.isLegalPerson()) {
                        val data = paymentRepository.getPaymentData("")
                        BasicPaymentData(data.name, data.firstLastname, data.secondLastname, data.email)
                    } else {
                        BasicPaymentData(payment.name, payment.firstLastname, payment.secondLastname, payment.email)
                    }
                )) {
                is DataResult.Success -> onPaymentKeySuccess(saveProcess, result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onPaymentKeySuccess(saveProcess: SaveProcess, item: VisaNetItem) {
        loading.value = false
        onSaveProcess.value = saveProcess
        onVisaConfiguration.value = item
        Log.d(TAG, "onPaymentKeySuccess")
    }

    private fun onPaymentProcessSuccess(payment: VisaNetResult, data: PaymentProcess) {
        Log.d(TAG, "onPaymentProcessSuccess")
        data.visaNetResult = payment
        loading.value = false
        onPaymentProcess.value = data
    }
    private fun onPaymentProcessExSuccess(data: PaymentProcess) {
        Log.d(TAG, "onPaymentProcessExSuccess")
        loading.value = false
        onPaymentProcessEx.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun getDocumentType(typePer: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getDocTypeList(typePer)) {
                is DataResult.Success -> {
                    when (typePer) {
                        Constants.PERSON_NATURAL -> {
                            onDocSuccess(result.data.filter { it.personId == typePer })
                        }
                        Constants.PERSON_LEGAL -> {
                            onDocSuccess(result.data.filter { it.id == "05" || it.name == "R.U.C." || it.name == "RUC" })
                        }
                        else -> onDocSuccess(result.data)
                    }
                }
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onDocSuccess(data: List<DocumentType>) {
        val listafsort: MutableList<DocumentType> = mutableListOf()
        val item = data.find { it.name == "DNI" }
        if (item != null) {
            listafsort.add(item)
            listafsort.addAll(data.filter { itemfilter -> itemfilter.name != "DNI" })
            docTypes.value = listafsort
        } else {
            docTypes.value = data
        }
        loading.value = false
    }
}