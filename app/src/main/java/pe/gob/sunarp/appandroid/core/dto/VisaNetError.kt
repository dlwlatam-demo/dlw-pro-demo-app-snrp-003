package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import pe.gob.sunarp.appandroid.data.payment.PaymentDataSource
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@JsonClass(generateAdapter = true)
data class VisaNetError  (
    @field:Json(name = "errorCode")
    val errorCode: Int,
    @field:Json(name = "errorMessage")
    val errorMessage: String,
    @field:Json(name = "header")
    val header: HeaderError,
    @field:Json(name = "data")
    val data: DataError,
)
{
   //preferences.purchaseNumber
    fun toPayment(transId:String?,page:Page): PaymentResult {
        var format = SimpleDateFormat("yyMMddHHmmSS")

        val newDate: Date? = format.parse(data.transactionDate)

        format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()
        items.add(Pair2(PaymentResultItem("Medio de pago:","App Sunarp"), null))
        transId?.let { items.add(Pair2(PaymentResultItem("Pedido Nº:", it), null)) }
        items.add(Pair2(PaymentResultItem("Fecha / Hora:",format.format(newDate ?: Date()),), null))
        items.add(Pair2(PaymentResultItem("Desembolso:","S/ ${data.amount}"), null))
        items.add(Pair2(PaymentResultItem("Tipo de pago",data.brand.orEmpty()), null))
        return PaymentResult("ERROR AL REALIZAR EL PAGO","Ha ocurrido un problema con el pago: ${data.actionDescription}, inténtelo nuevamente",page,items)
    }

    companion object {
        fun toModel(json: String): VisaNetError? {
            val moshi: Moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<VisaNetError> = moshi.adapter(VisaNetError::class.java)
            return adapter.fromJson(json)
        }
    }
}

@JsonClass(generateAdapter = true)
data class DataError(
    @field:Json(name = "YAPE_ID")
    val yapeId: String?,
    @field:Json(name = "CURRENCY")
    val currency: String?,
    @field:Json(name = "TERMINAL")
    val terminal: String?,
    @field:Json(name = "TRANSACTION_DATE")
    val transactionDate: String?,
    @field:Json(name = "ACTION_CODE")
    val actionCode: String?,
    @field:Json(name = "TRACE_NUMBER")
    val traceNumber: String?,
    @field:Json(name = "CARD_TYPE")
    val cardType: String?,
    @field:Json(name = "ECI_DESCRIPTION")
    val eciDescription: String?,
    @field:Json(name = "ECI")
    val eci: String?,
    @field:Json(name = "SIGNATURE")
    val signature: String?,
    @field:Json(name = "CARD")
    val card: String?,
    @field:Json(name = "MERCHANT")
    val merchant: String?,
    @field:Json(name = "BRAND")
    val brand: String?,
    @field:Json(name = "STATUS")
    val status: String?,
    @field:Json(name = "ACTION_DESCRIPTION")
    val actionDescription: String?,
    @field:Json(name = "ADQUIRENTE")
    val adquirente: String?,
    @field:Json(name = "ID_UNICO")
    val uniqueId: String?,
    @field:Json(name = "AMOUNT")
    val amount: String?,
    @field:Json(name = "PROCESS_CODE")
    val processCode: String?,
    @field:Json(name = "TRANSACTION_ID")
    val transactionId: String?,
    @field:Json(name = "PURCHASE_NUMBER")
    var purchaseNumber:  String?,
)

@JsonClass(generateAdapter = true)
data class HeaderError(
    @field:Json(name = "ecoreTransactionDate")
    val ecoreTransactionDate: Long?,
    @field:Json(name = "ecoreTransactionUUID")
    val ecoreTransactionUUID: String?,
    @field:Json(name = "millis")
    val millis: Int?,
)

