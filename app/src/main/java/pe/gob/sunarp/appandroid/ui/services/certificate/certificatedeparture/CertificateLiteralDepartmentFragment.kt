package pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture

import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.Constants.Companion.REGISTRO_DE_BIENES_MUEBLES
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.databinding.FragmentLiteralDepartmentCertificateBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity
import pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture.adapter.CertificateLiteralDepartmentAdapter
import pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture.adapter.CertificateLiteralDepartmentRegistroBienesMueblesAdapter

@AndroidEntryPoint
class CertificateLiteralDepartmentFragment :
    BaseFragment<FragmentLiteralDepartmentCertificateBinding>(
        FragmentLiteralDepartmentCertificateBinding::inflate
    ) {

    private val viewModel: CertificateLiteralDepartmentViewModel by viewModels()
    private var legalRecord: LegalRecord? = null
    private var certificate: CertificateType? = null
    private var office: RegistrationOffice? = null
    private var certificateService: CertificateService? = null
    private var literalCertificate: LiteralCertificateRequest? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar(certificate?.name.orEmpty())
        }
    }

    private fun initViews() {
        legalRecord = arguments?.getParcelable("legalRecord")
        certificate = arguments?.getParcelable("certificate")
        setTittle()
        with(binding) {
            etSearch.filterOnlyMayusLiteral()
            etSearch2.filterOnlyMayusLiteralPlaca()
            if (certificate?.certificateId != "69") {
                binding.tvFilterFooter.visibility = View.GONE
                binding.etSearch.inputType = InputType.TYPE_CLASS_NUMBER
            }
            etSearch.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                   hideKeyboard()
                    etSearch.filterOnlyMayusLiteral()
                }
            }
            etSearch2.setOnFocusChangeListener { view, hasFocus ->
                if (!hasFocus) {
                    hideKeyboard()
                }
            }
            tvCertificateType.text = certificate?.name.orEmpty()
            if (certificate?.areaId.orEmpty() == Constants.REAL_STATE_AREA_ID) {
                rbDate.text = getString(R.string.literal_departure_plate)
                // etSearch.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            }

            tilSearch.editText?.onChanged { validateInformation() }
            tilSearch2.editText?.onChanged { validateInformation() }

            radioGroup.setOnCheckedChangeListener { _, checkedId ->
                etSearch.setText("")
                etSearch2.setText("")
                hideKeyboard()
                if (isPlaca(checkedId)) {
                    tilSearch.hide()
                    tilSearch2.show()
                    tilSearch2.requestFocus()
                    etSearch.inputType = InputType.TYPE_CLASS_NUMBER
                } else {
                    tilSearch2.hide()
                    tilSearch.show()
                    tilSearch.requestFocus()
                }
            }
            btnRequest.setOnClickListener {
                etSearch.clearFocus()
                hideKeyboard()
                val tipoPartidaFicha =
                    if (radioGroup.checkedRadioButtonId == R.id.rb_certificate) Constants.CERTIFICATE_TYPE_ID
                    else if (certificate?.areaId.orEmpty() != Constants.REAL_STATE_AREA_ID) Constants.FILE_TYPE_ID
                    else Constants.PLATE_TYPE_ID
                viewModel.validateLiteralCertificate(
                    LiteralCertificateRequest.toRequest(
                        tipoPartidaFicha,
                        (if (isPlaca(radioGroup.checkedRadioButtonId)) etSearch2 else etSearch).text.toString().replace("-", ""),
                        legalRecord,
                        certificate,
                        office,
                        certificateService
                    )
                )
            }
            etService.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etRegisterOffice.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
        }
    }

    private fun isPlaca(checkedId: Int): Boolean =
        certificate?.areaId.orEmpty() == Constants.REAL_STATE_AREA_ID && checkedId == R.id.rb_date

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getSpinners(legalRecord?.id.orEmpty())
            onChange(offices) { fillOffices(it) }
            onChange(services) { fillServices(it) }
            onChange(literalCertificate) { onLiteralCertificate(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun onLiteralCertificate(list: List<LiteralCertificate>) {
        with(binding) {
            informationContainer.show()
            hideKeyboard()
            if (legalRecord?.id == REGISTRO_DE_BIENES_MUEBLES) {
                val transactionsAdapter = CertificateLiteralDepartmentRegistroBienesMueblesAdapter(list)
                transactionsAdapter.setOnSeatsPressed {
                    goToSeatsPage(it)
                }
                rvOfficeRetail.apply {
                    adapter = transactionsAdapter
                }
            } else {
                val transactionsAdapter = CertificateLiteralDepartmentAdapter(list)
                transactionsAdapter.setOnSeatsPressed {
                    goToSeatsPage(it)
                }
                rvOfficeRetail.apply {
                    adapter = transactionsAdapter
                }
            }
            rvOfficeRetail.apply {
                val manager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
                addItemDecoration(dividerItemDecoration)
                layoutManager = manager
            }
        }
    }

    private fun goToSeatsPage(literalCertificate: LiteralCertificate) {
        findNavController().navigate(
            R.id.action_certificateLiteralDepartmentFragment_to_seatDetailFragment,
            bundleOf(
                "certificate" to certificate,
                "literalCertificate" to literalCertificate,
                "certificateService" to certificateService,
                "office" to office,
                "legalRecord" to legalRecord,
                "numDocumento" to  (if (isPlaca(binding.radioGroup.checkedRadioButtonId)) binding.etSearch2 else binding.etSearch).text.toString().replace("-", "")

            )
        )
    }

    private fun fillServices(items: List<CertificateService>) {
        binding.etService.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                hideKeyboard()
                val certificateSelected = (parent.getItemAtPosition(position) as CertificateService)
                certificateService = certificateSelected
                tag = certificateSelected.id
                Log.e("tag", tag.toString())
                if (tag.toString() == "181") {
                    binding.etSearch.inputType = InputType.TYPE_CLASS_TEXT
                } else {
                    binding.etSearch.inputType = InputType.TYPE_CLASS_NUMBER
                }
                validateInformation()
                if (certificateSelected.description.contains("VEHICULAR")) {
                    binding.rbDate.isChecked = true
                }
                if (certificateSelected.description.contains("RMC")) {
                    binding.rbCertificate.isChecked = true
                    binding.rbDate.hide()
                } else {
                    binding.rbDate.show()
                }
            }
        }
    }

    private fun fillOffices(items: List<RegistrationOffice>) {
        binding.etRegisterOffice.apply {
            clean()
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    R.layout.layout_simple_spinner_item,
                    R.id.tv_title,
                    items
                )
            )
            setOnItemClickListener { parent, _, position, _ ->
                hideKeyboard()
                val officeSelected = (parent.getItemAtPosition(position) as RegistrationOffice)
                office = officeSelected
                tag = officeSelected.oficRegId
                validateInformation()
            }
        }
    }

    private fun validateInformation() {
        with(binding) {
            val offices = (etRegisterOffice.tag ?: "").toString()
            val services = (etService.tag ?: "").toString()
            val search = (if (isPlaca(radioGroup.checkedRadioButtonId)) etSearch2 else etSearch).text.toString()
            if (offices.isNotEmpty() && services.isNotEmpty() && search.isNotEmpty()) {
                btnRequest.enable()
            } else {
                btnRequest.disable()
                informationContainer.hide()
            }
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it) }
        }
    }
}
