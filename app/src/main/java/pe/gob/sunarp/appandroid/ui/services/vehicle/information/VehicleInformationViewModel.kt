package pe.gob.sunarp.appandroid.ui.services.vehicle.information

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.data.vehicle.VehicleRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class VehicleInformationViewModel @Inject constructor(
    private val repository: VehicleRemoteRepository
) : BaseViewModel() {
    val fragment = VehicleInformationFragment()

    val loading = SingleLiveEvent<Boolean>()
    val vehicleImage = SingleLiveEvent<String>()
    val receipt = SingleLiveEvent<Boolean>()

    fun getVehicleInformation(information: VehicleInformation) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getVehicleData(information)) {
                is DataResult.Success -> onImageSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun validateReceipt(information: VehicleInformation) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.validateReceipt(information)) {
                is DataResult.Success -> onValidationSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onValidationSuccess(data: Boolean) {
        loading.value = false
        receipt.value = data
    }

    private fun onImageSuccess(image: String) {
        loading.value = false
        vehicleImage.value = image
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}