package pe.gob.sunarp.appandroid.core.model

data class PaymentKeys(
    val instance: String?,
    val merchantId: String?,
    val accesskeyId: String?,
    val secretAccessKey: String?,
    var pinHash: String?,
    var pinHashExpired: String?,
    val urlVisanetToken: String?,
    val urlVisanet: String?,
    val mdd4: String?,
    val mdd21: String?,
    val mdd32: String?,
    val mdd75: String?,
    val mdd77: String?,
    var email: String?,
    var name: String?,
    var lastnames: String?,
)

data class PaymentModel(
    val keys: PaymentKeys,
    val token: String
)