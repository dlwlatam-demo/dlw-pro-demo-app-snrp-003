package pe.gob.sunarp.appandroid.ui.alerta.mandatos.add

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.AddMandatoRequest
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaMandatoAddViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val successCreateMandato = SingleLiveEvent<Boolean>()
    var tipoDocumentos = SingleLiveEvent<List<ItemSpinner>>()
    var tipoDocumentoSelect = SingleLiveEvent<ItemSpinner?>()

    fun getParameters() {
        getTipoDocumento()
    }

    private fun getTipoDocumento() {
        viewModelScope.launch {
            tipoDocumentos.value = SunarpSharedPreferences.Tipo_Documents
        }
    }

    fun setSelectTipoDocumento(data: ItemSpinner?) {
        tipoDocumentoSelect.value = data
    }


    fun createMandato(
        nroDocu: String,
        nombre: String,
        apPate: String,
        apMate: String,
    ) {
        viewModelScope.launch {
            loading.value = true
            val data = AddMandatoRequest(
                apPate,
                apMate,
                nombre,
                tipoDocumentoSelect.value?.id ?: "",
                nroDocu,
            )
            when (val result = repository.addMandato(data)) {
                is DataResult.Success -> onCreateMandatoSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    private fun onCreateMandatoSuccess(result: Boolean) {
        loading.value = false
        successCreateMandato.value = result
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}