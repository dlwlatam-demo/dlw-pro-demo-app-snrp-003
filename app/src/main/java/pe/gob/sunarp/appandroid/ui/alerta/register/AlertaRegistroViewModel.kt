package pe.gob.sunarp.appandroid.ui.alerta.register

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaRegistroViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
) :
    BaseViewModel() {
    val isAuthenticated = SingleLiveEvent<String>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun register(username: String, recovery: Boolean) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.registerUserAlerta(username, recovery)) {
                is DataResult.Success -> sendGUID(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        super.onError(err)
    }

    private fun sendGUID(guid: String) {
        isAuthenticated.value = guid
        showLoading.value = false
    }
}
