package pe.gob.sunarp.appandroid.ui.register.create

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_CE
import pe.gob.sunarp.appandroid.core.Constants.Companion.DOCUMENT_TYPE_DNI
import pe.gob.sunarp.appandroid.core.Constants.Companion.LEGAL_AGE
import pe.gob.sunarp.appandroid.core.Constants.Companion.REGEX_EMAIL_ADDRESS
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.isValidAge
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterCreateViewModel @Inject constructor(private val repository: UserRegistrationRemoteRepository) :
    BaseViewModel() {

    val inDocTypes = SingleLiveEvent<List<DocumentType>>()
    val inGenderType = SingleLiveEvent<List<Gender>>()

    val onFormError = SingleLiveEvent<String>()
    val onError = SingleLiveEvent<String?>()
    val isCreated = SingleLiveEvent<RegisterInformation>()
    val isValidDocumentDNI = SingleLiveEvent<DocumentDNI>()
    val isValidDocumentCE = SingleLiveEvent<DocumentCE>()

    val showLoading = SingleLiveEvent<Boolean>()

    init {
        viewModelScope.launch {
            when (val result = repository.getGenderList()) {
                is DataResult.Success -> inGenderType.value = result.data
                is DataResult.Failure -> onError(result.exception)
            }

            when (val result = repository.getDocTypesList()) {
                is DataResult.Success -> inDocTypes.value = result.data
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        onError.value = err.message
    }

    fun create(
        registerInformation: RegisterInformation,
        terms: Boolean
    ) {
        viewModelScope.launch {
            when (val status = validateForm(
                registerInformation,
                terms
            )) {
                is FormStatus.Ok -> isCreated.value = registerInformation
                is FormStatus.Ko -> onFormError.value = status.error
            }
        }
    }

    fun validateDocument(documentType: String, documentNumber: String, issueDate: String = "") {
        viewModelScope.launch {
            showLoading.value = true
            when (documentType) {
                DOCUMENT_TYPE_DNI -> {
                    when (val result = repository.validateDocumentDNI(documentNumber, issueDate)) {
                        is DataResult.Success -> {
                            isValidDocumentDNI.value = result.data
                            showLoading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
                DOCUMENT_TYPE_CE -> {
                    when (val result = repository.validateDocumentCE(documentNumber)) {
                        is DataResult.Success -> {
                            isValidDocumentCE.value = result.data
                            showLoading.value = false
                        }
                        is DataResult.Failure -> onError(result.exception)
                    }
                }
            }
        }
    }

    private fun validateForm(
        registerInformation: RegisterInformation,
        terms: Boolean
    ): FormStatus {
        if (registerInformation.documentType.isEmpty()) return FormStatus.Ko("Olvidó el campo Tipo de documento")
        if (registerInformation.documentNumber.isEmpty()) return FormStatus.Ko("Olvidó el campo Número de documento")
        if (registerInformation.date.isNullOrBlank() || registerInformation.date.isEmpty()) return FormStatus.Ko("Olvidó el campo Fecha")
        if (registerInformation.date.isValidAge(LEGAL_AGE).not()) return FormStatus.Ko("Debe tener 18 años o más")
        if (registerInformation.names.isEmpty()) return FormStatus.Ko("Olvidó el campo Nombres")
        if (registerInformation.firstLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido")
        //if (registerInformation.secondLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido") // Opcional
        if (registerInformation.gender.isEmpty()) return FormStatus.Ko("Olvidó el campo Tipo de género")
        if (registerInformation.email.isEmpty()) return FormStatus.Ko("Olvidó el campo Email")
        if (registerInformation.email.isValidString(REGEX_EMAIL_ADDRESS).not()) return FormStatus.Ko("Revise el campo Email")
        if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        return FormStatus.Ok
    }
}