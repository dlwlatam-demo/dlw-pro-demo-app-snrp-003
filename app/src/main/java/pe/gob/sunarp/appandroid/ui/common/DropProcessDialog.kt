package pe.gob.sunarp.appandroid.ui.common

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.LayoutDropProcessModalBinding


class DropProcessDialog : DialogFragment() {

    private var onCancelListener: (() -> Unit)? = null
    private var onDropListener: (() -> Unit)? = null

    private var _binding: LayoutDropProcessModalBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = LayoutDropProcessModalBinding.inflate(LayoutInflater.from(context))
        binding.btnBack.setOnClickListener {
            onCancelListener?.invoke()
        }
        binding.btnAccept.setOnClickListener {
            onDropListener?.invoke()
        }

        return AlertDialog.Builder(requireActivity(), R.style.MyThemeOverlayAlertDialog)
            .setView(binding.root)
            .setCancelable(false)
            .create()
    }

    fun onCancelListener(listener: () -> Unit) {
        this.onCancelListener = listener
    }

    fun onDropListener(listener: () -> Unit) {
        this.onDropListener = listener
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}