package pe.gob.sunarp.appandroid.ui.editprofile

import android.graphics.Bitmap
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.DocumentType
import pe.gob.sunarp.appandroid.core.model.Gender
import pe.gob.sunarp.appandroid.core.model.ProfileInformation
import pe.gob.sunarp.appandroid.data.profile.information.ProfileRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class EditProfileViewModel @Inject constructor(
    private val repository: ProfileRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val gender = SingleLiveEvent<List<Gender>>()
    val docTypes = SingleLiveEvent<List<DocumentType>>()
    val onUpdateSuccess = SingleLiveEvent<Boolean>()
    val onLoadPhoto = SingleLiveEvent<Bitmap?>()

    init {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getGenderList()) {
                is DataResult.Success -> onGenderSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
            when (val result = repository.getDocTypeList()) {
                is DataResult.Success -> onDocSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
            onLoadPhoto.value = repository.getPhoto()
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onGenderSuccess(data: List<Gender>) {
        loading.value = false
        gender.value = data
    }

    private fun onDocSuccess(data: List<DocumentType>) {
        loading.value = false
        docTypes.value = data
    }

    fun updateUserInformation(profileInformation: ProfileInformation) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.updateUserInformation(profileInformation)) {
                is DataResult.Success -> onUpdateSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun updateImage(encodedImage: String?) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.updateUserPhoto(encodedImage ?: "")) {
                is DataResult.Success -> onImageUpdateSuccess()
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onImageUpdateSuccess() {
        onLoadPhoto.value = repository.getPhoto()
        loading.value = false
    }

    private fun onUpdateSuccess(data: Boolean) {
        onUpdateSuccess.value = data
        loading.value = false
    }
}