package pe.gob.sunarp.appandroid.data.maps.request

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
data class ListOfficeResponse(
	@Json(name="listOfficeResult")
	val listOfficeResult: List<ListOfficeResultItem>
)

@Parcelize
@JsonClass(generateAdapter = true)
data class ListOfficeResultItem(

	@Json(name="head")
	val head: String,

	@Json(name="address")
	val address: String,

	@Json(name="zone")
	val zone: String,

	@Json(name="attentions")
	val attentions: List<AttentionsItem>,

	@Json(name="phones")
	val phones: List<PhonesItem>,

	@Json(name="lon")
	val lon: String,

	@Json(name="office")
	val office: String,

	@Json(name="type")
	val type: String,

	@Json(name="lat")
	val lat: String,

	@Json(name="distance")
	var distance: String?= ""
): Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class AttentionsItem(

	@Json(name="area")
	val area: String,

	@Json(name="schedules")
	val schedules: List<String>
): Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class PhonesItem(

	@Json(name="phone")
	val phone: String,

	@Json(name="annex")
	val annex: String
): Parcelable
