package pe.gob.sunarp.appandroid.ui.transactions.payment.result.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.databinding.LayoutPaymentItemBinding
import pe.gob.sunarp.appandroid.databinding.LayoutPaymentItemDoubleRowBinding
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem

class PaymentResultAdapter(
    private val items: List<Pair2<PaymentResultItem, PaymentResultItem?>>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == TYPE_ROW.ONE_ROW.ordinal) {
            ViewHolderOne(LayoutPaymentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }else {
            ViewHolderTwo(LayoutPaymentItemDoubleRowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return if(items[position].second == null) {
            (holder as PaymentResultAdapter.ViewHolderOne).bind(items[position])
        } else {
            (holder as PaymentResultAdapter.ViewHolderTwo).bind(items[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if(items[position].second == null) TYPE_ROW.ONE_ROW.ordinal else TYPE_ROW.TWO_ROW.ordinal
    }

    override fun getItemCount() = items.size

    inner class ViewHolderOne(private val binding: LayoutPaymentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Pair2<PaymentResultItem, PaymentResultItem?>) = with(binding) {
            with(item.first) {
                tvTitle.text = label
                tvDescription.text = description
            }
        }
    }
    inner class ViewHolderTwo(private val binding: LayoutPaymentItemDoubleRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Pair2<PaymentResultItem, PaymentResultItem?>) = with(binding) {
            with(item) {
                tvTitle1.text = item.first.label
                tvDescription1.text = item.first.description
                tvTitle2.text = item.second?.label
                tvDescription2.text = item.second?.description
            }
        }
    }

    enum class TYPE_ROW {
        ONE_ROW, TWO_ROW
    }

}