package pe.gob.sunarp.appandroid.ui.home.profile.model

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProfileItem(
    val userId: String,
    val name: String,
    val firstLastname: String,
    val secondLastname: String,
    val gender: String,
    val birthday: String,
    val docType: String,
    val docNumber: String,
    val mobilePhone: String,
    val email: String,
    val photo: Bitmap?
) : Parcelable