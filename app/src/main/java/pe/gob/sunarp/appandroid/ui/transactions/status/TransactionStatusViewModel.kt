package pe.gob.sunarp.appandroid.ui.transactions.status

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.status.model.TransactionStatus
import javax.inject.Inject

@HiltViewModel
class TransactionStatusViewModel @Inject constructor(
    private val repository: TransactionRemoteRepository,
    private val preferences: SunarpSharedPreferences,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val onDropComplete = SingleLiveEvent<Boolean>()
    val transaction = SingleLiveEvent<TransactionStatus>()

    val userKeyId = preferences.userKeyId
    fun getTransactionStatus(transId: String,year: String) {
        loading.value = true
        viewModelScope.launch {
            if(year.isEmpty()){
                when (val result = repository.getTransactionStatus(transId)) {
                    is DataResult.Success -> onTransactionSuccess(result.data)
                    is DataResult.Failure -> onError(result.exception)
                }
            }else{
                when (val result = repository.getAplicationStatus(year,transId)) {

                    is DataResult.Success -> onTransactionSuccess(result.data)
                    is DataResult.Failure -> onError(result.exception)
                }
            }
        }
    }

    fun dropProcess(transKey: String) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.dropProcess(transKey)) {
                is DataResult.Success -> onDropSuccess()
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onDropSuccess() {
        loading.value = false
        onDropComplete.value = true
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onTransactionSuccess(data: TransactionStatus) {
        loading.value = false
        transaction.value = data
    }
}