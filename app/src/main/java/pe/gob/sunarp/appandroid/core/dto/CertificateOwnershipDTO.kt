package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.CertificateOwnership

@JsonClass(generateAdapter = true)
data class CertificateOwnershipDTO(
    @field:Json(name = "numPartida")
    val numPartida: String?,
    @field:Json(name = "regPubSiglas")
    val regPubSiglas: String?,
    @field:Json(name = "nombreOfic")
    val officeName: String?,
    @field:Json(name = "fichaId")
    val fichaId: String?,
    @field:Json(name = "tomoId")
    val tomoId: String?,
    @field:Json(name = "fojaId")
    val fojaId: String?,
    @field:Json(name = "areaRegisDescripcion")
    val regDescription: String?,
    @field:Json(name = "libroDescripcion")
    val libroDescripcion: String?,
    @field:Json(name = "participanteDesc")
    val participanteDesc: String?,
    @field:Json(name = "tipoDocumPartic")
    val tipoDocumPartic: String?,
    @field:Json(name = "numDocumPartic")
    val numDocumPartic: String?,
    @field:Json(name = "direccionPredio")
    val direccionPredio: String?,
    @field:Json(name = "estadoInd")
    val estadoInd: String?,
) : BaseModel() {
    fun toModel() = CertificateOwnership(
        numPartida.orEmpty(),
        regPubSiglas.orEmpty(),
        officeName.orEmpty(),
        fichaId.orEmpty(),
        tomoId.orEmpty(),
        fojaId.orEmpty(),
        regDescription.orEmpty(),
        libroDescripcion.orEmpty(),
        participanteDesc.orEmpty(),
        tipoDocumPartic.orEmpty(),
        numDocumPartic.orEmpty(),
        direccionPredio.orEmpty(),
        estadoInd.orEmpty()
    )
}