package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DetailMandatoResponse(

	@Json(name="sno")
	val sno: String? = null,

	@Json(name="feNoti")
	val feNoti: String? = null,

	@Json(name="message")
	val message: String? = null
)
