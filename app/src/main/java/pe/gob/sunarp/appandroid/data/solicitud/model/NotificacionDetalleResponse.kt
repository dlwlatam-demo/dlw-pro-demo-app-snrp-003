package pe.gob.sunarp.appandroid.data.solicitud.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NotificacionDetalleResponse(

	@Json(name="solicitudId")
	val solicitudId: String,

	@Json(name="estado")
	val estado: Estado,

	@Json(name="tsDesis")
	val tsDesis: Any,

	@Json(name="sistema")
	val sistema: String,

	@Json(name="solicitante")
	val solicitante: Solicitante,

	@Json(name="flags")
	val flags: Flags,

	@Json(name="pago")
	val pago: Pago,

	@Json(name="ncopAdic")
	val ncopAdic: Any,

	@Json(name="nsolMonLiq")
	val nsolMonLiq: Any,

	@Json(name="certificado")
	val certificado: Certificado,

	@Json(name="destinatario")
	val destinatario: Destinatario,

	@Json(name="userKeyId")
	val userKeyId: String,

	@Json(name="tsMaxReing")
	val tsMaxReing: Any
)

@JsonClass(generateAdapter = true)
data class Pago(

	@Json(name="fecha")
	val fecha: String,

	@Json(name="monto")
	val monto: String,

	@Json(name="codTpoEsquela")
	val codTpoEsquela: String,

	@Json(name="mayorDerecho")
	val mayorDerecho: String,

	@Json(name="tpoPago")
	val tpoPago: Any
)

@JsonClass(generateAdapter = true)
data class Estado(

	@Json(name="estado")
	val estado: String,

	@Json(name="codEstado")
	val codEstado: String
)

@JsonClass(generateAdapter = true)
data class Destinatario(

	@Json(name="tpoEnvioDesc")
	val tpoEnvioDesc: String,

	@Json(name="distrito")
	val distrito: Any,

	@Json(name="oficina")
	val oficina: Any,

	@Json(name="direccion")
	val direccion: Any,

	@Json(name="departamento")
	val departamento: Any,

	@Json(name="tpoEnvio")
	val tpoEnvio: Any,

	@Json(name="provincia")
	val provincia: Any,

	@Json(name="codPostal")
	val codPostal: Any,

	@Json(name="nombre")
	val nombre: String
)

@JsonClass(generateAdapter = true)
data class Solicitante(

	@Json(name="numDoc")
	val numDoc: String,

	@Json(name="tpoDoc")
	val tpoDoc: String,

	@Json(name="nombre")
	val nombre: String
)

@JsonClass(generateAdapter = true)
data class Certificado(

	@Json(name="certificadoId")
	val certificadoId: String,

	@Json(name="annoTitulo")
	val annoTitulo: String,

	@Json(name="nombrePers")
	val nombrePers: String,

	@Json(name="numPartida")
	val numPartida: Any,

	@Json(name="codOficina")
	val codOficina: String,

	@Json(name="asiento")
	val asiento: String,

	@Json(name="aaPubl")
	val aaPubl: String,

	@Json(name="numPaginas")
	val numPaginas: String,

	@Json(name="acto")
	val acto: Any,

	@Json(name="areaRegistral")
	val areaRegistral: String,

	@Json(name="numTitulo")
	val numTitulo: String,

	@Json(name="oficina")
	val oficina: String,

	@Json(name="tpoPers")
	val tpoPers: String,

	@Json(name="nuPubl")
	val nuPubl: String,

	@Json(name="codZona")
	val codZona: String,

	@Json(name="tpoCertificado")
	val tpoCertificado: String
)

@JsonClass(generateAdapter = true)
data class Flags(

	@Json(name="flgDesis")
	val flgDesis: String,

	@Json(name="flgLiq")
	val flgLiq: String,

	@Json(name="flgAclar")
	val flgAclar: String,

	@Json(name="flgDeneg")
	val flgDeneg: Any,

	@Json(name="flgEstado")
	val flgEstado: String,

	@Json(name="flgTipoLiq")
	val flgTipoLiq: Any,

	@Json(name="flgAban")
	val flgAban: String,

	@Json(name="flgObs")
	val flgObs: String
)
