package pe.gob.sunarp.appandroid.data.payment.dto

import android.content.Context
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.emptyOrHyphen
import pe.gob.sunarp.appandroid.core.orHyphen
import pe.gob.sunarp.appandroid.data.payment.SavePaymentRequest
import pe.gob.sunarp.appandroid.data.transaction.dto.TransactionType
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Pair2
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResultItem

@JsonClass(generateAdapter = true)
data class PaymentResultDTO(
    @field:Json(name = "solicitudId")
    val requestId: Int?,
    @field:Json(name = "descripcion")
    val description: String?,
    @field:Json(name = "tsCrea")
    val tsCrea: String?,
    @field:Json(name = "total")
    val total: String?,
    @field:Json(name = "email")
    val email: String?,
    @field:Json(name = "solicitante")
    val requester: String?,
    @field:Json(name = "tpoPago")
    val paymentType: String?,
    @field:Json(name = "pagoSolicitudId")
    val paymentRequestId: Int?,
    @field:Json(name = "numeroRecibo")
    val billNumber: String?,
    @field:Json(name = "numeroPublicidad")
    val adNumber: String?,
    @field:Json(name = "codVerificacion")
    val verificationCode: String?,
    @field:Json(name = "secReciDetaAtenNac")
    val secReciDetaAtenNac: Int?,
) {
    fun toModel(
        context: Context,
        data: SavePaymentRequest,
        type: String,
        description: String,
        currency: String,
        source: Page
    ): PaymentResult {
        val title = when (type) {
            TransactionType.ADVERTISING.type -> context.getString(R.string.informative_ticket)
            TransactionType.SEARCH_BY_NAME.type -> context.getString(R.string.search_by_name)
            TransactionType.INFORMATIVE_TICKET.type -> context.getString(R.string.compendious_advertising)
            TransactionType.SEARCH_DATA_BY_NAME.type -> context.getString(R.string.search_data_by_name)
            TransactionType.INF_ADVERTISING.type -> context.getString(R.string.vehicular_search_info_adv)
            else -> ""
        }
        val items: ArrayList<Pair2<PaymentResultItem, PaymentResultItem?>> = arrayListOf()
        if (title.isNotEmpty()) items.add(Pair2(PaymentResultItem("Servicio:", title), null))
        items.add(Pair2(PaymentResultItem("Resultado de la operación:", data.detail), null))
        items.add(Pair2(PaymentResultItem("Número de la tarjeta:", data.niubizData.pan),null))
        items.add(Pair2(PaymentResultItem("Nombre / Razón social:", requester.orEmpty()),null))
        items.add(Pair2(PaymentResultItem("Descripción:", description),null))
        items.add(Pair2(
            PaymentResultItem("Número Publicidad:", adNumber.emptyOrHyphen()),
            PaymentResultItem("Número Recibo:", billNumber.emptyOrHyphen())
        ))
        items.add(Pair2(
            PaymentResultItem("Medio de Pago:", "App Sunarp"),
            PaymentResultItem("Solicitud N°:", requestId.toString().emptyOrHyphen())
        ))
        items.add(Pair2(
            PaymentResultItem("Fecha / Hora:", tsCrea.toString()),
            PaymentResultItem("Monto pagado:", "S/ $total")
        ))
        items.add(Pair2(
            PaymentResultItem("Tipo de moneda:", currency),
            PaymentResultItem("Tipo de pago", paymentType.orEmpty().replace("Tarjeta", "Tarjeta de Crédito"))
        ))
        items.add(Pair2(PaymentResultItem("Cod. Verificación:", verificationCode.emptyOrHyphen()), null))
        return PaymentResult("PAGO EXITOSO", "", source, items)
    }


}