package pe.gob.sunarp.appandroid.ui.office

object CountViewsMaps {
    var count = 0

    fun incrementViews() {
        count++
    }

    fun getCountViews(): Int {
        return count
    }
}