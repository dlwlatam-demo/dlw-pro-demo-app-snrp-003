package pe.gob.sunarp.appandroid.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint
import lib.visanet.com.pe.visanetlib.VisaNet
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.dto.VisaNetError
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.formatText
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.databinding.ActivityHomeBinding
import pe.gob.sunarp.appandroid.ui.base.PaymentInterface
import pe.gob.sunarp.appandroid.ui.office.GpsUtils
import pe.gob.sunarp.appandroid.ui.office.OfficeSearchFragment

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var navController: NavController
    private lateinit var navHostFragment: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupViewModel()
        binding.btnNotification.setOnClickListener { navController.navigate(R.id.action_to_notification) }
    }

    private fun setupViewModel() {
        viewModel.getDrawerInformation()
        viewModel.model.observe(this) { model -> setupNavigators(model) }
        viewModel.logOutSuccess.observe(this) {
            navController.navigate(R.id.action_to_LoginActivity)
            finish()
        }
    }

    private fun setupNavigators(model: DrawerModel) {
        val drawerView: NavigationView = binding.navigation
        val navView: BottomNavigationView = binding.navView

        val header = drawerView.getHeaderView(0)
        val tvUsername = header.findViewById<TextView>(R.id.tvUserName)
        tvUsername.text = model.username
        val tvDNI = header.findViewById<TextView>(R.id.tvDocumentNumber)
        tvDNI.text = getString(R.string.dni, model.docNumber).formatText()

        val ivAvatar = header.findViewById<ImageView>(R.id.ivUserAvatar)
        ivAvatar.setImageBitmap(model.photo)

        val btnProfile = header.findViewById<Button>(R.id.btnProfile)
        btnProfile.setOnClickListener {
            navController.navigate(R.id.nav_profile)
            binding.container.closeDrawer(GravityCompat.START)
        }

        navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.navController

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_profile,
                R.id.nav_qr
            ),
            binding.container
        )

        binding.btnBack.setOnClickListener {
            navController.navigateUp()
        }

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.nav_home -> {
                    navController.navigate(R.id.action_to_home)
                }
                R.id.nav_profile -> {
                    navController.navigate(R.id.nav_profile)
                }
                R.id.nav_qr -> {
                    navController.navigate(R.id.nav_qr)
                }
            }
            true
        }
        drawerView.setupWithNavController(navController)
        drawerView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_logout -> {
                    viewModel.logOut()
                }
                R.id.nav_home -> {
                    navController.navigate(R.id.action_to_home)
                }
            }

            NavigationUI.onNavDestinationSelected(it, navController)
            binding.container.closeDrawer(GravityCompat.START)
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == VisaNet.VISANET_AUTHORIZATION) {
            if (data != null) {
                if (resultCode == RESULT_OK) {
                    val response = data.extras?.getString("keySuccess").orEmpty()
                    sendPaymentData(true, response)
                } else {
                    val response = data.extras?.getString("keyError").orEmpty()
                    sendPaymentData(false, response)
                }
            } else {
                toast("cancel")
            }
        }
        if (requestCode == GpsUtils.GPS_REQUEST) {
            gpsRequest(resultCode)
        }
    }

    private fun gpsRequest(resultCode: Int) {
        val current: Fragment = navHostFragment.childFragmentManager.fragments[0]
        if (current is OfficeSearchFragment) {
            if (resultCode == RESULT_OK) {
                current.isActiveGps()
            } else {
                current.isFailActiveGps()
            }
        }
    }

    private fun sendPaymentData(ok: Boolean, data: String) {
        val current: Fragment = navHostFragment.childFragmentManager.fragments[0]
        if (current is PaymentInterface) {
            if (ok) {
                val result = VisaNetResult.toModel(data)
                result?.data?.purchaseNumber = viewModel.getPreferences().purchaseNumber
                result?.let { current.sendPaymentConfirmation(it) }
            } else {
                if (data.contains("{")) {
                    val result = VisaNetError.toModel(data)
                    Log.e("VisaNetError: ", result?.data?.transactionId.toString())
                    Log.e("VisaNetError: ", result?.data?.actionDescription.toString())
                    Log.e("VisaNetError: ", result?.data?.traceNumber.toString())
                    Log.e("VisaNetError: ", result?.data?.actionCode.toString())
                    result?.data?.purchaseNumber = viewModel.getPreferences().purchaseNumber
                    result?.let { current.sendPaymentFailed(ok, result) }
                } else {
                    toast(data)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        navController.removeOnDestinationChangedListener(listener)
        super.onPause()
    }

    private val listener =
        NavController.OnDestinationChangedListener { nav, destination, _ ->
            if (destination.id == R.id.nav_home || destination.id == R.id.nav_qr || destination.id == R.id.profileFragment) {
                supportActionBar?.show()
                binding.appBackLayout.hide()
            } else {
                supportActionBar?.hide()
                destination.label?.apply {
                    binding.backToolbar.title = this
                }
                binding.appBackLayout.show()
            }
        }

    fun setToolbarTitle(title: String) {
        binding.backToolbar.title = title
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
