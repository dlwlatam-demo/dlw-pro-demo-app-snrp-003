package pe.gob.sunarp.appandroid.core.custom.ui

import android.widget.CompoundButton

val Any?.exhaustive get() = Unit

fun CompoundButton.onChanged(onChanged: (Boolean) -> Unit) {
    this.setOnCheckedChangeListener { _, isChecked -> onChanged(isChecked) }
}