package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.LibroRegistral

@JsonClass(generateAdapter = true)
data class LibroRegistralDTO(
    @field:Json(name = "codLibro")
    val codLibro: String?,
    @field:Json(name = "nomLibro")
    val nomLibro: String?,
    @field:Json(name = "areaRegId")
    val areaRegId: String?,
    @field:Json(name = "grupoLibroArea")
    val grupoLibroArea: String?,
) {
    fun toModel() = LibroRegistral(
        codLibro,
        nomLibro,
        areaRegId,
        grupoLibroArea,
    )
}