package pe.gob.sunarp.appandroid.ui.welcome.onboarding.items

import android.os.Bundle
import android.view.View
import pe.gob.sunarp.appandroid.databinding.FragmentOnboardingitemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.welcome.onboarding.adapter.OnBoardingViewPageItem

class OnBoardingItemFragment: BaseFragment<FragmentOnboardingitemBinding>(FragmentOnboardingitemBinding::inflate) {

    companion object {
        fun newInstance(item: OnBoardingViewPageItem): OnBoardingItemFragment {
            val fragmentFirst = OnBoardingItemFragment()
            val args = Bundle()
            args.putSerializable("item", item)
            fragmentFirst.arguments = args
            return fragmentFirst
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val item = (arguments?.getSerializable("item"))
        if (item is OnBoardingViewPageItem){
            binding.ivOnBoardingItem.setImageResource(item.image)
//            binding.tvTitle.text = item.title
//            binding.tvDescription.text = item.description
        }

    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}