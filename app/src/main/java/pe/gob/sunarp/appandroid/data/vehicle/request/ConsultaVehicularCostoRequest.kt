package pe.gob.sunarp.appandroid.data.vehicle.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ConsultaVehicularCostoRequest(

	@Json(name="flg")
	val flg: String,

	@Json(name="zona")
	val zona: String,

	@Json(name="oficina")
	val oficina: String,

	@Json(name="codLibroArea")
	val codLibroArea: Int,

	@Json(name="partida")
	val partida: String
)
