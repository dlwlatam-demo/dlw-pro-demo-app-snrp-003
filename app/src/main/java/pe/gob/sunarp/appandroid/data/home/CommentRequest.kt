package pe.gob.sunarp.appandroid.data.home

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CommentRequest(
    @field:Json(name = "txt")
    val comment: String,
    @field:Json(name = "img")
    val image: String,
    @field:Json(name = "appName")
    val appName: String
)