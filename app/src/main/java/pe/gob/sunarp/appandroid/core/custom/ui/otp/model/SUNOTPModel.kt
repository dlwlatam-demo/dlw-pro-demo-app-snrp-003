package pe.gob.sunarp.appandroid.core.custom.ui.otp.model

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.annotation.StyleRes
import pe.gob.sunarp.appandroid.R

enum class SUNOTPState {
    DEFAULT,
    FOCUSED,
    VALIDATING,
    VALIDATED,
    ERROR
}

enum class SUNOTPStateRes(@StyleRes val textAppearance: Int, @StringRes val message: Int = -1, @DrawableRes val drawable: Int = -1, @ColorRes val drawableTint: Int = -1) {
    DEFAULT(R.style.sun_otp_text_active, R.string.sun_otp_request_new_code_in_seconds),
    FOCUSED(R.style.sun_otp_text_active),
    VALIDATING(R.style.sun_otp_text_active, R.string.sun_otp_checking_code),
    VALIDATED(R.style.sun_otp_text_active, R.string.sun_otp_verified_code),
    ERROR(R.style.sun_otp_text_active, R.string.sun_otp_invalid_code),
    RESEND(R.style.sun_otp_text_inactive, R.string.sun_otp_request_new_code)
}