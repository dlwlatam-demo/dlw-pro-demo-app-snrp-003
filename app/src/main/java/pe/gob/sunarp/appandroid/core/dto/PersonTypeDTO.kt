package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.PersonType

@JsonClass(generateAdapter = true)
data class PersonTypeDTO(
    @field:Json(name = "coTipoPersona")
    val id: String?,
    @field:Json(name = "noTipoPersona")
    val name: String?,
) : BaseModel() {
    fun toModel() = PersonType(id.orEmpty(), name.orEmpty())
}