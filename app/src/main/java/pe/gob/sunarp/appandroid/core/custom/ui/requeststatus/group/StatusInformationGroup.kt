package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.LinearLayoutManager
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.LayoutTransactionStatusBinding

class StatusInformationGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), StatusInfoInterface {

    private var title: String = ""

    private val binding: LayoutTransactionStatusBinding =
        LayoutTransactionStatusBinding.inflate(LayoutInflater.from(context), this)

    init {
        context.withStyledAttributes(attrs, R.styleable.REQSTATUS) {
            getString(R.styleable.REQSTATUS_title).let { title = it ?: "" }
        }
    }

    fun setTitle(title: String) {
        binding.tvTitle.text = title
    }

    fun setAdapter(items: List<StatusInfoItem>) {
        binding.rvRequestStatus.apply {
            adapter = StatusInformationAdapter(items)
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun getComponentView(): StatusInformationGroup = this

    override fun getView(type: Int): LayoutTransactionStatusBinding = binding
}