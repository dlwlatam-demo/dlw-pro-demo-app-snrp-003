package pe.gob.sunarp.appandroid.ui.services.vehicle.payment

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.VehicleReceiptAsyncDTO
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.core.model.CertificateOwnership
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.core.model.VehiclePayment
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.data.vehicle.VehicleRemoteRepository
import pe.gob.sunarp.appandroid.data.vehicle.request.ConsultaVehicularCostoRequest
import pe.gob.sunarp.appandroid.data.vehicle.request.ConsultaVehicularCostoResponse
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.BasicPaymentData
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import javax.inject.Inject

@HiltViewModel
class VehiclePaymentViewModel @Inject constructor(
    private val repository: VehicleRemoteRepository,
    private val paymentRepository: PaymentRemoteRepository,
    private val preference: SharedPreferenceRepository
) : BaseViewModel() {

    lateinit var information: VehicleInformation
    val loading = SingleLiveEvent<Boolean>()
    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onReceipt = SingleLiveEvent<PaymentModel>()
    val dataCosto = SingleLiveEvent<String>()
    val onFormError = SingleLiveEvent<String>()

    val status = SingleLiveEvent<Boolean>()
    val guid = SingleLiveEvent<String>()

    fun getPersonData() = paymentRepository.getPaymentData("")

    fun getNiubizInformation(
        visaNetResult: VisaNetResult
    ) = visaNetResult.toNiubisInformation(preference.getDoc())

    fun create(payment: PaymentItem, terms: Boolean, transId: Int) {
        viewModelScope.launch {
            when (val status = validateForm(
                payment,
                terms
            )) {
                is FormStatus.Ok -> getPaymentKeys(payment, transId)
                is FormStatus.Ko -> onFormError.value = status.error
            }
        }
    }

    fun generateReceipt(information: VehiclePayment, data: VisaNetResult) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.generateReceipt(information)) {
                is DataResult.Success -> onReceiptSuccess(result.data, information, data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun generateReceiptAsync(information: VehiclePayment, data: VisaNetResult) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.generateReceiptAsync(information)) {
                is DataResult.Success -> onReceiptSuccessAsync(result.data, information, data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun obtenerTarifa() {
        loading.value = true
        val data = ConsultaVehicularCostoRequest("1",information.regPubId ,information.oficRegId, 0, "")
        viewModelScope.launch {
            when (val result = repository.obtenerTarifa(data)) {
                is DataResult.Success -> onObtenerTarifaSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onObtenerTarifaSuccess(data: ConsultaVehicularCostoResponse) {
        loading.value = false
        dataCosto.value = data.monto
    }

    private fun onReceiptSuccess(
        transId: String, information: VehiclePayment, data: VisaNetResult
    ) {
        loading.value = false
        onReceipt.value = PaymentModel(transId,information.toPaymentResult(data))
    }

    private fun onReceiptSuccessAsync(dataAsync: VehicleReceiptAsyncDTO, information: VehiclePayment, data: VisaNetResult) {

        if (!dataAsync.transId.isNullOrEmpty() && dataAsync.transId != "0") {
            onReceipt.value = PaymentModel(dataAsync.transId!!,information.toPaymentResult(data))
        }
        else {
            Log.e("generateReceipt", "empty...")
        }
        Log.e("generateReceipt-status", dataAsync.status.toString())
        if (dataAsync.status.equals("1") ) {
            status.value = true
            loading.value = false
            guid.value = dataAsync.guid.toString()
        }
        else {
            status.value = false
            guid.value = dataAsync.guid.toString()
            Log.e("generateReceipt: ", guid.value.toString())

            runBlocking {
                delay(5000)
            }
            generateReceiptAsyncStatus(guid.value.toString(), information, data)
        }

    }

    private fun generateReceiptAsyncStatus (guid: String, information: VehiclePayment, data: VisaNetResult) {
        viewModelScope.launch {
            Log.e("generateReceipt", "generateReceiptAsyncStatus")

            when (val result = repository.generateReceiptAsyncStatus (guid))  {
                is DataResult.Success -> onReceiptSuccessAsync(result.data, information, data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    private fun validateForm(
        item: PaymentItem,
        terms: Boolean
    ): FormStatus {
        if (item.name.isEmpty()) return FormStatus.Ko("Olvidó el nombre")
        if (item.firstLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido Paterno")
        if (item.email.isEmpty()) return FormStatus.Ko("Olvidó el campo Email")
        if (item.email.isValidString(Constants.REGEX_EMAIL_ADDRESS)
                .not()
        ) return FormStatus.Ko("Revise el campo Email")
        if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        return FormStatus.Ok
    }

    private fun getPaymentKeys(payment: PaymentItem, transId: Int) {
        loading.value = true
        viewModelScope.launch {
            when (val result = paymentRepository.getPaymentKeys(payment.ammount, transId,
                        BasicPaymentData(payment.name, payment.firstLastname, payment.secondLastname, payment.email))) {
                is DataResult.Success -> onPaymentKeySuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onPaymentKeySuccess(item: VisaNetItem) {
        loading.value = false
        onVisaConfiguration.value = item
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun setArgumentsData(arguments: Bundle) {
        information = arguments.getParcelable("information")!!
    }
}