package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselection.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.SearchZone
import pe.gob.sunarp.appandroid.databinding.LayoutCheckOptionBinding

class SearchByNameAdapter(
    private val items: List<SearchZone>
) : RecyclerView.Adapter<SearchByNameAdapter.ViewHolder>() {

    private var setOpenChecked: ((itemsSize: Int) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchByNameAdapter.ViewHolder {
        val itemBinding: LayoutCheckOptionBinding =
            LayoutCheckOptionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SearchByNameAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setOpenChecked(listener: (itemsSize: Int) -> Unit) {
        this.setOpenChecked = listener
    }

    fun clearSelected() {
        items.forEach {
            it.selected = false
        }
        notifyDataSetChanged()
    }

    fun getZonesSelected() : List<SearchZone>  = items.filter { it.selected }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutCheckOptionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchZone) = with(binding) {
            with(item) {
                rbItem.text = name
                rbItem.isChecked = selected
                rbItem.setOnCheckedChangeListener { _, checked ->
                    item.selected = checked
                    setOpenChecked?.invoke(items.filter { it.selected }.size)
                }
            }
        }
    }

}
