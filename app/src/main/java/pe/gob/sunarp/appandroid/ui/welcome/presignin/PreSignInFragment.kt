package pe.gob.sunarp.appandroid.ui.welcome.presignin

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.databinding.FragmentPresigninBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

class PreSignInFragment :
    BaseFragment<FragmentPresigninBinding>(FragmentPresigninBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            btnSignIn.setOnClickListener {
                findNavController().navigate(R.id.action_preSignInFragment_to_loginActivity)
            }
            btnRegister.setOnClickListener {
                findNavController().navigate(R.id.action_onBoardingFragment_to_registerActivity)
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}