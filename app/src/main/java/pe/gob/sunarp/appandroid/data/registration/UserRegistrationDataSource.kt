package pe.gob.sunarp.appandroid.data.registration

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.DocumentCeDTO
import pe.gob.sunarp.appandroid.core.dto.DocumentDniDTO
import pe.gob.sunarp.appandroid.core.dto.DocumentTypeDTO
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.registration.register.CeRequest
import pe.gob.sunarp.appandroid.data.registration.register.DniRequest
import pe.gob.sunarp.appandroid.data.registration.register.RegisterRequest
import pe.gob.sunarp.appandroid.data.registration.sendSMS.SMSRequest
import pe.gob.sunarp.appandroid.data.registration.validateSMS.ValidationSMSRequest
import retrofit2.http.*

interface UserRegistrationDataSource {

    @Headers("Content-Type: application/json",
                Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/registro/usuario/solicitud")
    suspend fun sendSMSMessage(
        @Body request: SMSRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/registro/usuario/solicitud/valida")
    suspend fun validateSMSMessage(
        @Body request: ValidationSMSRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/registro/tipoDocumentos/{guid}")
    suspend fun getDocTypes(
        @Path("guid") guid: String,
        @Header("Host") host: String?
    ): DataResult<List<DocumentTypeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/registro/usuario")
    suspend fun registerUser(
        @Body request: RegisterRequest,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/registro/usuario/valida/dni")
    suspend fun validateDocumentDNI(
        @Body request: DniRequest,
        @Header("Host") host: String?
    ): DataResult<DocumentDniDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/registro/usuario/valida/ce")
    suspend fun validateDocumentCE(
        @Body request: CeRequest,
        @Header("Host") host: String?
    ): DataResult<DocumentCeDTO>


    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/consultas/valida/dni/{dni}")
    suspend fun informationDocumentDNI(
        @Header("Host") host: String?,
        @Path("dni") dni: String,
        @Header("Authorization") authorization: String
    ): DataResult<DocumentDniDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/valida/ce")
    suspend fun informationDocumentCE(
        @Body request: CeRequest,
        @Header("Host") host: String?,
        @Header("Authorization") authorization: String
    ): DataResult<DocumentCeDTO>

}