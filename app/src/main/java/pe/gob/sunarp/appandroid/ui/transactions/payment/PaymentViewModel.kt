package pe.gob.sunarp.appandroid.ui.transactions.payment

import android.content.Context
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.Constants
import pe.gob.sunarp.appandroid.core.FormStatus
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.api.NotControlledException
import pe.gob.sunarp.appandroid.core.api.TokenException
import pe.gob.sunarp.appandroid.core.dto.VisaNetResult
import pe.gob.sunarp.appandroid.core.isValidString
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.BasicPaymentData
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.PaymentItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.model.VisaNetItem
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.Page
import pe.gob.sunarp.appandroid.ui.transactions.payment.result.model.PaymentResult
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val repository: PaymentRemoteRepository,
    private val preference: SharedPreferenceRepository
) : BaseViewModel() {

    val paymentInformation = SingleLiveEvent<PaymentItem>()
    val onSuccessPayment = SingleLiveEvent<PaymentResult>()
    val loading = SingleLiveEvent<Boolean>()
    val onVisaConfiguration = SingleLiveEvent<VisaNetItem>()
    val onFormError = SingleLiveEvent<String>()

    fun getPaymentInformation(ammount: String) {
        val result = repository.getPaymentData(ammount)
        onSuccess(result)
    }

    fun create(payment: PaymentItem, terms: Boolean, transId: Int) {
        viewModelScope.launch {
            when (val status = validateForm(
                payment,
                terms
            )) {
                is FormStatus.Ok -> getPaymentKeys(payment, transId)
                is FormStatus.Ko -> onFormError.value = status.error
            }
        }
    }

    fun savePayment(
        context: Context,
        transactionId: Int,
        type: String,
        description: String,
        payment: VisaNetResult
    ) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.savePayment(
                context,
                payment.toRequest(transactionId, preference.getDoc()),
                type,
                description,
                payment.order.currency.orEmpty(),
                Page.TRANSACTIONS
            )) {
                is DataResult.Success -> {
                    //onSucces("message")
                    onSuccessPayment(result.data)
                }
                is DataResult.Failure -> {
                    onError(result.exception)
                    /*when(result.exception){
                        is TokenException, is NotControlledException -> {
                            onError(result.exception)
                        }

                        else -> {
                            onInfo(result.exception)
                        }
                    }*/
                }
            }
        }
    }

    private fun validateForm(
        item: PaymentItem,
        terms: Boolean
    ): FormStatus {
        if (item.name.isEmpty()) return FormStatus.Ko("Olvidó el nombre")
        if (item.firstLastname.isEmpty()) return FormStatus.Ko("Olvidó el campo Apellido Paterno")
        if (item.email.isEmpty()) return FormStatus.Ko("Olvidó el campo Email")
        if (item.email.isValidString(Constants.REGEX_EMAIL_ADDRESS)
                .not()
        ) return FormStatus.Ko("Revise el campo Email")
        if (terms.not()) return FormStatus.Ko("Debe aceptar los Términos y condiciones")
        return FormStatus.Ok
    }

    private fun getPaymentKeys(payment: PaymentItem, transId: Int) {
        loading.value = true
        viewModelScope.launch {
            when (val result = repository.getPaymentKeys(payment.ammount, transId,
                     BasicPaymentData(payment.name, payment.firstLastname, payment.secondLastname, payment.email))) {
                is DataResult.Success -> onPaymentKeySuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onPaymentKeySuccess(item: VisaNetItem) {
        loading.value = false
        onVisaConfiguration.value = item
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onSuccess(item: PaymentItem) {
        loading.value = false
        paymentInformation.value = item
    }

    private fun onSuccessPayment(item: PaymentResult) {
        loading.value = false
        onSuccessPayment.value = item
    }
}