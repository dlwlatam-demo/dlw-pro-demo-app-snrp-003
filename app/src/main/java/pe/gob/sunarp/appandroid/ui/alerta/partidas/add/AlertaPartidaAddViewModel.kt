package pe.gob.sunarp.appandroid.ui.alerta.partidas.add

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.ItemSpinner
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.alerta.modelservices.AddPartidaRequest
import pe.gob.sunarp.appandroid.data.alerta.modelservices.BuscarPartidaItemResponse
import pe.gob.sunarp.appandroid.data.alerta.modelservices.BuscarPartidaRequest
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AlertaPartidaAddViewModel @Inject constructor(
    private val repository: AlertaRemoteRepository,
    private val paramsrepository: CertificateRemoteRepository,
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    var selectOficinaRegistral = SingleLiveEvent<RegistrationOffice>()
    var tipoPartidas = SingleLiveEvent<List<ItemSpinner>>()
    var tipoPartidaSelect = SingleLiveEvent<ItemSpinner?>()
    var tipoBusquedas = SingleLiveEvent<List<ItemSpinner>>()
    var tipoBusquedaSelect = SingleLiveEvent<ItemSpinner?>()
    var partidasBuscadas = SingleLiveEvent<List<BuscarPartidaItemResponse>>()
    val addPartidaSuccess = SingleLiveEvent<Boolean>()

    fun getParameters() {
        getTipoPartida()
        getOficinas()
    }

    private fun getTipoPartida() {
        viewModelScope.launch {
            tipoPartidas.value = listOf(
                ItemSpinner("21000", "Propiedad Inmueble"),
                ItemSpinner("22000", "Persona Jurídica"),
                ItemSpinner("24000", "Propiedad Vehicular"),
            )
        }
    }
    fun getTipoBusqueda() {
        viewModelScope.launch {
            val data = arrayListOf(
                ItemSpinner("P", "Partida"),
                ItemSpinner("F", "Ficha"),
                ItemSpinner("T", "Tomo"),
            )
            if (tipoPartidaSelect.value?.id != "22000")
                data.add(ItemSpinner("S", "Partida SARP"))

            tipoBusquedas.value = data
        }
    }


    fun getOficinas() =
        viewModelScope.launch {
            loading.value = true
            when (val result = paramsrepository.getRegistrationOffices()) {
                is DataResult.Success -> onGetOficinasSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    private fun onGetOficinasSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    fun setSelectOficinaRegistral(data: RegistrationOffice) {
        selectOficinaRegistral.value = data
    }

    fun setSelectTipoPartida(data: ItemSpinner?) {
        tipoPartidaSelect.value = data
    }

    fun setSelectTipoBusqueda(data: ItemSpinner?) {
        tipoBusquedaSelect.value = data
    }

    fun searchPartida(
        documento: String,
        foja: String,
        placa: String,
    ) =
        viewModelScope.launch {
            loading.value = true
            val data = BuscarPartidaRequest(
                area = tipoPartidaSelect.value?.id.orEmpty(),
                tipo = if (tipoPartidaSelect.value?.id == "24000") "V" else tipoBusquedaSelect.value?.id.orEmpty(),
                ofic =selectOficinaRegistral.value?.oficRegId.orEmpty(),
                region = selectOficinaRegistral.value?.regPubId.orEmpty(),
                foja = foja,
                placa = placa,
                partida = if (tipoBusquedaSelect.value?.id.orEmpty() == "P") documento else null,
                ficha = if (tipoBusquedaSelect.value?.id.orEmpty() == "F") documento else null,
                tomo = if (tipoBusquedaSelect.value?.id.orEmpty() == "T") documento else null,
                partiSarp = if (tipoBusquedaSelect.value?.id.orEmpty() == "S") documento else null
            )
            when (val result = repository.searchPartida(data)) {
                is DataResult.Success -> onSearchPartidaSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    fun addPartida(item: BuscarPartidaItemResponse, documento: String) =
        viewModelScope.launch {
            loading.value = true
            item.placa = item.placa?.substringBefore(" ")
            val data = AddPartidaRequest(
                coRegi = selectOficinaRegistral.value?.regPubId.orEmpty(),
                coSede = selectOficinaRegistral.value?.oficRegId.orEmpty(),
                coArea = tipoPartidaSelect.value?.id.orEmpty(),
                nuPart = item.numPartida,
                numPlaca = item.placa,
                nuPartSarp = if (tipoBusquedaSelect.value?.id.orEmpty() == "S") documento else null,
                coLibr = item.codLibro,
            )
            when (val result = repository.addPartida(data)) {
                is DataResult.Success -> onAddPartidaSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }

    private fun onSearchPartidaSuccess(data: List<BuscarPartidaItemResponse>) {
        loading.value = false
        partidasBuscadas.value = data
    }

    private fun onAddPartidaSuccess(data: Boolean) {
        loading.value = false
        addPartidaSuccess.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}