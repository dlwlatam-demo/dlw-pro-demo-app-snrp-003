package pe.gob.sunarp.appandroid.ui.transactions.history.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import okhttp3.internal.notify
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.formatMoneySunarp
import pe.gob.sunarp.appandroid.core.formatText
import pe.gob.sunarp.appandroid.data.transaction.dto.TransactionType
import pe.gob.sunarp.appandroid.databinding.LayoutHistoryItemBinding
import pe.gob.sunarp.appandroid.ui.transactions.history.model.Transaction

class TransactionHistoryAdapter(
    private val context: Context,
    private var items: List<Transaction> = emptyList()
) : RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder>() {

    private var onOpenTransaction: ((id: Int?, type: String?) -> Unit)? = null
    private var onOpenDocument: ((id: Int?) -> Unit)? = null

    fun setItems(items: List<Transaction>) {
        this.items = items
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutHistoryItemBinding =
            LayoutHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setOpenTransactions(listener: (id: Int?, type: String?) -> Unit) {
        this.onOpenTransaction = listener
    }

    fun setOpenDocument(listener: (id: Int?) -> Unit) {
        this.onOpenDocument = listener
    }

    inner class ViewHolder(private val binding: LayoutHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Transaction) = with(binding) {
            with(item) {
                tvTitle.text = reason
                tvTransactionNumber.text =
                    context.getString(R.string.label_transaction_number, transactionNumber)
                        .formatText()
                tvServiceDetail.text =
                    context.getString(R.string.label_service, service).formatText()
                tvAdvertasingNumber.text =
                    context.getString(R.string.label_advertising, advertisingNumber?.split(" - ")?.reversed()?.joinToString(" - ")).formatText()
                tvDate.text = date
                tvBill.text = "${ammount.formatMoneySunarp()}"
                btnViewRequest.text = buttonText
                if (type.equals(TransactionType.SEARCH_BY_NAME.type)) {
                    btnViewRequest.visibility = View.GONE
                } else {
                    btnViewRequest.visibility = View.VISIBLE
                }

                btnViewRequest .setOnClickListener {
                    if (type.equals(TransactionType.ADVERTISING.type)) {
                        onOpenDocument?.invoke(key)
                    } else {
                        onOpenTransaction?.invoke(key, type)
                    }
                }

                bar.setBackgroundColor(ContextCompat.getColor(context, color))
            }
        }
    }
}
