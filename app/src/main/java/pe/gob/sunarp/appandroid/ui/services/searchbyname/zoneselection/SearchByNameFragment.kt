package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselection

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.disable
import pe.gob.sunarp.appandroid.core.custom.ui.enable
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.model.SearchZone
import pe.gob.sunarp.appandroid.core.model.ZonesSelected
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.databinding.FragmentSearchByNameBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselection.adapter.SearchByNameAdapter

@AndroidEntryPoint
class SearchByNameFragment :
    BaseFragment<FragmentSearchByNameBinding>(FragmentSearchByNameBinding::inflate) {

    private val viewModel: SearchByNameViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        with(binding) {

            ivClose.setOnClickListener {
                val adapter = (rvZones.adapter as SearchByNameAdapter)
                adapter.clearSelected()
            }
            btnSearch.setOnClickListener {
                val adapter = (rvZones.adapter as SearchByNameAdapter)
                val items = ZonesSelected(adapter.getZonesSelected().toMutableList())
                findNavController().navigate(
                    R.id.action_searchByNameFragment_to_zoneSelectedFragment,
                    bundleOf("zones" to items)
                )
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getZones()
            onChange(zones) { fillZones(it) }
            onChange(loading) { showLoading(it) }
        }
    }

    private fun fillZones(items: List<SearchZone>) {
        val zonesAdapter = SearchByNameAdapter(items)
        binding.rvZones.apply {
            zonesAdapter.setOpenChecked { size ->
                validateSelectedZones(size)
            }
            adapter = zonesAdapter
            val manager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(context, manager.orientation)
            addItemDecoration(dividerItemDecoration)
            layoutManager = manager
        }
    }

    private fun validateSelectedZones(size: Int) {
        with(binding) {
            if (size > 0) {
                btnSearch.enable()
                zoneSelectedCount.show()
            } else {
                btnSearch.disable()
                zoneSelectedCount.hide()
            }
            tvSelectedZones.text = size.toString()
        }
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}