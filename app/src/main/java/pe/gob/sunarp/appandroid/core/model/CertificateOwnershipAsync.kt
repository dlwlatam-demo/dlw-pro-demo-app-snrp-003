package pe.gob.sunarp.appandroid.core.model

import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipDTO

class CertificateOwnershipAsync(
    val guid: String,
    val status: String,
    val response: List<CertificateOwnershipDTO>,
)