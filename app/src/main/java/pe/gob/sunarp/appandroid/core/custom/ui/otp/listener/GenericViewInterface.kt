package pe.gob.sunarp.appandroid.core.custom.ui.otp.listener

internal interface GenericViewInterface<T> {
    fun getView(type : Int = 0) : T
}