package pe.gob.sunarp.appandroid.data.profile.information

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.DocumentCeDTO
import pe.gob.sunarp.appandroid.core.dto.DocumentDniDTO
import pe.gob.sunarp.appandroid.core.dto.DocumentTypeDTO
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.common.ServiceResponse
import pe.gob.sunarp.appandroid.data.profile.information.dto.ProfileDTO
import pe.gob.sunarp.appandroid.data.profile.information.request.ProfileRequest
import pe.gob.sunarp.appandroid.data.profile.information.request.UpdateUserInformationRequest
import pe.gob.sunarp.appandroid.data.profile.information.request.UploadPhotoRequest
import pe.gob.sunarp.appandroid.data.registration.register.CeRequest
import pe.gob.sunarp.appandroid.data.registration.register.DniRequest
import retrofit2.http.*

interface ProfileDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/seguridad/login")
    suspend fun getProfileInformation(
        @Body request: ProfileRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String
    ): DataResult<ProfileDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/tipoDocumentos")
    suspend fun getDocumentTypes(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String
    ): DataResult<List<DocumentTypeDTO>>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/seguridad/registro/usuario/foto")
    suspend fun updateUserPhoto(
        @Body request: UploadPhotoRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @PUT(Constants.CONTEXTO_URL + "/seguridad/registro/usuario")
    suspend fun updateUserInformation(
        @Body request: UpdateUserInformationRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<ServiceResponse>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST(Constants.CONTEXTO_URL + "/consultas/valida/dni")
    suspend fun validateDocumentDNI(
        @Body request: DniRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<DocumentDniDTO>

    @Headers(Constants.HEADER_API_GATEWAY)
    @POST("Constants.CONTEXTO_URL + \"/consultas/valida/ce")
    suspend fun validateDocumentCE(
        @Body request: CeRequest,
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?
    ): DataResult<DocumentCeDTO>

}