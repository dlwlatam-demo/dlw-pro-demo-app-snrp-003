package pe.gob.sunarp.appandroid.core.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import pe.gob.sunarp.appandroid.data.alerta.AlertaDataSource
import pe.gob.sunarp.appandroid.data.alerta.AlertaRemoteRepository
import pe.gob.sunarp.appandroid.data.certificate.CertificateDataSource
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.common.SharedPreferenceRepository
import pe.gob.sunarp.appandroid.data.home.HomeDataSource
import pe.gob.sunarp.appandroid.data.home.HomeRemoteRepository
import pe.gob.sunarp.appandroid.data.login.LoginDataSource
import pe.gob.sunarp.appandroid.data.login.LoginRemoteRepository
import pe.gob.sunarp.appandroid.data.maps.MapsDataSource
import pe.gob.sunarp.appandroid.data.maps.MapsRemoteRepository
import pe.gob.sunarp.appandroid.data.payment.PaymentDataSource
import pe.gob.sunarp.appandroid.data.payment.PaymentRemoteRepository
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.profile.information.ProfileDataSource
import pe.gob.sunarp.appandroid.data.profile.information.ProfileRemoteRepository
import pe.gob.sunarp.appandroid.data.property.PropertyDataSource
import pe.gob.sunarp.appandroid.data.property.PropertyRemoteRepository
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationDataSource
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.data.search.SearchDataSource
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.data.solicitud.SolicitudDataSource
import pe.gob.sunarp.appandroid.data.solicitud.SolicitudRemoteRepository
import pe.gob.sunarp.appandroid.data.tive.TiveDataSource
import pe.gob.sunarp.appandroid.data.tive.TiveRemoteRepository
import pe.gob.sunarp.appandroid.data.transaction.TransactionDataSource
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.data.vehicle.VehicleDataSource
import pe.gob.sunarp.appandroid.data.vehicle.VehicleRemoteRepository
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Providers {

    @Singleton
    @Provides
    fun provideLoginDataSource(retrofit: Retrofit): LoginDataSource =
        retrofit.create(LoginDataSource::class.java)

    @Singleton
    @Provides
    fun provideProfileDataSource(retrofit: Retrofit): ProfileDataSource =
        retrofit.create(ProfileDataSource::class.java)

    @Singleton
    @Provides
    fun provideUserRegistrationDataSource(retrofit: Retrofit): UserRegistrationDataSource =
        retrofit.create(UserRegistrationDataSource::class.java)

    @Singleton
    @Provides
    fun provideTransactionDataSource(retrofit: Retrofit): TransactionDataSource =
        retrofit.create(TransactionDataSource::class.java)

    @Singleton
    @Provides
    fun providePropertyDataSource(retrofit: Retrofit): PropertyDataSource =
        retrofit.create(PropertyDataSource::class.java)

    @Singleton
    @Provides
    fun provideCertificateDataSource(retrofit: Retrofit): CertificateDataSource =
        retrofit.create(CertificateDataSource::class.java)

    @Singleton
    @Provides
    fun provideSearchDataSource(retrofit: Retrofit): SearchDataSource =
        retrofit.create(SearchDataSource::class.java)

    @Singleton
    @Provides
    fun providePaymentDataSource(retrofit: Retrofit): PaymentDataSource =
        retrofit.create(PaymentDataSource::class.java)

    @Singleton
    @Provides
    fun provideHomeDataSource(retrofit: Retrofit): HomeDataSource =
        retrofit.create(HomeDataSource::class.java)

    @Singleton
    @Provides
    fun provideVehicleDataSource(retrofit: Retrofit): VehicleDataSource =
        retrofit.create(VehicleDataSource::class.java)

    @Singleton
    @Provides
    fun providesLoginRemoteRepository(
        loginDataSource: LoginDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        LoginRemoteRepository(loginDataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesUserRegistrationRemoteRepository(
        userRegistrationDataSource: UserRegistrationDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        UserRegistrationRemoteRepository(userRegistrationDataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesSunarpSharedPreferenceRepository(
        sharedPreferences: SunarpSharedPreferences
    ) =
        SharedPreferenceRepository(sharedPreferences)

    @Singleton
    @Provides
    fun providesProfileRemoteRepository(
        profileDataSource: ProfileDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        ProfileRemoteRepository(profileDataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesTransactionRemoteRepository(
        dataSource: TransactionDataSource,
        @ApplicationContext context: Context,
        sharedPreferences: SunarpSharedPreferences
    ) =
        TransactionRemoteRepository(dataSource, sharedPreferences, context)

    @Singleton
    @Provides
    fun providesPropertyRemoteRepository(
        dataSource: PropertyDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        PropertyRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesCertificateRemoteRepository(
        dataSource: CertificateDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        CertificateRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesSearchRemoteRepository(
        dataSource: SearchDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        SearchRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesVehicleRemoteRepository(
        dataSource: VehicleDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        VehicleRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesPaymentRemoteRepository(
        dataSource: PaymentDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        PaymentRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun providesHomeRemoteRepository(
        dataSource: HomeDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) = HomeRemoteRepository(dataSource, sharedPreferences)

    @Singleton
    @Provides
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences("SunarpApp", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideTiveDataSource(retrofit: Retrofit): TiveDataSource =
        retrofit.create(TiveDataSource::class.java)

    @Singleton
    @Provides
    fun providesTiveRemoteRepository(
        dataSource: TiveDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        TiveRemoteRepository(dataSource, sharedPreferences)


    @Singleton
    @Provides
    fun providesAlertaRemoteRepository(
        datasource: AlertaDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        AlertaRemoteRepository(datasource, sharedPreferences)

    @Singleton
    @Provides
    fun provideAlertaDataSource(retrofit: Retrofit): AlertaDataSource =
        retrofit.create(AlertaDataSource::class.java)

    @Singleton
    @Provides
    fun providesSolicitudRemoteRepository(
        datasource: SolicitudDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        SolicitudRemoteRepository(datasource, sharedPreferences)

    @Singleton
    @Provides
    fun provideSolicitudDataSource(retrofit: Retrofit): SolicitudDataSource =
        retrofit.create(SolicitudDataSource::class.java)

    @Singleton
    @Provides
    fun providesMapsRemoteRepository(
        datasource: MapsDataSource,
        sharedPreferences: SunarpSharedPreferences
    ) =
        MapsRemoteRepository(datasource, sharedPreferences)

    @Singleton
    @Provides
    fun provideMapsDataSource(retrofit: Retrofit): MapsDataSource =
        retrofit.create(MapsDataSource::class.java)

}