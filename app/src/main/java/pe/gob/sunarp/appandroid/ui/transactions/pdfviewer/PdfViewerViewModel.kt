package pe.gob.sunarp.appandroid.ui.transactions.pdfviewer

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.transaction.TransactionRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PdfViewerViewModel @Inject constructor(
    private val repository: TransactionRemoteRepository
) : BaseViewModel() {

    val documentLoaded = SingleLiveEvent<String>()
    val loading = SingleLiveEvent<Boolean>()

    fun getDocument(key: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getDocument(key)) {
                is DataResult.Success -> onSuccess(result.data.document ?: "")
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getNewsletter(key: String) {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getNewsletter(key)) {
                is DataResult.Success -> onSuccess(result.data.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    private fun onSuccess(data: String) {
        loading.value = false
        documentLoaded.value = data
    }

}