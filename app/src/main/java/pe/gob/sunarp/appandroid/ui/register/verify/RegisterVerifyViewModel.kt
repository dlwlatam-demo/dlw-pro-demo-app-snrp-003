package pe.gob.sunarp.appandroid.ui.register.verify

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.registration.UserRegistrationRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterVerifyViewModel @Inject constructor(private val repository: UserRegistrationRemoteRepository) : BaseViewModel() {

    val onError = SingleLiveEvent<String?>()
    val onErrorResend = SingleLiveEvent<String?>()
    val isVerified = SingleLiveEvent<String?>()
    val isResend = SingleLiveEvent<String?>()
    val showLoading = SingleLiveEvent<Boolean>()

    fun verify(code: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.validateSMS(code)) {
                is DataResult.Success -> {
                    isVerified.value = result.data
                    showLoading.value = false
                }
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    override fun onError(err: Exception) {
        showLoading.value = false
        onError.value = err.message
    }

    fun resend(phone: String) {
        viewModelScope.launch {
            showLoading.value = true
            when (val result = repository.sendSMS(phone)) {
                is DataResult.Success -> {
                    isResend.value = result.data
                    showLoading.value = false
                }
                is DataResult.Failure -> {
                    onErrorResend.value = result.exception.message
                    showLoading.value = false
                }
            }
        }
    }
}