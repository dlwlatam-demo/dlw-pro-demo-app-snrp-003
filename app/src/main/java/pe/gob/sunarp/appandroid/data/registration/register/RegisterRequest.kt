package pe.gob.sunarp.appandroid.data.registration.register

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class RegisterRequest(
    @field:Json(name = "email")
    val email: String,
    @field:Json(name = "sexo")
    val gender: String,
    @field:Json(name = "tipoDoc")
    val documentType: String,
    @field:Json(name = "nroDoc")
    val documentNumber: String,
    @field:Json(name = "nombres")
    val names: String,
    @field:Json(name = "priApe")
    val firstLastname: String,
    @field:Json(name = "segApe")
    val secondLastname: String,
    @field:Json(name = "fecNac")
    val date: String,
    @field:Json(name = "nroCelular")
    val mobilePhone: String,
    @field:Json(name = "password")
    val password: String,
    @field:Json(name = "geoLat")
    val geoLat: Int,
    @field:Json(name = "geoLong")
    val geoLong: Int,
    @field:Json(name = "appId")
    val appId: String,
    @field:Json(name = "guid")
    val guid: String?,
    @field:Json(name = "appVersion")
    val appVersion: String,
) : BaseModel() {
    constructor() : this("", "", "", "", "", "", "", "", "", "", 0, 0, "", "", "")
}