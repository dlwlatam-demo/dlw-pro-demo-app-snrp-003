package pe.gob.sunarp.appandroid.data.solicitud

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.alerta.modelservices.*
import pe.gob.sunarp.appandroid.data.solicitud.model.SolicitudNotiItemResponse
import retrofit2.http.*

interface SolicitudDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/solicitud/news/transaccion")
    suspend fun getNotificaciones(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO",
    ): DataResult<List<SolicitudNotiItemResponse>>

}