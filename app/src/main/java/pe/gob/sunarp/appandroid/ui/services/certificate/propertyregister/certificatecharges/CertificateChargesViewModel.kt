package pe.gob.sunarp.appandroid.ui.services.certificate.propertyregister.certificatecharges

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarPartidaCargadaDesgravamenRequest
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class CertificateChargesViewModel @Inject constructor(
    private val parameterRepository: CertificateRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()
    var selectOficinaRegistral = SingleLiveEvent<RegistrationOffice>()
    var appliesTypes = SingleLiveEvent<List<ItemSpinner>>()
    var selectAppliesTypes = SingleLiveEvent<ItemSpinner>()
    var librosRegistrales = SingleLiveEvent<List<LibroRegistral>>()
    var selectLibroRegistral = SingleLiveEvent<LibroRegistral>()

    val estadoPartida = SingleLiveEvent<Partida>()

    fun getParameters() {
        getOficinasRegistrales()
        getAppliesTypes()
    }

    private fun getOficinasRegistrales() {
        viewModelScope.launch {
            loading.value = true
            when (val result = parameterRepository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    fun getLibrosRegistrales(certificate: CertificateType?) {
        viewModelScope.launch {
            loading.value = true
            when (val result =
                parameterRepository.getLibrosRegistrales(certificate?.areaId.orEmpty())) {
                is DataResult.Success -> OnGetLibrosRegistrales(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun OnGetLibrosRegistrales(data: List<LibroRegistral>) {
        loading.value = false
        librosRegistrales.value = data
    }


    private fun getAppliesTypes() {
        viewModelScope.launch {
            appliesTypes.value =
                listOf(ItemSpinner("01", "Número de partida"), ItemSpinner("02", "Tomo/Folio"))
        }
    }

    fun setSelectOficinaRegistral(data: RegistrationOffice) {
        selectOficinaRegistral.value = data
    }

    fun getSelectOficinaRegistral(): RegistrationOffice? {
        return selectOficinaRegistral?.value
    }

    fun setSelectApplyTypes(data: ItemSpinner) {
        selectAppliesTypes.value = data
    }

    fun getSelectApplyTypes(): ItemSpinner? {
        return selectAppliesTypes.value
    }

    fun selectLibroRegistral(data: LibroRegistral) {
        selectLibroRegistral.value = data
    }

    fun getLibroRegistral(): LibroRegistral? {
        return selectLibroRegistral?.value
    }

    fun isNroPartidaSelect() = selectAppliesTypes.value?.id == "01"

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

    fun solicitarCertificado(
        certificate: CertificateType?,
        nroPartida: String,
        folio: String,
        tomo: String
    ) {
        viewModelScope.launch {
            loading.value = true
            val request = ValidarPartidaCargadaDesgravamenRequest(
                selectOficinaRegistral.value?.regPubId.orEmpty(),
                selectOficinaRegistral.value?.oficRegId.orEmpty(),
                certificate?.areaId.orEmpty(),
                if (isNroPartidaSelect()) nroPartida else "$tomo-$folio",
                certificate?.codGrupoLibroArea.orEmpty(),
                if (isNroPartidaSelect()) "-" else selectLibroRegistral.value?.codLibro.orEmpty(),
                selectAppliesTypes.value?.id ?: "01"
            )
            when (val result = parameterRepository.validarPartidaCargadaDesgravamen(request)) {
                is DataResult.Success -> onValidarPartidaSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }


    fun onValidarPartidaSuccess(data: Partida) {
        loading.value = false
        estadoPartida.value = data
    }

}