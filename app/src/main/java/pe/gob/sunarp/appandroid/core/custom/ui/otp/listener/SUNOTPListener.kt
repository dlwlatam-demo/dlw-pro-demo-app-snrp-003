package pe.gob.sunarp.appandroid.core.custom.ui.otp.listener

interface SUNOTPListener {

    /**
     * Callback fired every time the counter is updated.
     */
    fun onShowCount(time: String)

    /**
     * Callback fired when timer has started.
     */
    fun onStarCount()

    /**
     * Callback fired when timer has finished.
     */
    fun onStopCount()

    /**
     * Callback fired when timer has finished and user presses it.
     */
    fun onResendCode()

    /**
     * @param otp Filled OTP
     * Callback fired when user has completed filling the OTP.
     */
    fun onComplete(otp: CharSequence)
}
