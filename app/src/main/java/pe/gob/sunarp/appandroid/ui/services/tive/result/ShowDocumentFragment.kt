package pe.gob.sunarp.appandroid.ui.services.tive.result

import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.core.toBitmap
import pe.gob.sunarp.appandroid.databinding.FragmentTiveShowDocumentBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class ShowDocumentFragment :
    BaseFragment<FragmentTiveShowDocumentBinding>(FragmentTiveShowDocumentBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val image64: String? = arguments?.getString("image64")
        renderPdf(image64?:"")
    }

    private fun renderPdf(base64EncodedString: String) = try {
        val decodedString = Base64.decode(base64EncodedString, Base64.DEFAULT)
        binding.contentImage.fromBytes(decodedString).load()
    } catch (e: Exception) {
        Log.e("Debug", "Error de render")
    }

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }
}
