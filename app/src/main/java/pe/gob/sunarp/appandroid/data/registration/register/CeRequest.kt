package pe.gob.sunarp.appandroid.data.registration.register

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class CeRequest(
    @field:Json(name = "ce")
    val documentNumber: String,
    @field:Json(name = "guid")
    val guid: String? = null
) : BaseModel() {
    constructor() : this("", "")
}