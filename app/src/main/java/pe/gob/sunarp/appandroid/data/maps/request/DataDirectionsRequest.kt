package pe.gob.sunarp.appandroid.data.maps.request

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class DataDirectionsRequest(
	var origin: String= "",
	var destination: String= "",
	var key: String= ""
): Parcelable

