package pe.gob.sunarp.appandroid.data.profile.information.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UpdateUserInformationRequest(

    @field:Json(name = "tipoDoc")
    val docType: String,
    @field:Json(name = "nroDoc")
    val docNumber: String,
    @field:Json(name = "nombres")
    val names: String,
    @field:Json(name = "priApe")
    val firstLastname: String,
    @field:Json(name = "segApe")
    val secondLastname: String,
    @field:Json(name = "fecNac")
    val birthday: String,
    @field:Json(name = "sexo")
    val gender: String,
    @field:Json(name = "nroCelular")
    val mobilePhone: String,
    @field:Json(name = "rememberToken")
    val token: String,
    @field:Json(name = "appVersion")
    val appVersion: String,
    @field:Json(name = "ipAddress")
    val ipAddress: String,
    @field:Json(name = "lastConn")
    val lastConnection: String,
    @field:Json(name = "geoLat")
    val latitude: Int,
    @field:Json(name = "geoLong")
    val longitude: Int,
)