package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.data.vehicle.request.VehicleReceiptRequest
import pe.gob.sunarp.appandroid.data.vehicle.request.VehicleRequest

@Parcelize
class VehicleInformation(
    val plate: String,
    val regPubId: String,
    val oficRegId: String
) : Parcelable {
    fun toRequest() = VehicleRequest(plate, regPubId, oficRegId)

    fun toReceiptRequest() = VehicleReceiptRequest(plate, oficRegId, regPubId)
}