package pe.gob.sunarp.appandroid.core.custom.ui.requeststatus.group

data class StatusInfoItem(
    val title: String,
    val description: String?,
)

data class StatusInfoContent(
    val title: String,
    val data: List<StatusInfoItem>,
)

data class StatusInfoGroup(
    val requestData: StatusInfoContent,
    val userData: StatusInfoContent,
    val destinationData: StatusInfoContent,
    val paymentData: StatusInfoContent,
)