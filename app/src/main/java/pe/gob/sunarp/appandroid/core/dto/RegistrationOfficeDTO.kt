package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice

@JsonClass(generateAdapter = true)
data class RegistrationOfficeDTO(
    @field:Json(name = "nombre")
    val name: String?,
    @field:Json(name = "regPubId")
    val regPubId: String?,
    @field:Json(name = "oficRegId")
    val oficRegId: String?,
    @field:Json(name = "codOficina")
    val codOficina: String?,
    @field:Json(name = "jurisId")
    val preOfficjurisIde: String?,
    ) : BaseModel() {
    fun toModel() = RegistrationOffice(
        name.orEmpty(),
        regPubId.orEmpty(),
        oficRegId.orEmpty(),
        codOficina.orEmpty(),
        preOfficjurisIde.orEmpty()
    )

}