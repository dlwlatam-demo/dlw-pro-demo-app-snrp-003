package pe.gob.sunarp.appandroid.ui.home.menu

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import pe.gob.sunarp.appandroid.data.Utils
import pe.gob.sunarp.appandroid.databinding.FragmentHomeBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.menu.adapter.HomeAdapter

class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = HomeAdapter(Utils.getHomeItems())
        binding.rvHome.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            this.adapter = adapter
        }


    }

    

    override fun initViewModelsErrorToken() {
        TODO("Not yet implemented")
    }

}