package pe.gob.sunarp.appandroid.ui.services.searchbyname.zoneselected.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.SearchZone
import pe.gob.sunarp.appandroid.databinding.LayoutZoneSelectedItemBinding

class ZoneSelectedAdapter(
    val items: MutableList<SearchZone>
) : RecyclerView.Adapter<ZoneSelectedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutZoneSelectedItemBinding =
            LayoutZoneSelectedItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], position)
    }

    override fun getItemCount() = items.size

    fun getZones() = items.map { it.id }.toList()

    inner class ViewHolder(private val binding: LayoutZoneSelectedItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchZone, position: Int) = with(binding) {
            with(item) {
                tvRegisterArea.text = name
                /*btnDelete.setOnClickListener {
                    items.remove(item)
                    notifyItemChanged(position)
                }*/
            }
        }
    }


}