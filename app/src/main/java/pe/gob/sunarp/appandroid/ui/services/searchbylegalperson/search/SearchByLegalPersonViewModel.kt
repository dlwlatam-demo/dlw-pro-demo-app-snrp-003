package pe.gob.sunarp.appandroid.ui.services.searchbylegalperson.search

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import pe.gob.sunarp.appandroid.core.SingleLiveEvent
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.model.LegalPerson
import pe.gob.sunarp.appandroid.core.model.RegistrationOffice
import pe.gob.sunarp.appandroid.core.model.SearchLegalPerson
import pe.gob.sunarp.appandroid.data.certificate.CertificateRemoteRepository
import pe.gob.sunarp.appandroid.data.search.SearchRemoteRepository
import pe.gob.sunarp.appandroid.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SearchByLegalPersonViewModel @Inject constructor(
    private val repository: CertificateRemoteRepository,
    private val searchRepository: SearchRemoteRepository
) : BaseViewModel() {

    val loading = SingleLiveEvent<Boolean>()
    val hasData = SingleLiveEvent<Boolean>()
    val offices = SingleLiveEvent<List<RegistrationOffice>>()

    fun getZones() {
        viewModelScope.launch {
            loading.value = true
            when (val result = repository.getRegistrationOffices()) {
                is DataResult.Success -> onRegistrationOfficeSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }
    fun searchLegalPerson(search: SearchLegalPerson) {
        viewModelScope.launch {
            loading.value = true
            when (val result = searchRepository.searchLegalPerson(search)) {
                is DataResult.Success -> onSearchLegalSuccess(result.data)
                is DataResult.Failure -> onError(result.exception)
            }
        }
    }

    private fun onSearchLegalSuccess(data: List<LegalPerson>) {
        loading.value = false
        hasData.value = data.isNotEmpty()
    }

    private fun onRegistrationOfficeSuccess(data: List<RegistrationOffice>) {
        loading.value = false
        offices.value = data
    }

    override fun onError(err: Exception) {
        loading.value = false
        super.onError(err)
    }

}