package pe.gob.sunarp.appandroid.ui.alerta.mandatos.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.snack
import pe.gob.sunarp.appandroid.data.alerta.modelservices.MandatoItemResponse
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaMandatoListBinding
import pe.gob.sunarp.appandroid.databinding.LayoutAlertaMandatoListItemBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaMandatoListFragment : BaseFragment<FragmentAlertaMandatoListBinding>(FragmentAlertaMandatoListBinding::inflate) {

    private val viewModel: AlertaMandatoListViewModel by viewModels()
    private lateinit var adapterMandatos: ItemMandatoAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private var countReg = 0
    private fun initViews() {
        with(binding) {
            val maxReg = 5
            initViewModels()
            adapterMandatos = ItemMandatoAdapter(arrayListOf(), { mandatoitem ->
                activity?.let {
                    val alertaCancelAccountDialog = AlertaMandatoRemoverDialog()
                    alertaCancelAccountDialog.apply {
                        onCancelListener {
                            this.dismiss()
                        }
                        onAcceptListener {
                            this.dismiss()
                            viewModel.cancelMandato(mandatoitem)
                        }
                        show(it.supportFragmentManager, "drop")
                    }
                }
            }, {
                findNavController().navigate(
                    R.id.action_nav_alerta_mandato_list_to_alertaMandatoDetailFragment,
                    bundleOf("mandato" to it)
                )
            })
            rvListMandatos.adapter = adapterMandatos
            rvListMandatos.layoutManager = LinearLayoutManager(requireContext())
            countReg = adapterMandatos.itemCount
            Log.e("rv0",countReg.toString())

            updateButtonState(adapterMandatos.itemCount, maxReg)

            btnAdd.setOnClickListener {
                Log.e("rv",countReg.toString())
                if (adapterMandatos.itemCount < 5) {
                    findNavController().navigate(R.id.action_nav_alerta_mandato_list_to_alertaMandatoAddFragment)

                    btnAdd.isEnabled = true
                } else {
                    // Mostrar un mensaje de error o notificación
                    btnAdd.isEnabled = true
                    requireView().snack("No se pueden agregar más de 5 registros.")
                }
            }
        }
    }
    private fun updateButtonState(currentCount: Int, maxCount: Int) {
        binding.btnAdd.isEnabled = currentCount < maxCount
    }
    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { showLoading(it) }
            onChange(listData) { setListData(it) }
            getListData()
        }
    }

    private fun setListData(listdata: List<MandatoItemResponse>) {
        adapterMandatos.items.clear()
        adapterMandatos.items.addAll(listdata)
        adapterMandatos.notifyDataSetChanged()
    }

    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItemMandatoAdapter(
        var items: MutableList<MandatoItemResponse> = arrayListOf(),
        private val alertRemove: (item: MandatoItemResponse) -> Unit,
        private val openDetail: (item: MandatoItemResponse) -> Unit
    ) :RecyclerView.Adapter<ItemMandatoAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutAlertaMandatoListItemBinding =
                LayoutAlertaMandatoListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(itemBinding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item, alertRemove, openDetail)
        }

        override fun getItemCount() : Int {
            Log.e("pruebacontador",items.size.toString())
            return items.size
        }

        inner class ViewHolder(private val binding: LayoutAlertaMandatoListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(
                item: MandatoItemResponse,
                deleteFuntion: (item: MandatoItemResponse) -> Unit,
                openDetailFunction: (item: MandatoItemResponse) -> Unit
            ) = with(binding) {
                statusContainer.setOnClickListener { openDetailFunction(item) }
                btnDelete.setOnClickListener { deleteFuntion(item) }
                tvName.text = "${item.noPersNatu} ${item.apPatePersNatu} ${item.apMatePersNatu}"
                tvFecha.text = item.feAltaCont
            }
        }
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}
