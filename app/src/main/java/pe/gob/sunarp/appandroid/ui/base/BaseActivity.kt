package pe.gob.sunarp.appandroid.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB : ViewBinding>: AppCompatActivity() {

    private var _binding: VB? = null
    val binding get() = requireNotNull(_binding)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = viewBinding()
        setContentView(binding.root)
    }

    abstract fun viewBinding(): VB

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}