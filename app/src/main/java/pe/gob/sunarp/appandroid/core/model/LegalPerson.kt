package pe.gob.sunarp.appandroid.core.model

data class LegalPerson(
    val legalPerson: String,
    val acronym: String,
    val office: String,
    val request: String,
    val registerCode: String,
    val siteCode: String,
)