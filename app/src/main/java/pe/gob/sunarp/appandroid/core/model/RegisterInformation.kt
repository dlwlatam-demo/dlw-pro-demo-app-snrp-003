package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import pe.gob.sunarp.appandroid.data.registration.register.RegisterRequest

@Parcelize
data class RegisterInformation(
    val email: String,
    val gender: String,
    val documentType: String,
    val documentNumber: String,
    val names: String,
    val firstLastname: String,
    val secondLastname: String,
    val date: String,
    val numberPhone: String,
    val password: String
) : Parcelable {
    fun toRequest(guid: String?, appId: String) =
        RegisterRequest(
            email,
            gender,
            documentType,
            documentNumber,
            names,
            firstLastname,
            secondLastname,
            date,
            numberPhone,
            password,
            0,
            0,
            appId,
            guid,
            "3.0"
        )
}