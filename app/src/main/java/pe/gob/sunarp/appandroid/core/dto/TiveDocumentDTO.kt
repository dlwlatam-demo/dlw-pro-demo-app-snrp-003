package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.TiveDocument

@JsonClass(generateAdapter = true)
data class TiveDocumentDTO(
    @field:Json(name = "documento")
    val documento: String?,
) : BaseModel() {
    fun toModel() = TiveDocument(documento.orEmpty())
}