package pe.gob.sunarp.appandroid.core.custom.ui

import android.view.View
import pe.gob.sunarp.appandroid.core.custom.ui.otp.model.SUNOTPState
import pe.gob.sunarp.appandroid.core.custom.ui.otp.view.SUNOTP

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}

fun View.toggleVisibility() {
    this.visibility = if (this.visibility == View.VISIBLE) {
        View.GONE
    } else {
        View.VISIBLE
    }
}
fun SUNOTP.default() { state = SUNOTPState.DEFAULT }
fun SUNOTP.focused() { state = SUNOTPState.FOCUSED }
fun SUNOTP.validating() { state = SUNOTPState.VALIDATING }
fun SUNOTP.validated() { state = SUNOTPState.VALIDATED }
fun SUNOTP.error() { state = SUNOTPState.ERROR }