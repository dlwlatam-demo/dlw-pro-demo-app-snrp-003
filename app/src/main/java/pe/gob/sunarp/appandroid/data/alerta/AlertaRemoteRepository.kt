package pe.gob.sunarp.appandroid.data.alerta

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.alerta.modelservices.*
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import javax.inject.Inject


class AlertaRemoteRepository @Inject constructor(
    private val dataSource: AlertaDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun loginAlerta(email: String, password: String): DataResult<LoginResponse> = withContext(dispatcher) {
        when (val result = dataSource.loginAlerta(
            UserRequest(email, password),
            preferences.accessToken.toBearer(),
            getHost(BuildConfig.SUNARP_ALERTA_KEY)
        )) {
            is DataResult.Success -> {
                preferences.alertaLoginEmail = email
                preferences.alertaLoginPass = password
                preferences.saveUserDataAlerta(result.data)
                DataResult.Success(result.data)
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun getInfoUSer(): DataResult<LoginResponse> = withContext(dispatcher) {
        when (val result = dataSource.loadUserByEmailAndPass(
            preferences.accessToken.toBearer(),
            getHost(BuildConfig.SUNARP_ALERTA_KEY)
        )) {
            is DataResult.Success -> {
                preferences.saveUserDataAlerta(result.data)
                DataResult.Success(result.data)
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }
    suspend fun registerUserAlerta(email: String, recovery: Boolean = false): DataResult<String> = withContext(dispatcher) {
        when (val result = if(recovery) {
            dataSource.loadUserByEmailChnPwd(email, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))
        } else {

            dataSource.loadUserByEmail(email, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))
        }) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "0" -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                    "1" -> {
                        when (val resultValidationCode = sendValidationCode(result.data.guid.orEmpty())) {
                            is DataResult.Success -> DataResult.Success(resultValidationCode.data)
                            is DataResult.Failure -> DataResult.Failure(Exception(resultValidationCode.exception.message ?: ""))
                        }
                    }
                    "2" -> {
                        when (val resultCreateUserAlerta = createUserAlerta(result.data.guid.orEmpty(), email)) {
                            is DataResult.Success -> {
                                when (val resultValidationCode = sendValidationCode(resultCreateUserAlerta.data.guid.orEmpty())) {
                                    is DataResult.Success -> DataResult.Success(resultValidationCode.data)
                                    is DataResult.Failure -> DataResult.Failure(Exception(resultValidationCode.exception.message ?: ""))
                                }
                            }
                            is DataResult.Failure -> DataResult.Failure(Exception(resultCreateUserAlerta.exception.message ?: ""))
                        }
                    }
                    else -> DataResult.Failure(Exception("Error al procesar solicitud"))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun sendValidationCode(guid: String): DataResult<String> = withContext(dispatcher) {
        when (val result = dataSource.sendValidationCodeByMail(guid, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.guid.orEmpty())
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun activateUserById(guid: String, code: String): DataResult<String> = withContext(dispatcher) {
        when (val result = dataSource.activateUserById(guid, code ,preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.guid.orEmpty())
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun createUserAlerta(guid: String, email: String): DataResult<GenericResponse> = withContext(dispatcher) {
        when (val result = dataSource.createUserAlerta(UserRegisterAlertRequest(email, "3.0", guid) ,preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data)
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun restorePassAlerta(guid: String, pass: String): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.restorePassAlerta(guid, pass ,preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun cancelAccount(): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.cancelAccount(CancelAccountRequest(0),preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun updateUser(data: UpdateAccountRequest): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.updateUser(data,preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun addMandato(data: AddMandatoRequest): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.addMandato(data,preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_MANDATO_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun selectAllMandatos(): DataResult<List<MandatoItemResponse>> = withContext(dispatcher) {
        when (val result = dataSource.selectAllMandatos(preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_MANDATO_KEY))) {
            is DataResult.Success -> DataResult.Success(result.data)
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun deleteMandato(data: DeleteMandatoRequest): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.deleteMandato(data, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_MANDATO_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun selectAllPartidas(): DataResult<List<PartidaItemResponse>> = withContext(dispatcher) {
        when (val result = dataSource.selectAllPartidas(preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_PARTIDA_KEY))) {
            is DataResult.Success -> DataResult.Success(result.data)
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun deletePartida(data: DeleteMandatoRequest): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.deletePartida(data, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_PARTIDA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun searchPartida(data: BuscarPartidaRequest): DataResult<List<BuscarPartidaItemResponse>> = withContext(dispatcher) {
        when (val result = dataSource.searchPartida(data, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_PARTIDA_KEY))) {
            is DataResult.Success -> {
                if (result.data.isNotEmpty()) {
                    DataResult.Success(result.data)
                } else {
                    DataResult.Failure(Exception("No se encontraron partidas"))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun addPartida(data: AddPartidaRequest): DataResult<Boolean> = withContext(dispatcher) {
        when (val result = dataSource.addPartida(data, preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_PARTIDA_KEY))) {
            is DataResult.Success -> {
                when(result.data.codResult) {
                    "1" -> DataResult.Success(result.data.codResult == "1")
                    else -> DataResult.Failure(Exception(result.data.msgResult ?: ""))
                }
            }
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

    suspend fun getNotificationDetail(notification: NotificationResponse, tipo: String, source: String): DataResult<List<DetailMandatoResponse>> = withContext(dispatcher) {
        when (val result = dataSource.geteDetailMandato(
            notification.aaCont,
            notification.nuCont,
            tipo,
            source,
            preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_ALERTA_PARTIDA_KEY))) {
            is DataResult.Success -> DataResult.Success(result.data)
            is DataResult.Failure -> DataResult.Failure(Exception(result.exception.message ?: ""))
        }
    }

}