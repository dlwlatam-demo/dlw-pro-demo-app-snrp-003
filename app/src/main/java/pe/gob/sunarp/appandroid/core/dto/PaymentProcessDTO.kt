package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.PaymentProcess

@JsonClass(generateAdapter = true)
data class PaymentProcessDTO(
    @field:Json(name = "solicitudId")
    val solicitudId: Int?,
    @field:Json(name = "descripcion")
    val descripcion: String?,
    @field:Json(name = "tsCrea")
    val tsCrea: String?,
    @field:Json(name = "total")
    val total: String?,
    @field:Json(name = "email")
    val email: String?,
    @field:Json(name = "solicitante")
    val solicitante: String?,
    @field:Json(name = "tpoPago")
    val tpoPago: String?,
    @field:Json(name = "pagoSolicitudId")
    val pagoSolicitudId: Int?,
    @field:Json(name = "numeroRecibo")
    val numeroRecibo: String?,
    @field:Json(name = "numeroPublicidad")
    val numeroPublicidad: String?,
    @field:Json(name = "codVerificacion")
    val codVerificacion: String?,
    @field:Json(name = "secReciDetaAtenNac")
    val secReciDetaAtenNac: String?,
    @field:Json(name = "codCertificado")
    val codCertificado: String?,
    @field:Json(name = "bgr")
    val bgr: Boolean?,
    @field:Json(name = "codDescarga")
    val codDescarga: String?,
    @field:Json(name = "codLibroOpc")
    val codLibroOpc: String?,
) {
    fun toModel() = PaymentProcess(
        solicitudId ?: 0,
        descripcion.orEmpty(),
        tsCrea.orEmpty(),
        total.orEmpty(),
        email.orEmpty(),
        solicitante.orEmpty(),
        tpoPago.orEmpty(),
        pagoSolicitudId ?: 0,
        numeroRecibo.orEmpty(),
        numeroPublicidad.orEmpty(),
        codVerificacion.orEmpty(),
        secReciDetaAtenNac.orEmpty(),
        codCertificado.orEmpty(),
        bgr ?: false,
        codDescarga.orEmpty(),
        codLibroOpc.orEmpty(),
    )
}