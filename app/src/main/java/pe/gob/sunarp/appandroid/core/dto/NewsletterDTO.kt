package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.Newsletter

@JsonClass(generateAdapter = true)
data class NewsletterDTO(
    @field:Json(name = "boleta")
    val data: String?,
) : BaseModel() {
    constructor() : this("")

    fun toModel() =
        Newsletter(data ?: ""
        )
}