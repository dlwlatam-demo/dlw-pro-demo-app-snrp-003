package pe.gob.sunarp.appandroid.ui.services.certificate.legalperson.certificadovigenciapoderpj

import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.core.model.*
import pe.gob.sunarp.appandroid.data.certificate.request.ValidarPartidaCargadaDesgravamenRequest
import pe.gob.sunarp.appandroid.databinding.FragmentCertificadoVigenciaPoderPjBinding
import pe.gob.sunarp.appandroid.databinding.LayoutSimpleItemButtomBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.services.certificate.utils.PaymentDataItem
import android.widget.EditText



@AndroidEntryPoint
class CertificadoVigenciaPoderPJFragment :
    BaseFragment<FragmentCertificadoVigenciaPoderPjBinding>(FragmentCertificadoVigenciaPoderPjBinding::inflate){

    private var representanteSelect: String = "J"
    private lateinit var adapterAsientos: ItemButtonAdapter
    private val viewModel: CertificadoVigenciaPoderPJViewModel by viewModels()
    private var certificate: CertificateType? = null
    private var legalRecord: LegalRecord? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModels()
    }

    private fun initViews() {
        certificate = arguments?.getParcelable("certificate")
        legalRecord = arguments?.getParcelable("legalRecord")

        with(binding) {
            binding.spOficinaRegistral.clean()
            etNumero.filterOnlyMayus()
            //etFolio.filterOnlyMayus()
            //etTomo.filterOnlyMayus()
            etNroAsiento.filterMaxLengthAsiento()
            etCargoDatoAdicional.filterMaxLengthCargoDatoAdicional()
            etCargoOApoderado.visibility = View.GONE
            etCargoOApoderado.onChanged { validateFields() }
            etCargoOApoderado.filterMaxLengthCargoDatoAdicional()
            etNumero.onChanged { validateFields() }
            etRazonSocial.onChanged { validateFields() }
            etRazonSocial.filterMayus()
            etCargoDatoAdicional.onChanged { validateFields() }

            adapterAsientos =
                ItemButtonAdapter(arrayListOf(), {
                    boxAsientosHost.visibility = View.GONE
                }, { validateFields() })
            rvAsientos.adapter = adapterAsientos
            rvAsientos.layoutManager = LinearLayoutManager(requireContext())
            etNroAsiento.onChanged {
                if (it.isNotEmpty())
                    btnAddAsiento.enable()
                else
                    btnAddAsiento.disable()
            }
            btnAddAsiento.setOnClickListener {
                hideKeyboard()
                adapterAsientos.addItem(etNroAsiento.text.toString())
                adapterAsientos.items.let {
                    boxAsientosHost.visibility = if (it.isNotEmpty()) View.VISIBLE else View.GONE
                }
                etNroAsiento.text.clear()
            }

            btnSearch.setOnClickListener {
                hideKeyboard()
                viewModel.solicitar(
                    certificate,
                    if (viewModel.isTomoFolio()) "${etTomo.text}-${etFolio.text}" else etNumero.text.toString(),
                    etFolio.text.toString(),
                    etTomo.text.toString(),
                    representanteSelect,
                    adapterAsientos.items.joinToString(","),
                    etRazonSocial.text.toString(),
                    etCargoDatoAdicional.text.toString(),
                )
            }

            spOficinaRegistral.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etSolicitarPor.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
            etCertificateType.setOnFocusChangeListener { view, hasFocus ->
                hideKeyboard()
            }
        }
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            getParameters()
            onChange(loading) { showLoading(it) }
            onChange(offices) { setListenerOffice(it) }
            onChange(appliesTypes) { setApplyTypes(it) }
            onChange(selectAppliesTypes) { listenerApplySelect() }
            onChange(librosRegistrales) { setLibrosRegistrales(it) }
            onChange(selectLibroRegistral) { validateFields() }
            onChange(estadoPartida) { savePartida(it) }
            onChange(estadoPartidaBool) { savePartidaBool(it) }
            onChange(estadoPartidaReal) { savePartidaReal(it) }
        }
    }
    private fun savePartida(partida: Partida) {

        if (partida.estado == 0) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.partida = partida

            if (viewModel.isFicha()) {
                paymentDataItem.partida!!.numFicha = binding.etNumero.text.toString()
            }
            else if (viewModel.isPartida()) {
                paymentDataItem.partida!!.numPartida = binding.etNumero.text.toString()
            }
            paymentDataItem.typePerson = representanteSelect
            paymentDataItem.folio = binding.etFolio.text.toString()
            paymentDataItem.tomo = binding.etTomo.text.toString()
            paymentDataItem.asientos = adapterAsientos.items.joinToString(",")
            if (viewModel.isTomoFolio()) "${binding.etTomo.text}-${binding.etFolio.text}" else binding.etNumero.text.toString()
            paymentDataItem.businessName = binding.etRazonSocial.text.toString()
            paymentDataItem.apoderado = binding.etCargoOApoderado.text.toString()
            paymentDataItem.etDato = binding.etCargoDatoAdicional.text.toString()

            paymentDataItem.requestPCD = ValidarPartidaCargadaDesgravamenRequest(
                viewModel.getSelectOficinaRegistral()?.regPubId.orEmpty(),
                viewModel.getSelectOficinaRegistral()?.oficRegId.orEmpty(),
                certificate?.areaId.orEmpty(),
                if (viewModel.isTomoFolio()) "${binding.etTomo.text}-${binding.etFolio.text}" else binding.etNumero.text.toString(),
                certificate?.codGrupoLibroArea.orEmpty(),
                if (viewModel.isTomoFolio()) viewModel.selectLibroRegistral.value?.codLibro.orEmpty() else "-",
                Constants.EMPTY
            )


            findNavController().navigate(
                R.id.action_certificadoVigenciaPoderPJFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to null,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )

        } else {
            binding.container.snack(partida.msj)
        }
    }

    private fun savePartidaBool(result: Boolean) {

        if (result) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.partida = Partida(
                0,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )

            paymentDataItem.typePerson = representanteSelect
            paymentDataItem.folio = binding.etFolio.text.toString()
            paymentDataItem.tomo = binding.etTomo.text.toString()
            paymentDataItem.asientos = adapterAsientos.items.joinToString(",")
            paymentDataItem.numero =
                if (viewModel.isTomoFolio()) "${binding.etTomo.text}-${binding.etFolio.text}" else binding.etNumero.text.toString()
            paymentDataItem.businessName = binding.etRazonSocial.text.toString()
            paymentDataItem.etDato = binding.etCargoDatoAdicional.text.toString()
            paymentDataItem.apoderado = binding.etCargoOApoderado.text.toString()

            paymentDataItem.requestPCD = ValidarPartidaCargadaDesgravamenRequest(
                viewModel.getSelectOficinaRegistral()?.regPubId.orEmpty(),
                viewModel.getSelectOficinaRegistral()?.oficRegId.orEmpty(),
                certificate?.areaId.orEmpty(),
                if (viewModel.isTomoFolio()) "${binding.etTomo.text}-${binding.etFolio.text}" else binding.etNumero.text.toString(),
                certificate?.codGrupoLibroArea.orEmpty(),
                if (viewModel.isTomoFolio()) viewModel.selectLibroRegistral.value?.codLibro.orEmpty() else "-",
                Constants.EMPTY
            )

            findNavController().navigate(
                R.id.action_certificadoVigenciaPoderPJFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to null,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )

        } else {
            binding.container.snack("Lo sentimos, ninguna partida cumple con los criterios de búsqueda especificados.")
        }
    }
    private fun savePartidaReal(result: List<String>?) {

        if (!result.isNullOrEmpty()) {

            var paymentDataItem =
                PaymentDataItem(Constants.EMPTY, Constants.EMPTY, Constants.EMPTY)
            paymentDataItem.documentNumebr = Constants.EMPTY
            paymentDataItem.partida = Partida(
                0,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY,
                Constants.EMPTY
            )

            paymentDataItem.typePerson = representanteSelect
            paymentDataItem.folio = binding.etFolio.text.toString()
            paymentDataItem.tomo = binding.etTomo.text.toString()
            paymentDataItem.asientos = adapterAsientos.items.joinToString(",")
            val regex = "\\d+".toRegex()
            val matchResult = regex.find(result.toString())
            paymentDataItem.partida!!.numPartida = matchResult?.value.toString()
            paymentDataItem.businessName = binding.etRazonSocial.text.toString()
            paymentDataItem.etDato = binding.etCargoDatoAdicional.text.toString()
            paymentDataItem.apoderado = binding.etCargoOApoderado.text.toString()

            paymentDataItem.requestPCD = ValidarPartidaCargadaDesgravamenRequest(
                viewModel.getSelectOficinaRegistral()?.regPubId.orEmpty(),
                viewModel.getSelectOficinaRegistral()?.oficRegId.orEmpty(),
                certificate?.areaId.orEmpty(),
                if (viewModel.isTomoFolio()) "${binding.etTomo.text}-${binding.etFolio.text}" else binding.etNumero.text.toString(),
                certificate?.codGrupoLibroArea.orEmpty(),
                if (viewModel.isTomoFolio()) viewModel.selectLibroRegistral.value?.codLibro.orEmpty() else "-",
                Constants.EMPTY
            )

            findNavController().navigate(
                R.id.action_certificadoVigenciaPoderPJFragment_to_certificatePaymentFragment,
                bundleOf(
                    "certificate" to certificate,
                    "legalRecord" to legalRecord,
                    "dataItem" to paymentDataItem,
                    "office" to null,
                    "documentType" to null,
                    "ammount" to certificate?.preOffice!!.toDouble()
                )
            )

        } else {
            binding.container.snack("Lo sentimos, ninguna partida cumple con los criterios de búsqueda especificados.")
        }
    }


    private fun setLibrosRegistrales(items: List<LibroRegistral>) {
        with(binding) {
            etCertificateType.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as LibroRegistral)
                    this.tag = item.codLibro
                    viewModel.selectLibroRegistral(item)
                    validateFields()
                }
            }
        }
    }

    private fun listenerApplySelect() {
        with(binding) {
            etTomo.clean()
            etNumero.clean()
            etFolio.clean()
            if(viewModel.isTomoFolio()) {
                boxTomoFolio.visibility = View.VISIBLE
                boxNumero.visibility = View.GONE
                viewModel.getLibrosRegistrales(certificate)
            } else {
                boxTomoFolio.visibility = View.GONE
                boxNumero.visibility = View.VISIBLE
            }
            validateFields()
        }
    }

    private fun setListenerOffice(items: List<RegistrationOffice>) {
        with(binding) {
            spOficinaRegistral.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as RegistrationOffice)
                    this.tag = item.regPubId + item.oficRegId
                    viewModel.setSelectOficinaRegistral(item)
                    validateFields()
                }
            }
        }
    }


    private fun setApplyTypes(items: List<ItemSpinner>) {
        with(binding) {
            etSolicitarPor.apply {
                clean()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.layout_simple_spinner_item,
                        R.id.tv_title,
                        items
                    )
                )
                setOnItemClickListener { parent, view, position, id ->
                    val item = (parent.getItemAtPosition(position) as ItemSpinner)
                    this.tag = item.id
                    viewModel.setSelectApplyTypes(item)
                }
            }
        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (spOficinaRegistral.tag ?: "").toString().isNotEmpty() &&
                (etSolicitarPor.tag ?: "").toString().isNotEmpty() &&
                etRazonSocial.text.toString().isNotEmpty() &&
                //etCargoOApoderado.text.toString().isNotEmpty() &&
                //etCargoDatoAdicional.text.toString().isNotEmpty() &&
                if (viewModel.isTomoFolio()) {
                    (etCertificateType.tag ?: "").toString().isNotEmpty() &&
                    etFolio.text.toString().isNotEmpty() &&
                    etTomo.text.toString().isNotEmpty()
                } else etNumero.text.toString().isNotEmpty()
            ) {
                btnSearch.enable()
            } else {
                btnSearch.disable()
            }
        }
    }


    private fun showLoading(show: Boolean) {
        binding.loadingContainer.apply {
            if (show) loading.show() else loading.hide()
        }
    }

    

    class ItemButtonAdapter(var items: MutableList<String> = arrayListOf(), var invisibleHostBox: () -> Unit, var removeItemListener: () -> Unit) :
        RecyclerView.Adapter<ItemButtonAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemBinding: LayoutSimpleItemButtomBinding =
                LayoutSimpleItemButtomBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ViewHolder(itemBinding)
        }

        fun addItem(item: String) {
            if (!items.contains(item)) {
                items.add(item)
                notifyDataSetChanged()
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            holder.bind(item) {
                if (items.isNotEmpty()) {
                    val pos = items.indexOf(item)
                    items.removeAt(pos)
                    notifyItemRemoved(pos)
                    if (items.isEmpty()) {
                        invisibleHostBox.invoke()
                    }
                    removeItemListener.invoke()
                }
            }
        }

        override fun getItemCount() = items.size

        inner class ViewHolder(private val binding: LayoutSimpleItemButtomBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: String, deleteFuntion: () -> Unit) = with(binding) {
                tvTitle.text = item
                btnDelete.setOnClickListener { deleteFuntion() }
            }
        }


    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }

}