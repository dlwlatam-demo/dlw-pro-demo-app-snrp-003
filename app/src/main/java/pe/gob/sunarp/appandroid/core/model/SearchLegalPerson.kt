package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchLegalPerson(
    val office: String,
    val legalName: String,
    val acronym: String
): Parcelable