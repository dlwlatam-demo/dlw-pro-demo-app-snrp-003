package pe.gob.sunarp.appandroid.ui.alerta.login

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.*
import pe.gob.sunarp.appandroid.core.custom.ui.*
import pe.gob.sunarp.appandroid.databinding.FragmentAlertaLoginBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment

@AndroidEntryPoint
class AlertaLoginFragment : BaseFragment<FragmentAlertaLoginBinding>(FragmentAlertaLoginBinding::inflate) {

    private val viewModel: AlertaLoginViewModel by viewModels()

    override fun onStart() {
        super.onStart()
        with(viewModel) {
            initViewModelsErrorToken()
            if(isLoginAlert()) {
                openAlertHome()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        with(binding) {

            viewModel.showLoading.observe(viewLifecycleOwner) {
                if (it) loadingContainer.loading.show() else loadingContainer.loading.hide()
            }

            viewModel.isAuthenticated.observe(viewLifecycleOwner) {
                it?.let { _ ->
                    openAlertHome()
                }
            }

            btnRegistro.setOnClickListener {
                findNavController().navigate(R.id.action_alertaloginfragment_to_alertapreregistrofragment)
            }
            textViewGoToRecovery.setOnClickListener {
                findNavController().navigate(R.id.action_alertLoginFragment_to_alertaregistrofragment, bundleOf("title" to "Alerta Registral", "recovery" to true))
            }

            actionEnter.setOnClickListener {
                viewModel.login(
                    textInputEmail.editText?.text.toString(),
                    textInputPassword.editText?.text.toString()
                )
            }

            textInputEmail.editText?.onChanged { validateFields() }
            textInputPassword.editText?.onChanged { validateFields() }

        }
    }

    private fun validateFields() {
        with(binding) {
            if (
                (textInputEmail.editText?.text ?: "").toString().isValidString(Constants.REGEX_EMAIL_ADDRESS) &&
                (textInputPassword.editText?.text ?: "").toString().isNotEmpty()
            ) {
                actionEnter.enable()
            } else {
                actionEnter.disable()
            }
        }
    }

    private fun openAlertHome() {
        findNavController().navigate(R.id.action_alertLoginFragment_to_alertaHomeActivity)
        finishActivity()
    }

    

    override fun initViewModelsErrorToken() {
        with(viewModel) {
            onChange(onErrorData) { validateError(it)}
        }
    }
}
