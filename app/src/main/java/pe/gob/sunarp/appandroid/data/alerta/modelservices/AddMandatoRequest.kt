package pe.gob.sunarp.appandroid.data.alerta.modelservices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AddMandatoRequest(

	@Json(name="apPatePersNatu")
	val apPatePersNatu: String? = null,

	@Json(name="apMatePersNatu")
	val apMatePersNatu: String? = null,

	@Json(name="noPersNatu")
	val noPersNatu: String? = null,

	@Json(name="tiDocuIden")
	val tiDocuIden: String? = null,

	@Json(name="nuDocu")
	val nuDocu: String? = null,
)
