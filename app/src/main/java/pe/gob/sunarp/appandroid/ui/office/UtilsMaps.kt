package pe.gob.sunarp.appandroid.ui.office

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.location.LocationManager


class GpsUtils() {


    companion object {
        val GPS_REQUEST = 666
    }

    interface onGpsListener {
        fun gpsStatus(isGPSEnable: Boolean)
    }
}

class GPSCheck(private val locationCallBack: LocationCallBack) : BroadcastReceiver() {
    interface LocationCallBack {
        fun turnedOn()
        fun turnedOff()
    }

    override fun onReceive(context: Context, intent: Intent) {
        val locationManager =
            context.getSystemService(LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) locationCallBack.turnedOff() else locationCallBack.turnedOn()
    }

}