package pe.gob.sunarp.appandroid.ui.office.route

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.huawei.hms.maps.*
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.PolylineOptions
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONException
import pe.gob.sunarp.appandroid.R
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.core.custom.ui.show
import pe.gob.sunarp.appandroid.core.onChange
import pe.gob.sunarp.appandroid.core.toast
import pe.gob.sunarp.appandroid.data.maps.request.DirectionsResponse
import pe.gob.sunarp.appandroid.data.maps.request.LegsItem
import pe.gob.sunarp.appandroid.data.maps.request.RoutesItem
import pe.gob.sunarp.appandroid.data.maps.request.StepsItem
import pe.gob.sunarp.appandroid.databinding.FragmentOfficeRouteBinding
import pe.gob.sunarp.appandroid.ui.base.BaseFragment
import pe.gob.sunarp.appandroid.ui.home.HomeActivity


@AndroidEntryPoint
class OfficeRouteFragment :
    BaseFragment<FragmentOfficeRouteBinding>(FragmentOfficeRouteBinding::inflate),
    OnMapReadyCallback {

    private val viewModel: OfficeRouteViewModel by viewModels()
    lateinit var map: HuaweiMap

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MapsInitializer.setApiKey("CV75JcRY8OJ9Aw2ymXBvdjvg86CAyWbk8gfEA4Vcv998RLqXEU6+CF144FngY3pp2E0+2sPX2ecKDVuliu0gdkyf7xk0")
        this.activity?.let {
            MapsInitializer.initialize(it)
            binding.map.onCreate(savedInstanceState)
            binding.map.getMapAsync(this)
        }
        setLoading(true)
        initViews()
        initViewModels()
    }

    override fun onMapReady(hmap: HuaweiMap) {
        map = hmap
        setLoading(false)
        viewModel.mapChargeCompleate()
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(viewModel.myposition, 16F))
    }

    override fun setTittle() {
        if (activity is HomeActivity) {
            putTitleToolbar("Oficina Registral")
        }
    }

    private fun initViews() {
        setTittle()
        viewModel.initData( 
            OfficeRouteFragmentArgs.fromBundle(arguments!!).destiny,
            OfficeRouteFragmentArgs.fromBundle(arguments!!).myposition,
            resources.getString(R.string.google_maps_key)
        )
    }

    private fun initViewModels() {
        with(viewModel) {
            initViewModelsErrorToken()
            onChange(loading) { setLoading(it) }
            onChange(drawingLinesdrawingLines) { drawLines(parse(it)) }
        }
    }

    private fun setLoading(it: Boolean) {
        binding.loadingContainer.apply {
            if (it) loading.show() else loading.hide()
        }
    }

    private fun drawLines(result: List<List<HashMap<String, String>>>) {
        var points: MutableList<LatLng>
        var lineOptions = PolylineOptions()

        for (i in result.indices) {
            points = arrayListOf()
            lineOptions = PolylineOptions()
            val path: List<java.util.HashMap<String, String>> = result[i]
            for (j in path.indices) {
                val point = path[j]
                val lat = point["lat"]!!.toDouble()
                val lng = point["lng"]!!.toDouble()
                val position = LatLng(lat, lng)
                points.add(position)
            }
            lineOptions.addAll(points)
            lineOptions.width(12f)
            lineOptions.color(Color.RED)
            lineOptions.geodesic(true)
        }
        map.addPolyline(lineOptions)
    }

    fun parse(data: DirectionsResponse): List<List<HashMap<String, String>>> {
        val routes: MutableList<List<HashMap<String, String>>> = ArrayList()
        val jRoutes: List<RoutesItem>?
        var jLegs: List<LegsItem>?
        var jSteps: List<StepsItem>?
        try {
            jRoutes = data.routes
            for (element in jRoutes) {
                jLegs = element.legs?: arrayListOf()
                val path: MutableList<HashMap<String, String>> = mutableListOf()
                for (elLegs in jLegs) {
                    jSteps = elLegs.steps
                    for (elStep in jSteps) {
                        var polyline = ""
                        polyline = elStep.polyline?.points?: ""
                        val list: List<LatLng> = decodePoly(polyline)
                        for (l in list.indices) {
                            val hm = HashMap<String, String>()
                            hm["lat"] = list[l].latitude.toString()
                            hm["lng"] = list[l].longitude.toString()
                            path.add(hm)
                        }
                    }
                    routes.add(path)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return routes
    }
    private fun decodePoly(encoded: String): List<LatLng> {
        val poly: MutableList<LatLng> = mutableListOf()
        var index = 0
        var lat = 0
        var lng = 0
        while (index < encoded.length) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(lat.toDouble() / 1E5,
                lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }

    

    override fun initViewModelsErrorToken() {

    }
}