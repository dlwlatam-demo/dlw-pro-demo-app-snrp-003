package pe.gob.sunarp.appandroid.data.registration.validateSMS

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel

@JsonClass(generateAdapter = true)
data class ValidationSMSRequest(
    @field:Json(name = "codtemporal")
    val code: String,
    @field:Json(name = "guid")
    val guid: String
): BaseModel() {
    constructor() : this("","")
}