package pe.gob.sunarp.appandroid.ui.services.searchbyname.certificate.adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.CertificateOwnership
import pe.gob.sunarp.appandroid.databinding.LayoutSearchCertificateItemBinding

class CertificateResultAdapter(
    private val items: List<CertificateOwnership>
) : RecyclerView.Adapter<CertificateResultAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutSearchCertificateItemBinding =
            LayoutSearchCertificateItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: LayoutSearchCertificateItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CertificateOwnership) = with(binding) {
            with(item) {
                certificate.apply {
                    tvTitle.text = "Partida:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = numPartida
                    tvDescription.setTypeface(null, Typeface.BOLD)
                }

                publicRegisterItem.apply {
                    tvTitle.text = "Registro Público:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = regPubSiglas
                }

                registerOffice.apply {
                    tvTitle.text = "Oficina Registral:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = officeName
                }

                ficha.apply {
                    tvTitle.text = "Ficha:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = fichaId
                }

                tomo.apply {
                    tvTitle.text = "Tomo:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = tomoId
                }

                folio.apply {
                    tvTitle.text = "Folio:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = fojaId
                }

                registerArea.apply {
                    tvTitle.text = "Área registral:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = regDescription
                }

                register.apply {
                    tvTitle.text = "Registro:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = libroDescripcion
                }

                participant.apply {
                    tvTitle.text = "Participante:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = participanteDesc
                }

                documentType.apply {
                    tvTitle.text = "Documento Identidad:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = tipoDocumPartic
                }

                documentNumber.apply {
                    tvTitle.text = "Número de Documento:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = numDocumPartic
                }

                address.apply {
                    tvTitle.text = "Dirección:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = direccionPredio
                }

                status.apply {
                    tvTitle.text = "Estado:"
                    tvTitle.setTypeface(null, Typeface.BOLD)
                    tvDescription.text = estadoInd
                }

            }
        }
    }


}