package pe.gob.sunarp.appandroid.core

sealed class FormStatus {
    object Ok : FormStatus()
    data class Ko(val error: String) : FormStatus()
}