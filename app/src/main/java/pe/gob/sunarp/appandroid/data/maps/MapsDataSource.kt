package pe.gob.sunarp.appandroid.data.maps

import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.data.Constants
import pe.gob.sunarp.appandroid.data.maps.request.DirectionsResponse
import pe.gob.sunarp.appandroid.data.maps.request.ListOfficeResponse
import pe.gob.sunarp.appandroid.data.vehicle.request.*
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.http.Url

interface MapsDataSource {

    @Headers(Constants.HEADER_API_GATEWAY)
    @GET(Constants.CONTEXTO_URL + "/parametros/oficinas/mapa")
    suspend fun getOffices(
        @Header("Authorization") authorization: String,
        @Header("Host") host: String?,
        @Header("systemName") systemName: String? = "APPSNRPANDRO"
    ): DataResult<ListOfficeResponse>

    @GET
    suspend fun getMapaApis(
        @Url host: String = "https://maps.googleapis.com/maps/api/directions/json",
        @Query("origin") origin: String = "-12.0751484,-77.046832",
        @Query("destination") destination: String = "-12.07975,-77.040871",
        @Query("key") key: String,
        @Query("sensor") sensor: String = "false",
        @Query("mode") mode: String = "walking",
    ): DataResult<DirectionsResponse>

}