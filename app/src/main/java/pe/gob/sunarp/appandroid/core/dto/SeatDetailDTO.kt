package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.SeatDetail

@JsonClass(generateAdapter = true)
data class SeatDetailDTO(
    @field:Json(name = "codigoSede")
    val codigoSede: String?,
    @field:Json(name = "numPlaca")
    val numPlaca: String?,
    @field:Json(name = "nsAsiento")
    val nsAsiento: String?,
    @field:Json(name = "desAsiento")
    val desAsiento: String?,
    @field:Json(name = "anioTitulo")
    val anioTitulo: String?,
    @field:Json(name = "numTitulo")
    val numTitulo: String?,
    @field:Json(name = "nsActo")
    val nsActo: String?,
    @field:Json(name = "codigoConcepto")
    val codigoConcepto: String?,
    @field:Json(name = "descripcionConcepto")
    val descripcionConcepto: String?,
    @field:Json(name = "codigoActo")
    val codigoActo: String?,
    @field:Json(name = "descriActo")
    val descriActo: String?,
    @field:Json(name = "codigoRegi")
    val codigoRegi: String?,
    @field:Json(name = "registrador")
    val registrador: String?,
    @field:Json(name = "numPartida")
    val numPartida: String?,
    @field:Json(name = "codigoEmpleado")
    val codigoEmpleado: String?,
    @field:Json(name = "codigoEmpleadoOrig")
    val codigoEmpleadoOrig: String?,
    @field:Json(name = "nsEmpleadoOrig")
    val nsEmpleadoOrig: String?,
    @field:Json(name = "codigoRegiOrig")
    val codigoRegiOrig: String?,
    @field:Json(name = "codigoSedeOrig")
    val codigoSedeOrig: String?,
    @field:Json(name = "numPlacaAnte")
    val numPlacaAnte: String?,
    @field:Json(name = "fgExonPub")
    val fgExonPub: String?,
    @field:Json(name = "fgExonPub1")
    val fgExonPub1: String?,
    @field:Json(name = "fgMandJud")
    val fgMandJud: String?,
    @field:Json(name = "idLavaActi")
    val idLavaActi: String?,
    @field:Json(name = "idImagAsiento")
    val idImagAsiento: String?,
    @field:Json(name = "numPagina")
    val numPagina: String?,
    @field:Json(name = "fgExonPub2")
    val fgExonPub2: String?,
    @field:Json(name = "in_sele")
    val inSele: String?,
    @field:Json(name = "sede")
    val sede: String?,
    @field:Json(name = "fechaInscripcion")
    val fechaInscripcion: String?,
    @field:Json(name = "desActo")
    val desActo: String?,
    @field:Json(name = "flagExonerado")
    val flagExonerado: String?,
    @field:Json(name = "paginas")
    val paginas: String?,
    @field:Json(name = "detalle")
    val detalle: String?,
    @field:Json(name = "nuPaginasCantidad")
    val nuPaginasCantidad: String?,
    @field:Json(name = "nuSecu")
    val nuSecu: String?,

) {
    fun toModel(): SeatDetail {
        val exon = if (!fgExonPub.isNullOrEmpty()) "Si" else "No"
        return SeatDetail(
            descriActo.orEmpty(),
            desAsiento.orEmpty(),
            nsAsiento.orEmpty(),
            exon,
            numPagina.orEmpty(),
            inSele.orEmpty(),
            descripcionConcepto.orEmpty(),
            "${anioTitulo.orEmpty()}-${numTitulo}" ,
            if (fgExonPub1 == "0") "No" else "Si",
            desActo = desActo.orEmpty(),
            desAsiento = desAsiento.orEmpty(),
            flagExonerado = if (flagExonerado == "0") "No" else "Si",
            detalle = detalle.orEmpty(),
            nuPaginasCantidad= nuPaginasCantidad.orEmpty(),
            paginas = paginas.orEmpty(),
            nuSecu = nuSecu.orEmpty(),
        )
    }
}