package pe.gob.sunarp.appandroid.ui.services.certificate.certificatedeparture.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.model.LiteralCertificate
import pe.gob.sunarp.appandroid.databinding.LayoutLiteralCertificateRegistroBienMuebleBinding

class CertificateLiteralDepartmentRegistroBienesMueblesAdapter(
    val items: List<LiteralCertificate>
): RecyclerView.Adapter<CertificateLiteralDepartmentRegistroBienesMueblesAdapter.ViewHolder>() {

    private var onSeatsPressed: ((id: LiteralCertificate) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutLiteralCertificateRegistroBienMuebleBinding =
            LayoutLiteralCertificateRegistroBienMuebleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setOnSeatsPressed(listener: (id: LiteralCertificate) -> Unit) {
        this.onSeatsPressed = listener
    }

    inner class ViewHolder(private val binding: LayoutLiteralCertificateRegistroBienMuebleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: LiteralCertificate) = with(binding) {
            with(item) {
                registerOffice.tvTitle.text = "Registro Público:"
                registerOffice.tvDescription.text = regPubSiglas
                certificate.tvTitle.text = "Oficina Registral:"
                certificate.tvDescription.text = officeName
                ficha.tvTitle.text = "Partida:"
                ficha.tvDescription.text = certNumber
                tomo.tvTitle.text = "Placa:"
                tomo.tvDescription.text = plateNumber
                folio.tvTitle.text = "Estado de Vehículo:"
                folio.tvDescription.text = baja

                btnSeats.setOnClickListener {
                    onSeatsPressed?.invoke(item)
                }
            }
        }
    }

}