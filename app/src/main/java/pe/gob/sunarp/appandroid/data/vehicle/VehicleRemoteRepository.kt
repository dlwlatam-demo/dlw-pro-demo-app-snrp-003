package pe.gob.sunarp.appandroid.data.vehicle

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.gob.sunarp.appandroid.BuildConfig
import pe.gob.sunarp.appandroid.core.api.DataResult
import pe.gob.sunarp.appandroid.core.dto.CertificateOwnershipAsyncDTO
import pe.gob.sunarp.appandroid.core.dto.VehicleReceiptAsyncDTO
import pe.gob.sunarp.appandroid.core.getHost
import pe.gob.sunarp.appandroid.core.model.VehicleInformation
import pe.gob.sunarp.appandroid.core.model.VehiclePayment
import pe.gob.sunarp.appandroid.core.toBearer
import pe.gob.sunarp.appandroid.data.preferences.SunarpSharedPreferences
import pe.gob.sunarp.appandroid.data.vehicle.request.ConsultaVehicularCostoRequest
import pe.gob.sunarp.appandroid.data.vehicle.request.ConsultaVehicularCostoResponse

class VehicleRemoteRepository(
    private val dataSource: VehicleDataSource,
    private val preferences: SunarpSharedPreferences,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getVehicleData(
        information: VehicleInformation
    ): DataResult<String> =
        withContext(dispatcher) {
            when (val result =
                dataSource.getVehicleData(
                    information.toRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_VEHICLE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.image.orEmpty())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun validateReceipt(
        information: VehicleInformation
    ): DataResult<Boolean> =
        withContext(dispatcher) {
            when (val result =
                dataSource.validateReceipt(
                    information.toReceiptRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_VEHICLE_KEY)
                )) {
                is DataResult.Success -> {
                    if (result.data.responseId == "1") {
                        DataResult.Success(true)
                    } else {
                        DataResult.Failure(Exception(result.data.description))
                    }
                }
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun generateReceipt(
        information: VehiclePayment
    ): DataResult<String> =
        withContext(dispatcher) {
            when (val result =
                dataSource.generateReceipt(
                    information.toRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_VEHICLE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data.transId.orEmpty())
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun generateReceiptAsync(
        information: VehiclePayment
    ): DataResult<VehicleReceiptAsyncDTO> =
        withContext(dispatcher) {
            when (val result =
                dataSource.generateReceiptAsync(
                    information.toRequest(),
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_VEHICLE_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }

    suspend fun generateReceiptAsyncStatus(guid: String): DataResult<VehicleReceiptAsyncDTO> =
        withContext(dispatcher) {

            when (val result =
                dataSource.generateReceiptAsyncStatus(
                    guid,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PROPERTIES_KEY)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }

        }

    suspend fun obtenerTarifa(
        request: ConsultaVehicularCostoRequest
    ): DataResult<ConsultaVehicularCostoResponse> =
        withContext(dispatcher) {
            when (val result =
                dataSource.obtenerTarifa(
                    request,
                    preferences.accessToken.toBearer(), getHost(BuildConfig.SUNARP_PARAMETERS_HOST)
                )) {
                is DataResult.Success -> DataResult.Success(result.data)
                is DataResult.Failure -> DataResult.Failure(result.exception)
            }
        }
}