package pe.gob.sunarp.appandroid.core.api

class NotControlledException : Exception {
    constructor() : super("Se ha presentado un error, volver a intentar en unos minutos") {}
    constructor(message: String?) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
    constructor(cause: Throwable?) : super(cause) {}
}