package pe.gob.sunarp.appandroid.core.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pe.gob.sunarp.appandroid.core.model.BaseModel
import pe.gob.sunarp.appandroid.core.model.DataModel

@JsonClass(generateAdapter = true)
data class PropertyDTO(
    @field:Json(name = "msj")
    val message: String?,
    @field:Json(name = "estado")
    val status: String?,
    @field:Json(name = "lstTitularidad")
    val ownershipList: List<PropertyItemDTO>? = emptyList(),
) : BaseModel() {
    fun toModel(): List<DataModel> {
        val items = ownershipList?.map {
            with(it) {
                DataModel.PropertyInformation(
                    documentNumber.orEmpty(),
                    owner.orEmpty(),
                    register.orEmpty(),
                    zone.orEmpty(),
                    office.orEmpty()
                )
            }

        } ?: emptyList()
        return items
    }
}

@JsonClass(generateAdapter = true)
data class PropertyItemDTO(
    @field:Json(name = "nroDocumento")
    val documentNumber: String?,
    @field:Json(name = "titular")
    val owner: String?,
    @field:Json(name = "partida")
    val register: String?,
    @field:Json(name = "zona")
    val zone: String?,
    @field:Json(name = "oficina")
    val office: String?,
    @field:Json(name = "inPred")
    val inPred: String?,
    @field:Json(name = "coUnidRegi")
    val coUnidRegi: String?,
    @field:Json(name = "refNumPart")
    val refNumPart: Int?,
) : BaseModel()