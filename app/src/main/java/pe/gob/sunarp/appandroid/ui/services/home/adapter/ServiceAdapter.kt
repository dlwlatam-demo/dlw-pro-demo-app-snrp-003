package pe.gob.sunarp.appandroid.ui.services.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.gob.sunarp.appandroid.core.custom.ui.hide
import pe.gob.sunarp.appandroid.data.ServiceOption
import pe.gob.sunarp.appandroid.databinding.LayoutServiceItemBinding
import pe.gob.sunarp.appandroid.ui.services.home.ServiceItem

class ServiceAdapter(
    private val items: List<ServiceItem>
) : RecyclerView.Adapter<ServiceAdapter.ViewHolder>() {

    private var onServicePressed: ((id: ServiceOption) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: LayoutServiceItemBinding =
            LayoutServiceItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun setOnServicePressed(listener: (id: ServiceOption) -> Unit) {
        this.onServicePressed = listener
    }

    inner class ViewHolder(private val binding: LayoutServiceItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ServiceItem) = with(binding) {
            with(item) {
                imageViewIconStart.setImageResource(icon)
                tvTitle.text = title
                tvSubtitle.text = subtitle
                if (subtitle.isEmpty()) tvSubtitle.hide()
                cardItemLayout.setOnClickListener {
                    onServicePressed?.invoke(key)
                }

            }
        }
    }
}