package pe.gob.sunarp.appandroid.core.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ItemSpinner(
    val id: String,
    val name: String,
): Parcelable{
    override fun toString(): String {
        return name
    }
}